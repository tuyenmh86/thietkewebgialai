<?php 
return [
	
    'locale' => [
        0 => [
            'name' => 'Tiếng Việt',
            'code' => 'vi',
            'flag' => '/language/icon/vietnam.png'
        ],
        // 1 => [
        //     'name' => 'English',
        //     'code' => 'en',
        //     'flag' => '/language/icon/united-kingdom.png'
        // ],
        // 2 => [
        //     'name' => 'China',
        //     'code' => 'cn',
        //     'flag' => '/language/icon/china.png'
        // ]
    ],

    'locale_default' => 'vi',

    'productHasManyAttribute' => false,

    'eva_prefix' => 'App\Models\Product\ProductAttribute',

    'productAdvanced' => false,
    
    'cache_time' => \Carbon\Carbon::now()->addSecond(10)
];