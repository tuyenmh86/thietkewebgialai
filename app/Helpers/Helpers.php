<?php 
if(!function_exists('site_public_url')){
    function site_public_url(){
      return asset('site');
    }
}

if(!function_exists('login_public_url')){
    function login_public_url()
    {
      return asset('login');
    }
}

if(!function_exists('admin_public_url')){
    function admin_public_url()
    {
    	return asset('admin/admin');
    }
}

if(!function_exists('admin_plugin_url')){
    function admin_plugin_url(){
      echo asset('admin/plugins');
    }
}

if(!function_exists('admincp_url')){
    function admincp_url($url){
    	return Redirect::to('admincp/'.$url);
    }
}

if(!function_exists('contains_at_least_one_word')){
    function contains_at_least_one_word($input_string) {
      foreach (explode(' ', $input_string) as $word) {
        if (!empty($word)) {
          return true;
        }
      }
      return false;
    }
}

if(!function_exists('getIP')){
    function getIP(){
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
      }
}

if(!function_exists('recursiveCategorys')){
    function recursiveCategorys($data,$parent_id = 0,$Character = ''){
        foreach($data as $key => $value){
            if($value->parent_id == $parent_id){
              echo "<option value='".$value->id."'>";
              echo $Character.$value->title;  
              echo "</option>";
              unset($data[$key]);
              recursiveCategorys($data,$value->id,$Character.'[--]');
            }
        }
    }
}

if(!function_exists('recursiveCategory')){
    function recursiveCategory($data,$parent_id,$Character = '[--]'){
        foreach($data as $key => $value){
            if($value->parent_id == $parent_id){
              echo "<option value='".$value->id."'>";
              echo $Character.$value->title;  
              echo "</option>";
              unset($data[$key]);
              recursiveCategory($data,$value->id,$Character.'[--]');
            }
        }
    }
}


if(!function_exists('getPostByPos')){
    function getPostByPos($data,$pos){
      foreach ($data as $key => $value) {
        if($value->pos == $pos){
          return $value;
          break;
        }
      }
    }
}

if(!function_exists('attr_type')){
    function attr_type ($input){
      $arr = array("text","textarea","number","checkbox","select");
      $html = "<option value='".$input."'>".ucwords($input)."</option>";
      foreach ($arr as $value) {
        if($value != $input){
            $html.="<option value='".$value."'>".ucwords($value)."</option>";
        }
      }
      return $html;
    }
}


if(!function_exists('to_slug')){
    function to_slug($str) {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '_', $str);
        return $str;
    }
}


if(!function_exists('to_slug_url')){
    function to_slug_url($str) {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        return $str;
    }
}

if(!function_exists('discount')){
   function discount($itemSelected){
      $data = array('10', '20', '30', '40', '50', '60', '70', '80', '90', '100' );
      $html = '';
      if($itemSelected == null){
        $html.='<option value="0">-- None --</option>';
        foreach ($data as $value) {
          $html.='<option value="'.$value.'">'.$value.'%</option>';
        }
      }else {
        $html.='<option value="'.$itemSelected.'">'.$itemSelected.'%</option>';
        foreach ($data as $value) {
          if($value != $itemSelected){
            $html.='<option value="'.$value.'">'.$value.'%</option>';
          }
        }
        $html.='<option value="0">-- None --</option>';
      }
      return $html;
   }
}

if(!function_exists('st_type_product')){
     function st_type_product ($typeList,$typeSelected){
        $html = '';
        if($typeSelected == 0){
            $html.='<option value="0">-- Multi Product --</option>';
            foreach ($typeList as $key => $value) {
              $html.='<option value="'.$value->id.'">'.$value->name.'</option>';
            }
        }
        else {
          foreach ($typeList as $key => $value) {
              if($value->id == $typeSelected){
                $html.='<option value="'.$value->id.'">'.$value->name.'</option>';
              }   
            }
            foreach ($typeList as $key => $value) {
              if($value->id != $typeSelected){
                $html.='<option value="'.$value->id.'">'.$value->name.'</option>';
              }   
            }
            $html.='<option value="0">-- Multi Product --</option>';
        } 
        return $html; 
     }
}


if(!function_exists('formatPrice')){
     function formatPrice ($price) {
        if(($price / 1000000000) > 1 ){
            return '₫ '.number_format($price / 1000000000)." Tỷ";
        }else {
            return '₫ '.number_format($price / 1000000)." Triệu";
        }
     }
}
