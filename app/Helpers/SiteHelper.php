<?php 

if(!function_exists('get_viewed_products')){
	
	function get_viewed_products($id) {

		$product = app()->make(App\Repositories\Product\EloquentProductRepository::class);
		
		return $product->find($id);
		
	}
}

if(!function_exists('get_list_products'))

{

	function get_list_products($published = true,$categoryId = null, $limit = null,$orderby = 'featured', $objectId = null , $byAccount = null ) {

		$product = app()->make(App\Repositories\Product\EloquentProductRepository::class);

		return $product->getLists($published ,$categoryId , $limit ,$orderby, $objectId , $byAccount);
		
	}

}



if(!function_exists('get_list_product_categories'))

{

	function get_list_product_categories($published = true, $parent_id = 0 ,$objectId = null, $limit = null,$orderby = 'latest') {

		$category = app()->make(App\Repositories\Product\EloquentCategoryRepository::class);
		
		return $category->getLists($published , $parent_id , $objectId, $limit, $orderby);
		
	}

}



if(!function_exists('get_list_posts'))

{

	function get_list_posts($published = true,$categoryId = null, $limit = 10,$orderby = 'featured', $objectId = null , $byAccount = null ) {

		$post = app()->make(App\Repositories\Post\EloquentPostRepository::class);

		return $post->getLists($published ,$categoryId , $limit ,$orderby , $objectId , $byAccount);

	}

}


if(!function_exists('slide'))

{

	function slide($slug)

	{

		$slide = app()->make(App\Repositories\Slide\EloquentSlideRepository::class);

		return $slide->getViaSlug($slug);

	}

}



if(!function_exists('app_menu'))

{

	function app_menu($name) 

	{

		$menu = app()->make(App\Repositories\Menu\EloquentMenuRepository::class);

		return $menu->getViaName($name);

	}

}



if(!function_exists('getViaProductCateId'))

{

	function getViaProductCateId($id) 

	{

		$productCategories = app()->make(App\Repositories\Product\EloquentCategoryRepository::class);
		return $productCategories->getViaId($id);

	}

}



if(!function_exists('getViaPostCateId'))

{

	function getViaPostCateId($id) 

	{

		$postCategories = app()->make(App\Repositories\Post\EloquentCategoryRepository::class);

		return $postCategories->getViaId($id);

	}

}



if(!function_exists('getViaPageId'))

{

	function getViaPageId($id) 

	{

		$page = app()->make(App\Repositories\Page\EloquentPageRepository::class);

		return $page->getViaId($id);

	}

}

if(!function_exists('format_datetime'))

{

	function format_datetime ($time) 

	{

		return Carbon\Carbon::parse($time)->format('jS , M ,Y');         

	}

}



if(!function_exists('price_format'))

{

	function price_format ($price) 

	{

		if(!session()->has('current') || session()->get('current') === 'VNĐ'){

			return '‎₫ '.number_format($price);

		}

		// $dom=new \DOMDocument();

		// $dom->load("http://www.vietcombank.com.vn/ExchangeRates/ExrateXML.aspx");

		// $exrate=$dom->getElementsByTagName("Exrate");

		// foreach ($exrate as $item) {

		// 	if($item->getAttribute('Buy')){

		// 		$code=$item->getAttribute("CurrencyCode");



		// 		$buy=$item->getAttribute("Buy");



		// 		if($code === session()->get('current')){	

		// 			$price = $price / $buy;

		//     		return '<i class="mr-sm-2 fa fa-'.strtolower($code).'" aria-hidden="true"></i> '.number_format($price);	  

		// 			break;

		// 		}

		// 	}

		// }      

	}

}



if(!function_exists('widget_replace'))

{

	function widget_replace ($widget) 

	{

		$data = str_replace("<p>", "", $widget);

		$data = str_replace("</p>", "", $data);  

		return $data;       

	}

}

