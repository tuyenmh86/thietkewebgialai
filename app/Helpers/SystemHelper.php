<?php 
if(!function_exists('getConfigSite')){
	function getConfigSite($field ) {
		$setting = app()->make(App\Repositories\Setting\EloquentSettingRepository::class);
		return $setting->findWithLanguage(1)->$field;
	}
}

if(!function_exists('getListsByPosision')){
	function getListsByPosision($pos,$limit = null) {
		$permissions = app()->make(App\Repositories\Permission\EloquentPermissionRepository::class);
		return $permissions->getListsByPosision($pos,$limit);
	}
}

if(!function_exists('CAN')){
	function CAN($module,$action,$advanced = null) {
		if(Auth::guard('admin')->check()){
			// ** IS SUPPER ADMIN
			if(Auth::guard('admin')->user()->is_supperAdmin()){
				return true;
			}else{
				foreach (Auth::guard('admin')->user()->role->permissions as $key =>$permission) {
	                if(trim($permission->display_name) == trim($module.".".$action)){
	                	if($advanced){
		                	if(Auth::guard('admin')->user()->role->hasAdvanced()){
		                		return true;
		                	}
		                	return false;
	                	}
	                	return true;
	                }
		        }
		        return false;
	        } 
        }
	}
}

if(!function_exists('preventUpdateSPAdmin')){
	function preventUpdateSPAdmin($id) {
		if($id != 1){
			return true;
		}
		return false;
	}
}

function is_array_empty($arr){
  if(is_array($arr)){     
      foreach($arr as $key => $value){
          if(!empty($value) || $value != NULL || $value != ""){
              return true;
              break;//stop the process we have seen that at least 1 of the array has value so its not empty
          }
      }
      return false;
  }
}

if(!function_exists('customFileds')){
	function customFileds($table) {
		$customFiled = app()->make(\App\Models\Customfield\Customfield::class);
		return $customFiled->where('table_name',$table)->get();
	}
}