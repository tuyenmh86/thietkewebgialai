<?php
namespace App\Traits;
use App\Models\Translation\Translation;

trait Translator
{

    protected $model;
    
    public function translation(){
        return new Translation;
    }

    public function createWithLanguage ($data){
        foreach(config('cms.locale') as $key => $item){
            if($key == 0){
                $data_single = $data;
                foreach ($data_single['language'][$item['code']] as $key => $value) {
                    $data_single[$key] = $value;
                }
                $data_single = $this->model->fill($data_single)->getAttributes();
                $data_single = $this->model->create($data_single);
            }else {
                foreach ($data['language'][$item['code']] as $key => $value) {
                    $translation['table_name'] = $this->model->getTable();
                    $translation['column_name'] = $key;
                    $translation['foreign_key'] = $data_single->id;
                    $translation['locale'] = $item['code'];
                    $translation['value'] = $value;
                    $this->translation()->create($translation);
                }
            }   
        }

        return $data_single;
    }

    public function updateWithLanguage ($id,$data){
        $this->translation()->where('foreign_key',$id)
            ->where('table_name',$this->model->getTable())
            ->delete();
        foreach(config('cms.locale') as $key => $item){
            if($key == 0){
                $data_single = $data;
                foreach ($data_single['language'][$item['code']] as $key => $value) {
                    $data_single[$key] = $value;
                }
                $data_single = $this->model->fill($data_single)->getAttributes();
                $this->model->find($id)->update($data_single);
            }else {
                foreach ($data['language'][$item['code']] as $key => $value) {
                    $translation['table_name'] = $this->model->getTable();
                    $translation['column_name'] = $key;
                    $translation['foreign_key'] = $id;
                    $translation['locale'] = $item['code'];
                    $translation['value'] = $value;
                    $this->translation()->create($translation);
                }
            }   
        }
        return true;
    }

    public function findWithLanguage ($id) 
    {
        // return cache()->remember('find_with_language',config()->get('cms.cache_time'),function() use ($id){
            if($data = $this->model->find($id)){
                $merge_data[config('cms.locale')[0]['code']] = $this->model->fill($data->getAttributes())->getAttributes();
                if(count(config('cms.locale')) > 1){
                    $translations = $this->translation()->where('foreign_key',$data->id)
                        ->where('table_name',$this->model->getTable())
                        ->get();
                    if($translations){
                        foreach ($translations as $key => $value) {
                            $merge_data[$value->locale][$value->column_name] = $value->value;
                        }
                    }
                }
                $data['language'] = $merge_data;
                return $data;
            }
        // });
    }

    public function deleteWithLanguage ($id)
    {
        if(is_array($id)){
            $this->model->whereIn('id',$id)->delete();
            $this->translation()->whereIn('foreign_key',$id)
                ->where('table_name',$this->model->getTable())
                ->delete();
            return true;
        }else{
            $this->model->find($id)->delete();
            $this->translation()->where('foreign_key',$id)
                ->where('table_name',$this->model->getTable())
                ->delete();
            return true;
        }
    }
    public function language ($field,$lang = null)
    {
        // return cache()->remember('language_language',config()->get('cms.cache_time'),function() use ($field,$lang){
            if(!is_null($lang)){
                $locale = $lang;
            }else{
                $locale = session()->has('locale') ? session()->get('locale') : config('cms.locale_default');
            }

            if($locale == config('cms.locale_default')){
                return $this->$field;

            }else {

                $data = $this->translation()->where('table_name',$this->table)
                    ->where('locale',$locale)
                    ->where('foreign_key',$this->id)
                    ->where('column_name',$field)
                    ->first();
                if($data){
                    return  $data->value;
                }
            }
        // });
    }

    public function getSlug ($slug){
        // return cache()->rememberForever('language_get_slug_'.\Hash::make(str_random(8)),function() use ($slug){
            $locale = session()->has('locale') ? session()->get('locale') : config('cms.locale_default');
            if($locale == config('cms.locale_default')){
                return $this->query->where('alias',$slug)->first();
            }else {
                $data = $this->translation()->where('table_name',$this->model->getTable())
                    ->where('locale',$locale)
                    ->where('column_name','alias')
                    ->where('value',$slug)
                    ->first();
                if($data){
                   return $this->query->where('id',$data->foreign_key)->first();
                }
            }
        // });
    }
}