<?php 
namespace App\Traits;

trait ModelsTrait {

	public function is_published () {
		return $this->query->where('published',true);
	}

	public function unsetRoot () {
		return $this->query->where('id','!=',1);
	}

	public function unsetOject ($objectId) {
		if(is_array($objectId)){
            foreach ($objectId as $key => $item) {
                $this->query->where('id','!=',$item);
            }
        }
        return $this->query->where('id','!=',$objectId);
	}

	public function countDataAddInWeekAgo () {
		date_default_timezone_set('Asia/Ho_Chi_Minh');
        $time7weeksAgo=date_create();
        date_modify($time7weeksAgo,"-7 days");
        $time7weeksAgo = date_format($time7weeksAgo,"Y-m-d H:i:s");
        $now = date_create();
        $now = date_format($now,"Y-m-d H:i:s");
        return $this->model->whereBetween('created_at',[$time7weeksAgo,$now])->count();
	}

	public function addAttribute($attributeType,$productId){
		foreach ($attributeType as $key => $value) {
			$eav_table = config('cms.eva_prefix').$key;
			$model = app($eav_table);
			foreach ($value as $key2 => $value2) {
				foreach ($value2 as $key3 => $value3) {
					$data['attribute_id'] = $key2;
					$data['product_id'] = $productId;
					$data['value'] = $value3;
					$model->create($data);
				}
			}
		}
	}
}