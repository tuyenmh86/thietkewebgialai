<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\customResetPassword as ResetPasswordNotification;
use App\Traits\Translator;

class User extends Authenticatable
{
    use Translator;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 
        'password', 
        'email',
        'facebook_id',
        'facebook_token',
        'provider',
        'last_login',
        'address',
        'phone',
        'introduce',
        'profile_image',
        'published',
        'pos'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    // ********************* RETURN IS PUBLISHED *********************************
    public function is_published(){
        return $this->published;
    }
    // ********************* CONNECT TO ROLES LOGS ***********************************
    public function logs(){
        return $this->hasMany('App\Models\LogUser\LogUser');
    }

    // ********************* CONNECT TO PRODUCT TABLE **********************************
    public function products(){
        return $this->hasMany('App\Models\Product\Product');
    }

    public function sendPasswordResetNotification($token){
        $this->notify(new ResetPasswordNotification($token));
    }

    public function link ()
    {
        return url(trans('route.agent').'/'.$this->id.'-'.to_slug_url($this->full_name));
    }
}
