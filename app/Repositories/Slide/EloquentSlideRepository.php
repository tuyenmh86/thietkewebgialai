<?php
namespace App\Repositories\Slide;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelsTrait;
use App\Traits\Translator;
use App\Models\Slides\Slide;

class EloquentSlideRepository {

    use ModelsTrait,Translator;
    
    protected $model;
    protected $query;
     /**
     * @var \Illuminate\Database\Eloquent\Model An instance of the Eloquent Model
     */
    public function __construct(Slide $model){
         $this->model = $model;
         $this->query = $this->model->query();
    }

    public function getViaId ($id , $published = true) {
        // return cache()->remember('slide_get_via_id',config()->get('cms.cache_time'),function() use($id,$published){
            if($published){
                $this->is_published();
            }if(is_array($id)){
                foreach ($id as $key => $value) {
                   $this->query->where('id',$value);
                }
                return $this->query->get();
            }else{
                $this->query->where('id',$id);
                return $this->query->first();
            }
        // });
    }

    public function getViaSlug ($slug , $published = true) {
        // return cache()->remember('slide_get_via_slug',config()->get('cms.cache_time'),function() use($slug,$published){
            if($published)
                $this->is_published();
            $this->query->where('alias',$slug);
            return count($this->query->first()) > 0 ? $this->query->first()->items : null;
        // });
    }
    
    /**
     * ** Lấy danh sách
     * @param $published : trạng thái xuất bản
     * @param $parent_id : lấy danh mục cha
     * @param $limit : giới hạng record
     * @param $orderby : latest (mới nhất) : oldest (cũ nhất)
     * @param $objectId : loại bỏ đối tượng id
     */
    public function getLists($orderby = 'latest') {
        switch ($orderby) {
            case 'latest':
                $this->query->latest();
                break;
            default:
                $this->query->oldest();
                break;
        }
        return $this->query->get();
    }

    public function find($id){
        return $this->model->find($id);
    }

    public function create($data){
        $data = $this->model->fill($data)->getAttributes();
        return $this->model->create($data);
    }

    public function update($id, $data) {
        $data = $this->model->fill($data)->getAttributes();
        if(is_array($id))
            return $this->model->whereIn('id',$id)->update($data);
        return $this->model->where('id',$id)->update($data);
    }

    public function delete($id) {
        if(is_array($id))
            return $this->model->whereIn('id',$id)->delete();
        return $this->model->where('id',$id)->delete();
    }
}