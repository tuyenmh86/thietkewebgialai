<?php

namespace App\Repositories\Menu;



use Illuminate\Database\Eloquent\Model;

use App\Traits\ModelsTrait;

use App\Models\Menu\Menu;



class EloquentMenuRepository{

    use ModelsTrait;



    protected $model;

     /**

     * @var \Illuminate\Database\Eloquent\Model An instance of the Eloquent Model

     */

    public function __construct(Menu $model){

         $this->model = $model;

    }

    

    public function getViaName ($name) {

        // return cache()->remember('menu',config()->get('cms.cache_time'),function() use($name){
            return $this->model->where('name',$name)->first() ? $this->model->where('name',$name)->first()->attributes : null;
        // });
    }

}