<?php
namespace App\Repositories\Page;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelsTrait;
use App\Traits\Translator;
use App\Models\Page\Page;

class EloquentPageRepository{
    use ModelsTrait,Translator;

    protected $model;
    protected $query;

    public function __construct(Page $model){
         $this->model = $model;
         $this->query = $this->model->query();
    }

    public function getViaId ($id , $published = true) 
    {
        // return cache()->remember('page_get_via_id',config()->get('cms.cache_time'),function() use ($id,$published){
            if($published){
                $this->is_published();
            }
            $this->query->where('id',$id);
            return $this->query->first();
        // });
    }

    public function getViaSlug ($slug , $published = true) 
    {
        // return cache()->remember('page_get_via_slug',config()->get('cms.cache_time'),function() use ($slug,$published){
            if($published){
               $this->is_published();
            }
            
            
            return $this->getSlug($slug);
        // });
    }
 
    /**
     * ** Lấy danh sách
     * @param $published : trạng thái xuất bản
     * @param $limit : giới hạng record
     * @param $orderby : latest (mới nhất) : oldest (cũ nhất)
     * @param $objectId : loại bỏ đối tượng đang sử dụng bởi Id
     * @param $posison : vị trí
     */
    public function getLists($published = null,$objectId = null, $limit = null,$orderby = 'latest') {
        if($published){
            $this->is_published();
        }
        if($objectId){
            $this->unsetOject($objectId);
        }
        switch ($orderby) {
            case 'latest':
                $this->query->latest();
                break;
            case 'oldest':
                $this->query->oldest();
                break;
            case 'view':
                $this->query->latest('view');
                break;
            default:
                $this->query->latest('pos');
                break;
        }
        return $limit ? $limit == 1 ? $this->query->first() : $this->query->paginate($limit) : $this->query->get();
    }

    public function find($id)
    {
        
        return $this->model->find($id);
    }

    public function update($id, $data) {
        $data = $this->model->fill($data)->getAttributes();
        if(is_array($id))
            return $this->model->whereIn('id',$id)->update($data);
        return $this->model->where('id',$id)->update($data);
    }
}