<?php
namespace App\Repositories\Product;

use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelsTrait;
use App\Models\Product\Attribute;

class EloquentAttributeRepository {

    use ModelsTrait;
    
    protected $model;

    public function __construct(Attribute $model){
         $this->model = $model;
    }
     
    public function find($id)
    {
        return $this->model->find($id);
    }

    public function create($data)
    {
        $data = $this->model->fill($data)->getAttributes();
        return $this->model->create($data);
    }

    public function update($id, $data) {
        $data = $this->model->fill($data)->getAttributes();
        $Attribute = Attribute::where('product_id', $id)->first();
        if (isset($Attribute))
            return $this->model->where('product_id',$id)->update($data);
        return $this->create($data);
    }
}