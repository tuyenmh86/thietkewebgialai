<?php

namespace App\Repositories\Product;



use Illuminate\Database\Eloquent\Model;

use App\Traits\ModelsTrait;
use App\Traits\Translator;

use App\Models\Product\Product;



class EloquentProductRepository{

    

    use ModelsTrait,Translator;



    protected $model;

    protected $query;

     /**

     * @var \Illuminate\Database\Eloquent\Model An instance of the Eloquent Model

     */

    public function __construct(Product $model){

         $this->model = $model;

         $this->query = $this->model->query();

    }



    public function getViaSlug ($slug , $published = true) {

        
        return cache()->remember('product_get_via_slug',config()->get('cms.cache_time'),function() use ($slug,$published){
            if($published){
               $this->is_published();
            }
            return $this->getSlug($slug);
        });
    }

    

    /**

     * ** Lấy danh sách

     * @param $published : trạng thái xuất bản

     * @param $limit : giới hạng record

     * @param $orderby : latest (mới nhất) : oldest (cũ nhất)

     * @param $objectId : loại bỏ đối tượng đang sử dụng bởi Id

     * @param $categoryId : Danh mục

     * @param $byAccount : lấy theo account post

     */

    public function getLists($published = null,$categoryId = null, $limit = null,$orderby = 'latest', $objectId = null , $byAccount = null ) {

        // return cache()->remember('products_get_list',config()->get('cms.cache_time'),function() use ($published,$categoryId, $limit,$orderby, $objectId , $byAccount ){
            
            if($published){

               $this->is_published();

            }
            
            if($categoryId){
                
               $this->query->whereHas('categories', function($q) use ($categoryId){
                    
                    $q->where('id',$categoryId);

                });
            }

            if($objectId){

                $this->unsetOject($objectId);

            }

            if($byAccount){

                if(is_array($byAccount)){

                    $this->query->where($byAccount[0],$byAccount[1]);

                }else{

                    $this->query->where('user_id',$byAccount);

                }

            }

            switch ($orderby) {

                case 'latest':

                    $this->query->latest();

                    break;

                case 'oldest ':

                    $this->query->oldest ();

                    break;

                case 'popular':

                    $this->query->latest ('view');

                    break;

                case 'featured':

                    $this->query->where('featured',true)->latest();

                    break;
                
                case 'featured_asc':
                    $this->query->where('featured',true)->orderBy('pos','asc');
                    break; 
                    
                case 'pos_asc':
                    $this->query->orderBy('pos','asc');
                    break;  

                default:
                    $this->query->latest('pos');
                    break;

            }

            
            return $limit ? $limit == 1 ? $this->query->first() : $this->query->paginate($limit) : $this->query->get();
        // });
    }

    

    public function searchs($tag = null, $keyword = null, $location = null ,$category = null ,$type = null ,$status = null ,$price = null ,$area = null ,$bedroom = null,$bathroom = null){
        
        $this->is_published();

        if(!is_null($tag))

        {
            
            $this->query->whereHas('tags',function($q) use ($tag){

                $q->orWhere('alias',$tag);

            });

        }



        if(!is_null($keyword)){

            $this->query->where('title', 'like', '%'.$keyword.'%');

        }



        if(!is_null($location)){

            $this->query->whereHas('attribute',function($q) use ($location){

                $q->where('address','like', '%'.$location.'%');

            });

        }



        if(!is_null($category)){

            $this->query->whereHas('categories',function($q) use ($category){

                $q->where('id',$category);

            });

        }



        if(!is_null($type)){

            $this->query->whereHas('categories',function($q) use ($type){

                $q->where('id',$type);

            });

        }



        if(!is_null($status)){

            switch ($status) 

            {

                case 'popular':

                    $this->query->latest ('view');

                    break;

                default:

                     $this->query->where('featured',true)->latest();

                    break;

            }

        }

        if(!is_null($price)){

            $price = explode('-',$price);

            $this->query->whereHas('attribute',function($q) use ($price){

                $q->whereBetween('price',[$price[0],$price[1]]);

            });

        }

        if(!is_null($area)){

            $area = explode('-', $area);

            $this->query->whereHas('attribute',function($q) use ($area){

                $q->whereBetween('acreage',[$area[0],$area[1]]);

            });

        }

        if(!is_null($bedroom)){

            if($bedroom == 5){

                $this->query->whereHas('attribute',function($q) use ($bedroom){

                    $q->where('bedroom','>=',5);

                });

            }else{

                $bedroom = explode('-', $bedroom);

                $this->query->whereHas('attribute',function($q) use ($bedroom){

                    $q->whereBetween('bedroom',[$bedroom[0],$bedroom[1]]);

                });

            }

        }

        if(!is_null($bathroom)){

            if($bathroom == 5){

                $this->query->whereHas('attribute',function($q) use ($bathroom){

                    $q->where('bathroom','>=',5);

                });

            }else{

                $bathroom = explode('-', $bathroom);

                $this->query->whereHas('attribute',function($q) use ($bathroom){

                    $q->whereBetween('bathroom',[$bathroom[0],$bathroom[1]]);

                });

            }

        }

        return $this->query->orderBy('id','DESC')->paginate(12);

    }
    
    public function find($id){

        return $this->model->find($id);

    }

    public function update($id, $data) {
        $data = $this->model->fill($data)->getAttributes();

        if(is_array($id))

            return $this->model->whereIn('id',$id)->update($data);

        return $this->model->where('id',$id)->update($data);

    }
}