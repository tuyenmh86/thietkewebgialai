<?php
namespace App\Repositories\Product;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelsTrait;
use App\Traits\Translator;
use App\Models\Product\Category;

class EloquentCategoryRepository {

    use ModelsTrait,Translator;
    
    protected $model;
    protected $query;
     /**
     * @var \Illuminate\Database\Eloquent\Model An instance of the Eloquent Model
     */
    public function __construct(Category $model){
         $this->model = $model;
         $this->query = $this->model->query();
    }

    public function getViaId ($id , $published = true) 
    {
        // return cache()->remember('product_categories_get_via_id',config()->get('cms.cache_time'),function() use ($id,$published){

            if($published){
                $this->is_published();
            }
            if(is_array($id)){
                foreach ($id as $key => $value) {
                   $this->query->where('id',$value);
                }
                return $this->query->get();
            }else{
                $this->query->where('id',$id);
                return $this->query->first();
            }
        // });
    }

    public function getViaSlug ($slug , $published = true) 
    {
        // return cache()->remember('product_categories_get_via_slug',config()->get('cms.cache_time'),function() use ($slug,$published){
            if($published){
               $this->is_published();
            }
            return $this->getSlug($slug);
        // });
    }
    
    /**
     * ** Lấy danh sách
     * @param $published : trạng thái xuất bản
     * @param $parent_id : lấy danh mục cha
     * @param $limit : giới hạng record
     * @param $orderby : latest (mới nhất) : oldest (cũ nhất)
     * @param $objectId : loại bỏ đối tượng id
     */
    public function getLists($published = null, $parent_id = null ,$objectId = null, $limit = null,$orderby = 'latest') {
        // return cache()->remember('product_categories_get_list',config()->get('cms.cache_time'),function() use ($published, $parent_id ,$objectId, $limit,$orderby){
            if($published){
               $this->is_published();
            }
            if($parent_id){
                $this->query->where('parent_id',$parent_id);
            }

            if($objectId){
                $this->unsetOject($objectId);
            }
            switch ($orderby) {
                case 'latest':
                    $this->query->latest();
                    break;
                case 'oldest':
                    $this->query->oldest();
                    break;
                case 'featured':
                    $this->query->where('featured',true);
                    break;
                case 'sort_asc':
                    // $this->query->where('featured',true);
                    $this->query->orderBy('pos','ASC');
                    break;
                case 'featured_asc':
                    $this->query->where('featured',true);
                    $this->query->orderBy('pos','ASC');
                    break;
                default:
                    $this->query->orderBy('pos','ASC');
                    break;
            }
            
            return $limit ? $limit == 1 ? $this->query->first() : $this->query->paginate($limit) : $this->query->get();
        // });
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function update($id, $data) {
        $data = $this->model->fill($data)->getAttributes();
        if(is_array($id))
            return $this->model->whereIn('id',$id)->update($data);
        return $this->model->where('id',$id)->update($data);
    }

    public function delete($id) {
        if(is_array($id)){
            $this->model->whereIn('parent_id',$id)->update(['parent_id' => 0]);
        }else{
            $this->model->where('parent_id',$id)->update(['parent_id' => 0]);
        }
        return $this->deleteWithLanguage($id);
    }
}