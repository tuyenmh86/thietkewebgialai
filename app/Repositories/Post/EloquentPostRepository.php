<?php
namespace App\Repositories\Post;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelsTrait;
use App\Traits\Translator;
use App\Models\Post\Post;

class EloquentPostRepository{
    use ModelsTrait,Translator;

    protected $model;
    protected $query;

    public function __construct(Post $model){
         $this->model = $model;
         $this->query = $this->model->query();
    }

    public function getViaSlug ($slug , $published = true) 
    {
        // return cache()->remember('post_get_via_slug',config()->get('cms.cache_time'),function() use ($slug,$published){
            if($published){
               $this->is_published();
            }
            return $this->getSlug($slug);
        // });
    }
    /**
     * ** Lấy danh sách
     * @param $published : trạng thái xuất bản
     * @param $limit : giới hạng record
     * @param $orderby : latest (mới nhất) : oldest (cũ nhất)
     * @param $objectId : loại bỏ đối tượng đang sử dụng bởi Id
     * @param $posison : vị trí
     */
    public function getLists($published = null,$categoryId = null, $limit = null,$orderby = 'latest', $objectId = null , $byAccount = null ) {
        // return cache()->remember('posts_get_list',config()->get('cms.cache_time'),function() use ($published,$categoryId, $limit,$orderby, $objectId , $byAccount ){
            if($published){
               $this->is_published();
            }
            if(!is_null($categoryId)){
                $this->query->whereHas('categories', function($q) use ($categoryId){
                    $q->where('id',$categoryId);
                });
            }
            if(!is_null($objectId)){
                $this->unsetOject($objectId);
            }
            if(!is_null($byAccount)){
                $this->query->where('user_id',$byAccount);
            }
            switch ($orderby) {
                case 'latest':
                    $this->query->latest();
                    break;
                case 'oldest ':
                    $this->query->oldest ();
                    break;
                case 'popular':
                    $this->query->latest ('view');
                    break;
                case 'featured':
                    $this->query->where('featured',true)->latest();
                    break;
                default:
                    $this->query->latest('pos');
                    break;
            }
            // if($limit==1){
            //     return  $this->query->first();
            // }else{
            //     return $this->query->paginate($limit);
            // }
            // return $this->query->get();
            return $limit ? $limit == 1 ? $this->query->first() : $this->query->paginate($limit) : $this->query->get();
        // });
    }

    public function searchs($request){

        $this->is_published();

        if($request->has('tag')){
            $this->query->whereHas('tags',function($q) use ($tag){
                $q->where('alias',$request->get('tag'));
            });
        }

        if($request->has('q')){
            $this->query->where('title', 'like', '%'.$request->get('q').'%');
        }

        if($request->has('category')){
            $this->query->whereHas('categories',function($q) use ($category){
                $q->where('id',$request->get('category'));
            });
        }
        return $this->query->paginate(12);
    }

    public function find($id){
        
        return $this->model->find($id);
    }

    public function update($id, $data) {
        $data = $this->model->fill($data)->getAttributes();
        if(is_array($id))
            return $this->model->whereIn('id',$id)->update($data);
        return $this->model->where('id',$id)->update($data);
    }

    public function max(){
        return $this->model->max('pos');
    }

    public function create($data) {
        return $this->model->create($data);
    }

}