<?php

namespace App\Repositories\Post;



use Illuminate\Database\Eloquent\Model;

use App\Traits\ModelsTrait;
use App\Traits\Translator;

use App\Models\Post\Tag;



class EloquentTagRepository{



    use ModelsTrait,Translator;

    

    protected $model;

    protected $query;

     /**

     * @var \Illuminate\Database\Eloquent\Model An instance of the Eloquent Model

     */

    public function __construct(Tag $model){

         $this->model = $model;

         $this->query = $this->model->query();

    }

    

    /**

     * ** Lấy danh sách

     * @param $published : trạng thái xuất bản

     * @param $limit : giới hạng record

     * @param $orderby : latest (mới nhất) : oldest (cũ nhất)

     * @param $objectId : loại bỏ đối tượng id

     */

    public function getLists($published = null,$objectId = null, $limit = null,$orderby = 'latest') {

        if($published){

            $this->is_published();

        }

        if($objectId){

            $this->unsetOject($objectId);

        }

        switch ($orderby) {

            case 'latest':

                $this->query->latest();

                break;

            case 'oldest':

                $this->query->oldest();

                break;

            case 'featured':

                $this->query->where('featured',true)->latest();

                break;

            default:

                $this->query->latest('pos');

                break;

        }

        return $limit ? $limit == 1 ? $this->query->first() : $this->query->paginate($limit) : $this->query->get();

    }

 

    public function find($id){

        return $this->model->find($id);

    }

    public function update($id, $data) {

        $data = $this->model->fill($data)->getAttributes();

        if(is_array($id))

            return $this->model->whereIn('id',$id)->update($data);

        return $this->model->where('id',$id)->update($data);

    }

}