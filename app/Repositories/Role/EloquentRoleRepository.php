<?php

namespace App\Repositories\Role;



use Illuminate\Database\Eloquent\Model;

use App\Traits\ModelsTrait;

use App\Models\Role\Role;



class EloquentRoleRepository{



    use ModelsTrait;



    protected $model;

    protected $query;



    public function __construct(Role $model){

         $this->model = $model;

         $this->query = $this->model->query();



    }



    /**

     * ** Lấy danh sách

     * @param $published : trạng thái xuất bản

     * @param $limit : giới hạng record

     * @param $orderby : latest (mới nhất) : oldest (cũ nhất)

     * @param $objectId : loại bỏ đối tượng đang sử dụng bởi Id

     */

    public function getLists($published = null,$objectId = null, $limit = null,$orderby = 'latest') {

        $this->unsetOject(1);

        if($published){

            $this->is_published();

        }

        if($objectId){

            $this->unsetOject($objectId);

        }

        switch ($orderby) {

            case 'latest':

                $this->query->latest();

                break;

            case 'oldest':

                $this->query->oldest();

                break;

            default:

                $this->query->latest('pos');

                break;

        }



        return $limit ? $limit == 1 ? $this->query->first() : $this->query->paginate($limit) : $this->query->get();

    }

 

    public function find($id){

        return $this->model->find($id);

    }



    public function create($data){

        return $this->model->create($data);

    }



    public function update($id, $data) {

        $data = $this->model->fill($data)->getAttributes();

        if(is_array($id))

            return $this->model->whereIn('id',$id)->update($data);

        return $this->model->where('id',$id)->update($data);

    }



    public function delete($id) {

        if(is_array($id)){

            return $this->model->whereIn('id',$id)->delete();

        }else{

            return $this->model->where('id',$id)->delete();

        }

    }

}