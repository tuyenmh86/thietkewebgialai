<?php

namespace App\Repositories\Admin;



use App\Admin;

use App\Traits\ModelsTrait;
use App\Traits\Translator;



class EloquentAdminRepository {



	use ModelsTrait,Translator;



	protected $model;

	protected $query;



	public function __construct(Admin $model) {

		$this->model = $model;

		$this->query = $this->model->query();

	}



	public function getViaId($id, $published = true) {

		if ($published) {

			$this->query->where('published', true);

		}

		$this->query->where('id', $id);

		return $this->query->first();

	}



	/**

	 * ** Lấy danh sách

	 * @param $published : trạng thái xuất bản

	 * @param $limit : giới hạng record

	 * @param $orderby : latest (mới nhất) : oldest (cũ nhất)

	 * @param $roleId : lấy danh sách bởi quyền quản trị

	 * @param $objectId : loại bỏ đối tượng đang sử dụng bởi Id

	 * @param $pos : vị trí

	 */

	public function getLists($published = null, $roleId = null, $objectId = null, $limit = null, $orderby = 'latest') {

		$this->unsetRoot();

		if ($published) {

			$this->is_published();

		}

		if ($roleId) {

			$this->query->where('role_id', $roleId);

		}

		if ($objectId) {

			$this->unsetOject($objectId);

		}

		switch ($orderby) {

		case 'latest':

			$this->query->latest();

			break;

		case 'oldest ':

			$this->query->oldest();

			break;

		default:

			$this->query->latest('pos');

			break;

		}

		return $limit ? $limit == 1 ? $this->query->first() : $this->query->paginate($limit) : $this->query->get();

	}



	public function find($id) {

		return $this->model->find($id);

	}



	public function update($id, $data) {

		$data = $this->model->fill($data)->getAttributes();

		if (is_array($id)) {

			return $this->model->whereIn('id', $id)->update($data);

		}

		return $this->model->where('id', $id)->update($data);

	}
}