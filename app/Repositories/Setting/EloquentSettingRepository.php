<?php
namespace App\Repositories\Setting;

use Illuminate\Database\Eloquent\Builder;
use App\Models\Setting\Setting;
use App\Traits\Translator;

class EloquentSettingRepository{

    use Translator;

    protected $model;

    public function __construct(Setting $model){
         $this->model = $model;
    }

    public function find($id)
    {
        
        return $this->model->find($id);
    }

    public function create($data)
    {
        return $this->model->create($data);
    }
}