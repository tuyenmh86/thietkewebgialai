<?php
namespace App\Repositories\Widget;

use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelsTrait;
use App\Traits\Translator;
use App\Models\Widgets\Widgets;

class EloquentWidgetRepository{
    use ModelsTrait,Translator;

    protected $model;
    protected $query;
     /**
     * @var \Illuminate\Database\Eloquent\Model An instance of the Eloquent Model
     */
    public function __construct(Widgets $model){
         $this->model = $model;
         $this->query = $this->model->query();
    }

    public function getViaSlug ($alias,$published = true) 
    {
        if($published) {
            $this->is_published();
        }
        $this->query->where('alias',$alias);
        if($resutl = $this->query->first()){
            return $resutl->language('content');
        }
    }
 
    /**
     * ** Lấy danh sách
     * @param $published : trạng thái xuất bản
     * @param $limit : giới hạng record
     * @param $orderby : latest (mới nhất) : oldest (cũ nhất)
     * @param $objectId : loại bỏ đối tượng đang sử dụng bởi Id
     */
    public function getLists($published = null,$objectId = null, $limit = null,$orderby = 'latest') {
        $this->model = $this->model->query();
        if($published){
            $this->is_published();
        }
        if($objectId){
            $this->unsetOject($objectId);
        }
        switch ($orderby) {
            case 'latest':
                $this->query->latest();
                break;
            case 'oldest ':
                $this->query->oldest ();
                break;
            default:
                $this->query->latest('pos');
                break;
        }
        return $limit ? $limit == 1 ? $this->query->first() : $this->query->paginate($limit) : $this->query->get();
    }
 
    public function find($id)
    {
        return $this->model->find($id);
    }

    public function create($data)
    {
        return $this->model->create($data);
    }

    public function update($id, $data) {
        $data = $this->model->fill($data)->getAttributes();
        if(is_array($id))
            return $this->model->whereIn('id',$id)->update($data);
        return $this->model->where('id',$id)->update($data);
    }

    public function delete($id) {
        if(is_array($id))
            return $this->model->whereIn('id',$id)->delete();
        return $this->model->where('id',$id)->delete();
    }
}