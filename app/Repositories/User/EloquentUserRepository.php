<?php

namespace App\Repositories\User;



use Illuminate\Database\Eloquent\Builder;

use App\Traits\ModelsTrait;
use App\Traits\Translator;

use App\User;



class EloquentUserRepository{



    use ModelsTrait,Translator;



    protected $model;

    protected $query;

     /**

     * @var \Illuminate\Database\Eloquent\Model An instance of the Eloquent Model

     */

    public function __construct(User $model){

         $this->model = $model;

         $this->query = $this->model->query();

    }

    

    public function getViaId ($id , $published = true) {

        if($published){

            $this->query->where('published',true);

        }

        $this->query->where('id',$id);

        return $this->query->first();

    }

    /**

     * ** Lấy danh sách

     * @param $published : trạng thái xuất bản

     * @param $limit : giới hạng record

     * @param $orderby : latest (mới nhất) : oldest (cũ nhất)

     * @param $objectId : loại bỏ đối tượng đang sử dụng bởi Id

     */

    public function getLists($published = null,$objectId = null, $limit = null,$orderby = 'latest') {

        $this->model = $this->model->query();

        if($published){

            $this->is_published();

        }

        if($objectId){

            $this->unsetOject($objectId);

        }

        switch ($orderby) {

            case 'latest':

                $this->query->latest();

                break;

            case 'oldest ':

                $this->query->oldest ();

                break;

            default:

                $this->query->latest('pos');

                break;

        }

        return $limit ? $limit == 1 ? $this->query->first() : $this->query->paginate($limit) : $this->query->get();

    }

 

    public function find($id){

        return $this->model->find($id);

    }

    public function update($id, $data) {

        $data = $this->model->fill($data)->getAttributes();

        if(is_array($id))

            return $this->model->whereIn('id',$id)->update($data);

        return $this->model->where('id',$id)->update($data);

    }


    public function checkLogedFB($FacebookId){ 

        return $this->model->where('facebook_id',$FacebookId)->where('provider','facebook')->first();

    }


    public function create($data) {
        return $this->model->create($data);
    }
    
    public function checkEmail($Email){

        return $this->model->where('email',$Email)->first();

    }

}