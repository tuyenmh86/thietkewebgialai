<?php
namespace App\Repositories\Permission;

use Illuminate\Database\Eloquent\Builder;
use App\Models\Permission\Permission;
use App\Traits\ModelsTrait;

class EloquentPermissionRepository {

    use ModelsTrait;

    protected $model;
    protected $query;

    public function __construct(Permission $model){
         $this->model = $model;
         $this->query = $this->model->query();
    }

    /**
     * ** Lấy danh sách
     * @param $published : trạng thái xuất bản
     * @param $limit : giới hạng record
     * @param $orderby : latest (mới nhất) : oldest (cũ nhất)
     * @param $objectId : loại bỏ đối tượng đang sử dụng bởi Id
     * @param $posison : vị trí
     */
    public function getLists($published = null,$objectId = null, $limit = null,$orderby = 'latest') {
        if($published){
            $this->is_published();
        }
        if($objectId){
            $this->unsetOject($objectId);
        }
        switch ($orderby) {
            case 'latest':
                $this->query->latest();
                break;
            case 'oldest':
                $this->query->oldest();
                break;
            default:
                $this->query->latest('pos');
                break;
        }
        return $limit ? $limit == 1 ? $this->model->first() : $this->model->paginate($limit) : $this->model->get();
    }
    
    public function find($id)
    {
        
        return $this->model->find($id);
    }

    public function create($data)
    {
        return $this->model->create($data);
    }

    public function update($id, $data) {
        $data = $this->model->fill($data)->getAttributes();
        if(is_array($id))
            return $this->model->whereIn('id',$id)->update($data);
        return $this->model->where('id',$id)->update($data);
    }

    public function delete($id) {
        if(is_array($id))
            return $this->model->whereIn('id',$id)->delete();
        return $this->model->where('id',$id)->delete();
    }

    public function groupPosision()
    {
        return $this->model->groupBy('pos')->pluck('pos');
    }

    public function getListsByPosision($pos,$limit = null)
    {
        if($limit == 1)
            return $this->model->where('pos',$pos)->oldest('created_at')->first();
        return $this->model->where('pos',$pos)->oldest('created_at')->get();
    }   
}