<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Models\Role\Role;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Blade;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // DELETING ROLES 
        Role::deleting(function ($role) {
            $role->admins()->delete();
        });
        // Add to Schame
        Schema::defaultStringLength(191);
        //Add this custom validation rule.
        Validator::extend('alpha_spaces', function ($attribute, $value) {
        // This will only accept alpha and spaces. 
        // If you want to accept hyphens use: /^[\pL\s-]+$/u.
        return preg_match("/^[A-Za-z0-9_!@#$%^&*()-+<>]*$/", $value); 

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (config('app.env') == 'local') {
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
        }
    }
}
