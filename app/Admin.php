<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\Translator;

class Admin extends Authenticatable
{
    use Notifiable,Translator;

    protected $table = "admins";
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'username',
        'password', 
        'email', 
        'last_login',
        'full_name',
        'address',
        'phone',
        'introduce',
        'profile_image',
        'role_id',
        'is_spadmin',
        'published',
        'pos',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function is_supperAdmin (){
        return $this->is_spadmin;
    }

    public function is_published (){
        return $this->published;
    }

    public function logs(){
        return $this->hasMany('App\Models\LogAdmin\LogAdmin');
    }

    public function products(){
        return $this->hasMany('App\Models\Product\Product');
    }

    public function posts(){
        return $this->hasMany('App\Models\Post\Post');
    }

    public function pages(){
        return $this->hasMany('App\Models\Page\Page','admin_id');
    }

    public function role(){
        return $this->belongsTo('App\Models\Role\Role');
    }

    public function hasRole()
    {
        if($this->role){
            return $this->role;
        }
    }
    public function checkRoleSupperAdmin(){
        if($this->role->id == 1){
            return true;
        }
    }

    public function checkRolePublic(){
            return $this->role->published;
    }

    public function access($actionRoute){
        foreach ($this->role->permissions as $key => $value) {
           if(trim($value->display_name) == $actionRoute){
                if($this->is_published()){
                    return true;
                }
           }
        }
    }

    public function link()
    {
        return url(trans('route.investors').'/'.$this->id.'-'.to_slug_url($this->full_name));
    }
}
