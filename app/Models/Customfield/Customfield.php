<?php

namespace App\Models\Customfield;

use Illuminate\Database\Eloquent\Model;

class Customfield extends Model
{

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'name', 
        'field', 
        'table_name',
        'type',  
    ];

    public $timestamps = true;
}
