<?php

namespace App\Models\Menu;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translator;

class MenuItem extends Model
{
    use Translator;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu_items';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'label',
        'link',
        'parent',
        'sort',
        'class',
        'menu',
        'depth'
    ];

    public $timestamps = true;

    
    /*
    |--------------------------------------------------------------------------
    | Relationship Methods
    |--------------------------------------------------------------------------
    */
    /**
     * Return Status is published
     *
     * @return QueryBuilder
     */
    
    public function menu()
    {
        return $this->belongsTo('App\Menu\Menu');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Menu\MenuItem','parent');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Menu\MenuItem');
    }

    public function renderLink ($link)
    {
        // return cache()->remember('menu_render_link',config()->get('cms.cache_time'),function() use ($link){
            $link = explode('?',$link);
   
            switch ($link[0]) {
                case 'page':

                    $s_segment = getViaPageId($link[1]) ? getViaPageId($link[1])->language('alias'): '';
                    return url($s_segment ? trans('route.pages').'/'.$s_segment : '');
                    break;

                case 'post':
                    $s_segment = getViaPostCateId($link[1]) ? getViaPostCateId($link[1])->language('alias') : '';
                    return url($s_segment ? trans('route.posts').'/'.$s_segment : '/');
                    break;

                case 'product':
                    $s_segment = getViaProductCateId($link[1]) ? getViaProductCateId($link[1])->language('alias') : '';
                    return url($s_segment ? trans('route.products').'/'.$s_segment : '/');
                    break;
                
                default:
                    return url($link[0]);
                    break;
            }
        // });
    }
}
