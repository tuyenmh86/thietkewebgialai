<?php



namespace App\Models\Menu;



use Illuminate\Database\Eloquent\Model;



class Menu extends Model

{

    /**

     * The database table used by the model.

     *

     * @var string

     */

    protected $table = 'menus';



    protected $primaryKey = 'id';

    

    protected $fillable = [

        'name'

    ];



    public $timestamps = true;

    

    /*

    |--------------------------------------------------------------------------

    | Relationship Methods

    |--------------------------------------------------------------------------

    */

    /**

     * Return Status is published

     *

     * @return QueryBuilder

     */

    

    public function attributes(){
        return $this->hasMany('App\Models\Menu\MenuItem','menu')->orderBy('sort');
    }



}

