<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'setting';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'site_title',
        'site_logo',
        'site_favicon',
        'site_copyright',
        'site_email',
        'site_phone', 
        'site_address', 
        'site_description', 
        'google_analytics', 
        'google_site_verification', 
        'seo_title', 
        'seo_keywords', 
        'seo_description', 
        'custom_css',
        'custom_js',
    ];

    public $timestamps = true;
}
