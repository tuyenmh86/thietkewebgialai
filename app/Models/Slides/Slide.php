<?php

namespace App\Models\Slides;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'slides';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'title','alias','published'
    ];

    public $timestamps = true;

    
    public function is_published()
    {
        return $this->published;
    }

    public function items ()
    {
    	return $this->hasMany('App\Models\Slides\Slide_item','slide_id');
    }
}
