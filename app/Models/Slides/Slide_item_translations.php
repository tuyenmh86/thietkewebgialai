<?php

namespace App\Models\Slides;

use Illuminate\Database\Eloquent\Model;

class Slide_item_translations extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'slide_item_translations';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'slide_item','name','content','link','locale'
    ];

    public $timestamps = true;

}
