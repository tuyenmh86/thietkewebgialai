<?php

namespace App\Models\Slides;

use Illuminate\Database\Eloquent\Model;

class Slide_item extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'slide_items';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'slide_id','image','link','content','caption'
    ];

    public $timestamps = true;


    public function slide ()
    {
    	return $this->belongsTo('App\Models\Slides\Slide');
    }

}
