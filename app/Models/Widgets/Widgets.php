<?php

namespace App\Models\Widgets;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translator;

class Widgets extends Model
{
    use Translator;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'widgets';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'title', 
        'alias', 
        'content', 
        'published'
    ];

    public $timestamps = true;


    public function is_published()
    {
        return $this->published;
    }
}
