<?php

namespace App\Models\Page;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translator;

class Page extends Model
{
    use Translator;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pages';

    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    public $timestamps = true;

    protected $fillable = [
        'title',
        'alias',
        'description',
        'content',
        'seo_title',
        'seo_description',
        'seo_keywords',
        'admin_id',
        'featured_image', 
        'view', 
        'promotion_price',
        'published', 
        'featured', 
        'pos'
    ];
    
    /*
    |--------------------------------------------------------------------------
    | Relationship Methods
    |--------------------------------------------------------------------------
    */
    /**
     * Return Status is published
     *
     * @return QueryBuilder
     */
    public function is_published()
    {
        return $this->published;
    }
    /**
     * Return is Featured
     *
     * @return QueryBuilder
     */
    public function is_featured()
    {
        return $this->featured;
    }

    public function admin(){
        return $this->belongsTo('App\Admin');
    }

    public function link()
    {
        return url(trans('route.pages').'/'.$this->language('alias'));
    }
}
