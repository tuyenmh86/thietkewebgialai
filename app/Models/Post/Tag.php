<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translator;

class Tag extends Model
{
    use Translator;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tags_post';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'title',
        'alias',
        'description',
        'published',
        'featured',
        'pos',
    ];

    public $timestamps = true;


    /*
    |--------------------------------------------------------------------------
    | Relationship Methods
    |--------------------------------------------------------------------------
    */
    /**
     * Return Status is published
     *
     * @return QueryBuilder
     */
    public function is_published()
    {
        return $this->published;
    }

    /**
     * Return is Featured
     *
     * @return QueryBuilder
     */
    public function is_featured()
    {
        return $this->featured;
    }
    /**
     * many-to-many relationship method
     *
     * @return QueryBuilder
     */
    public function posts()
    {
        return $this->belongsToMany('App\Models\Post\Post');
    }

    public function link()
    {
        return trans('route.searchs').'/'.trans('route.posts').'?q='.$this->language('alias');
    }

}
