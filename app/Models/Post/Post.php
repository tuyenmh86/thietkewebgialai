<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translator;
use Carbon\Carbon;

class Post extends Model
{

    use Translator;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'title',
        'alias',
        'description',
        'content', 
        'seo_title', 
        'seo_description',
        'seo_keywords', 
        'admin_id', 
        'user_id',
        'featured_image',
        'view', 
        'published', 
        'featured',
        'pos'
    ];

    public $timestamps = true;
    
    /*
    |--------------------------------------------------------------------------
    | Relationship Methods
    |--------------------------------------------------------------------------
    */
    /**
     * Return Status is published
     *
     * @return QueryBuilder
     */
    public function is_published()
    {
        return $this->published;
    }
    /**
     * Return is Featured
     *
     * @return QueryBuilder
     */
    public function is_featured()
    {
        return $this->featured;
    }
   
    public function categories(){
        return $this->belongsToMany('App\Models\Post\Category','category_post','post_id','category_id')->withTimestamps();
    }

    public function admin(){
        return $this->belongsTo('App\Admin');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Post\Tag','tag_post','post_id','tag_id')->withTimestamps();
    }

    public function link()
    {
        return url(trans('route.posts').'/'.$this->categories[0]->language('alias').'/'.$this->alias);
    }

    public function getCreatedAtAttribute($value){
        return Carbon::parse($value);
    }
}
