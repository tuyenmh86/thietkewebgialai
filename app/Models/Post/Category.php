<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translator;

class Category extends Model
{
    use Translator;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories_post';

    protected $primaryKey = 'id';

    protected $guarded = [];

    public $timestamps = true;
    protected $fillable = [
        'title',
        'alias',
        'description',
        'parent_id', 
        'published', 
        'featured',
        'pos'
    ];
    
    /**
     * Return Status is published
     *
     * @return QueryBuilder
     */
    public function is_published()
    {
        return $this->published;
    }

    /**
     * Return is Featured
     *
     * @return QueryBuilder
     */
    public function is_featured()
    {
        return $this->featured;
    }


    public function parent()
    {
        return $this->belongsTo('App\Models\Post\Category');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Post\Category','parent_id');
    }

    public function posts(){
        return $this->belongsToMany('App\Models\Post\Post');
    }

    public function link()
    {
        return url(trans('route.posts').'/'.$this->language('alias'));
    }
}
