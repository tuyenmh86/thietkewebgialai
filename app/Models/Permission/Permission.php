<?php

namespace App\Models\Permission;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    //
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'name', 'display_name', 'module_name','pos','published',
    ];

    public $timestamps = true;

    /*
    |--------------------------------------------------------------------------
    | Relationship Methods
    |--------------------------------------------------------------------------
    */

    /**
     * return status is Published
     *
     * @return QueryBuilder
     */
    public function is_published()
    {
        return $this->published;
    }
    /**
     * many-to-many relationship method
     *
     * @return QueryBuilder
     */
    public function roles()
    {
        return $this->belongsToMany('App\Models\Role\Role');
    }
}
