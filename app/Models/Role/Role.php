<?php

namespace App\Models\Role;

use Illuminate\Database\Eloquent\Model;
use App\Admin;

class Role extends Model
{
    //
    protected $table = 'roles';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'name', 'display_name', 'description','published','advanced','pos'
    ];

    public $timestamps = true;

    //****************************** IS PUBLISHED *************************
    public function is_published() {
        return $this->published;
    }
    //****************************** HAS ADVANCED *************************
    public function hasAdvanced() {
        return $this->advanced;
    }
    public function admins(){
        return $this->hasMany('App\Admin','role_id');
    }

    public function permissions() {
        return $this->belongsToMany('App\Models\Permission\Permission','permission_role','role_id','permission_id')->withTimestamps();
    }
}
