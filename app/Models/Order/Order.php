<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
    	'customer_info',
    	'order_detail',
    	'total'
    ];

    protected $casts = [
    	'customer_info' => 'json',
    	'order_detail' => 'json'
    ];

    public $timestamps = true;
}
