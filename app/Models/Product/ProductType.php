<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $table = 'product_types';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'name', 
        'description',  
        'published'
    ];

    public $timestamps = true;


    public function is_published(){
        return $this->published;
    }

    
    public function products(){
        return $this->hasMany('App\Models\Product\Product','type_id');
    }
}
