<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translator;

class Tag extends Model
{
    use Translator;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tags_product';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'title',
        'alias',
        'description',
        'published',
        'featured',
        'pos',
    ];

    public $timestamps = true;


    /*
    |--------------------------------------------------------------------------
    | Relationship Methods
    |--------------------------------------------------------------------------
    */
    /**
     * Return Status is published
     *
     * @return QueryBuilder
     */
    public function is_published()
    {
        return $this->published;
    }

    /**
     * Return is Featured
     *
     * @return QueryBuilder
     */
    public function is_featured()
    {
        return $this->featured;
    }
    /**
     * many-to-many relationship method
     *
     * @return QueryBuilder
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product\Product');
    }

    public function link()
    {
        return trans('route.searchs').'/'.trans('route.products').'?q='.$this->language('alias');
    }

}
