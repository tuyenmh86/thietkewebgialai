<?php

namespace App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use App\Traits\ModelsTrait;

class ProductAttributeVarchar extends Model
{
	use ModelsTrait;
	
    protected $table = 'product_attribute_varchar';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'attribute_id', 
        'product_id',  
        'value'
    ];

    protected $casts = [
        'value' => 'json'
    ];

    public $timestamps = false;

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function attribute(){
        return $this->belongsTo(ProductAttribute::class);
    }
}
