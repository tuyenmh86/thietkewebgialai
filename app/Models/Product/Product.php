<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translator;
use App\Traits\ModelsTrait;

class Product extends Model
{

    use Translator,ModelsTrait;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'title',
        'alias',
        'description',
        'content',
        'seo_title',
        'seo_description',
        'seo_keywords',
        'parent_id', 
        'admin_id',
        'user_id', 
        'featured_image', 
        'image_list', 
        'view', 
        'sku',
        'price',
        'promotion_price',
        'published', 
        'featured', 
        'pos',
        'ung-dung',
        'huong-dan-thi-cong'
    ];

    public $timestamps = true;

    protected $casts = [
        'image_list' => 'json'
    ];

    protected $with = ['attribute_value_integer','attribute_value_varchar','attribute_value_text'];

    /*
    |--------------------------------------------------------------------------
    | Relationship Methods
    |--------------------------------------------------------------------------
    */
    /**
     * Return Status is published
     *
     * @return QueryBuilder
     */
    public function is_published()
    {
        return $this->published;
    }
    /**
     * Return is Featured
     *
     * @return QueryBuilder
     */
    public function is_featured()
    {
        return $this->featured;
    }

    public function categories()
    {
        return 
            $this->belongstoMany(
                'App\Models\Product\Category',
                'category_product',
                'product_id',
                'category_id'
            )
            ->withTimestamps();
    }

    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Product\Tag','tag_product','product_id','tag_id')->withTimestamps();
    }

    public function contacts()
    {
        return $this->hasMany('App\Models\Contact\Contact','product_id');
    }

    public function product_type(){
        return $this->belongsTo('App\Models\Product\ProductType');
    }

    public function link(){

        return url(trans('route.products').'/'.$this->categories[0]->language('alias').'/'.$this->alias);
    }

    public function attribute_value_integer(){
        return $this->hasMany(ProductAttributeInteger::class,'product_id');
    }

    public function attribute_value_varchar(){
        return $this->hasMany(ProductAttributeVarchar::class,'product_id');
    }

    public function attribute_value_text(){
        return $this->hasMany(ProductAttributeText::class,'product_id');
    }

    public function getProductAttributeValueAttribute(){
        $relations = $this->getRelations();
        unset($relations['categories']);
        return $relations;
    }

    public function percent_price(){
        if($this->promotion_price > 0 && $this->price > 0){
            return round((($this->price - $this->promotion_price) / $this->price) * 100,2);
        }
    }

    public function getImageListAttribute($value){
        return json_decode(json_decode($value));
    }
}
