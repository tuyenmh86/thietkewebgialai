<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $table = 'product_attributes';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'field', 
        'label',  
        'type',
        'is_required',
        'is_unique',
        'published',
        'position',
        'catalog'
    ];

    protected $casts = [
        'is_required' => 'json'
    ];

    public $timestamps = true;


    public function is_published(){
        return $this->published;
    }

    public function category() {
        return $this->belongsTo('App\Models\Product\Category','catalog');
    }

    public function attribute_value_integer(){
        return $this->hasMany(ProductAttributeInteger::class,'attribute_id');
    }

    public function attribute_value_varchar(){
        return $this->hasMany(ProductAttributeVarchar::class,'attribute_id');
    }

    public function attribute_value_text(){
        return $this->hasMany(ProductAttributeText::class,'attribute_id');
    }
}
