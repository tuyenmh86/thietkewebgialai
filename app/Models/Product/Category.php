<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translator;

class Category extends Model
{
    use Translator;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories_product';

    protected $primaryKey = 'id';
    
    protected $fillable = [
        'title', 
        'alias',  
        'description', 
        'parent_id',
        'featured_image',
        'published',
        'featured',
        'pos'
    ];

    public $timestamps = true;

    /**
     * Return Status is published
     *
     * @return QueryBuilder
     */
    public function is_published()
    {
        return $this->published;
    }

    /**
     * Return is Featured
     *
     * @return QueryBuilder
     */
    public function is_featured()
    {
        return $this->featured;
    }


    public function parent()
    {
        /**
         * Định nghĩa Invert với chính nó xác định đối tượng cha
         * Dựa vào ID của model Category 
         */
        return $this->belongsTo('App\Models\Product\Category');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Product\Category','parent_id');
    }

    public function products(){
        return $this->belongsToMany('App\Models\Product\Product');
    }

    public function link()
    {
        return url(trans('route.products').'/'.$this->language('alias'));
    }

    public function attributes()
    {
        return $this->hasMany('App\Models\Product\ProductAttribute','catalog');
    }
}
