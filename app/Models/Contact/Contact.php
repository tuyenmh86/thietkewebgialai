<?php

namespace App\Models\Contact;

use App\Models\Product\Product;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
    protected $fillable = ['name', 'email', 'phone', 'message', 'product_id', 'manager_id', 'published'];
    public $timestamp = true;

    public function is_published()
    {
        return $this->published;
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function manager()
    {
        return $this->belongsTo(User::class,'manager_id','id');
    }
}
