<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Redirect;

use Closure;
use Auth;

class CheckPermission
{
    protected $auth;
    public function __construct()
    {
        if(Auth::guard('admin')->check()){
            $this->auth = Auth::guard('admin')->user();
        } 
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $module = null, $permission = null)
    {
        if($this->auth->hasRole()){
            if(!$this->auth->checkRoleSupperAdmin()){
                if($module != null && $permission != null){
                    // Check Role is published
                    if($this->auth->checkRolePublic()){
                        if(CAN($module,$permission)){
                            return $next($request);
                        }
                        return redirect()->route('errors.404');
                    }
                   return Auth::guard('admin')->logout();
                }
                return redirect()->route('errors.404');
            }
             // Role Super Admin Default
            return $next($request);
        }
        return redirect()->route('errors.404');
    }
}
