<?php

namespace App\Http\Middleware;

use Closure, Auth, Session, App, Config;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!request()->has('lang')){
            if(session()->has('locale')){
                $locale = session()->get('locale');
            }
            else {
                session()->put('locale',config('app.fallback_locale'));
                $locale = config('app.fallback_locale');
            }

            app()->setLocale($locale);
        }
        else {
            session()->put('locale',request()->get('lang'));
            session()->save();
            app()->setLocale(session()->get('locale'));
        }

        return $next($request);
    }
}
