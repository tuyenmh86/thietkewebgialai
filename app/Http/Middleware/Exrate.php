<?php

namespace App\Http\Middleware;

use Closure;

class Exrate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('current'))
        {
            session()->put('current',$request->get('current'));
            session()->save();
        }

        return $next($request);
    }
}
