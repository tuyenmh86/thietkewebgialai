<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PUT')
          {
            // Update operation, exclude the record with id from the validation:
            $email_rules = 'required|email|max:100|unique:users,email,'. $this->getSegmentFromEnd() . ',id';
            $password = '';
            $re_password = '';
          }
          else
          {
            // Create operation. There is no id yet.
            $email_rules = 'required|email|max:100|unique:users';
            $password = 'required|min:6';
            $re_password = 'required|same:password';
          }
        return [
            //
            'full_name' => 'required|max:60',
            'email' => $email_rules,
            'password' => $password,
            're_password' => $re_password,
            // 'role_id' => 'required|not_in:-- Role --',
            'phone' => 'required|max:25',
            'address' => 'required|max:150',
            'pos' => 'max:11',
            
            
        ];
    }
    private function getSegmentFromEnd($position_from_end = 1) {
        $segments = $this->segments();
        return $segments[sizeof($segments) - $position_from_end];
    }
}
