<?php

namespace App\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PUT'){
            // Update operation, exclude the record with id from the validation:
            $name_rules = 'required|unique:roles,name,'. $this->getSegmentFromEnd() . ',id';
          }else{
            // Create operation. There is no id yet.
            $name_rules = 'required|unique:roles';
          }
        return [
            //
            'name' => $name_rules,
            'pos' => 'required|max:10',
        ];
    }
    private function getSegmentFromEnd($position_from_end = 1) {
        $segments = $this->segments();
    return $segments[sizeof($segments) - $position_from_end];
    }
}
