<?php

namespace App\Http\Requests\Permission;

use Illuminate\Foundation\Http\FormRequest;

class PermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PUT')
          {
            // Update operation, exclude the record with id from the validation:
            $display_name_rules = 'required|max:50|unique:permissions,display_name,'. $this->getSegmentFromEnd() . ',id';
          }
          else
          {
            // Create operation. There is no id yet.
            $display_name_rules = 'required|max:50|unique:permissions';
          }
        return [
            //
            'name' => 'required|max:50',
            'display_name' => $display_name_rules,
            'pos' => 'max:11',
            'module_name' => 'required|max:20',
        ];
    }
    private function getSegmentFromEnd($position_from_end = 1) {
        $segments = $this->segments();
    return $segments[sizeof($segments) - $position_from_end];
    }
}
