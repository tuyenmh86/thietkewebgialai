<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PUT')
          {
            // Update operation, exclude the record with id from the validation:
            $email_rules = 'required|email|unique:admins,email,'. $this->getSegmentFromEnd() . ',id';
            $username_rules = 'required|alpha|max:50|unique:admins,username,'. $this->getSegmentFromEnd() . ',id';
            $password = '';
            $re_password = '';
          }
          else
          {
            // Create operation. There is no id yet.
            $email_rules = 'required|email|unique:admins';
            $username_rules = 'required|alpha|max:50|unique:admins';
            $password = 'required|min:5';
            $re_password = 'required|same:password';
          }
        return [
            //
            'username' => $username_rules,
            'full_name' => 'required|max:60',
            'address' => 'required',
            'phone' => 'required',
            'email' => $email_rules,
            'password' => $password,
            'role_id' => 'required|not_in:-- Role --',
            're_password' => $re_password,
            'pos' => 'max:10', 
        ];
    }
    private function getSegmentFromEnd($position_from_end = 1) {
        $segments = $this->segments();
    return $segments[sizeof($segments) - $position_from_end];
    }
}
