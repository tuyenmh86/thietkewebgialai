<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'pos' => 'max:10',
        ];
    }
    private function getSegmentFromEnd($position_from_end = 1) {
        $segments = $this->segments();
      return $segments[sizeof($segments) - $position_from_end];
    }
}
