<?php

namespace App\Http\Controllers\Admincp\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Setting\EloquentSettingRepository;
use Illuminate\Support\Facades\Redirect;
use App\Traits\Translator;
use View;

class SettingController extends Controller
{
    use Translator;

    protected $settingRepository;

    public function __construct(EloquentSettingRepository $settingRepository){
        $this->settingRepository = $settingRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = $this->settingRepository->findWithLanguage(1);
        return view('admincp.setting.index',compact('setting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if($this->settingRepository->updateWithLanguage($id,$data)){
            return Redirect::back()->with('alert-success',trans('back-app.update_success'));
        }
        else {
            return Redirect::back()->with('alert-danger',trans('back-app.update_fail'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
