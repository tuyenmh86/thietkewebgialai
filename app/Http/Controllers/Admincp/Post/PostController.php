<?php

namespace App\Http\Controllers\Admincp\Post;

use App\Http\Requests\Post\PostRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Post\EloquentCategoryRepository;
use App\Repositories\Post\EloquentPostRepository;
use App\Repositories\Post\EloquentTagRepository;
use Illuminate\Support\Facades\Redirect;
use Sunra\PhpSimple\HtmlDomParser;
use View,Auth;
use Carbon\Carbon;

class PostController extends Controller
{
    protected $categoryRepository;
    protected $postRepository;
    protected $tagRepository;

    public function __construct(
        EloquentCategoryRepository $categoryRepository ,
        EloquentPostRepository $postRepository,
        EloquentTagRepository $tagRepository
        ){
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
        $this->tagRepository = $tagRepository;

        // -======= BEGIN : PERMISSONS ==================
        $this->middleware('permission:posts,index', ['only' => ['index']]);
        $this->middleware('permission:posts,create',   ['only' => ['create','store']]);
        $this->middleware('permission:posts,update',   ['only' => ['edit', 'update']]);
        $this->middleware('permission:posts,destroy',   ['only' => ['destroy']]);
        // -======= END : PERMISSONS ====================
    }

    public function index(){
        $posts = $this->postRepository->getLists();
        return view('admincp.posts.posts.index',compact('posts'));
    }

    public function create(){
        $categorys = $this->categoryRepository->getLists(true);
        $tags = $this->tagRepository->getLists(true);
        return view('admincp.posts.posts.create',compact('categorys','tags'));
    }

    public function store(PostRequest $request){
        if(Auth::guard('admin')->check()){
            $data   = $request->all();
            // dd($data);
            $data['admin_id'] = Auth::guard('admin')->user()->id;
            if($post = $this->postRepository->createWithLanguage($data)){
                !isset($data['tags']) ? : $post->tags()->sync($data['tags']);
                !isset($data['categories']) ? : $post->categories()->sync($data['categories']);
                return Redirect::back()->with('alert-success',trans('back-app.add_success'));
            } 
        }
    }

    public function show($id)
    {
    //
    }

    public function edit($id)
    {
        if($post = $this->postRepository->findWithLanguage($id)){
            $categorys = $this->categoryRepository->getLists(true);
            $tags = $this->tagRepository->getLists(true);
            return view('admincp.posts.posts.edit',compact('post','categorys','tags'));
        }
        else {
            return Redirect::back()->with('alert-danger',trans('back-app.date_not_found'));
        }
    }

    public function update(postRequest $request, $id)
    {
        $data = $request->all();
        $post = $this->postRepository->find($id);
        $data['featured'] = isset($data['featured']) ? $data['featured'] : 0;
        if(is_null($data['featured_image'])){
            $data['featured_image'] = $post->featured_image;
        }
        if($this->postRepository->updateWithLanguage($id, $data)){
            !isset($data['tags']) ? : $post->tags()->sync($data['tags']);
                !isset($data['categories']) ? : $post->categories()->sync($data['categories']);
            return Redirect::back()->with('alert-success',trans('back-app.update_success'));
        }
        else {
            return Redirect::back()->with('alert-danger',trans('back-app.update_fail'));
        } 
    }

    public function destroy($id)
    {
        if($this->postRepository->deleteWithLanguage($id)){
            return Redirect::back()->with('alert-success',trans('back-app.delete_success'));
        }else {
            return Redirect::back()->with('alert-success',trans('back-app.delete_success'));
        }
    }

    public function postDisable(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->postRepository->update($id_arr,['published' => 0])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        } 
    }

    public function postPublic(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->postRepository->update($id_arr,['published' => 1])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        } 
    }

    public function postDestroys(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->postRepository->deleteWithLanguage($id_arr)){
            session()->flash('alert-success', trans('back-app.delete_success'));
            return response()->json('success');
        }
    }

    public function postUpdatePos(Request $request){
        $pos = $request->input('pos');
        $id = $request->input('id');
        if($this->postRepository->update($id,['pos' => $pos])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }

    public function importRss(){
        return view('admincp.posts.posts.import-rss');
    }

    private function getDom($link)
    {
        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Khi thực thi lệnh sẽ k view ra trình duyệt mà lưu lại vào 1 biến kiểu string
        $content = curl_exec($ch);
        curl_close($ch);
        $dom = HtmlDomParser::str_get_html($content);

        return $dom;
    }


    public function postImportRss (Request $request) {
        $link = $request->url;
        $dom = $this->getDom($link);

        foreach ($dom->find('.post .entry-title a.entry-title') as $link) {
            $dom = $this->getDom($link->href);
            $data = array();
            $data['admin_id'] = auth('admin')->user()->id;
            
            
            if(!empty($dom->find('.entry-content .baidv h2 span'))){
                foreach ($dom->find('.entry-content .baidv h2 span') as $key => $content){
                   $data['title'] = strip_tags($content->innertext);
                   $data['alias'] = to_slug_url($data['title']).'-'.Carbon::now()->format('ds');
                }

                foreach ($dom->find('.entry-content .baidv p') as $key => $content) {
                    if($key == 0){
                        $data['description'] = strip_tags($content->innertext);
                    }
                }

                foreach ($dom->find('#content .content #main') as $key => $content) {
                    $data['content'] = $content->innertext;
                }

                foreach ($dom->find('.entry-content .baidv img.size-full') as $key => $content) {
                    if($key == 0){
                        $data['featured_image'] = $content->src;
                    }
                }
                $data['pos'] = $this->postRepository->max('pos')+1;

                $this->postRepository->create($data);
            }
        }
        return redirect()->back()->with('success','Import successfullu.');
    }

    
}
