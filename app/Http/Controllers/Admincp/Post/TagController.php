<?php

namespace App\Http\Controllers\Admincp\Post;

use App\Http\Requests\Post\TagRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Post\EloquentTagRepository;
use Redirect;
use View;

class TagController extends Controller
{
	protected $tagRepository;

    public function __construct(EloquentTagRepository $tagRepository){
    	$this->tagRepository = $tagRepository;

        // -======= BEGIN : PERMISSONS ==================
        $this->middleware('permission:post.tags,index', ['only' => ['index']]);
        $this->middleware('permission:post.tags,create',   ['only' => ['create','store']]);
        $this->middleware('permission:post.tags,update',   ['only' => ['edit', 'update']]);
        $this->middleware('permission:post.tags,destroy',   ['only' => ['destroy']]);
        // -======= END : PERMISSONS ====================
    }

    public function index()
    {
        $tags = $this->tagRepository->getLists();
        return view('admincp.posts.tags.index',compact('tags'));
    }

    public function create()
    {
        return view('admincp.posts.tags.create');
    }

    public function store(TagRequest $request)
    {
        $data  = $request->all();
        if($this->tagRepository->createWithLanguage($data)){
            return Redirect::back()->with('alert-success',trans('back-app.add_success'));
        }
        else {
            return Redirect::back()->with('alert-danger',trans('back-app.add_fail'));
        }
    }

    public function show($id)
    {
    //
    }

    public function edit($id)
    {
        if($tag = $this->tagRepository->findWithLanguage($id)){
        	return view('admincp.posts.tags.edit',compact('tag'));
        }else {
        	return Redirect::route('404');
        }
    }

    public function update(TagRequest $request, $id)
    {
        $data = $request->all();
        $data['featured'] = isset($data['featured']) ? $data['featured'] : 0;
        if($this->tagRepository->updateWithLanguage($id,$data)){
        	return Redirect::back()->with('alert-success',trans('back-app.update_success'));
        }else{
        	return Redirect::back()->with('alert-danger',trans('back-app.update_fail'));
        }
    }

    public function destroy($id)
    {
        if($this->tagRepository->deleteWithLanguage($id)){
            return Redirect::back()->with('alert-success',trans('back-app.delete_success'));
        }
    }

    public function postDisable(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->tagRepository->update($id_arr,['published' => false])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }

    public function postPublic(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->tagRepository->update($id_arr,['published' => true])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }

    public function postDestroys(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->tagRepository->deleteWithLanguage($id_arr)){
            session()->flash('alert-success',trans('back-app.delete_success'));
            return response()->json('success');
        }
    }

    public function postUpdatePos(Request $request){
        $pos = $request->input('pos');
        $id = $request->input('id');
        if($this->tagRepository->update($id,['pos' => $pos])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }
}
