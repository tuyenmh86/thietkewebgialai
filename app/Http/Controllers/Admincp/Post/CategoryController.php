<?php

namespace App\Http\Controllers\Admincp\Post;

use App\Http\Requests\Post\CategoryRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Post\EloquentCategoryRepository;
use Illuminate\Support\Facades\Redirect;
use View;

class CategoryController extends Controller
{
	protected $categoryRepository;

    public function __construct(EloquentCategoryRepository $categoryRepository){
    	$this->categoryRepository = $categoryRepository;

        // -======= BEGIN : PERMISSONS ==================
        $this->middleware('permission:post.categorys,index', ['only' => ['index']]);
        $this->middleware('permission:post.categorys,create', ['only' => ['create','store']]);
        $this->middleware('permission:post.categorys,update', ['only' => ['edit', 'update']]);
        $this->middleware('permission:post.categorys,destroy', ['only' => ['destroy']]);
        // -======= END : PERMISSONS ====================
    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $categorys = $this->categoryRepository->getLists();
        return view('admincp.posts.categorys.index',compact('categorys'));
    }
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $categorys = $this->categoryRepository->getLists();
        return view('admincp.posts.categorys.create',compact('categorys'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(CategoryRequest $request)
    {
        $data   = $request->all();
        if($this->categoryRepository->createWithLanguage($data)){
            return redirect()->back()->with('alert-success',trans('back-app.add_success'));
        }
        else {
            return redirect()->back()->with('alert-danger',trans('back-app.add_fail'));
        }
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
    //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id){
        if($category = $this->categoryRepository->findWithLanguage($id)){
            $categorys = $this->categoryRepository->getLists(null,null,array($category->parent_id,$category->id));
            return view('admincp.posts.categorys.edit',compact('category','categorys','cate_parent'));
        }
        else {
            return redirect()->back()->with('alert-danger',trans('back-app.data_not_found'));
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(categoryRequest $request, $id){
        // Supper Admin Default Do not Update
        if($this->categoryRepository->find($id)){
            $data = $request->all();
            $data['featured'] = isset($data['featured']) ? $data['featured'] : 0;
            if($this->categoryRepository->updateWithLanguage($id,$data)){
                return redirect()->back()->with('alert-success',trans('back-app.update_success'));
            }
            else {
                return redirect()->back()->with('alert-danger',trans('back-app.update_fail'));
            }
        }
        else {
            return redirect()->back()->with('alert-danger',trans('back-app.data_not_found'));
        } 
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id){
        if($this->categoryRepository->delete($id)){
            return redirect()->back()->with('alert-success',trans('back-app.delete_success'));
        }
    }

    /**
     * Post Disable
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postDisable(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->categoryRepository->update($id_arr,['published' => false])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }

    /**
     * Post Public
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postPublic(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->categoryRepository->update($id_arr,['published' => true])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }

    /**
     * Post Destroy
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postDestroys(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->categoryRepository->delete($id_arr)){
            session()->flash('alert-success', trans('back-app.delete_success'));
            return response()->json('success');
        }
    }

    /**
     * Update Position
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postUpdatePos(Request $request){
        $pos = $request->input('pos');
        $id = $request->input('id');
        if($this->categoryRepository->update($id,['pos' => $pos])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }
}
