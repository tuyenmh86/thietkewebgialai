<?php

namespace App\Http\Controllers\Admincp\RolePermission;

use App\Http\Requests\Permission\PermissionRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Permission\EloquentPermissionRepository;
use Illuminate\Support\Facades\Redirect;
use Auth;
use View;

class PermissionController extends Controller
{
    protected $permissionRepository;
    public function __construct(EloquentPermissionRepository $permissionRepository){
        $this->permissionRepository = $permissionRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = $this->permissionRepository->getLists();
        return view('admincp.role-permission.permission.index',compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admincp.role-permission.permission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {
        $data   = $request->all();
        if($this->permissionRepository->create($data)){
            return Redirect::back()->with('alert-success',trans('back-app.add_success'));
        }
        else {
            return Redirect::back()->with('alert-danger',trans('back-app.add_fail'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = $this->permissionRepository->find($id);
        if($permission){
            return view('admincp.role-permission.permission.edit',compact('permission'));
        }
        else {
            return Redirect::back()->with('alert-danger',trans('back-app.data_not_found'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionRequest $request, $id)
    {
        $data = $request->all();
        if($this->permissionRepository->update($id,$data)){
            return Redirect::back()->with('alert-success',trans('back-app.update_success'));
        }
        else {
            return Redirect::back()->with('alert-danger',trans('back-app.update_fail'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    //
        if($this->permissionRepository->delete($id)){
            return Redirect::back()->with('alert-success', trans('back-app.delete_success'));
        }
        else {
            return Redirect::back()->with('alert-danger',trans('back-app.delete_fail'));
        }
    }

    /**
     * Post Public Data
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postDisable(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->permissionRepository->update($id_arr,['published' => false])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }
    /**
     * Post Disable Data
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postPublic(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->permissionRepository->update($id_arr,['published' => true])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }

    /**
     * Post Destroy Data
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postDestroy(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->permissionRepository->delete($id_arr)){
            session()->flash('alert-success', trans('back-app.delete_success'));
            return response()->json('success');
        }
    }

    /**
     * Update Position
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postUpdatePos(Request $request){
        $pos = $request->input('pos');
        $id = $request->input('id');
        if($this->permissionRepository->update($id,['pos' => $pos])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }

    /**
     * Update DisplayName
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postUpdateDisplayName(Request $request){
        $displayName = $request->input('displayName');
        $id = $request->input('id');
        if($this->permissionRepository->update($id,['display_name' => $displayName])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }
}
