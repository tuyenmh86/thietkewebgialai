<?php

namespace App\Http\Controllers\Admincp\RolePermission;

use App\Http\Requests\Role\RoleRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Role\EloquentRoleRepository;
use App\Repositories\Permission\EloquentPermissionRepository;
use App\Repositories\Admin\EloquentAdminRepository;
use Illuminate\Support\Facades\Redirect;
use Auth;
use DB;
use View;

class RoleController extends Controller
{
    protected $roleRepository;
    protected $permissionRepository;
    protected $adminRepository;
    public function __construct(EloquentRoleRepository $roleRepository,EloquentPermissionRepository $permissionRepository,EloquentAdminRepository $adminRepository){
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
        $this->adminRepository = $adminRepository;

    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $roles = $this->roleRepository->getLists();
        return view('admincp.role-permission.role.index',compact('roles'));
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('admincp.role-permission.role.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(RoleRequest $request)
    {
        $data   = $request->all();
        if($this->roleRepository->create($data)){
            return Redirect::route('admincp.role.index')->with('alert-success',trans('back-app.add_success'));
        }
        else {
            return Redirect::route('admincp.role.index')->with('alert-danger',trans('back-app.add_fail'));
        }
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
    //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        // Super Admin Default Don't Edit
        if(preventUpdateSPAdmin($id)) {
            $role = $this->roleRepository->find($id);
            if($role){
                return view('admincp.role-permission.role.edit',compact('role'));
            }
        }else {
            return Redirect::route('404');
        }
        
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(RoleRequest $request, $id)
    {
        $data = $request->all();
        $data['advanced'] = isset($data['advanced']) ? $data['advanced'] : 0;
        if($this->roleRepository->update($id,$data)){
            return Redirect::back()->with('alert-success',trans('back-app.update_success'));
        }
        else {
            return Redirect::back()->with('alert-danger',trans('back-app.update_fail'));
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        if($this->roleRepository->delete($id)){
            return Redirect::back()->with('alert-success',trans('back-app.delete_success'));
        }
    }

    /**
     * Post Disable
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postDisable(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->roleRepository->update($id_arr,['published' => false])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }

    /**
     * Post Public
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postPublic(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->roleRepository->update($id_arr,['published' => true])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }

    /**
     * Post Destroy
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postDestroy(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->roleRepository->delete($id_arr)){
            session()->flash('alert-success', trans('back-app.delete_success'));
            return response()->json('success');
        }
    }

    /**
     * Update Position
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postUpdatePos(Request $request){
        $pos = $request->input('pos');
        $id = $request->input('id');
        if($this->roleRepository->update($id,['pos' => $pos])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }
    /**
     * ***************************** Get Permission Role Page *********************
     */
    /**
     * Get Permission Role
     * @return [type] [description]
     */
    public function getPermissionRole(){
        $roles = $this->roleRepository->getLists(null,null,null,$orderby = 'oldest',true);
        if($roles){
            if($modules = $this->permissionRepository->groupPosision()){
                return view('admincp.role-permission.role.permission',compact('roles','modules'));
            }
        }
    }
    /**
     * Post Permission Role
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postPermissionRole(Request $request){
        // ** DELETE MEDIATE TABLE 
        DB::table('permission_role')->truncate();
        // ** GET VALUE PERMISSIONS SELECTED 
        $role_per_arr = explode (',', $request->role_permission);
        // ** EACH GET ROLE_ID && PERMISSION_ID
        foreach($role_per_arr as $value){
            $role_per = explode ('_', $value);
            $this->roleRepository->find($role_per[1])->permissions()->attach($role_per[0]); 
        }
        return Redirect::back()->with('alert-success',trans('back-app.update_success'));
    }
}