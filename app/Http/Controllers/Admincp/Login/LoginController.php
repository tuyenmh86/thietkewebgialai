<?php

namespace App\Http\Controllers\Admincp\Login;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Session;

class LoginController extends Controller
{
    // Get Login
    public function getLogin ()
    {
        if (Auth::guard('admin')->check()) {
            return Redirect::to('admincp');
        } else {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $now=date_create();
            $timeCount = date_format($now,"Y-m-d H:i:s");
            if(Session::has('timeCount')){
                if($timeCount > Session::get('timeCount')){
                    Session::forget('timeCount');
                    Session::forget('timesLog');
                }
            }     
            return view('admincp.login.login');
        } 
    }
    //Post Login
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|alpha_spaces',
            'password' => 'required',
        ]);
        $username = trim($request->username);
        $password = trim($request->password);
        $remember = ($request->remember) ? true : false;
        if (Auth::guard('admin')->attempt(['username'=>$username,'password'=>$password,'published' => 1],$remember)){
            Auth::guard('admin')->user()->save();
            if(Session::has('timesLog')){
                Session::forget('timesLog');
            }
            return Redirect::to('admincp');
        }
        else {
            if (!Session::has('timesLog')){
                Session::put('timesLog', 1);
            }else {
                if(Session::get('timesLog') == 1){
                    Session::put('timesLog', 2);
                }else{
                    Session::put('timesLog', 3);
                    date_default_timezone_set('Asia/Ho_Chi_Minh');
                    $now=date_create();
                    date_modify($now,"+180 seconds");
                    $timeCount = date_format($now,"Y-m-d H:i:s");
                    if (!Session::has('timeCount')){
                        Session::put('timeCount', $timeCount);
                    }       
                    return redirect()->back()->with('error','You have entered the wrong password more than 3 times, please try again in 3 minutes.');             
                }
            }
            return redirect()->back()->with('error','Username or a password incorrect or was disabled.');
        }       
    }
    public function getLogout()
    {
        if(Auth::guard('admin')->check())
        {
             Auth::guard('admin')->logout();
             return Redirect::to('admincp/login');
        }
    }
}

