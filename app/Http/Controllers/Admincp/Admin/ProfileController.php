<?php

namespace App\Http\Controllers\Admincp\Admin;

use App\Http\Requests\Admin\AdminRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Admin\EloquentAdminRepository;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Hash;
use View;


class ProfileController extends Controller
{
    protected $adminRepository;

    public function __construct(EloquentAdminRepository $adminRepository){
        $this->adminRepository = $adminRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::guard('admin')->check()){
            $profile = Auth::guard('admin')->user();
            return view('admincp.admins.profile',compact('profile'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::guard('admin')->check()){
            if(Auth::guard('admin')->user()->id == $id){
                $profile = $this->adminRepository->findWithLanguage(Auth::guard('admin')->user()->id);
                return view('admincp.admins.profile-edit',compact('profile'));
            }   
            else {
                return Redirect::route('404');
            }  
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $profile = $this->adminRepository->find($id);
        if(!is_null($data['old_password']) || !is_null($data['password']) || !is_null($data['re_password'])){
            if(! Hash::check( $data['old_password'] ,$profile->password )){
                return redirect()->back()->with('alert-danger','Mật khẩu cũ không đúng');
            }
            else {
                $this->validate($request, [
                    'password' => 'required|min:5',
                    're_password' => 'required|same:password'
                ]);
                $data['password'] = Hash::make($data['password']);
                if($this->adminRepository->updateWithLanguage($id,$data)){
                    return redirect()->back()->with('alert-success',trans('back-app.update_success'));
                }
                return redirect()->back()->with('alert-danger',trans('back-app.update_fail')); 
            }
        }
        else {
            $data['password'] = $profile->password;
            if($this->adminRepository->updateWithLanguage($id,$data)){
                return redirect()->back()->with('alert-success',trans('back-app.update_success'));
            }
            return redirect()->back()->with('alert-danger',trans('back-app.update_fail'));  
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
    
}
