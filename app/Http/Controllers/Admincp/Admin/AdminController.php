<?php

namespace App\Http\Controllers\Admincp\Admin;

use App\Http\Requests\Admin\AdminRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Admin\EloquentAdminRepository;
use App\Repositories\Role\EloquentRoleRepository;
use Illuminate\Support\Facades\Redirect;
use Hash;
use Auth;
use View;


class AdminController extends Controller
{
    protected $adminRepository;
    protected $roleRepository;

    public function __construct(
        EloquentAdminRepository $adminRepository,
        EloquentRoleRepository $roleRepository){
        $this->adminRepository = $adminRepository;
        $this->roleRepository = $roleRepository;
        
        // -======= BEGIN : PERMISSONS ==================
        $this->middleware('permission:admins,index', ['only' => ['index']]);
        $this->middleware('permission:admins,create',   ['only' => ['create','store']]);
        $this->middleware('permission:admins,update',   ['only' => ['edit', 'update']]);
        $this->middleware('permission:admins,destroy',   ['only' => ['destroy']]);
        // -======= END : PERMISSONS ====================
        
        
    }

    public function index(Request $request)
    {
        if(Auth::guard('admin')->check()){
            // admins All List
            $admins = $this->adminRepository->getLists(null,null,Auth::guard('admin')->user()->id);
            return view('admincp.admins.index',compact('admins'));
        }
    }

    public function create()
    {
        $roles = $this->roleRepository->getLists();
        return view('admincp.admins.create',compact('roles'));
    }

    public function store(AdminRequest $request)
    {
        //
        $data = $request->alL();
        $data['username'] = trim($data['username']);
        $data['password'] = Hash::make($request->password);
        if($data['profile_image'] == ''){
            $data['profile_image'] = '/user-default.png';
        }
        if($this->adminRepository->createWithLanguage($data)){
            return Redirect::route('admincp.admins.index')->with('alert-success', trans('back-app.add_success'));
        }
        else {
            return Redirect::back()->with('alert-danger', trans('back-app.add_fail'));
        }    
    }

    public function edit($id)
    {
        if(preventUpdateSPAdmin($id)){
            // Check Have Row admin Find By Id
            if($admin = $this->adminRepository->findWithLanguage($id)){
                $roles = isset($admin->role) ? $this->roleRepository->getLists(null,$admin->role->id) : $this->roleRepository->getLists();
                return view('admincp.admins.edit',compact('admin','roles'));        
            }
            else {
                return redirect()->back()->with('alert-danger',trans('back-app.data_not_found'));
            }
        }
        else {
            return redirect()->route('errors.404');
        }
    }

    public function update(AdminRequest $request, $id)
    {
        $data = $request->all();
        $admin = $this->adminRepository->find($id);
        $data['profile_image'] =  $data['profile_image'] ? $data['profile_image'] : $admin->profile_image;
        if(!is_null($data['old_password']) || !is_null($data['password']) || !is_null($data['re_password'])){
            if(! Hash::check( $data['old_password'] ,$admin->password )){
                return redirect()->back()->with('alert-danger','Mật khẩu cũ không đúng');
            }
            else {
                $this->validate($request, [
                    'password' => 'required|min:5',
                    're_password' => 'required|same:password'
                ]);
                $data['password'] = Hash::make($data['password']);
                if($this->adminRepository->updateWithLanguage($id,$data)){
                    return redirect()->back()->with('alert-success',trans('back-app.update_success'));
                }
                return redirect()->back()->with('alert-danger',trans('back-app.update_fail')); 
            }
        }
        else {
            $data['password'] = $admin->password;
            if($this->adminRepository->updateWithLanguage($id,$data)){
                return redirect()->back()->with('alert-success',trans('back-app.update_success'));
            }
            return redirect()->back()->with('alert-danger',trans('back-app.update_fail'));  
        }
    }

    public function destroy($id)
    {
        //
        if($this->adminRepository->deleteWithLanguage($id)){
            return Redirect::back()->with('alert-success', trans('back-app.delete_success'));
        }
        else {
            return Redirect::back()->with('alert-danger',trans('back-app.delete_fail'));
        }
    }

    public function postDisable(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->adminRepository->update($id_arr,['published' => false])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }   
        
    }

    public function postPublic(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->adminRepository->update($id_arr,['published' => true])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }     
    }

    public function postDestroys(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->adminRepository->deleteWithLanguage($id_arr)){
            session()->flash('alert-success', trans('back-app.delete_success'));
            return response()->json('success');
        }       
    }
}
