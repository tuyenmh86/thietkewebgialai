<?php

namespace App\Http\Controllers\Admincp\Widget;

use App\Http\Requests\Widget\WidgetRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Widget\EloquentWidgetRepository;
use Illuminate\Support\Facades\Redirect;
use View;
use Auth;

class WidgetController extends Controller
{
    protected $widgetRepository;

    public function __construct(EloquentWidgetRepository $widgetRepository){
        $this->widgetRepository = $widgetRepository;

        // -======= BEGIN : PERMISSONS ==================
        $this->middleware('permission:widgets,index', ['only' => ['index']]);
        $this->middleware('permission:widgets,create', ['only' => ['create','store']]);
        $this->middleware('permission:widgets,update', ['only' => ['edit', 'update']]);
        $this->middleware('permission:widgets,destroy', ['only' => ['destroy']]);
        // -======= END : PERMISSONS ====================
    }
    
    public function index(){
        $widgets = $this->widgetRepository->getLists();
        return view('admincp.widgets.index',compact('widgets'));
    }

    public function create(){
        return view('admincp.widgets.create');
    }

    public function store(WidgetRequest $request){
        $data   = $request->all();
        $data['alias'] = '['.$request->alias.']';
        if($this->widgetRepository->createWithLanguage($data)){
            return Redirect::back()->with('alert-success',trans('back-app.add_success'));
        }
    }

    public function edit($id){
        if($widget = $this->widgetRepository->findWithLanguage($id)){
            return view('admincp.widgets.edit',['widget' => $widget]);
        }else {
            return Redirect::back()->with('alert-danger',trans('back-app.data_not_found'));
        }
    }
    
    public function update(WidgetRequest $request, $id){
        $data = $request->all();
        if($this->widgetRepository->updateWithLanguage($id, $data)){
            return Redirect::back()->with('alert-success',trans('back-app.update_success'));
        }else {
            return Redirect::back()->with('alert-danger',trans('back-app.update_fail'));
        } 
    }

    public function destroy($id){
        if($this->widgetRepository->deleteWithLanguage($id)){
            return Redirect::back()->with('alert-success',trans('back-app.delete_success'));
        }else {
            return Redirect::back()->with('alert-success',trans('back-app.delete_fail'));
        }
    }

    public function postDisable(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->widgetRepository->update($id_arr,['published' => 0])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        } 
    }


    public function postPublic(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->widgetRepository->update($id_arr,['published' => 1])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        } 
    }

    public function postDestroys(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->widgetRepository->deleteWithLanguage($id_arr)){
            session()->flash('alert-success', trans('back-app.delete_success'));
            return response()->json('success');
        }
    }
}
