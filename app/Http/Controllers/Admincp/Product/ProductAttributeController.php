<?php

namespace App\Http\Controllers\Admincp\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product\ProductAttribute;
use App\Models\Product\Category;

class ProductAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data =  ProductAttribute::orderBy('id','desc')->get();
        return view('admincp.products.attribute.index',['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('parent_id',0)->get();
        return view('admincp.products.attribute.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        ProductAttribute::create($data);
        return redirect()->back()->with('alert-success',trans('back-app.add_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::where('parent_id',0)->get();
        $item = ProductAttribute::findOrFail($id);
        return view('admincp.products.attribute.edit',['item' => $item,'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $data = $request->all();
        $item = ProductAttribute::findOrFail($id);
        $data['is_unique'] = isset($data['is_unique']) ? $data['is_unique'] : 0;
        $item->update($data);
        return redirect()->back()->with('alert-success',trans('back-app.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = ProductAttribute::findOrFail($id);
        $item->delete();
        return redirect()->back()->with('alert-success',trans('back-app.delete_success'));
    }
}
