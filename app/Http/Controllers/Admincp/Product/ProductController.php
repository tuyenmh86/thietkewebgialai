<?php

namespace App\Http\Controllers\Admincp\Product;

use App\Http\Requests\Product\ProductRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Product\EloquentCategoryRepository;
use App\Repositories\Product\EloquentProductRepository;
use App\Repositories\Product\EloquentTagRepository;
use Illuminate\Support\Facades\Redirect;
use App\Models\Product\ProductAttribute;
use App\Models\Product\Product;
use App\Models\Product\ProductAttributeInteger;
use App\Models\Product\ProductAttributeText;
use App\Models\Product\ProductAttributeVarchar;
use View;
use Auth;
use Excel;
use Carbon\Carbon;

class ProductController extends Controller
{
    protected $categoryRepository;
    protected $productRepository;
    protected $tagRepository;

    public function __construct(
        EloquentCategoryRepository $categoryRepository ,
        EloquentProductRepository $productRepository,
        EloquentTagRepository $tagRepository){
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
        $this->tagRepository = $tagRepository;

        // -======= BEGIN : PERMISSONS ==================
        $this->middleware('permission:products,index', ['only' => ['index']]);
        $this->middleware('permission:products,create',   ['only' => ['create','store']]);
        $this->middleware('permission:products,update',   ['only' => ['edit', 'update']]);
        $this->middleware('permission:products,destroy',   ['only' => ['destroy']]);
        // -======= END : PERMISSONS ====================
    }

    public function index()
    {
    // public function getLists($published = null,$categoryId = null, $limit = null,$orderby = 'latest', $objectId = null , $byAccount = null ) {
        $products = $this->productRepository->getLists(null,null,10);
        $categorys = $this->categoryRepository->getLists(true);
        $tags = $this->tagRepository->getLists(true);
        return view('admincp.products.products.index',compact('products','categorys','tags'));
    }
    
    public function postCategory(Request $request)
    {
        $categoryId = $request->categories;
        $products = $this->productRepository->getLists(null,$categoryId,1000);
        $categorys = $this->categoryRepository->getLists(true);
        $tags = $this->tagRepository->getLists(true);
        return view('admincp.products.products.index',compact('products','categorys','tags'));
    }
    
    public function create()
    {
        $categorys = $this->categoryRepository->getLists(true);
        $product_type = $this->categoryRepository->getLists(true,0);
        $tags = $this->tagRepository->getLists(true);
        if(config('cms.productHasManyAttribute')){
            return view('admincp.products.products.create',compact('categorys','tags','product_type'));
        }else{
            $attributes = ProductAttribute::where('published',true)->get();
            return view('admincp.products.products.create',compact('categorys','tags','product_type','attributes'));
        }
    }

    public function store(ProductRequest $request)
    {
        if(Auth::guard('admin')->check()){
            $data = $request->all();
           
            $data['admin_id'] = Auth::guard('admin')->user()->id;
            if($product = $this->productRepository->createWithLanguage($data)){
                !isset($data['tags']) ? : $product->tags()->sync($data['tags']);
                !isset($data['categories']) ? : $product->categories()->sync($data['categories']);
              
                !isset($data['attributes']) ? : $this->productRepository->addAttribute($data['attributes'],$product->id);
                return Redirect::back()->with('alert-success',trans('back-app.add_success'));
            } 
        }
    }

    public function show($id)
    {
    //
    }

    public function edit($id)
    {
        if($product = $this->productRepository->findWithLanguage($id)){
            $categorys = $this->categoryRepository->getLists(true);
            $tags = $this->tagRepository->getLists(true);
            return view('admincp.products.products.edit',compact('product','categorys','tags'));
        }else {
            return Redirect::back()->with('alert-danger',trans('back-app.data_not_found'));
        }
    }

    public function update(ProductRequest $request, $id)
    {
        $data = $request->all();
        $product = $this->productRepository->find($id);
        $data['featured'] = isset($data['featured']) ? $data['featured'] : 0;
        if($this->productRepository->updateWithLanguage($id, $data)){
            
            !isset($data['tags']) ? : $product->tags()->sync($data['tags']);
            !isset($data['categories']) ? : $product->categories()->sync($data['categories']);
           
            !$product->attribute_value_integer ? : $product->attribute_value_integer()->delete();
            !$product->attribute_value_varchar ? : $product->attribute_value_varchar()->delete();
            !$product->attribute_value_text ? : $product->attribute_value_text()->delete();


            !isset($data['attributes']) ? : $this->productRepository->addAttribute($data['attributes'],$product->id);
            
            return Redirect::back()->with('alert-success',trans('back-app.update_success'));
        }else {
            return Redirect::back()->with('alert-danger',trans('back-app.update_fail'));
        } 
    }

    public function destroy($id){
        if($this->productRepository->deleteWithLanguage($id)){

            return Redirect::back()->with('alert-success',trans('back-app.delete_success'));

        }else {

            return Redirect::back()->with('alert-success',trans('back-app.delete_fail'));
        }
    }

    public function postDisable(Request $request){

        $id_arr = $request->input('id_arr');

        if($this->productRepository->update($id_arr,['published' => 0])){
            
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        } 
    }

    public function postPublic(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->productRepository->update($id_arr,['published' => 1])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        } 
    }

    public function postDestroys(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->productRepository->deleteWithLanguage($id_arr)){
            session()->flash('alert-success', trans('back-app.delete_success'));
            return response()->json('success');
        }
    }

    public function postUpdatePos(Request $request){
        $pos = $request->input('pos');
        $id = $request->input('id');
        if($this->productRepository->update($id,['pos' => $pos])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }

    public function postLoadAttribute(Request $request){
        
        //load thuộc tính của category này
        $attributes = ProductAttribute::where('published',true)->where('catalog',$request->categoryId)->get();

        if($attributes){

            $html = view('components.attributes')->with('attributes', $attributes)->render();
            return response()->json(['html' => $html],200);
        }
        
    }
    public function downloadFile()
    {   
        $myFile = admincp_url("product/downloadFile/dummy_pdf.pdf");
    	$headers = ['Content-Type: application/pdf'];
    	$newName = 'itsolutionstuff-pdf-file-'.time().'.pdf';
    	return response()->download($myFile, $newName, $headers);
    }

    public function importExport(){
        return view('admincp.products.products.importExport');
    }

    public function import(Request $request){
        
       
       

        if($request->hasFile('import_file')){

            Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                
               
                $count_row=0;
                foreach ($reader->toArray() as $key => $row) {
                    $data['language'] = array(
                        "vi" =>([
                            "title" => $row['title'],
                            "alias" => to_slug_url($row['title']).'-'.Carbon::now()->format('ds'),
                            "description" => $row['description'],
                            "content" => $row['content'],
                            "seo_title" => $row['seo_title'],
                            "seo_description" => $row['seo_description'],
                            "seo_keywords" => $row['seo_keywords']
                        ])
                    );
                    $data['hang_san_xuat'] = $row['hang_san_xuat'];
                    $data['pos'] = $row['pos'];
                    $data['published'] = $row['published'];
                    $data['featured'] = $row['featured'];
                    $data['categories'] = explode(',',$row['categories']);
                    $data['tags'] =  explode(',',$row['tags']);
                    $data['featured_image'] = $row['featured_image'];
                    $data['image_list'] = explode(',', $row['image_list']);
                    $data['sku'] = $row['sku'];
                    $data['price'] = $row['price'];
                    $data['promotion_price'] = $row['promotion_price'];
                    if(Auth::guard('admin')->check()){

                        $data['admin_id'] = Auth::guard('admin')->user()->id;

                       if($row['title'] !=''){
                            if($product = $this->productRepository->createWithLanguage($data)){

                                $count_row++;
                                !isset($data['tags']) ? : $product->tags()->sync($data['tags']);
                                !isset($data['categories']) ? : $product->categories()->sync($data['categories']);
                                !isset($data['attributes']) ? : $this->productRepository->addAttribute($data['attributes'],$product->id);

                            }
                            else{
                                return Redirect::back()->with('alert-warning','Có thể cấu trúc file không đúng hoặc bị thiếu!!! Kiểm tra lại từ dòng số: '.$count_row+1);
                            }
                        }
                    }
                }
            },'UTF-8');
           
        }
       
        // Session::put('success', 'Youe file successfully import in database!!!');

        return Redirect::back()->with('alert-success','Your file successfully import to database!!! ');
    }
}
