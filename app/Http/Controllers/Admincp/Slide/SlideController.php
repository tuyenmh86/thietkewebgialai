<?php

namespace App\Http\Controllers\Admincp\Slide;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Slide\EloquentSlideRepository;
use Illuminate\Support\Facades\Redirect;
use App\Models\Slides\Slide_item;
use View;

class SlideController extends Controller
{
    protected $slideRepository;
    public function __construct(EloquentSlideRepository $slideRepository){
        $this->slideRepository = $slideRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = $this->slideRepository->getLists();
        return view('admincp.slides.index',compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admincp.slides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required'
        ]);

        $data = $request->all();


        if($slide = $this->slideRepository->create($data))
        {
            foreach ($data['image'] as $key => $value) {
                
                // Slide_item::create(['slide_id' => $slide->id, 'image' => $value]);

                Slide_item::create([
                    'slide_id' => $slide->id, 
                    'image' => $value,
                    'caption'=>$data['caption'][$key],
                    'content'=>$data['content'][$key],
                    'link'=>$data['link'][$key],
                ]);

            }
            return redirect()->back()->with('alert-success',trans('back-app.add_success'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($slide = $this->slideRepository->find($id))
            
            return view('admincp.slides.edit',['slide' => $slide]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required'
        ]);
        $data = $request->all();
        if($slide = $this->slideRepository->find($id)){
            if($this->slideRepository->update($id,$data)){
                $slide->items()->delete();
                foreach ($data['image'] as $key => $value) {
                    Slide_item::create([
                        'slide_id' => $slide->id, 
                        'image' => $value,
                        'caption'=>$data['caption'][$key],
                        'content'=>$data['content'][$key],
                        'link'=>$data['link'][$key],
                    ]);
                }
                return redirect()->back()->with('alert-success',trans('back-app.update_success'));
            }
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->slideRepository->delete($id)){
            return redirect()->back()->with('alert-success',trans('back-app.delete_success'));
        }
    }

    /**
     * Post Disable
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postDisable(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->slideRepository->update($id_arr,['published' => false])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }

    /**
     * Post Public
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postPublic(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->slideRepository->update($id_arr,['published' => true])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }

    /**
     * Post Destroy
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function postDestroys(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->slideRepository->delete($id_arr)){
            session()->flash('alert-success', trans('back-app.delete_success'));
            return response()->json('success');
        }
    }
}
