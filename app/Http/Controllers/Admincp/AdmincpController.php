<?php

namespace App\Http\Controllers\Admincp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Post\EloquentPostRepository;
use App\Repositories\Product\EloquentProductRepository;
use App\Repositories\Product\EloquentCategoryRepository;
use App\Repositories\User\EloquentUserRepository;
use Auth;

class AdmincpController extends Controller
{
    protected $postRepository;
    protected $productRepository;
    protected $productCateRepository;
    protected $userRepository;

    public function __construct(
        EloquentPostRepository $postRepository,
        EloquentProductRepository $productRepository,
        EloquentCategoryRepository $productCateRepository,
        EloquentUserRepository $userRepository
        ){
        $this->productCateRepository = $productCateRepository;
        $this->productRepository = $productRepository;
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
    }
    public function getDashboard(){
        $product_cate_c = $this->productCateRepository->countDataAddInWeekAgo();
        $product_c = $this->productRepository->countDataAddInWeekAgo();
        $produc_top_v = $this->productRepository->getLists(true,null,10,'view');
        $post_c = $this->postRepository->countDataAddInWeekAgo();
        $post_top_v = $this->postRepository->getLists(true,null,10,'view');
        $user_c = $this->userRepository->countDataAddInWeekAgo();
        return view('admincp.dashboard',[
            'product_cate_c' => $product_cate_c,
            'product_c' => $product_c,
            'produc_top_v' => $produc_top_v,
            'post_c' => $post_c,
            'post_top_v' => $post_top_v,
            'user_c' => $user_c
            ]);
    }

    public function get404page(Request $request)
    {
        return view('admincp.404');
    }

    public function getFilemanager () 
    {
        return view('admincp.filemanager/filemanager');
    }
}
