<?php

namespace App\Http\Controllers\Admincp\Page;

use App\Http\Requests\Page\PageRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Page\EloquentPageRepository;
use Illuminate\Support\Facades\Redirect;
use View;
use Auth;

class PageController extends Controller
{
    protected $pageRepository;

    public function __construct(EloquentPageRepository $pageRepository){
        $this->pageRepository = $pageRepository;

        // -======= BEGIN : PERMISSONS ==================
        $this->middleware('permission:pages,index', ['only' => ['index']]);
        $this->middleware('permission:pages,create',   ['only' => ['create','store']]);
        $this->middleware('permission:pages,update',   ['only' => ['edit', 'update']]);
        $this->middleware('permission:pages,destroy',   ['only' => ['destroy']]);
        // -======= END : PERMISSONS ====================
    }

    public function index()
    {
        $pages = $this->pageRepository->getLists();
        return view('admincp.pages.index',compact('pages'));
    }

    public function create()
    {
        return view('admincp.pages.create');
    }

    public function store(PageRequest $request)
    {
        if(Auth::guard('admin')->check()){
            $data   = $request->all();
            $data['admin_id'] = Auth::guard('admin')->user()->id;
            if($this->pageRepository->createWithLanguage($data)){
                return Redirect::back()->with('alert-success',trans('back-app.add_success'));
            } 
        }
    }

    public function edit($id)
    {
        if($page = $this->pageRepository->findWithLanguage($id)){
            return view('admincp.pages.edit',['page' => $page]);
        }
        else {
            return Redirect::back()->with('alert-danger',trans('back-app.data_not_found'));
        }
    }

    public function update(PageRequest $request, $id)
    {
        $data = $request->all();
        $data['featured'] = isset($data['featured']) ? $data['featured'] : 0;
        if($this->pageRepository->updateWithLanguage($id, $data)){
            return Redirect::back()->with('alert-success',trans('back-app.update_success'));
        }
        else {
            return Redirect::back()->with('alert-danger',trans('back-app.update_fail'));
        } 
    }

    public function destroy($id)
    {
        if($this->pageRepository->deleteWithLanguage($id)){
            return Redirect::back()->with('alert-success',trans('back-app.delete_success'));
        }else {
            return Redirect::back()->with('alert-success',trans('back-app.delete_fail'));
        }
    }

    public function show ($id){
        
    }

    public function postDisable(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->pageRepository->update($id_arr,['published' => 0])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        } 
    }

    public function postPublic(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->pageRepository->update($id_arr,['published' => 1])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        } 
    }

    public function postDestroys(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->pageRepository->deleteWithLanguage($id_arr)){
            session()->flash('alert-success', trans('back-app.delete_success'));
            return response()->json('success');
        }
    }

    public function postUpdatePos(Request $request){
        $data = $request->all();
        if($this->pageRepository->update($data['id'],['pos' => $data['pos']])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }
}
