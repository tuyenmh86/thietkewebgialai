<?php

namespace App\Http\Controllers\Admincp\CustomField;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customfield\Customfield;
use App\Models\Translation\Translation;
use Artisan;
use Schema;

class CustomFieldController extends Controller
{
    public function index (){
    	$data = Customfield::all();
    	return view('admincp.customfield.index',compact('data'));
    }

    public function create(){
    	return view('admincp.customfield.create');
    }

    public function store (Request $request){
    	$data = $request->all();
    	if(!Customfield::where('table_name',$request->table_name)->where('field',$request->field)->first()){

    		if(Customfield::Create($data)){
                switch ($request->type) {
                    case 'number':
                        Schema::table($request->table_name, function($table) use ($request) {
                            $table->integer($request->field)->nullable();
                        });
                        break;
                    
                    default:
                        Schema::table($request->table_name, function($table) use ($request) {
                            $table->text($request->field)->nullable();
                        });
                        break;
                }
	    		
	    		return redirect()->back()->with('alert-success',trans('back-app.add_success'));
	    	}
    	}else {
    		return redirect()->back()->with('alert-danger',trans('back-app.add_fail'));
    	}
    	
    }

    public function destroy($id){
        if($customfield = Customfield::find($id)){
        	Translation::where('column_name',$customfield->field)->where('table_name',$customfield->table_name)->delete();

        	Schema::table($customfield->table_name, function($table) use ($customfield) {
	           $table->dropColumn($customfield->field);
	        });

        	$customfield->delete();
        	return redirect()->back()->with('alert-success',trans('back-app.delete_success'));
        }
    }
}
