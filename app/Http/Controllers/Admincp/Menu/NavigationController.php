<?php

namespace App\Http\Controllers\Admincp\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Page\EloquentPageRepository;
use Harimayco\Menu\Controllers\MenuController;
use App\Models\Menu\MenuItem;
use App\Traits\Translator;

class NavigationController extends MenuController
{
    use Translator;

    protected $pageRepository;
    protected $model;

    public function __construct(
        EloquentPageRepository $pageRepository,
        MenuItem $model){
        $this->pageRepository = $pageRepository;
        $this->model = $model;
        
        // -======= BEGIN : PERMISSONS ==================
        // $this->middleware('permission:admins,index', ['only' => ['index']]);
        // $this->middleware('permission:admins,create',   ['only' => ['create','store']]);
        // $this->middleware('permission:admins,update',   ['only' => ['edit', 'update']]);
        // $this->middleware('permission:admins,destroy',   ['only' => ['destroy']]);
        // -======= END : PERMISSONS ====================
        
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index (Request $request)
    {
        return view('admincp.menus.index');
    }

    public function getModule (Request $request) {
    	$module = $request->input('module');
    	switch ($module) {
    		case 'page':
    			$data = $this->pageRepository->getLists(true);
                $view = view('admincp.menus.option_render',compact('data','module'))->render();
    			return response()->json($view,200);
    			break;
    		case 'post':
    			$data = app()->make(\App\Repositories\Post\EloquentCategoryRepository::class)->getLists(true);
    			$view = view('admincp.menus.option_render',compact('data','module'))->render();
                return response()->json($view,200);
    			break;
    		case 'product':
    			$data = app()->make(\App\Repositories\Product\EloquentCategoryRepository::class)->getLists(true);
    			$view = view('admincp.menus.option_render',compact('data','module'))->render();
                return response()->json($view,200);
    			break;
    	}
    }

    public function addcustommenu (){
        $this->createWithLanguage(request()->all());
        return redirect()->back();
    }
}
