<?php

namespace App\Http\Controllers\Admincp\User;

use App\Http\Requests\user\UserRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\User\EloquentUserRepository;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Hash;
use View;


class UserController extends Controller
{
    protected $userRepository;

    public function __construct(EloquentUserRepository $userRepository){
        $this->userRepository = $userRepository;

        // -======= BEGIN : PERMISSONS ==================
        $this->middleware('permission:users,index', ['only' => ['index']]);
        $this->middleware('permission:users,create',   ['only' => ['create','store']]);
        $this->middleware('permission:users,update',   ['only' => ['edit', 'update']]);
        $this->middleware('permission:users,destroy',   ['only' => ['destroy']]);
        // -======= END : PERMISSONS ====================
    }

    public function index(Request $request){
            $users = $this->userRepository->getLists();
            return view('admincp.users.index',compact('users'));
    }

    public function create(Request $request){
        return view('admincp.users.create',compact('roles'));
    }

    public function store(UserRequest $request){
        //
        $data = $request->all();
        $data['password'] = Hash::make($request->password);
        if($data['profile_image'] == ''){
            $data['profile_image'] = '/user-default.png';
        }
        if($this->userRepository->createWithLanguage($data)){
            return Redirect::route('admincp.users.index')->with('alert-success', trans('back-app.add_success'));
        }
        else {
            return Redirect::back()->with('alert-danger', trans('back-app.add_fail'));
        }    
    }

    public function edit($id){
        if($user = $this->userRepository->findWithLanguage($id)){
            return view('admincp.users.edit',compact('user'));
        }
        else {
            return Redirect::back()->with('alert-danger',trans('back-app.data_not_fail'));
        }
    }

    public function update(UserRequest $request, $id){
        $data = $request->all();
        $user = $this->userRepository->find($id);
        $data['profile_image'] =  $data['profile_image'] ? $data['profile_image'] : $user->profile_image;
        if(!is_null($data['old_password']) || !is_null($data['password']) || !is_null($data['re_password'])){
            if(! Hash::check( $data['old_password'] ,$user->password )){
                return redirect()->back()->with('alert-danger','Mật khẩu cũ không đúng');
            }
            else {
                $this->validate($request, [
                    'password' => 'required|min:5',
                    're_password' => 'required|same:password'
                ]);
                $data['password'] = Hash::make($data['password']);
                if($this->userRepository->updateWithLanguage($id,$data)){
                    return redirect()->back()->with('alert-success',trans('back-app.update_success'));
                }
                return redirect()->back()->with('alert-danger',trans('back-app.update_fail')); 
            }
        }
        else {
            $data['password'] = $user->password;
            if($this->userRepository->updateWithLanguage($id,$data)){
                return redirect()->back()->with('alert-success',trans('back-app.update_success'));
            }
            return redirect()->back()->with('alert-danger',trans('back-app.update_fail'));  
        }
    }

    public function destroy($id){
        //
        if($this->userRepository->deleteWithLanguage($id)){
            return Redirect::back()->with('alert-success', trans('back-app.delete_success'));
        }else {
            return Redirect::back()->with('alert-danger',trans('back-app.delete_fail'));
        }
    }

    public function postDisable(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->userRepository->update($id_arr,['published' => false])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }   
        
    }

    public function postPublic(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->userRepository->update($id_arr,['published' => true])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }     
    }

    public function postDestroys(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->userRepository->deleteWithLanguage($id_arr)){
            session()->flash('alert-success', trans('back-app.delete_fail'));
            return response()->json('success');
        }       
    }
}
