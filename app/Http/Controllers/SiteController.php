<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Page\EloquentPageRepository;
use App\Repositories\Post\EloquentPostRepository;
use App\Repositories\Product\EloquentProductRepository;
use App\Repositories\User\EloquentUserRepository;
use App\Repositories\Admin\EloquentAdminRepository;
use App\Models\Contact\Contact;
use App\Exceptions\SiteNotFoundException;
use Auth, Session, Input, Mail,Event, Cart;
use App\Models\Order\Order;
use Visitor;
use App\Repositories\Setting\EloquentSettingRepository;
use App\Repositories\Menu\EloquentMenuRepository;

class SiteController extends Controller
{
    protected $pageRepository;
    protected $categoryPostRepository;
    protected $postRepository;
    protected $categoryProductRepository;
    protected $productRepository;
    protected $userRepository;
    protected $adminRepository;

    protected $settingRepository;
    protected $menuRepository;

    protected $category_menu;
    protected $mail_admin;
    public function __construct(
            EloquentPageRepository $pageRepository,
            \App\Repositories\Post\EloquentCategoryRepository $categoryPostRepository,
            EloquentPostRepository $postRepository,
            \App\Repositories\Product\EloquentCategoryRepository $categoryProductRepository,
            EloquentUserRepository $userRepository,
            EloquentProductRepository $productRepository,
            EloquentAdminRepository $adminRepository,
            EloquentSettingRepository $settingRepository,
            EloquentMenuRepository $menuRepository
        ){

        $this->pageRepository = $pageRepository;
        $this->categoryPostRepository = $categoryPostRepository;
        $this->postRepository = $postRepository;
        $this->categoryProductRepository = $categoryProductRepository;
        $this->productRepository = $productRepository;
        $this->userRepository = $userRepository;
        $this->adminRepository = $adminRepository;
        $this->menuRepository = $menuRepository;
         $this->settingRepository = $settingRepository;
         // $this->category_menu = $this->menuRepository->getViaName('main-menu');
         $this->mail_admin = getConfigSite('site_email');
    }

    public function site_setting(){
        $this->settingRepository = $this->settingRepository->findWithLanguage(1);
    }
    public function index () 
    {
        /**
        * getLists($published = null,$objectId = null, $limit = null,$orderby = 'latest')
        * @param $published : trạng thái xuất bản
        * @param $limit : giới hạng record
        * @param $orderby : latest (mới nhất) : oldest (cũ nhất)
        * @param $objectId : loại bỏ đối tượng đang sử dụng bởi Id
        */
        // $category = get_list_product_categories(true,0,26);
        
        $setting = $this->site_setting();
        $featured_category = $this->categoryProductRepository->getLists(true,0,26,null,'featured_asc'); 
       
        $featured_categories_post = $this->categoryPostRepository->getLists(true,null,null,null,'pos'); 
        return view('site.index',[
            'featured_category' => $featured_category,
            'featured_categories_post'=>$featured_categories_post,
            'setting'=>$this->settingRepository
            // 'category_menu'=>$this->category_menu
        ]);
    }
    
    public function submitContact (Request $request) {
        
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'message' => 'nullable'
        ]);

        $data = $request->all();

        if (Contact::create($data)){
            Mail::send('email.report_for_user',['name'=>$request->email],function ($message) use ($request){
                $message->to($request->email);
                $message->subject('Mail thông báo');
            });

            return redirect()->back()->with('message' , trans('front-app.send_success'));;
        }
    }

    public function getViaPage ($slug) {
        if($page = $this->pageRepository->getViaSlug($slug)){

            $title = $page->language('title');
            $desc = $page->language('description');
            $setting = $this->site_setting();
            // Even::fire('posts.view',$page);
            return view('site.pages.index',[
                'page' => $page,

                'title' => $title,
                
                'desc' => $desc,
                
                'setting'=>$this->settingRepository
                
        ]);
        }
        throw new SiteNotFoundException();
        
    }

    public function getViaProductCate ($slug) {

        if($category = $this->categoryProductRepository->getViaSlug($slug)){
            $setting = $this->site_setting();
            if($products = $this->productRepository->getLists(true,$category->id,48,'featured_asc')){
                // dd($products);
                return view('site.products.index_pro',['title' => $category->language('title'),'desc' => $category->language('description'),
                    'products' => $products,
                    'category'=>$category,
                    'setting'=>$this->settingRepository
                    // 'category_menu'=>$this->category_menu
            ]);
            }
        }

        throw new SiteNotFoundException();
    }

    public function getViaProduct ($slugCate,$slug) {

        if($category = $this->categoryProductRepository->getViaSlug($slugCate)){
            if($product = $this->productRepository->getViaSlug($slug)){
                // dd($product);
                $setting = $this->site_setting();
                Event::fire('posts.view', $product);
                return view('site.products.detail',['category' => $category,'title' => $product->language('title'),'product' => $product,
                    'setting'=>$this->settingRepository
                    // 'category_menu'=>$this->category_menu
            ]);
            }
        }
        throw new SiteNotFoundException();
    }

    public function getViaPostCate ($slug) {
        if($category = $this->categoryPostRepository->getViaSlug($slug)){
            if($posts = $this->postRepository->getLists(true,$category->id,10)){
                return view('site.posts.index',['title' => $category->language('title'),'posts' => $posts,'desc' => $category->language('description')]);
            }
        }
        throw new SiteNotFoundException(); 
    }

    public function getViaPost ($slugCate,$slug) {
        if($category = $this->categoryPostRepository->getViaSlug($slugCate)){

            if($post = $this->postRepository->getViaSlug($slug)){
                 $setting = $this->site_setting();
                Event::fire('posts.view', $post);
                return view('site.posts.detail',['category' => $category,'title' => $post->language('title'),
                    'post' => $post,
                    'setting'=>$this->settingRepository
                    // 'category_menu'=>$this->category_menu
            ]);
            }
        }
        throw new SiteNotFoundException();
    }

    public function postContactMail (Request $request){
        $data = $request->all();
        // if (Contact::create($data)){
            Mail::send('email.report_phone_mail',['phone'=>$request->phone,'product_id'=>$request->product_id,'email'=>$request->email,'custommer_message'=>$request->txtMessage,'name'=>$request->name,'qty'=>$request->qtybutton],function ($message) use ($request){
                // $message->to($request->email);
                $message->to($this->mail_admin);
                $message->subject('Có khách vừa đặt hàng sdt '.$request->phone.' Sản phẩm:'.$request->product_id);
            });
            return redirect()->back()->with('message' , trans('front-app.send_success'));
        // }
    }

    public function postContact (Request $request){
        $data = $request->all();
        if (Contact::create($data)){
            Mail::send('email.report_for_user',['name'=>$request->name],function ($message) use ($request){
                $message->to($this->mail_admin);
                // $message->from('tuyenmh86@gmail.com');
                $message->subject('Mail thông báo');
            });
            return redirect()->back()->with('message' , trans('front-app.send_success'));
        }
    }

    public function getAgents (){
        $agents = $this->userRepository->getLists(true, null, 20);
        $title = trans('front-app.real_astate_agent');
         $setting = $this->site_setting();
        return view('site.agents.index',['agents' => $agents,'title' => $title,
            'setting'=>$this->settingRepository
            // 'category_menu'=>$this->category_menu
        ]);
    }

    public function getViaAgent ($id,$slug){
        if($agent = $this->userRepository->getViaId($id)){
            $setting = $this->site_setting();
            $title = $agent->full_name;
            $products = $this->productRepository->getLists(true, null, 12, 'latest', null, $agent->id);
            return view('site.agents.detail',['agent' => $agent,'title' => $title,'products' => $products,
                'setting'=>$this->settingRepository
                // 'category_menu'=>$this->category_menu
        ]);
        }
        throw new SiteNotFoundException();
    }

    public function getViaInvestor ($id,$slug) {
        if($agent = $this->adminRepository->getViaId($id)){
            $setting = $this->site_setting();
            $title = $agent->full_name;
            $products = $this->productRepository->getLists(true, null, 12, 'latest', null, ['admin_id',$agent->id]);
            return view('site.agents.detail',['agent' => $agent,'title' => $title,'products' => $products,
                'setting'=>$this->settingRepository
                // 'category_menu'=>$this->category_menu
            ]);
        }
        throw new SiteNotFoundException();
    }

    public function searchs (Request $request) {
        $data = $request->all();
        $setting = $this->site_setting();
        $title = trans('front-app.search');
        //seach by tags Or title
        $products = $this->productRepository->searchs(null,$request->q);
        
        // return view('site.posts.search',['posts' => $posts,'title' => $title]);
        return view('site.products.search',['products' => $products,'title' => $title,
        'setting'=>$this->settingRepository
        // 'category_menu'=>$this->category_menu
        ]);
    }

    public function getContact () {
        $title = trans('front-app.contact');
        $setting = $this->site_setting();
        return view('site.contact.index',[
            'title' => $title,
            'setting'=>$this->settingRepository
            // 'category_menu'=>$this->category_menu
        ]);
    }

    public function postAddToCart (Request $request) {
        $product = $this->productRepository->find($request->data);
        $price = $product->promotion_price < $product->price && $product->promotion_price > 0 ? $product->promotion_price : $product->price;

        Cart::add($product->id, $product->language('title'), $request->qty, $price,['image' => $product->featured_image]);
        return response()->json(['status' => 'Thêm sản phẩm vào giỏ hàng thành công.','cart_count' => Cart::count()],200);

    }

    public function getCartPage (Request $request) {
        $carts = Cart::content();
        $title = trans('front-app.cart');
        $setting = $this->site_setting();
        return view('site.cart.cart',['carts' => $carts,'title' => $title,'setting'=>$this->settingRepository
        ]);
    }

    public function postRemoveItemCart (Request $request) {
        Cart::remove($request->params['rowId']);
        return response()->json([],200);
    }

    public function postCartDestroy () {
        Cart::destroy();
        return response()->json([],200);
    }

    public function postUPdateQuantityCart (Request $request){
        Cart::update($request->rowId, $request->qty);
        return response()->json([],200);
    }

    public function getCheckoutPage (Request $request) {

        if(Cart::count()==0){
            return redirect()->back()->with('message' ,'Bạn chưa mua sản phẩm nào!! Vui lòng mua sản phẩm rồi thanh toán nhé!!!'); 
        }
         $setting = $this->site_setting();
        $carts = Cart::content();
        $title = trans('front-app.checkout');
        return view('site.cart.checkout',['carts' => $carts,'title' => $title,'setting'=>$this->settingRepository
            // 'category_menu'=>$this->category_menu
        ]);
    }

    public function postCheckOutFinished (Request $request) {
        // dd($request->all());
        $data['customer_info'] = $request->all();
        unset($data['customer_info']['_token']);
        $data['order_detail'] = Cart::content();
        $data['total'] = Cart::subtotal();
        $data['hoten'] = $request->last_name .' '.  $request->first_name;
        $data['company_name'] = $request->company_name;
        $data['address'] = $request->address;
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;
        
        if(Order::create($data)){

            // return view('email.report_for_admin',['data' => $data]);
            Mail::send('email.report_for_admin',['data'=>$data,'name'=>$request->name],function ($message) use ($request){
                $message->to($request->email)
                
                        ->cc($request->email);
                $message->subject('Đơn hàng được đặt qua website');
            });

            Cart::destroy();
            return redirect('/')->with('order_success','Hoàn thành thanh toán.Cảm ơn quý khách.' );
        }
    }

    public function getSitemap(){
        // $featured_categories_post = $this->categoryPostRepository->getLists(true,null,null,null,'pos'); 
        $categories = $this->categoryProductRepository->getLists(true,0,26,null,'featured_asc'); 
        
        return response()->view('sitemap', [
            'categories' => $categories,
        ])->header('Content-Type', 'text/xml');
      
    }
}
