<?php

namespace App\Http\Controllers\UserManager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Product\EloquentProductRepository;
use App\Repositories\User\EloquentUserRepository;
use Auth;

class UserManagerController extends Controller
{
    protected $productRepository;

    public function __construct(EloquentProductRepository $productRepository){
        $this->productRepository = $productRepository;
    }
    public function getDashboard(){
        if(Auth::check()){
            $produc_top_v = $this->productRepository->getLists(null,null,10,'view',null,Auth::user()->id);
            return view('usercpanel.dashboard',['produc_top_v' => $produc_top_v]);
        }
    }

    public function get404page(Request $request)
    {
        return view('admincp.404');
    }
}
