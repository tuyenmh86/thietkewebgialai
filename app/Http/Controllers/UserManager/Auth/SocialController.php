<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Interfaces\Social\SocialRepositoryInterface;
use App\Repositories\Interfaces\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Redirect;
use Socialite;
use Auth;

class SocialController extends Controller
{
    protected $socialRepository;
    protected $userRepository;
    public function __construct(SocialRepositoryInterface $socialRepository,UserRepositoryInterface $userRepository){
        $this->socialRepository = $socialRepository;
        $this->userRepository = $userRepository;
    }
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user_fb = Socialite::driver('facebook')->user();
        $user = $this->userRepository->checkLogedFB($user_fb->id);
        if($user){
            Auth::login($user);
            return Redirect::route('user.dashboard');
        }
        else {
            if($this->userRepository->checkEmail($user_fb->email)){
                $data = [
                    'full_name' => $user_fb->name,
                    'facebook_id' => $user_fb->id,
                    'facebook_token' => $user_fb->token,
                    'provider' => 'facebook',
                    'profile_image' => $user_fb->avatar_original,
                    'role_id' => 2,
                    'status' => 1,
                ];
            }
            else {
                $data = [
                    'full_name' => $user_fb->name,
                    'facebook_id' => $user_fb->id,
                    'facebook_token' => $user_fb->token,
                    'email' => $user_fb->email,
                    'provider' => 'facebook',
                    'profile_image' => $user_fb->avatar_original,
                    'role_id' => 2,
                    'status' => 1,
                ];
            }
            
            if($this->userRepository->create($data)){
                Auth::login($user);
                return Redirect::route('user.dashboard');
            }

        }
    }
}
