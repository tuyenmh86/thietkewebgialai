<?php

namespace App\Http\Controllers\UserManager\Auth;
use App\Http\Requests\user\UserRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\User\EloquentUserRepository;
use Auth;
use Hash;
use Session;

class AuthController extends Controller
{
    protected $userRepository;
    public function __construct(EloquentUserRepository $userRepository){
         $this->userRepository = $userRepository;
    }

    public function getLogin ()
    {
        if (Auth::check()) 
        {
           return redirect()->route('user.dashboard');
        } 
        else 
        {
            date_default_timezone_set('Asia/Ho_Chi_Minh');
            $now=date_create();
            $timeCount_user = date_format($now,"Y-m-d H:i:s");
            if(Session::has('timeCount_user'))
            {
                if($timeCount_user > Session::get('timeCount_user'))
                {
                    Session::forget('timeCount_user');
                    Session::forget('timesLog_user');
                }
            }     
            return view('usercpanel.auth.login');
        } 
    }
    public function postLogin(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $email = trim($request->email);
        $password = trim($request->password);
        $remember = ($request->remember) ? true : false;
        if (Auth::attempt(['email'=>$email,'password'=>$password,'published' => true],$remember))
        {
            Auth::user()->save();
            if(Session::has('timesLog_user'))
            {
                Session::forget('timesLog_user');
            }
            return redirect()->route('user.dashboard');
        }
        else 
        {
            if (!Session::has('timesLog_user'))
            {
                Session::put('timesLog_user', 1);
            }
            else 
            {
                if(Session::get('timesLog_user') == 1)
                {
                    Session::put('timesLog_user', 2);
                }
                else
                {
                    Session::put('timesLog_user', 3);
                    // Set time zone for login attemp
                    date_default_timezone_set('Asia/Ho_Chi_Minh');
                    $now=date_create();
                    date_modify($now,"+180 seconds");
                    $timeCount_user = date_format($now,"Y-m-d H:i:s");
                    if (!Session::has('timeCount_user'))
                    {
                        Session::put('timeCount_user', $timeCount_user);
                    }       
                    return redirect()->back()->with('error','You have entered the wrong password more than 3 times, please try again in 3 minutes.');             
                }
            }
            return redirect()->back()->with('error','Username or a password incorrect or was disabled.');
        }
    }

    public function getRegister()
    {
        return view('usercpanel.auth.register');
    }
    // Post Register Page
    public function postRegister(Request $request)
    {
        // Validation Form Login
        $this->validate($request, [
            'full_name' => 'required|max:60',
            'email' => 'required|email|max:100|unique:users',
            'phone' => 'max:15',
            'address' => 'max:150',
            'password' => 'required|min:6|alpha_spaces',
            're_password' => 'required|same:password',
        ]);
        $data = $request->all();
        $data['published'] = 1;
        $data['profile_image'] = '/uploads/user_img_default.png';
        $data['password'] = Hash::make($request->password);
        // dd($request->all());
        if($this->userRepository->create($data))
        {
            return redirect()->back()->with('alert-success','Register Account Successful.');
        }
        else 
        {
            return redirect()->back()->with('alert-danger','Register Account Failed.');
        }
    }

    public function getLogout()
    {
        if(Auth::check())
        {
             Auth::logout();
             return redirect()->route('user.logout');
        }
    }
}

