<?php

namespace App\Http\Controllers\UserManager\Product;

use App\Http\Requests\Product\ProductRequest;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Product\EloquentCategoryRepository;
use App\Repositories\Product\EloquentProductRepository;
use App\Repositories\Product\EloquentTagRepository;
use App\Repositories\Product\EloquentAttributeRepository;
use Illuminate\Support\Facades\Redirect;
use View;
use Auth;

class ProductController extends Controller
{
    protected $categoryRepository;
    protected $productRepository;
    protected $attributeRespository;
    protected $tagRepository;

    public function __construct(
        EloquentCategoryRepository $categoryRepository ,
        EloquentProductRepository $productRepository,
        EloquentTagRepository $tagRepository
        // EloquentAttributeRepository $attributeRespository
        ){
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
        $this->tagRepository = $tagRepository;
        // $this->attributeRespository = $attributeRespository;
    }

    public function index()
    {
       
        /**
         * Get product posted by User id
         */
        $products = $this->productRepository->getLists(null,null,null,'latest',null,Auth::user()->id);
        return view('usercpanel.products.index',compact('products'));
    }

    public function create()
    {
        $categorys = $this->categoryRepository->getLists(true);
        $tags = $this->tagRepository->getLists(true);
        return view('usercpanel.products.create',compact('categorys','tags'));
    }

    public function store(ProductRequest $request)
    {
        if(Auth::check()){
            $data = $request->all();
            $data['user_id'] = Auth::user()->id;
            if($product = $this->productRepository->createWithLanguage($data))
            {
                !isset($data['tags']) ? : $product->tags()->sync($data['tags']);
                !isset($data['categories']) ? : $product->categories()->sync($data['categories']);
                $data['product_id'] = $product->id;
                // $this->attributeRespository->create($data);
                return Redirect::back()->with('alert-success',trans('back-app.add_success'));
            } 
        }
    }

    public function show($id)
    {
    //
    }

    public function edit($id)
    {

        if($product = $this->productRepository->findWithLanguage($id)){
            if($product->user->id == auth()->user()->id){
                $categorys = $this->categoryRepository->getLists(true);
                $tags = $this->tagRepository->getLists(true);
                return view('usercpanel.products.edit',compact('product','categorys','tags'));
            }else {
                return redirect()->back()->with('alert-danger',trans('back-app.data_not_found'));
            }
        }else {
            return redirect()->back()->with('alert-danger',trans('back-app.data_not_found'));
        }
    }

    public function update(ProductRequest $request, $id)
    {
        $data = $request->all();
        // dd($data);
        $product = $this->productRepository->find($id);
        $data['featured'] = isset($data['featured']) ? $data['featured'] : 0;
        if($this->productRepository->updateWithLanguage($id, $data)){
            !isset($data['tags']) ? : $product->tags()->sync($data['tags']);
            !isset($data['categories']) ? : $product->categories()->sync($data['categories']);
            $this->attributeRespository->update($id, $data);
            return Redirect::back()->with('alert-success',trans('back-app.update_success'));
        }
        else {
            return Redirect::back()->with('alert-danger',trans('back-app.update_fail'));
        } 
    }

    public function destroy($id)
    {
        if($this->productRepository->deleteWithLanguage($id)){
            return Redirect::back()->with('alert-success',trans('back-app.delete_success'));
        }else {
            return Redirect::back()->with('alert-success',trans('back-app.delete_fail'));
        }
    }

    public function postDisable(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->productRepository->update($id_arr,['published' => 0])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        } 
    }

    public function postPublic(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->productRepository->update($id_arr,['published' => 1])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        } 
    }

    public function postDestroys(Request $request){
        $id_arr = $request->input('id_arr');
        if($this->productRepository->deleteWithLanguage($id_arr)){
            session()->flash('alert-success', trans('back-app.delete_success'));
            return response()->json('success');
        }
    }

    public function postUpdatePos(Request $request){
        $pos = $request->input('pos');
        $id = $request->input('id');
        if($this->productRepository->update($id,['pos' => $pos])){
            session()->flash('alert-success', trans('back-app.update_success'));
            return response()->json('success');
        }
    }
}
