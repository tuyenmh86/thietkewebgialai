<?php

namespace App\Http\Controllers\UserManager\Contact;

use Illuminate\Http\Request;
use App\Models\Contact\Contact;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use View;
use Auth;

class ContactController extends Controller
{

    public function index()
    {
        if(auth()->check()){
            $contacts = Contact::where('manager_id',Auth::user()->id)->get();
            return view('usercpanel.contacts.index',compact('contacts'));
        }
    }

    public function updatestatus(Request $request)
    {
        $contact = Contact::find($request->id);

        if (!$contact)
            return response()->json(['error' => 'Contact not found!'], 404);

        if ($contact->update(['status' => 'success']))
            return response()->json('Update contact success', 200);

    }

    public function destroy($id)
    {
        $contact = Contact::find($id);

        if (!$contact)
            return Redirect::back()->with('alert-danger','Delete Contact Failed!');

        if ($contact->delete())
            return Redirect::back()->with('alert-success','Delete Contact Successfully!');
    }

    public function deleteSelect(Request $request)
    {
        if (!is_array($request->id))
            return response()->json(['error' => 'Param not accept!'], 500);

        if (is_null($request->id))
            return response()->json(['error' => 'Please select contacts for delete!'], 403);

        foreach ($request->id as $id){
            $contact = Contact::find($id);

            if (!$contact)
                continue;

            $contact->delete();
        }
        return response()->json('Delete contacts success', 200);
    }
}
