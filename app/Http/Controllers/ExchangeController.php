<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExchangeController extends Controller
{
    public function changeprice(Request $request){
    	if($request->ajax()){

	    	$currency = $request->currency;

	    	$dom=new \DOMDocument();

			$dom->load("http://www.vietcombank.com.vn/ExchangeRates/ExrateXML.aspx");

			$exrate=$dom->getElementsByTagName("Exrate");

			if($currency=="VND"){

				$request->session()->put([

					'currency' => 'VND',

					'currency-icon' => '<strong class="mr-sm-2">₫</strong> '

				]);

				$request->session()->forget('currency-buy');

			}

	    	foreach ($exrate as $item) {

				$code=$item->getAttribute("CurrencyCode");

				$buy=$item->getAttribute("Buy");
				
				if($code===$currency){	

		    		$request->session()->put([

		    			'currency' => $code,

		    			'currency-icon' => '<i class="mr-sm-2 fa fa-'.strtolower($code).'" aria-hidden="true"></i> ',

		    			'currency-buy' => $buy
		    		]);


					break;
				}

			}
		}
    }
}
