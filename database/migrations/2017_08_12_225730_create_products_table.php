<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title')->nullable();
            $table->text('alias')->nullable();
            $table->text('description')->nullable();
            $table->longText('content')->nullable();
            $table->text('seo_title')->nullable();
            $table->text('seo_keywords')->nullable();
            $table->text('seo_description')->nullable();

            $table->integer('parent_id')->unsigned()->index()->nullable();
            $table->integer('admin_id')->unsigned()->index()->nullable();
            $table->foreign('admin_id')->references('id')->on('admins')
                ->onDelete('cascade');

            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');

            $table->text('featured_image')->nullable();
            $table->text('image_list')->nullable();
            $table->integer('view')->default(0);
            $table->string('sku')->nullable();
            $table->integer('price',20)->nullable();
            $table->integer('promotion_price',20)->nullable();
            $table->boolean('published')->default(false);
            $table->boolean('featured')->default(false);
            $table->softDeletes();  
            $table->integer('pos')->nullable();
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
