<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique()->nullable();
            $table->string('full_name')->nullable();
            $table->string('email')->unique();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('profile_image')->nullable();
            $table->longText('introduce')->nullable();
            $table->string('password')->nullable();
            $table->timestamp('last_login');

            $table->integer('role_id')->unsigned()->index();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->boolean('is_spadmin')->default(false);
            $table->boolean('published')->default(false);
            $table->softDeletes();
            $table->integer('pos')->nullable();
            $table->rememberToken()->nullable();;
            $table->timestamps();
        });
        Schema::table('admins', function (Blueprint $table) {

        });
        DB::table('admins')->insert([
                'username' => 'admin',
                'password' => bcrypt('admin'),
                'email' => 'admin@admin.com',
                'last_login' => '2017-07-29 19:12:46',
                'address' => 'HCM',
                'full_name' => 'admin',
                'phone' => '(+84) 938 310 064',
                'introduce' => 'This is Supper Admin',
                'profile_image' => '/uploads/user_img_default.png',
                'role_id' => 1,
                'published' => 1,
                'is_spadmin' => 1,
                'pos' => '0',
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
