<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('display_name')->nullable();
            $table->text('description')->nullable();
            $table->boolean('published')->default(false);
            $table->boolean('advanced')->default(false);
            $table->softDeletes();  
            $table->integer('pos')->nullable();
            $table->timestamps();
        });
        DB::table('roles')->insert([
                'name' => 'Supper Admin',
                'display_name' => 'supper_admin',
                'description' => 'This is Supper Admin Role',
                'advanced' => 1,
                'pos' => '0',
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
