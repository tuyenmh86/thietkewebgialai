<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags_post', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('alias')->nullable();
            $table->text('description')->nullable();
            $table->boolean('published')->default(false);
            $table->boolean('featured')->default(false);
            $table->softDeletes();  
            $table->integer('pos')->nullable();
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags_post');
    }
}
