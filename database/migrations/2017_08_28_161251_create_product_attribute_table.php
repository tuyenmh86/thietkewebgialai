<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAttributeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_attribute', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned()->index()->nullable();;
            $table->foreign('product_id')->references('id')->on('products')
                ->onDelete('cascade');

            $table->integer('price')->nullable();
            $table->integer('discount')->nullable();
            $table->string('address')->nullable();

            $table->integer('beedroom')->nullable();
            $table->integer('bathroom')->nullable();
            $table->integer('acreage')->nullable();
            $table->integer('floor')->nullable();
            $table->string('floor_plan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_attribute');
    }
}
