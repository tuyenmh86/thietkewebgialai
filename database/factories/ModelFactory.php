<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

// $faker->randomDigit;
// $faker->numberBetween(1,100);
// $faker->word;
// $faker->paragraph;
// $faker->lastName;
// $faker->city;
// $faker->year;
// $faker->domainName;
// $faker->creditCardNumber;
// 'title' => $faker->catchPhrase,
// 'content' => $faker->paragraph,
// 'created_at' => $faker->dateTime($max = 'now'),
// 'updated_at' => $faker->dateTime($max = 'now'),
// $faker->region; // fr_FR Region "Saint-Pierre-et-Miquelon"
// $faker->bankAccountNumber; // pl_PL "PL14968907563953822118075816"
// $faker->cellNumber; // nz_NZ "021 123 4567"

// Post Category
$factory->define(App\Models\Post\Category::class, function (Faker\Generator $faker) {

    return [
        'vi_title' => $faker->catchPhrase,
        'en_title' => $faker->catchPhrase,
        'cn_title' => $faker->catchPhrase,

        'vi_alias' => to_slug_url(str_random(25)),
        'en_alias' => to_slug_url(str_random(25)),
        'cn_alias' => to_slug_url(str_random(25)),
        'description' => $faker->text,
        'parent_id' => 0,
        'active' => 1,
        'status' => 1,
    ];
});
// Post
$factory->define(App\Models\Post\Post::class, function (Faker\Generator $faker) {

    return [
        'vi_title' => $faker->catchPhrase,
        'vi_alias' => to_slug_url($faker->catchPhrase),
        'vi_description' => $faker->paragraph,
        'vi_content' => $faker->paragraph,

        'en_title' => $faker->catchPhrase,
        'en_alias' => to_slug_url($faker->catchPhrase),
        'en_description' => $faker->paragraph,
        'en_content' => $faker->paragraph,

        'cn_title' => $faker->catchPhrase,
        'cn_alias' => to_slug_url($faker->catchPhrase),
        'cn_description' => $faker->paragraph,
        'cn_content' => $faker->paragraph,

        'vi_seo_title' => $faker->text,
        'vi_seo_description' => $faker->text,
        'vi_seo_keywords' => $faker->text,

        'en_seo_title' => $faker->text,
        'en_seo_description' => $faker->text,
        'en_seo_keywords' => $faker->text,

        'cn_seo_title' => $faker->text,
        'cn_seo_description' => $faker->text,
        'cn_seo_keywords' => $faker->text,

        'user_id' => $faker->numberBetween(1,5),
        'category_id' => $faker->numberBetween(1,6),
        'featured_image' =>"/uploads/images/Gallery/g8.jpg",
        'featured_post' => $faker->numberBetween(1,60),
        'view' => $faker->numberBetween(100,5000),
        'pos' => $faker->numberBetween(1,30),
    ];
});

// Product Category
$factory->define(App\Models\Product\Category::class, function (Faker\Generator $faker) {

    return [
        'vi_title' => $faker->catchPhrase,
        'en_title' => $faker->catchPhrase,
        'cn_title' => $faker->catchPhrase,
        
        'vi_alias' => to_slug_url(str_random(25)),
        'en_alias' => to_slug_url(str_random(25)),
        'cn_alias' => to_slug_url(str_random(25)),

        'description' => $faker->text,
        'parent_id' => 0,
        'status' => 1,
        'pos' => $faker->numberBetween(0,30),
    ];
});

// Product
$factory->define(App\Models\Product\Product::class, function (Faker\Generator $faker) {

    return [
        'vi_name' => $faker->catchPhrase,
        'vi_alias' => to_slug_url($faker->catchPhrase),
        'vi_description' => $faker->paragraph,
        'vi_content' => $faker->paragraph,

        'en_name' => $faker->catchPhrase,
        'en_alias' => to_slug_url($faker->catchPhrase),
        'en_description' => $faker->paragraph,
        'en_content' => $faker->paragraph,

        'cn_name' => $faker->catchPhrase,
        'cn_alias' => to_slug_url($faker->catchPhrase),
        'cn_description' => $faker->paragraph,
        'cn_content' => $faker->paragraph,

        'vi_seo_title' => $faker->text,
        'vi_seo_description' => $faker->text,
        'vi_seo_keywords' => $faker->text,

        'en_seo_title' => $faker->text,
        'en_seo_description' => $faker->text,
        'en_seo_keywords' => $faker->text,

        'cn_seo_title' => $faker->text,
        'cn_seo_description' => $faker->text,
        'cn_seo_keywords' => $faker->text,

        'category_id' => $faker->numberBetween(1,13),
        'user_id' => $faker->numberBetween(1,10),
        'parent_id' => $faker->numberBetween(1,10),

        'price' => $faker->numberBetween(1000000,90000000)."000",
        'bedroom' => $faker->numberBetween(1,3),
        'address' => $faker->address,
        'discount' => 0,
        'function' => '<ul class="list-unstyled facilities">
                            <li class="col-xs-12 col-sm-4"><i class="icon icon-air-conditioning icon-lg"></i> Máy Lạnh</li>
                            <li class="col-xs-12 col-sm-4"><i class="icon icon-bbq-area icon-lg"></i> Khu Nướng BBQ</li>
                            <li class="col-xs-12 col-sm-4"><i class="icon icon-cctv icon-lg"></i> CCTV</li>
                            <li class="col-xs-12 col-sm-4"><i class="icon icon-fitness icon-lg"></i> Phòng Tập</li>
                            <li class="col-xs-12 col-sm-4"><i class="icon icon-garden icon-lg"></i> Sân Vườn</li>
                            <li class="col-xs-12 col-sm-4"><i class="icon icon-library icon-lg"></i> Thư Viện</li>
                            <li class="col-xs-12 col-sm-4"><i class="icon icon-parking icon-lg"></i> Bãi Đậu Xe</li>
                            <li class="col-xs-12 col-sm-4"><i class="icon icon-playground icon-lg"></i> Sân Chơi</li>
                            <li class="col-xs-12 col-sm-4"><i class="icon icon-security icon-lg"></i> Bảo Vệ</li>
                            <li class="col-xs-12 col-sm-4"><i class="icon icon-swimming-pool icon-lg"></i> Hồ Bơi</li>
                            <li class="col-xs-12 col-sm-4"><i class="icon icon-tennis icon-lg"></i> Sân Tennis</li>
                            <li class="col-xs-12 col-sm-4"><i class="icon icon-wifi icon-lg"></i> Wi Fi</li>
                        </ul>',
        // 'quantity' => $faker->numberBetween(1,30),
        'view' => $faker->numberBetween(100,2000),
        'ordered' => $faker->numberBetween(100,2000),
        'featured_image' => "/uploads/images/Bds/z2.jpg",
        'image_list' => '|/uploads/images/Bds/z2.jpg|/uploads/images/Bds/z2.jpg|/uploads/images/Bds/z2.jpg|/uploads/images/Bds/z2.jpg|/uploads/images/Bds/z2.jpg|/uploads/images/Bds/z2.jpg|/uploads/images/Bds/z2.jpg',
        'floor_plan' => '|/uploads/images/Bds/z2.jpg|/uploads/images/Bds/z2.jpg|/uploads/images/Bds/z2.jpg|/uploads/images/Bds/z2.jpg|/uploads/images/Bds/z2.jpg|/uploads/images/Bds/z2.jpg|/uploads/images/Bds/z2.jpg',
        'status' =>1,
        'pos' => $faker->numberBetween(1,90),
    ];
});

// Product Attributes
$factory->define(App\Models\Product\AttributeProduct::class, function (Faker\Generator $faker) {

    return [
        'product_id' => $faker->numberBetween(1,250),
        'product_type_id' => 1,
        'attribute_id' => $faker->numberBetween(1,4),
        'value' => $faker->numberBetween(2,4),
    ];
});
// Orders
$factory->define(App\Models\Order\Order::class, function (Faker\Generator $faker) {

    return [
        'customer_id' => $faker->numberBetween(1,10),
        'total' => $faker->numberBetween(20000,200000),
    ];
});

// Order Detail
$factory->define(App\Models\Order\OrderDetail::class, function (Faker\Generator $faker) {

    return [
        'order_id' => $faker->numberBetween(1,15),
        'product_id' => $faker->numberBetween(1,30),
        'price' => $faker->numberBetween(10000,150000),
        'quanlity' => $faker->numberBetween(1,5),
        'total' => $faker->numberBetween(20000,90000),
    ];
});

// Users
$factory->define(App\User::class, function (Faker\Generator $faker) {

    return [
        'full_name' => $faker->name,
        'password' => bcrypt('secret'),
        'email' => $faker->email,
        'facebook_id' => $faker->numberBetween(1111111,999999999),
        'facebook_token' =>bcrypt('secret'),
        'provider' => 'Facebook',
        'last_login' => $faker->dateTime($max = 'now'),
        'address' => $faker->address,
        'phone' => $faker->phoneNumber,
        'profile_image' => '/uploads/user_img_default.png',
        'status' => 1,
        'pos' => $faker->numberBetween(1,30),
        'remember_token' => bcrypt('secret'),
    ];
});