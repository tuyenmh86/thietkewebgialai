<?php

use Illuminate\Database\Seeder;
use App\Models\Order\OrderDetail;

class OrderDetailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Order\OrderDetail::class,40)->create();
    }
}
