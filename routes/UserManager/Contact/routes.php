<?php
Route::group(
    [	'prefix' => 'usercpanel',
        'namespace' => 'UserManager\Contact',
        'middleware' => ['user'],
        'as' => 'user.'
    ],
    function(){

        Route::get('contacts', ['as' => 'contacts.index', 'uses' => 'ContactController@index']);
        Route::delete('contacts/{id}/destroy', ['as' => 'contacts.destroy', 'uses' => 'ContactController@destroy']);
        Route::get('contacts/updatestatus','ContactController@updatestatus');
        Route::get('contacts/deleteSelect','ContactController@deleteSelect');
    });