<?php 
Route::group(
	[	'prefix' => 'usercpanel',
		'namespace' => 'UserManager\Profile',
		'middleware' => ['user']
	],
	function(){
		Route::resource('profile','ProfileController',['as' => 'user']);
	});