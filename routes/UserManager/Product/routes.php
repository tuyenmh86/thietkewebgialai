<?php 

Route::group(
	[	'prefix' => 'usercpanel',
		'namespace' => 'UserManager\Product',
		'middleware' => ['user']
	],
	function(){
		Route::resource('products','ProductController',['as' => 'user']);

		// Disable
		Route::post('products/disable',
			[
			'as' => 'user.products.disable',
			'uses' => 'ProductController@postDisable'
			]);
		// Public
		Route::post('products/public',
			[
			'as' => 'user.products.public',
			'uses' => 'ProductController@postPublic'
			]);
		//  Trash restore
		Route::post('products/destroys',
			[
			'as' => 'user.products.destroys',
			'uses' => 'ProductController@postDestroys'
			]);
		
		// Update Position
		Route::post('products/pos/update',
			[
			'as' => 'user.products.pos.update',
			'uses' => 'ProductController@postUpdatePos'
			]);
	});