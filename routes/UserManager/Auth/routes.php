<?php 
Route::group([
		'middleware' => ['web'],
		'namespace' => 'UserManager\Auth'
		], function() 
		{
		// Socialite
		// Route::get('facebook/redirect', 'SocialController@redirectToProvider');
		// Route::get('facebook/callback', 'SocialController@handleProviderCallback');
		// Authentication
        Route::get('usercpanel/login', 
    	[
    		'as' => 'user.login',
    		'uses' => 'AuthController@getLogin'
    	]);
	    Route::post('usercpanel/login',
	    	[
	    		'as' => 'user.login',
	    		'uses' => 'AuthController@postLogin'
	    	]);
	    Route::get('usercpanel/register',
	    	[
	    		'as' => 'user.register',
	    		'uses' => 'AuthController@getRegister'
	    	]);
	    Route::post('usercpanel/register',
	    	[
	    		'as' => 'user.register',
	    		'uses' => 'AuthController@postRegister'
	    	]);
	    Route::get('usercpanel/logout',
	    	[
	    		'as' => 'user.logout',
	    		'uses' => 'AuthController@getLogout'
	    		
	    	])->middleware('user');
        // Route::post('logout', 'LoginController@logout')->name('logout');
        // Password Reset Routes...
        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        
        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'ResetPasswordController@reset');

});