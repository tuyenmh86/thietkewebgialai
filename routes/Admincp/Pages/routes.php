<?php 

Route::group(
	[	'prefix' => 'admincp',
		'namespace' => 'Admincp\Page',
		'middleware' => ['admin']
	],
	function(){
		// ************************ ROUTE PAGES *****************************
		Route::resource('pages','PageController',['as' => 'admincp']);

		// Disable
		Route::post('pages/disable',
			[
				'as' => 'admincp.pages.disable',
				'uses' => 'PageController@postDisable'
			])->middleware('permission:pages,update');
		// Public
		Route::post('pages/public',
			[
				'as' => 'admincp.pages.public',
				'uses' => 'PageController@postPublic'
			])->middleware('permission:pages,update');
		//  Trash restore
		Route::post('pages/destroys',
			[
				'as' => 'admincp.pages.destroys',
				'uses' => 'PageController@postDestroys'
			])->middleware('permission:pages,destroy');
		
		// Update Position
		Route::post('pages/pos/update',
			[
				'as' => 'admincp.pages.pos.update',
				'uses' => 'PageController@postUpdatePos'
			])->middleware('permission:pages,update');

	});