<?php 



Route::group(

	[	'prefix' => 'admincp',

		'namespace' => 'Admincp\Contact',

		'middleware' => ['admin','permission:contacts,index'],

        'as' => 'admincp.'

	],

	function(){

		// ************************ ROUTE ATTRIBUTES *****************************

        Route::get('contacts', ['as' => 'contacts.index', 'uses' => 'ContactController@index']);

        Route::delete('contacts/{id}/destroy', ['as' => 'contacts.destroy', 'uses' => 'ContactController@destroy']);

        Route::get('contacts/updatestatus','ContactController@updatestatus');

        Route::get('contacts/deleteSelect','ContactController@deleteSelect');

	});