<?php 

    Route::get('admincp/login', 
    	[
    		'as' => 'admincp.login',
    		'uses' => 'Admincp\Login\LoginController@getLogin'
    	]);
    Route::post('admincp/login',
    	[
    		'as' => 'admincp.login',
    		'uses' => 'Admincp\Login\LoginController@postLogin'
    	]);
    Route::get('admincp/logout',
    	[
    		'as' => 'admincp.logout',
    		'uses' => 'Admincp\Login\LoginController@getLogout'
    		
    	])->middleware('admin');
