<?php 



Route::group(

	[	'prefix' => 'admincp',

		'namespace' => 'Admincp\Menu',

		'middleware' => ['admin'],

        'as' => 'admincp.'

	],

	function(){

		// ************************ ROUTE ATTRIBUTES *****************************

        Route::name('menu.index')->get('menu',['uses' => 'NavigationController@index'])->middleware('permission:menus,index');

        Route::name('menu.module')->post('menu/module',['uses' => 'NavigationController@getModule']);

        Route::post('harimayco/addcustommenu', array( 'as' => 'haddcustommenu', 'uses'=>'NavigationController@addcustommenu'));
		Route::post('harimayco/deleteitemmenu', array('as' => 'hdeleteitemmenu', 'uses'=>'NavigationController@deleteitemmenu'));
		Route::post('harimayco/deletemenug', array('as' => 'hdeletemenug', 'uses'=>'NavigationController@deletemenug'));
		Route::post('harimayco/createnewmenu', array('as' => 'hcreatenewmenu', 'uses'=>'NavigationController@createnewmenu'));
		Route::post('harimayco/generatemenucontrol', array('as' => 'hgeneratemenucontrol', 'uses'=>'NavigationController@generatemenucontrol'));
		Route::post('harimayco/updateitem', array('as' => 'hupdateitem', 'uses'=>'NavigationController@updateitem'));

	});