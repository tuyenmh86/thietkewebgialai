<?php 

Route::group(
	[	
		'prefix' => 'admincp/product',
		'namespace' => 'Admincp\Order',
		'middleware' => ['admin']
	],
	function(){
		// ************************ ROUTE PRODUCTS *****************************
		Route::resource('orders','OrderController',['as' => 'admincp.product']);
	});