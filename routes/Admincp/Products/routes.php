<?php 

Route::group(
	[	'prefix' => 'admincp',
	'namespace' => 'Admincp\Product',
	'middleware' => ['admin']
	],
	function(){
		// ************************ ROUTE TAGS *****************************
		Route::group(['prefix' => 'product'],function(){
			Route::resource('tags','TagController',['as' => 'admincp.product']);

			Route::get('products/importExport',
			[
			'as' => 'admincp.products.importExport',
			'uses' => 'ProductController@importExport'
			]);

			Route::get('products/downloadFile', [
				'as'=>'admincp.product.downloadFile',
				'uses' => 'ProductController@downloadFile'
			]);

			Route::post('products/import',
			[
			'as' => 'admincp.products.import',
			'uses' => 'ProductController@import'
			]);	
			
			// Disable
			Route::post('tags/disable',
				[
				'as' => 'admincp.product.tags.disable',
				'uses' => 'TagController@postDisable'
				])->middleware('permission:product.tags,update');
			// Public
			Route::post('tags/public',
				[
				'as' => 'admincp.product.tags.public',
				'uses' => 'TagController@postPublic'
				])->middleware('permission:product.tags,update');
			// Destroys
			Route::post('tags/destroys',
				[
				'as' => 'admincp.product.tags.destroys',
				'uses' => 'TagController@postDestroys'
				])->middleware('permission:product.tags,destroy');
			// Update Position
			Route::post('tags/pos/update',
				[
				'as' => 'admincp.product.tags.pos.update',
				'uses' => 'TagController@postUpdatePos'
				])->middleware('permission:product.tags,update');

		});	
		// ************************ ROUTE CATEGORY *****************************
		Route::group(['prefix' => 'product'],function(){
			Route::resource('categorys','CategoryController',['as' => 'admincp.product']);

			// Disable
			Route::post('categorys/disable',
				[
				'as' => 'admincp.product.categorys.disable',
				'uses' => 'CategoryController@postDisable'
				])->middleware('permission:product.categorys,update');
			// Public
			Route::post('categorys/public',
				[
				'as' => 'admincp.product.categorys.public',
				'uses' => 'CategoryController@postPublic'
				])->middleware('permission:product.categorys,update');
			// Destroys
			Route::post('categorys/destroys',
				[
				'as' => 'admincp.product.categorys.destroys',
				'uses' => 'CategoryController@postDestroys'
				])->middleware('permission:product.categorys,destroy');

			// Update Position
			Route::post('categorys/pos/update',
				[
				'as' => 'admincp.product.categorys.pos.update',
				'uses' => 'CategoryController@postUpdatePos'
				])->middleware('permission:product.categorys,update');

		});	
		// ************************ ROUTE PRODUCTS *****************************
		Route::resource('products','ProductController',['as' => 'admincp']);
		
		
		// Route for export/download tabledata to .csv, .xls or .xlsx
		// Route::get('downloadExcel/{type}', 'MaatwebsiteController@downloadExcel');
		// Route for import excel data to database.
		// Route::post('importExcel', 'MaatwebsiteController@importExcel');

		Route::get('products/disable',
			[
			'as' => 'admincp.products.disable',
			'uses' => 'ProductController@postDisable'
			])->middleware('permission:products,update');

		// Disable
		Route::post('products/disable',
			[
			'as' => 'admincp.products.disable',
			'uses' => 'ProductController@postDisable'
			])->middleware('permission:products,update');
		// Public
		Route::post('products/public',
			[
			'as' => 'admincp.products.public',
			'uses' => 'ProductController@postPublic'
			])->middleware('permission:products,update');

		Route::post('products/category',[
			'as' => 'admincp.products.category',
			'uses' => 'ProductController@postCategory'
		]);
		
		//  Trash restore
		
		Route::post('products/destroys',
			[
			'as' => 'admincp.products.destroys',
			'uses' => 'ProductController@postDestroys'
			])->middleware('permission:products,destroy');
		
		// Update Position
		Route::post('products/pos/update',
			[
			'as' => 'admincp.products.pos.update',
			'uses' => 'ProductController@postUpdatePos'
			])->middleware('permission:products,update');

		// Load Attribute by Product Type
		Route::post('product/add/load-attribute',
			[
			'as' => 'admincp.product.load-attribute',
			'uses' => 'ProductController@postLoadAttribute'
			])->middleware('permission:products,create');


		// ************************ ROUTE PRODUCT TYPES *****************************
		Route::group(['prefix' => 'product'],function(){
			Route::resource('product-types','ProductTypeController',['as' => 'admincp.product']);
		});

		// ************************ ROUTE PRODUCT ATTRIBUTES *****************************
		Route::group(['prefix' => 'product'],function(){
			Route::resource('product-attributes','ProductAttributeController',['as' => 'admincp.product']);
		});

		// Route for view/blade file.
		
		

	});