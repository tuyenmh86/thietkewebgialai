<?php 

Route::group(
	[	'prefix' => 'admincp',
		'namespace' => 'Admincp\RolePermission',
		'middleware' => ['admin','permission']
	],
	function(){
		// ************************ Route Role *****************************
		Route::resource('role','RoleController',['as' => 'admincp']);

		// Role Disable
		Route::post('role/disable',
			[
				'as' => 'admincp.role.disable',
				'uses' => 'RoleController@postDisable'
			]);
		// Role Public
		Route::post('role/public',
			[
				'as' => 'admincp.role.public',
				'uses' => 'RoleController@postPublic'
			]);

		// Role destroy
		Route::post('role/destroys',
			[
				'as' => 'admincp.role.destroys',
				'uses' => 'RoleController@postDestroy'
			]);

		// Update Position
		Route::post('role/pos/update',
			[
				'as' => 'admincp.role.pos.update',
				'uses' => 'RoleController@postUpdatePos'
			]);


		// ************************ Route Permission *****************************
		Route::resource('permission','PermissionController',['as' => 'admincp']);

		// Role Disable
		Route::post('permission/disable',
			[
				'as' => 'admincp.permission.disable',
				'uses' => 'PermissionController@postDisable'
			]);
		// Role Public
		Route::post('permission/public',
			[
				'as' => 'admincp.permission.public',
				'uses' => 'PermissionController@postPublic'
			]);
		// Trash destroy
		Route::post('permission/destroys',
			[
				'as' => 'admincp.permission.destroys',
				'uses' => 'PermissionController@postDestroy'
			]);

		// Update Position
		Route::post('permission/pos/update',
			[
				'as' => 'admincp.permission.pos.update',
				'uses' => 'PermissionController@postUpdatePos'
			]);
		// Update DisplayName
		Route::post('permission/displayname/update',
			[
				'as' => 'admincp.permission.displayname.update',
				'uses' => 'PermissionController@postUpdateDisplayName'
			]);

		// ************************ Permission Role ****************************
		Route::get('permission-role',
			[
				'as' => 'admincp.permission-role',
				'uses' => 'RoleController@getPermissionRole'
			]);
		// Permission Role
		Route::post('permission-role',
			[
				'as' => 'admincp.permission-role.post',
				'uses' => 'RoleController@postPermissionRole'
			]);
	});