<?php 



Route::group(

	[	'prefix' => 'admincp',

		'namespace' => 'Admincp\Setting',

		'middleware' => ['admin']

	],

	function(){

		// *************************** ADMINS MODULE **********************

		Route::resource('setting','SettingController',['as' => 'admincp']);



});