<?php 
Route::group(['namespace' => 'Admincp','middleware' => 'admin'],function(){
	Route::get('admincp',
	[
		'as' => 'dashboard',
		'uses' => 'AdmincpController@getDashboard'
	]);

	Route::get('admincp/file/manager',
	[
		'as' => 'admincp.filemanager',
		'uses' => 'AdmincpController@getFilemanager'
	]);
	
	// Route::post('back-language-chooser', 'LanguageController@backchangeLanguage');
	// Route::post('back-language',['before' => 'csrf', 'as' => 'back-language-chooser', 'uses' => 'LanguageController@backchangeLanguage']);
	
});



