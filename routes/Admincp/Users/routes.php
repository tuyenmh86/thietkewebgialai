<?php 

Route::group(
	[	'prefix' => 'admincp',
		'namespace' => 'Admincp\User',
		'middleware' => ['admin']
	],
	function(){

		Route::resource('users','UserController',['as' => 'admincp'
		]);
		// Disable
		Route::post('users/disable',
			[
				'as' => 'admincp.users.disable',
				'uses' => 'UserController@postDisable'
			])->middleware('permission:users,update');
		// Public
		Route::post('users/public',
			[
				'as' => 'admincp.users.public',
				'uses' => 'UserController@postPublic'
			])->middleware('permission:users,update');
		Route::post('users/destroys',
			[
				'as' => 'admincp.users.destroys',
				'uses' => 'UserController@postDestroys'
			])->middleware('permission:users,destroy');
		// ************************** LOGS USER *********************
		Route::group(['prefix' => 'user'],function(){
			// Route::resource('logs','LogController',['as' => 'admincp.user']);
			// Destroy List
			// Route::post('logs/destroys',
			// 	[
			// 		'as' => 'admincp.user.log.destroys',
			// 		'uses' => 'LogController@postToDestroy'
			// 	])->middleware('permission:user.logs,destroy');
		});
});