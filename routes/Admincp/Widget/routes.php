<?php 



Route::group(

	[	'prefix' => 'admincp',

		'namespace' => 'Admincp\Widget',

		'middleware' => ['admin']

	],

	function(){

		Route::resource('widgets','WidgetController',['as' => 'admincp']);



		// Disable

		Route::post('widgets/disable',

			[

				'as' => 'admincp.widgets.disable',

				'uses' => 'WidgetController@postDisable'

			])->middleware('permission:widgets,update');

		// Public

		Route::post('widgets/public',

			[

				'as' => 'admincp.widgets.public',

				'uses' => 'WidgetController@postPublic'

			])->middleware('permission:widgets,update');

		//  Trash restore

		Route::post('widgets/destroys',

			[

				'as' => 'admincp.widgets.destroys',

				'uses' => 'WidgetController@postDestroys'

			])->middleware('permission:widgets,destroy');

		

		// Update Position

		Route::post('widgets/pos/update',

			[

				'as' => 'admincp.widgets.pos.update',

				'uses' => 'WidgetController@postUpdatePos'

			])->middleware('permission:widgets,update');



	});