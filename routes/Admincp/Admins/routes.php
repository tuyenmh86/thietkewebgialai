<?php 

Route::group(
	[	'prefix' => 'admincp',
		'namespace' => 'Admincp\Admin',
		'middleware' => ['admin']
	],
	function(){
		Route::resource('admins','AdminController',['as' => 'admincp']);

		// Disable
		Route::post('admins/disable',
			[
				'as' => 'admincp.admins.disable',
				'uses' => 'AdminController@postDisable'
			])->middleware('permission:admins,update');
		// Public
		Route::post('admins/public',
			[
				'as' => 'admincp.admins.public',
				'uses' => 'AdminController@postPublic'
			])->middleware('permission:admins,update');
		// Destroys
		Route::post('admins/destroys',
			[
				'as' => 'admincp.admins.destroys',
				'uses' => 'AdminController@postDestroys'
			])->middleware('permission:admins,destroy');
		// ************************** PROFILE *********************
		Route::resource('profile','ProfileController',['as' => 'admincp']);
});
		