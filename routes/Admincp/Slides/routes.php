<?php 



Route::group(

	[	'prefix' => 'admincp',

		'namespace' => 'Admincp\Slide',

		'middleware' => ['admin']

	],

	function(){

		Route::resource('slides','SlideController',['as' => 'admincp']);

		// Disable

		Route::post('slides/disable',

			[

			'as' => 'admincp.slides.disable',

			'uses' => 'SlideController@postDisable'

			])->middleware('permission:slides,update');

		// Public

		Route::post('slides/public',

			[

			'as' => 'admincp.slides.public',

			'uses' => 'SlideController@postPublic'

			])->middleware('permission:slides,update');

		// Destroys

		Route::post('slides/destroys',

			[

			'as' => 'admincp.slides.destroys',

			'uses' => 'SlideController@postDestroys'

			])->middleware('permission:slides,destroy');



});