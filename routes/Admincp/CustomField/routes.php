<?php 



Route::group(

	[	'prefix' => 'admincp',

		'namespace' => 'Admincp\CustomField',

		'middleware' => ['admin'],

        'as' => 'admincp.'

	],

	function(){

		Route::resource('customfields','CustomFieldController');

	});