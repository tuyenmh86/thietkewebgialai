<?php 

Route::group(
	[	'prefix' => 'admincp',
		'namespace' => 'Admincp\Post',
		'middleware' => ['admin']
	],
	function(){
		// ************************ ROUTE TAGS *****************************
		Route::group(['prefix' => 'post'],function(){
			Route::resource('tags','TagController',['as' => 'admincp.post']);

			// Disable
			Route::post('tags/disable',
				[
					'as' => 'admincp.post.tags.disable',
					'uses' => 'TagController@postDisable'
				])->middleware('permission:post.tags,update');
			// Public
			Route::post('tags/public',
				[
					'as' => 'admincp.post.tags.public',
					'uses' => 'TagController@postPublic'
				])->middleware('permission:post.tags,update');
			// Destroys
			Route::post('tags/destroys',
				[
					'as' => 'admincp.post.tags.destroys',
					'uses' => 'TagController@postDestroys'
				])->middleware('permission:post.tags,destroy');
			// Update Position
			Route::post('tags/pos/update',
				[
					'as' => 'admincp.post.tags.pos.update',
					'uses' => 'TagController@postUpdatePos'
				])->middleware('permission:post.tags,update');

		});	
		// ************************ ROUTE CATEGORY *****************************
		Route::group(['prefix' => 'post'],function(){
			Route::resource('categorys','CategoryController',['as' => 'admincp.post']);

			// Disable
			Route::post('categorys/disable',
				[
					'as' => 'admincp.post.categorys.disable',
					'uses' => 'CategoryController@postDisable'
				])->middleware('permission:post.categorys,update');
			// Public
			Route::post('categorys/public',
				[
					'as' => 'admincp.post.categorys.public',
					'uses' => 'CategoryController@postPublic'
				])->middleware('permission:post.categorys,update');
			// Destroys
			Route::post('categorys/destroys',
				[
					'as' => 'admincp.post.categorys.destroys',
					'uses' => 'CategoryController@postDestroys'
				])->middleware('permission:post.categorys,destroy');

			// Update Position
			Route::post('categorys/pos/update',
				[
					'as' => 'admincp.post.categorys.pos.update',
					'uses' => 'CategoryController@postUpdatePos'
				])->middleware('permission:post.categorys,update');

		});	
		// ************************ ROUTE POSTS *****************************
		Route::resource('posts','PostController',['as' => 'admincp']);

		// Disable
		Route::post('posts/disable',
			[
				'as' => 'admincp.posts.disable',
				'uses' => 'PostController@postDisable'
			])->middleware('permission:posts,update');
		// Public
		Route::post('posts/public',
			[
				'as' => 'admincp.posts.public',
				'uses' => 'PostController@postPublic'
			])->middleware('permission:posts,update');
		//  Trash restore
		Route::post('posts/destroys',
			[
				'as' => 'admincp.posts.destroys',
				'uses' => 'PostController@postDestroys'
			])->middleware('permission:posts,destroy');
		
		// Update Position
		Route::post('posts/pos/update',
			[
				'as' => 'admincp.posts.pos.update',
				'uses' => 'PostController@postUpdatePos'
			])->middleware('permission:posts,update');

		// Import RSS

		Route::get('posts/import/rss',
			[
				'as' => 'admincp.posts.importRss',
				'uses' => 'PostController@importRss'
			]);

		Route::post('posts/import/rss',
			[
				'as' => 'admincp.posts.importRss',
				'uses' => 'PostController@postImportRss'
			]);

	});