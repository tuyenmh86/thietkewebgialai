<?php 
app()->setLocale(session()->get('locale'));
Route::group(['before' => 'web','middleware' => ['widget','web']],function(){
	
	Route::get('/',
	[
		'as' => 'index',
		'uses' => 'SiteController@index'
	]);
	// submit contact
	Route::post('contact',['as' => 'postContact','uses' => 'SiteController@submitContact']);

	Route::get(trans('route.pages').'/{alias}',['as' => 'pages','uses' => 'SiteController@getViaPage']);

	Route::get(trans('route.posts').'/{alias}',['as' => 'posts.category','uses' => 'SiteController@getViaPostCate']);

	Route::get(trans('route.posts').'/{aliasCate}/{alias}',['as' => 'posts.post','uses' => 'SiteController@getViaPost']);

	Route::get(trans('route.products').'/{alias}',['as' => 'products.category','uses' => 'SiteController@getViaProductCate']);

	Route::get(trans('route.products').'/{aliasCate}/{alias}',['as' => 'products.post','uses' => 'SiteController@getViaProduct']);

	Route::post('contact',['as' => 'contact.send','uses' => 'SiteController@postContact']);
	
	Route::post('contact_tomail',['as' => 'contact.mail','uses' => 'SiteController@postContactMail']);

	Route::get(trans('route.agent'),['as' => 'agents','uses' => 'SiteController@getAgents']);

	Route::get(trans('route.agent').'/{id}-{alias}',['as' => 'agents.detail','uses' => 'SiteController@getViaAgent']);

	Route::get(trans('route.investors').'/{id}-{alias}',['as' => 'investor.detail','uses' => 'SiteController@getViaInvestor']);
	
	Route::post(trans('route.searchs').'/'.trans('route.products'),['as' => 'searchs','uses' => 'SiteController@searchs']);

	Route::get(trans('route.searchs').'/'.trans('route.posts'),['as' => 'get.searchs','uses' => 'SiteController@searchs']);

	Route::get(trans('route.contact'),['as' => 'contact','uses' => 'SiteController@getContact']);

	Route::post('product/add-to-cart',
		[
			'as' => 'product.addToCart',
			'uses' => 'SiteController@postAddToCart'
		]
	);

	Route::get('gio-hang',
		[
			'as' => 'product.cart',
			'uses' => 'SiteController@getCartPage'
		]
	);

	Route::post('product/cart/remove/item',
		[
			'as' => 'product.cart.removeItem',
			'uses' => 'SiteController@postRemoveItemCart'
		]
	);

	Route::post('product/cart/destroy',
		[
			'as' => 'product.cart.destroy',
			'uses' => 'SiteController@postCartDestroy'
		]
	);

	Route::post('product/cart/update/qty',
		[
			'as' => 'product.cart.update.qty',
			'uses' => 'SiteController@postUPdateQuantityCart'
		]
	);


	Route::get('thanh-toan',
		[
			'as' => 'product.checkout',
			'uses' => 'SiteController@getCheckoutPage'
		]
	);

	Route::post('product/checkout/finished',
		[
			'as' => 'finished.checkout',
			'uses' => 'SiteController@postCheckOutFinished'
		]
	);

	Route::get('sitemap.xml', [
		'as' =>'sitemap',
		'uses'=>'SiteController@getSitemap'
	]);
});
