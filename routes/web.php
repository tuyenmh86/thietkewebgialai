<?php
// Redirect to 404 page
Route::name('errors.404')->get('admincp/errors/404',function(){
	return view('404');
});

Route::get('/laravel-filemanager', '\Unisharp\Laravelfilemanager\controllers\LfmController@show');
Route::post('/laravel-filemanager/upload', '\Unisharp\Laravelfilemanager\controllers\UploadController@upload');

