<?php
 return [

     /*
     |
     | Khai bao cac bien da ngon ngu su dung trong back end. Ngon ngu: tieng viet
     |
     */

     'dashboard' => 'Bảng điều khiển',
     'new' => 'Mới',
     'usermanager' => 'Nhóm người dùng',
     'admin_group' => 'Nhóm quản trị',
     'user_group' => 'Nhóm người dùng',
     'role_permission' => 'Phân quyền người dùng',
     'file_manager' => 'Quản lý files',
     'roles' => 'Nhóm quyền',
     'permissions' => 'Quyền hạn',
     'setting' => 'Cài đặt',
     'media' => 'Đa phương tiện',
     'menu' => 'Menu',
     'pages' => 'Trang nội dung',
     'posts' => 'Tin tức',
     'categories' => 'Danh mục',
     'tags' => 'Thẻ',
     'products' => 'Sản phẩm',
     'contact' => 'Liên hệ',
     'system' => 'Hệ thống',
     'add' => 'Thêm mới',
     'edit' => 'Chỉnh sửa',
     'cancel' => 'Thoát',
     'delete' => 'Xóa',
     'list' => 'Danh sách',
     'seo_advanced' => 'Seo nâng cao',
     'basic_information' => 'Thông tin cơ bản',
     'featured' => 'Nổi bật',
     'public' => 'Hiển thị',
     'disable' => 'Nháp',
     'name' => 'Tên',
     'email' => 'Email',
     'address' => 'Địa chỉ',
     'alias' => 'Đường dẫn',
     'title' => 'Tiêu đề',
     'content' => 'Nội dung',
     'desc' => 'Mô tả',
     'save' => 'Lưu',
     'pos' => 'Vị trí',
     'keyword' => 'Từ khóa',
     'phone' => 'Số điện thoại',
     'add_cate' => 'Thêm mới danh mục',
     'display_name' => 'Định danh',
     'password' => 'Mật khẩu',
     'created' => 'Ngày tạo',
     'updated' => 'Ngày chỉnh sửa',
     'action' => 'Hành động',
     'featured_image' => 'Hình ảnh hiển thị',
     'profile' => 'Thông tin cá nhân',
     'logout' => 'Thoát',
     'status' => 'Trạng thái',
     'view' => 'Lượt xem',
     'reset' => 'Nhập lại',
     'view' => 'Lượt xem',
     'custom_link' => 'Chỉnh sửa đường dẫn',
     'fullname' => 'Tên đầy đủ',
     'old_password' => 'Mật khẩu cũ',
     're_password' => 'Nhập lại mật khẩu',
     'cate_parent' => 'Danh mục cha',
     'username' => 'Tên đăng nhập',
     'introduce' => 'Giới thiệu',
     'option' => 'Tùy chọn',
     'avatar' => 'Ảnh đại diện',
     'choose' => 'Chọn',
     'caption'=>'Tiêu đề ảnh',
     'imgContent'=>'Nội dung ảnh',
     'link'=>'Đường dẫn đích',
     'information' => 'Thông tin cơ bản',
     'submit' => 'Thực thi',
     'name' => 'Tên',
     'null' => 'Rỗng',
     'manager' => 'Quản lý',
     'message' => 'Lời nhắn',
     'login' => 'Đăng nhập',
     'add_success' => 'Thêm mới dữ liệu thành công',
     'update_success' => 'Cập nhập dữ liệu thành công',
     'delete_success' => 'Xóa dữ liệu thành công',
     'web_master' => 'Seo nâng cao',
     'image_list' => 'Hình ảnh chi tiết',
     'data_not_found' => 'Dữ liệu không đúng.Vui lòng thử lại',

     'add_fail' => 'Thêm mới dữ liệu thất bại.Vui lòng thử lại',
     'update_fail' => 'Cập nhập dữ liệu thất bại.Vui lòng thử lại',
     'delete_fail' => 'Xóa dữ liệu thất bại.Vui lòng thử lại',

     'admincp' => 'Trang quản trị',

     'attribute' => 'Thuộc tính',
     'slides' => 'Slide',

     'field' => 'Trường',
     'type' => 'Loại',
     'customfield' => 'Trường tùy biến',
     'description' => 'Mô tả',
 

 ];