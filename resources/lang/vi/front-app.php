<?php

 return [



    'address' => 'Địa chỉ',

    'links' => 'Liên kết',

    'newsletter' => 'Nhận tin mới',

    'enter_email' => 'Nhập E-Mail',

    'home' => 'Trang chủ',

    'readmore' => 'Xem thêm',

    'symbol' => '₫',

    'details' => 'Chi tiết',

    'location' => 'Vị trí',

    'contact_agent' => 'Liên hệ chủ đầu tư',

    'day' => 'Ngày',

    'month' => 'Tháng',

    'year' => 'Năm',

    'related_product' => 'Dự án liên quan',

    'real_astate_agent' => 'Chủ đầu tư',

    'send_message' => 'Gửi nội dung',

    'name' => 'Tên',

    'email' => 'E-Mail',

    'message' => 'Nội dung',

    'phone' => 'Số điện thoại',

    'agent_contact' => 'Liên hệ chủ đầu tư',

    'projects' => 'Dự án',

    'featured' => 'Nổi bật',

    'popular' => 'Phổ biến',

    'featured_post' => 'Tin tức nổi bật',

    'featured_product' => 'Sản phẩm nổi bật',

    'featured_projects' => 'Dự án nổi bật',

    'featured_project' => 'Dự án tiêu biểu',

    'select_top_project' => 'Lựa chọn những dự án hàng đầu',

    'contact' => 'Liên hệ',

    'featured_agent' => 'Chủ đầu tư nổi bật',

    'clients_say' => 'Nhận xét từ khách hàng',

    'sale_rent_projects' => 'Dự án bán và cho thuê',

    'recent_projects' => 'Dự án tiêu biểu',

    'categories' => 'Dịch Vụ Quảng Cáo',

    'manufacturer' => 'TRANH DÁN TƯỜNG ĐẠI LONG',

    'product-type' => 'Loại máy lọc nước',

    'made-in' => 'Nước sản xuất',

    'online_support' => 'Hỗ Trợ Trực Tuyến',

    'viewed_products' => 'Sản phẩm đã xem',

    'access_count' => 'Thống Kê Truy Cập',

    'project_type' => 'Loại',

    'status' => 'Trạng thái',

    'price' => 'Trị giá',

    'area' => 'Diện tích',

    'search' => 'Tìm kiếm',

    'no_results' => 'Không tìm thấy kết quả tìm kiếm.',

    'search_properties' => 'Tìm kiếm dự án',

    'keyword' => 'Từ khóa',

    'enter_keyword' => 'Nhập từ khóa',

    'enter_location' => 'Nhập vị trí',

    'bedroom' => 'Phòng ngủ',

    'bathroom' => 'Phòng tắm',

    'all' => 'Tất cả',

    'agent' => 'nha-moi-gioi',

    'send_success' => 'Gửi thành công',



    'send_us' => 'Gửi yêu cầu cho chúng tôi',

    'your_name' => 'Tên của bạn',

    'your_email' => 'E-Mail của bạn',

    'your_phone' => 'Số điện thoại của bạn',

    'your_message' => 'Nội dung',

    'send' => 'Gửi',

    'post_by' => 'Đăng bởi',

    'updating' => 'Đang cập nhập ...',

    'cart' => 'Giỏ hàng',

    'checkout'=> 'Thông tin thanh toán',
    
    'product-category'=>'Danh mục sản phẩm'
 ]

?>