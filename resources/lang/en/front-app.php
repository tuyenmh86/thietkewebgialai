<?php

 return [



    'address' => 'Address',

    'links' => 'Links',

    'newsletter' => 'Newsletter',

    'enter_email' => 'Enter E-Mail',

    'home' => 'Home',

    'readmore' => 'Read more',

    'details' => 'Details',

    'location' => 'Location',

    'contact_agent' => 'Contact Agent',

    'day' => 'Day',

    'month' => 'Month',

    'year' => 'Year',

    'symbol' => '$',

    'related_product' => 'Related Products',

    'real_astate_agent' => 'Real astate agent',

    'send_message' => 'Send messgae',

    'name' => 'Name',

    'email' => 'E-Mail',

    'message' => 'Message',

    'phone' => 'Phone',

    'agent_contact' => 'Agent contact',

    'projects' => 'Projects',

    'featured' => 'Featured',

    'popular' => 'Popular',

    'featured_post' => 'Featured Post',

    'featured_product' => 'Featured Product',

    'featured_projects' => 'Featured Projects',

    'featured_project' => 'Featured Project',

    'select_top_project' => 'Select a projects top',

    'contact' => 'Contact',

    'featured_agent' => 'Featured Agent',

    'clients_say' => 'What clients say',

    'sale_rent_projects' => 'Properties for sale and rent',

    'recent_projects' => 'Recent Projects',

    'categories' => 'Categories',

    'project_type' => 'Type',

    'status' => 'Status',

    'price' => 'Price',

    'area' => 'Area',

    'search' => 'Search',

    'no_results' => 'No results.',

    'search_properties' => 'Search for properties',

    'keyword' => 'Keyword',

    'enter_keyword' => 'Enter keyword',

    'enter_location' => 'Enter location',

    'bedroom' => 'Bedroom',

    'bathroom' => 'Bathroom',

    'all' => 'All',

    'agent' => 'nha-moi-gioi',



    'send_us' => 'Send us message',

    'your_name' => 'Your name',

    'your_email' => 'Your E-Mail',

    'your_phone' => 'Your Phone',

    'your_message' => 'Your Message',

    'send' => 'Send',

    'send_success' => 'Send successfully',

    'post_by' => 'Post By',

    'updating' => 'Updating ...',

 ]

?>