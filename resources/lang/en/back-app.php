<?php
 return [

     /*
     |
     | Khai bao cac bien da ngon ngu su dung trong back end. Ngon ngu: tieng anh
     |
     */

    'engLang' => 'English',

    'viLang' => 'Vietnamese',

    // nav language

    'Profile' => 'Profile',

    'Setting' => 'Settings',

    'Logout' => 'Logout',

    //sidebar
    
    'Dash' => 'Dashboard',

    'UsersManager' => 'Users Manager',

    'AdminGroup' => 'Admin Group',

    'Role-Permission' => 'Role - Permission',

    'Posts' => 'Post',

    'Categorys' => 'Categorys',

    'Products' => 'Products',

    'ProductCategorys' => 'Categorys',

    'Orders' => 'Orders',

    'Customer' => 'Customer',

    'System' => 'System',

    'LogsAdmin' => 'Logs Admin',

    'LogsCustomer' => 'Logs User',

    'LogsViewer' => 'Logs Viewer',

 ];