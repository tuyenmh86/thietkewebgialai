@extends('usercpanel.layouts.main')
@section('title',trans('back-app.profile'))
@section('content')
<div class="container">
	<form action="{{route('user.profile.update',$profile->id)}}" method="post" class="form-horizontal " enctype="multipart/form-data" name="myform">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="put">
		{{-- Role hidden form if account login editing --}}
		<input type="hidden" name="role_id" value="{{$profile->role_id}}">
		<!-- genarator password leng -->
		<input type="hidden" name="length" value="15">
		{{-- Option --}}
		<div class="col-md-12 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						{{-- Save --}}
						<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{ trans('back-app.save') }}</button>
						{{-- Reset --}}
						<button type="reset" class="btn btn-sm btn-info"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.reset') }}</button>
						<a href="{{route('user.dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 1 --}}
		<div class="col-md-8 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-info-circle" aria-hidden="true"></i><strong>{{ trans('back-app.information') }}</strong>
					</div>
					<div class="card-block">
						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="full_name">{{ trans('back-app.fullname') }}(<span class="required_cl">*</span>)</label>
							<div class="col-md-9">
								<input type="text" id="full_name" name="full_name" class="form-control {{ ($errors->has('full_name') ? 'has-error' : '')}}" value="{{$profile->full_name}}" placeholder="Administrator">
								<span class="help-block mess-err">{{$errors->first('full_name')}}</span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="email">{{ trans('back-app.email') }}(<span class="required_cl">*</span>)</label>
							<div class="col-md-9">
								<input type="text" id="email" name="email" class="form-control {{ ($errors->has('email') ? 'has-error' : '')}}" value="{{$profile->email}}" placeholder="administrator@gmail.com">
								<span class="help-block mess-err">{{$errors->first('email')}}</span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="phone">{{ trans('back-app.phone') }}</label>
							<div class="col-md-9">
								<input type="number" id="phone" name="phone" class="form-control {{ ($errors->has('phone') ? 'has-error' : '')}}" value="{{$profile->phone}}" placeholder="088336633">
								<span class="help-block mess-err">{{$errors->first('phone')}}</span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="address">{{ trans('back-app.address') }}</label>
							<div class="col-md-9">
								<input type="text" id="address" name="address" class="form-control {{ ($errors->has('address') ? 'has-error' : '')}}" value="{{$profile->address}}" placeholder="HCM.C">
								<span class="help-block mess-err">{{$errors->first('address')}}</span>
							</div>
						</div>
					</div>
				</div>
				<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-info-circle" aria-hidden="true"></i><strong>{{ trans('back-app.introduce') }}</strong>
					</div>
					@if(count(config('cms.locale')) > 1)
						<ul class="nav nav-tabs" role="tablist">
							@foreach(config('cms.locale') as $key => $item)
								<li class="nav-item">
									<a class="nav-link {{ $key == 0 ? 'active' : '' }}" data-toggle="tab" href="#language_{{$item['code']}}" role="tab" aria-controls="home"><img src="{{$item['flag']}}"><span style="text-transform: uppercase;"> {{ $item['code'] }} </a>
								</li>
							@endforeach
						</ul>
					@endif
					<div class="tab-content">
						@foreach(config('cms.locale') as $key => $item)
							<div class="row tab-pane {{ $key == 0 ? 'active' : '' }}" id="language_{{$item['code']}}" role="tabpanel">
								<div class="col-md-12">
									<div class="card-block">
										<div class="form-group row">
											<label class="col-md-3 form-control-label" for="language[{{$item['code']}}][introduce]">{{ trans('back-app.introduce') }}</label>
											<div class="col-md-9">
												<textarea type="text" id="language[{{$item['code']}}][introduce]" name="language[{{$item['code']}}][introduce]" class="myEditor form-control" value="" placeholder="">{{ old("language[".$item['code']."][introduce]",@$profile->language[$item['code']]['introduce'])}}</textarea>
											</div>
										</div>
									</div>
								</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
				{{-- Password --}}
				<div class="animated fadeIn">
					<div class="card">
						<div class="card-header">
							<i class="fa fa-info-circle" aria-hidden="true"></i><strong>{{ trans('back-app.password') }}</strong>
						</div><br>
						<div class="col-md-12">
							<div id="btn-change-pw" class="form-group row">
								<div class="col-md-12">
									<label class="button btn-danger">Nhập mật khẩu nếu bạn muốn thay đổi.</label>
								</div>
							</div>
							<div id="box-pw-info">
								<div class="form-group row">
									<label class="col-md-3 form-control-label" for="password">{{ trans('back-app.old_password') }}</label>
									<div class="col-md-9">
										<input type="password" id="old_password" name="old_password" class="form-control {{ ($errors->has('old_password') ? 'has-error' : '')}}" placeholder="**************" value="" 	>
										<span class="help-block mess-err">{{$errors->first('old_password')}}</span>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-md-3 form-control-label" for="password">{{ trans('back-app.password') }}</label>
									<div class="col-md-9">
										<input type="password" id="row_password" name="password" class="form-control {{ ($errors->has('password') ? 'has-error' : '')}}" placeholder="**************" value="" 	>
										<span class="help-block mess-err">{{$errors->first('password')}}</span>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-md-3 form-control-label" for="re_password">{{ trans('back-app.re_password') }}</label>
									<div class="col-md-9">
										<input type="password" id="re_password" name="re_password" class="form-control {{ ($errors->has('re_password') ? 'has-error' : '')}}" value="" placeholder="**************">
										<span class="help-block mess-err">{{$errors->first('re_password')}}</span>
									</div>
								</div>
								<div class="form-group row">
									<label class="control-label col-md-3"></label>
									<div class="col-md-9">
										<input type="button" id="genarator_pw" class="btn btn-default" value="Generate" onClick="generate();" tabindex="2">
										<input type="button" id="show_pw" class="btn btn-default" value="Show" >
									</div> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 2 --}}
		<div class="col-md-4 pull-right">
			{{-- Bolock 2 | Bock 2 --}}
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-picture-o" aria-hidden="true"></i><strong>{{ trans('back-app.avatar') }}</strong>
					</div>
					<div class="card-block">
						<div class="form-group row">
							<div class="col-md-12 input-group">
								 <div class="input-group">
								   <span class="input-group-btn">
								     <a id="" data-input="thumbnail" data-preview="holder" class="lfm btn btn-primary">
								       <i class="fa fa-picture-o"></i> {{ trans('back-app.choose') }}
								     </a>
								   </span>
								   <input id="thumbnail" class="form-control" type="text" value="{{ $profile->profile_image }}" name="profile_image">
								 </div>
							</div>
							<div class="col-md-12 input-group">
								<img id="holder" height="100%" width="100%" src="{{ $profile->profile_image }}" style="margin-top:15px;"> 
							</div>
						</div>
					</div>
				</div>
			</div>
			{{-- End Block 2 | Bock 2 --}}
		</div>
	</form>
</div>
@endsection

@section('script')
<script type="text/javascript">
	// Show Hide Password
	$('#genarator_pw').click(function(){
		var row_password = $('#row_password').val();
		$('#re_password').attr('value',row_password);
	});
	$("#show_pw").click(function() {
		if ($("#row_password").attr("type") == "password") {
			$("#row_password").attr("type", "text");
			$("#re_password").attr("type", "text");
			$("#show_pw").attr('value','Hide');

		} else {
			$("#row_password").attr("type", "password");
			$("#re_password").attr("type", "password");
			$("#show_pw").attr('value','Show');
		}
	});
</script>
@endsection
