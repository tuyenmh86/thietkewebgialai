@extends('usercpanel.layouts.main')
@section('title','Profile')
@section('content')
<div class="container-fluid">
	<div class="col-md-12 pull-left">
		<div class="animated fadeIn">
			<div class="card">
				<div class="card-header">
					{{-- Add new --}}
					{{-- @if(Auth::guard('admin')->user()->cans('admincp.profile.update')) --}}
					<a href="{{route('user.profile.edit',$profile->id)}}"><button type="button" class="btn btn-sm btn-info"><i class="fa fa-edit "></i> Edit</button></a>
					{{-- @endif --}}
					{{-- Cancel --}}
					<a href="{{route('user.dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> Cancel</button></a>

				</div>
			</div>
		</div>
	</div>

	{{-- Profile --}}
	<div class="col-md-6 pull-left">
		<div class="animated fadeIn">
			<div class="card">
				<img width="100%" src="{{$profile->profile_image}}">
			</div>
		</div>
	</div>
	<div class="col-md-6 pull-left">
		<div class="animated fadeIn">
			<div class="card"><br>
				<div class="form-group row">
					<label class="col-md-2 badge badge-info">
						<strong>Full Name</strong>
					</label>
					<p class="col-md-10">{{$profile->full_name}}</p>
				</div>
				<div class="form-group row">
					<label class="col-md-2 badge badge-info">
						<strong>Email</strong>
					</label>
					<p class="col-md-10">{{$profile->email}}</p>
				</div>
				<div class="form-group row">
					<label class="col-md-2 badge badge-info">
						<strong>Phone</strong>
					</label>
					<p class="col-md-10">{{$profile->phone}}</p>
				</div>
				<div class="form-group row">
					<label class="col-md-2 badge badge-info">
						<strong>Address</strong>
					</label>
					<p class="col-md-10">{{$profile->address}}</p>
				</div>
				{{-- <div class="form-group row">
					<label class="col-md-2 badge badge-info">
						<strong>Role</strong>
					</label>
					<p class="col-md-10"><span class="badge badge-danger">{{$profile->roles->name}}</span></p>
				</div> --}}
				<div class="form-group row">
					<label class="col-md-2 badge badge-info">
						<strong>Status</strong>
					</label>
					<p class="col-md-10">
						@if($profile->active == 1)
						<span class="badge badge-success">Active</span>
						@else
						<span class="badge badge-danger">Disable</span>
						@endif
					</p>
				</div>
				<div class="form-group row">
					<label class="col-md-2 badge badge-info">
						<strong>Created</strong>
					</label>
					<p class="col-md-10">{{$profile->created_at}}</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection