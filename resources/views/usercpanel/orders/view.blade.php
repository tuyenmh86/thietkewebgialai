@extends('usercpanel.layouts.main')
@section('title','Order Details')
@section('content')

<div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        Invoice
                        <strong>#{{$order->id}}</strong>
                        <a href="#" class="btn btn-sm btn-info float-right" onclick="javascript:window.print();"><i class="fa fa-print"></i> Print</a>
                        <a href="#" class="btn btn-sm btn-info float-right"><i class="fa fa-save"></i> Save</a>
                    </div>
                    <div class="card-block">
                        <div class="row mb-2">

                            <div class="col-sm-4">
                                
                                <div>
                                    <strong>Billing Details</strong>
                                </div>
                                <div>Date create: {{date_format($order->created_at, 'l jS F Y')}}</div>
                            </div>
                            <!--/.col-->

                            <div class="col-sm-4">
                                <div>
                                    <strong>Customer Information</strong>
                                </div>
                                <div>Full Name: {{($order->user) ? $order->user->full_name : 'Null'}}</div>
                                <div>Email: {{($order->user) ? $order->user->email : 'Null'}}</div>
                            </div>
                            <!--/.col-->

                            <div class="col-sm-4">
                                <h6 class="mb-1"></h6>
                                <div>Tel: {{($order->user) ? $order->user->phone : 'Null'}}</div>
                                <div>Address: {{($order->user) ? $order->user->address : 'Null'}}</div>
                            </div>
                            <!--/.col-->
                        </div>
                        <!--/.row-->

                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="center">#</th>
                                        <th>Item</th>
                                        <th>Description</th>
                                        <th class="center">Price</th>
                                        <th class="right">Quantity</th>
                                        <th class="right">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($order->order_detail)
                                    @php
                                    $i = 1;
                                    @endphp
                                    @foreach($order->order_detail as $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{($value->product) ? substr($value->product->name, 0,30)."..." : 'Null'}}</td>
                                        <td>{{($value->product) ? substr($value->product->description, 0,30)."..." : 'Null'}}</td>
                                        <td class="text-left">
                                            {{($proSetting->currency == 2) ? '$' : ''}}{{number_format($value->price)}}{{($proSetting->currency == 1) ? '₫' : ''}}</td>
                                        <td class="text-left">{{($value->quanlity) ? $value->quanlity : 'Null'}}</td>
                                        <td class="text-left">{{($proSetting->currency == 2) ? '$' : ''}}{{number_format($value->total)}}{{($proSetting->currency == 1) ? '₫' : ''}}</td>
                                    </tr>
                                    @php
                                    $i++;
                                    @endphp
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div><hr>
    
                        <div class="row">
                            <div class="col-lg-4 offset-lg-8 col-sm-5 offset-sm-2">
                                <table class="table table-clear">
                                    <tbody>
                                        <tr>
                                            <td class="left">
                                                <strong>Subtotal</strong>
                                            </td>
                                            <td class="right">{{($proSetting->currency == 2) ? '$' : ''}}{{number_format($order->total)}}{{($proSetting->currency == 1) ? '₫' : ''}}</td>
                                        </tr>
                                        <tr>
                                            <td class="left">
                                                <strong>VAT (10%)</strong>
                                            </td>
                                            <td class="right">{{($proSetting->currency == 2) ? '$' : ''}}{{number_format(($order->total/100)*10)}}{{($proSetting->currency == 1) ? '₫' : ''}}</td>
                                        </tr>
                                        <tr>
                                            <td class="left">
                                                <strong>Total</strong>
                                            </td>
                                            <td class="right">
                                                <strong>{{($proSetting->currency == 2) ? '$' : ''}}{{number_format($order->total + ($order->total/100)*10)}}{{($proSetting->currency == 1) ? '₫' : ''}}</strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                {{-- <a href="#" class="btn btn-success"><i class="fa fa-usd"></i> Proceed to Payment</a> --}}
                            </div>
                        </div>
                            <!--/.col-->
                        <!--/.row-->
                    </div>
                </div>

            </div>
@endsection
