@extends('usercpanel.layouts.main')
@section('title',trans('back-app.products'))
@section('content')
<div class="container-fluid">
    {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">

    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                <a href="{{route('user.products.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
                {{-- Cancel --}}
                <a href="{{route('user.dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
                {{-- ************** Option Action ***************************** --}}
                <div class="form-group row">
                    <div class="col-md-2 ">
                        <select id="action_table" name="action_table" class=" form-control pull-left" size="1">
                            <option value="">-- {{ trans('back-app.action') }} --</option>
                            <option value="{{ route('user.products.destroys')}}">{{ trans('back-app.delete') }}</option>
                        </select>
                    </div>
                    <button type="button" onclick="return update_records();" id="submit_action" class="btn btn-sm btn-info pull-left"><i class="fa fa-location-arrow" aria-hidden="true"></i> {{ trans('back-app.submit') }}</button>&nbsp;          
                </div>
                {{-- ************** End Option Action ***************************** --}}
                <table class="table table-striped table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            <th><input type="checkbox" onchange="return check_lists();" id="checkAll" name="checkAll"/></th>
                            <th>Id</th>
                            <th>{{ trans('back-app.title') }}</th>
                            <th>{{ trans('back-app.created') }}</th>
                            <th>{{ trans('back-app.pos') }}</th>
                            <th>{{ trans('back-app.status') }}</th>
                            <th>{{ trans('back-app.action') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($products))
                        @foreach($products as $product)
                        <tr>
                            <td><input type="checkbox" id="checkSingle" name="checkSingle" value="{{$product->id}}" /></td>
                            <td>{{$product->id}}</td>
                            <td><span class="badge badge-success">{{ str_limit($product->title,25) }} </span></td>
                            <td>{{Carbon\Carbon::parse($product->created_at)->format('d-m-Y')}}</td>
                            <td><input width="20px" type="number" name="pos" class="form-control updatePos"  onkeyup="return update_posision('{!! route('user.products.pos.update') !!}',{!! $product->id; !!},this.value);" id="{{$product->id}}" value="{{$product->pos}}"></td>
                            <td>
                                @if($product->is_published())
                                    <span class="badge badge-success"><i class="fa fa-check-circle" aria-hidden="true"></i> Đã duyệt</span>
                                @else
                                    <span class="badge badge-warning"><i class="fa fa-spinner" aria-hidden="true"></i> Đang chờ ...</span>
                                @endif
                            </td>
                            <td>
                                {{-- Edit --}}
                                <a class="btn btn-info pull-left" href="{{route('user.products.edit',$product->id)}}">
                                    <i class="fa fa-edit "></i></a>
                                    {{-- Delete --}}
                                    <form action="{{route('user.products.destroy',$product->id)}}" method="post" class="pull-left">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                        <input type="hidden" name="_method" value="DELETE"> 
                                        <a class="btn btn-danger" href="#" onclick="$(this).closest('form').submit()">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
