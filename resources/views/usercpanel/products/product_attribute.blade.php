<div class="tab-pane" id="home2" role="tabpanel">
<div class="col-md-12 pull-left">
<div class="animated fadeIn">

{{-- ************ BEGIN:: PROPERTY | BLOCK 1 ***********--}}
<div class="card">
<div class="card-header">
	<i class="fa fa-info-circle" aria-hidden="true"></i><strong>Thuộc tính</strong>
</div>
<div class="tab-content">
	<div class="col-md-12">
		<div class="card-block">
			<div class="form-group row">
				<label class="col-md-3 form-control-label" for="price">Giá (<span class="required_cl">*</span>) </label>
				<div class="col-md-9">
					<input type="number" id="price" name="price" class="form-control {{ ($errors->has('price') ? 'has-error' : '')}}" value="@if(isset($product->attribute)){{old('price', trim($product->attribute->price))}}@else{{old('price')}}@endif" placeholder="2.000.000.000"  >
					<span class="help-block mess-err">{{$errors->first('price')}} </span>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-3 form-control-label" for="discount">Giảm giá(%)</label>
				<div class="col-md-9">
					<input type="number" id="discount" name="discount" class="form-control {{ ($errors->has('discount') ? 'has-error' : '')}}" value="@if(isset($product->attribute)){{old('discount', trim($product->attribute->discount))}}@else{{old('discount')}}@endif" placeholder="10%"  >
					<span class="help-block mess-err">{{$errors->first('discount')}} </span>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-3 form-control-label" for="address">Địa chỉ</label>
				<div class="col-md-9">
					<input type="text" id="address" name="address" class="form-control {{ ($errors->has('address') ? 'has-error' : '')}}" value="@if(isset($product->attribute)){{old('address', trim($product->attribute->address))}}@else{{old('address')}}@endif" placeholder="305 Bui Vien.str ,1 District , HCM"  >
					<span class="help-block mess-err">{{$errors->first('address')}} </span>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-3 form-control-label" for="beedroom">Phòng ngủ</label>
				<div class="col-md-9">
					<input type="number" id="beedroom" name="beedroom" class="form-control {{ ($errors->has('beedroom') ? 'has-error' : '')}}" value="@if(isset($product->attribute)){{old('beedroom', trim($product->attribute->beedroom))}}@else{{old('beedroom')}}@endif" placeholder="3"  >
					<span class="help-block mess-err">{{$errors->first('beedroom')}} </span>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-3 form-control-label" for="bathroom">Phòng tắm</label>
				<div class="col-md-9">
					<input type="number" id="bathroom" name="bathroom" class="form-control {{ ($errors->has('bathroom') ? 'has-error' : '')}}" value="@if(isset($product->attribute)){{old('bathroom', trim($product->attribute->bathroom))}}@else{{old('bathroom')}}@endif" placeholder="2"  >
					<span class="help-block mess-err">{{$errors->first('bathroom')}} </span>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-3 form-control-label" for="acreage">Diện tích(m2)</label>
				<div class="col-md-9">
					<input type="number" id="acreage" name="acreage" class="form-control {{ ($errors->has('acreage') ? 'has-error' : '')}}" value="@if(isset($product->attribute)){{old('acreage', trim($product->attribute->acreage))}}@else{{old('acreage')}}@endif" placeholder="2000 m2"  >
					<span class="help-block mess-err">{{$errors->first('acreage')}} </span>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-3 form-control-label" for="floor">Tầng</label>
				<div class="col-md-9">
					<input type="number" id="floor" name="floor" class="form-control {{ ($errors->has('floor') ? 'has-error' : '')}}" value="@if(isset($product->attribute)){{old('floor', trim($product->attribute->floor))}}@else{{old('floor')}}@endif" placeholder="2"  >
					<span class="help-block mess-err">{{$errors->first('floor')}} </span>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-3 form-control-label" for="vi_video">Video</label>
				<div class="col-md-9">
					<input type="text" id="vi_video" name="vi_video" class="form-control {{ ($errors->has('vi_video') ? 'has-error' : '')}}" value="@if(isset($product->attribute)){{old('vi_video', trim($product->attribute->vi_video))}}@else{{old('vi_video')}}@endif" placeholder="https://www.youtube.com/watch?v=82JqIDgtR-Y"  >
					<span class="help-block mess-err">{{$errors->first('vi_video')}} </span>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
{{-- ************ END:: PROPERTY | BLOCK 1 ***********--}}
{{-- ************* BEGIN::PROPERTY | BLOCK 2 *********** --}}
<div class="card">
<div class="card-header">
	<i class="fa fa-info-circle" aria-hidden="true"></i><strong>Tiện nghi</strong>
</div>
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item">
		<a class="nav-link active" data-toggle="tab" href="#home7" role="tab" aria-controls="home"><i class="fa fa-language" aria-hidden="true"></i> VI</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#home8" role="tab" aria-controls="profile"><i class="fa fa-language" aria-hidden="true"></i> EN</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#home9" role="tab" aria-controls="profile"><i class="fa fa-language" aria-hidden="true"></i> CN</a>
	</li>
</ul>
<div class="tab-content card">
	<div class="row tab-pane active" id="home7" role="tabpanel">
		<div class="col-md-12">
			<div class="card-block">
				<div class="form-group row">
					<div class="col-md-12">
						<textarea style="z-index: 200000;" type="text" id="vi_convenient" rows="40" name="vi_convenient" class="myEditor form-control {{ ($errors->has('vi_convenient') ? 'has-error' : '')}}" value="" placeholder="">@if(isset($product->attribute->vi_convenient)){{old('vi_convenient', trim($product->attribute->vi_convenient))}}@else{{old('vi_convenient')}}@endif</textarea>
						<span class="help-block mess-err">{{$errors->first('vi_convenient')}} </span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row tab-pane" id="home8" role="tabpanel">
		<div class="col-md-12">
			<div class="card-block">
				<div class="form-group row">
					<div class="col-md-12">
						<textarea style="z-index: 200000;" type="text" id="en_convenient" rows="40" name="en_convenient" class="myEditor form-control {{ ($errors->has('en_convenient') ? 'has-error' : '')}}" value="" placeholder="">@if(isset($product->attribute->en_convenient)){{old('en_convenient', trim($product->attribute->en_convenient))}}@else{{old('en_convenient')}}@endif</textarea>
						<span class="help-block mess-err">{{$errors->first('en_convenient')}} </span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row tab-pane" id="home9" role="tabpanel">
		<div class="col-md-12">
			<div class="card-block">
				<div class="form-group row">
					<div class="col-md-12">
						<textarea style="z-index: 200000;" type="text" id="cn_convenient" rows="40" name="cn_convenient" class="myEditor form-control {{ ($errors->has('cn_convenient') ? 'has-error' : '')}}" value="" placeholder="">@if(isset($product->attribute->cn_convenient)){{old('cn_convenient', trim($product->attribute->cn_convenient))}}@else{{old('cn_convenient')}}@endif</textarea>
						<span class="help-block mess-err">{{$errors->first('cn_convenient')}} </span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div> 

{{-- ************ END:: PROPERTY | BLOCK 2 --}}
{{-- ************ BEGIN:: PROPERTY | BLOCK 3 ***********--}}

<div class="card">
<div class="card-header">
	<i class="fa fa-info-circle" aria-hidden="true"></i><strong>Bản vẽ</strong>
</div>

<div class="tab-content card">
	<div class="col-md-12">
		<div class="card-block">
			<div class="form-group row field_wrapper1">
				@if(isset($product->attribute->floor_plan))
					@include('admincp.products.products.image_multiple1',['pos_image1' => 22,'input_name1' => 'floor_plan','imgArr1' => $product->attribute->floor_plan])
				@else
					@include('admincp.products.products.image_multiple1',['pos_image1' => 22,'input_name1' => 'floor_plan'])
				@endif
			</div>
		</div>
	</div>
</div>
</div> 
{{-- ************ END:: PROPERTY | BLOCK 3 ***********--}}
</div>
</div>
</div>