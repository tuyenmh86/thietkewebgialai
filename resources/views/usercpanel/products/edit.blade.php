@extends('usercpanel.layouts.main')
@section('title',trans('back-app.products'))
@section('content')
<div class="container">
	<form action="{{route('user.products.update', $product->id)}}" method="post" class="form-horizontal " enctype="multipart/form-data" name="myform">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="id" value="{{$product->id}}">
		<input type="hidden" name="product_id" value="{{$product->id}}">
		{{-- Option --}}
		<div class="col-md-12 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						{{-- Save --}}
						<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{ trans('back-app.save') }}</button>
						{{-- Reset --}}
						<button type="reset" class="btn btn-sm btn-info"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.reset') }}</button>
						{{-- Cancel --}}
						<a href="{{route('user.products.index')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
					</div>
				</div>
			</div>
		</div>
		{{-- ============ BEGIN:: BLOCK 1 ==============--}}
		<div class="col-md-12 pull-left">
			<div class="animated row fadeIn">
				<div class="col-md-12 mb-2">
					{{-- BEGIN:: CHOOSE TAB --}}
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#home1" role="tab" aria-controls="home"><i class="fa fa-question-circle" aria-hidden="true"></i> {{ trans('back-app.information') }}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#home2" role="tab" aria-controls="profile"><i class="fa fa-cog" aria-hidden="true"></i> {{ trans('back-app.attribute') }}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#home3" role="tab" aria-controls="profile"><i class="fa fa-globe" aria-hidden="true"></i> {{ trans('back-app.web_master') }}</a>
						</li>
					</ul>
					{{-- END:: CHOOSE TAB --}}
					<div class="tab-content card">
						{{-- **************** BEGIN::INFOMATION BASIC *******  --}}
						<div class="row tab-pane active" id="home1" role="tabpanel">
							<div class="col-md-8 pull-left">
								@include('components.data_form',['old_value' => $product])
							</div>
							{{-- BENGIN:: BLOCK 2 --}}
							<div class="col-md-4 pull-right">
								{{-- Block 2 | Block 1 --}}
								<div class="animated fadeIn">
									<div class="card">
										<div class="card-header">
											<i class="fa fa-globe" aria-hidden="true"></i><strong>{{ trans('back-app.option') }}</strong>
										</div>
										<div class="card-block">
											<div class="form-group row">
												<div class="col-md-12">
													<input type="number" id="pos" name="pos" class="form-control {{ ($errors->has('pos') ? 'has-error' : '')}}" value="{{old('pos', $product->pos)}}" placeholder="Position">
													<span class="help-block mess-err">{{$errors->first('pos')}}</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								{{-- Block 2 | Block 2 --}}
								<div class="animated fadeIn">
									<div class="card">
										<div class="card-header">
											<strong><i class="fa fa-gg" aria-hidden="true"></i> {{ trans('back-app.categories') }}</strong>
										</div>
										<div class="card-block">
											<div class="form-group row">
												<div class="col-md-12">
													<select class="categories form-control" name="categories[]" multiple="multiple">
														@foreach($categorys as $category)
														@if(in_array($category->id,$product->categories->pluck('id')->toArray()));
														<option selected="" value="{{ $category->id }}">{{ $category->title }}</option>
														@else
														<option value="{{ $category->id }}">{{ $category->title }}</option>
														@endif
														@endforeach
													</select>
													<span class="help-block mess-err">{{$errors->first('categories')}}</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								{{-- Bolock 2 | Bock 3 --}}
								<div class="animated fadeIn">
									<div class="card">
										<div class="card-header">
											<i class="fa fa-tags" aria-hidden="true"></i>
											<strong>{{ trans('back-app.tags') }}</strong>
										</div>
										<div class="card-block">
											<div class="row">
												<div class="col-md-12">
													<select class="tags form-control" name="tags[]" multiple="multiple">
														@foreach($tags as $tag)
														@if(in_array($tag->id,$product->tags->pluck('id')->toArray()));
														<option selected="" value="{{ $tag->id }}">{{ $tag->title }}</option>

														@else
														<option value="{{ $tag->id }}">{{ $tag->title }}</option>
														@endif
														@endforeach
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
								{{-- ==== BEGIN:: BLOCK 2 | BLOCK 4 ==== --}}
								<div class="animated fadeIn">
									<div class="card">
										<div class="card-header">
											<i class="fa fa-picture-o" aria-hidden="true"></i><strong>{{ trans('back-app.featured_image') }}</strong>
										</div>
										<div class="card-block">
											<div class="form-group row">
												<div class="col-md-12 input-group">
													<div class="input-group">
														<span class="input-group-btn">
															<a id="" data-input="thumbnail" data-preview="holder" class="lfm btn btn-primary">
																<i class="fa fa-picture-o"></i> {{ trans('back-app.choose') }}
															</a>
														</span>
														<input id="thumbnail" class="form-control" type="text" name="featured_image" value="{{ $product->featured_image }}">
													</div>
												</div>
												<div class="col-md-12 input-group">
													<img id="holder" src="{{ $product->featured_image}}" style="margin-top:15px;max-height:100px;">
												</div>
												<span class="col-md-12 help-block mess-err">{{$errors->first('featured_image')}}</span>
											</div>
										</div>
									</div>
								</div>
							</div>

							{{-- ************* BEGIN::SELECT IMAGES LIST *********** --}}
							<div class="col-md-8 pull-left">
								<div class="animated fadeIn">
									<div class="card">
										<div class="card-header">
											<i class="fa fa-info-circle" aria-hidden="true"></i><strong>{{ trans('back-app.image_list') }}</strong>
										</div>

										<div class="tab-content card">
											<div class="col-md-12">
												<div class="card-block">
													<div class="form-group row field_wrapper">
														@include('admincp.products.products.image_multiple',['pos_image' => 11,'input_name' => 'image_list','imgArr' => $product->image_list])
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							{{-- ************ END:: SELECT IMAGES LIST --}}
						</div>
						{{-- ************ END::INFOMATION BASIC *******  --}}

						{{-- ************ BEGIN:: PROPERTY ***********--}}
						@include('admincp.products.products.product_attribute')
						{{-- ************ END:: PROPERTY ***********--}}


						{{-- ************ BEGIN:: SEO ADVANCED ***********--}}
						@include('components.seo_advanced',['old_value' => $product])
						{{-- ************ END:: SEO ADVANCED ***********--}}
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection