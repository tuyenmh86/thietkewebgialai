@extends('usercpanel.layouts.main')
@section('title',trans('back-app.dashboard'))
@section('content')
<div class="container">
<div class="animated fadeIn">
<div class="card card-block">
<div class="row">
<div class="col-md-12">
    <!--/.row-->
    <hr class="mt-0">
    {{-- Statistics Top Product View --}}
    <div class="card">
        <div class="card-header">
            <i class="icon-check"></i>Lượt xem sản phẩm
        </div>
        <div class="card-block p-0">
            <div class="tab-content"> 
                <div class="tab-pane active" id="tickets" aria-expanded="true">
                    <table class="table table-hover mb-0">
                        <thead>
                            <tr>
                                <th>{{ trans('back-app.status') }}</th>
                                <th>{{ trans('back-app.created') }}</th>
                                <th>{{ trans('back-app.title') }}</th>
                                <th>{{ trans('back-app.view') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($produc_top_v))
                            @foreach($produc_top_v as $value )
                            <tr>
                                <td>
                                    @if($value->is_published())
                                    <span class="badge badge-success">{{ trans('back-app.public') }}</span>
                                    @else
                                    <span class="badge badge-danger">{{ trans('back-app.disable') }}</span>
                                    @endif
                                </td>
                                <td>
                                    {{date_format($value->created_at, 'l jS F Y')}}
                                </td>
                                <td><a href="{{route('user.products.edit',$value->id)}}"> {{str_limit($value->vi_title,10)}}</a></td>
                                <td><b>[#{{$value->view}}]</b>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!--/.row-->
</div>
</div>
</div>
@endsection