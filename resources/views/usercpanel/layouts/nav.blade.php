<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{route('user.dashboard')}}">
                    <i class="icon-speedometer"></i> 
                    {{ trans('back-app.dashboard') }}
                    <span class="badge badge-info">
                        {{ trans('back-app.new') }}
                    </span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('user.profile.index')}}">
                    <i class="fa fa-product-hunt" aria-hidden="true"></i>
                    Profile
                </a>
            </li>
            {{--  <li class="nav-item">
                <a class="nav-link" href="{{route('user.products.index')}}">
                    <i class="fa fa-product-hunt" aria-hidden="true"></i>
                    {{ trans('back-app.products') }}
                </a>
            </li>  --}}
            {{--  <li class="nav-item">
                <a class="nav-link" href="{{route('user.contacts.index')}}">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    {{ trans('back-app.contact') }}
                    <span class="badge badge-info">
                        {{ trans('back-app.new') }}
                    </span>
                </a>
            </li>  --}}
        </ul>
    </nav>
</div>