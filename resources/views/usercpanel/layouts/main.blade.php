<!--
 * GenesisUI - Bootstrap 4 Admin Template
 * @version v1.8.0
 * @link https://genesisui.com
 * Copyright (c) 2017 creativeLabs Łukasz Holeczek
 * @license Commercial
 -->
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from genesisui.com/demo/prime/bootstrap4-static/ by HTTrack Website Copier/3.x [XR&CO2014], Thu, 23 Mar 2017 08:53:09 GMT -->
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="#">
    <meta name="author" content="{{ getConfigSite('copyright') }}">
    <meta name="keyword" content="#">
    <link rel="shortcut icon" href="{{ getConfigSite('favicon') }}">

    <title>{{ getConfigSite('site_title') }} | @yield('title')</title>

    <!-- Icons -->
    <link href="{{admin_public_url()}}/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{admin_public_url()}}/css/simple-line-icons.css" rel="stylesheet">

    <!-- Premium Icons -->
    <link href="{{admin_public_url()}}/css/glyphicons.css" rel="stylesheet">
    <link href="{{admin_public_url()}}/css/glyphicons-filetypes.css" rel="stylesheet">
    <link href="{{admin_public_url()}}/css/glyphicons-social.css" rel="stylesheet">
    
    {{-- Input Tags --}}
    <link href="{{admin_public_url()}}/css/input_tags/bootstrap-tagsinput.css" rel="stylesheet">
    <link href="{{admin_public_url()}}/css/input_tags/custom_label_tag.css" rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="{{admin_public_url()}}/css/style.css" rel="stylesheet">
    <link href="{{admin_public_url()}}/css/custom.css" rel="stylesheet">

    {{-- Select Multilpe --}}
    <link href="{{admin_public_url()}}/css/select2/select2.css" rel="stylesheet">
    
    {{-- Log-Viewer Packed --}}
    <link href="{{admin_public_url()}}/css/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">

    {{-- CKFinder --}}
    <script src="{{asset('admin/plugins/ckfinder/ckfinder.js')}}"></script> 
    
    {{-- CKFinder Config --}}
    <script src="{{admin_public_url()}}/js/ckfinder_config.js"></script> 

    {{-- Custom JS Site --}}
    <script src="{{admin_public_url()}}/js/custom.js"></script> 

    {{-- CK Finder --}}
    <script src="{{asset('admin/plugins/ckeditor/ckeditor.js')}}"></script>
    
    <script src="{{admin_plugin_url()}}/tinymce/js/tinymce/tinymce.min.js"></script>
    
    @yield('header')


</head>

<!-- BODY options, add following classes to body to change options

// Header options
1. '.header-fixed'                  - Fixed Header

// Sidebar options
1. '.sidebar-fixed'                 - Fixed Sidebar
2. '.sidebar-hidden'                - Hidden Sidebar
3. '.sidebar-off-canvas'        - Off Canvas Sidebar
4. '.sidebar-compact'               - Compact Sidebar Navigation (Only icons)

// Aside options
1. '.aside-menu-fixed'          - Fixed Aside Menu
2. '.aside-menu-hidden'         - Hidden Aside Menu
3. '.aside-menu-off-canvas' - Off Canvas Aside Menu

// Footer options
1. 'footer-fixed'                       - Fixed footer

-->

<body class="app header-fixed aside-menu-fixed aside-menu-hidden">
    <header class="app-header navbar">
        <button class="navbar-toggler mobile-sidebar-toggler hidden-lg-up" type="button">☰</button>
        <a class="navbar-brand" href="{{route('user.dashboard')}}"><center><img width="50px" src="{{ auth()->user()->profile_image }}" alt=""></center></a>
        <ul class="nav navbar-nav hidden-md-down">
            <li class="nav-item">
                <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
            </li>
        </ul>
        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <img width="50px" src="{{ auth()->user()->profile_image}}" class="img-avatar" alt="">
                    <span class="hidden-md-down">{{auth()->user()->email}}</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{route('user.profile.edit',auth()->user()->id)}}"><i class="fa fa-bell-o"></i> {{ trans('back-app.profile') }}<span class="badge badge-info">1</span></a>
                    <a class="dropdown-item" href="{{route('user.logout')}}"><i class="fa fa-lock"></i> {{ trans('back-app.logout') }}</a>
                </div>
            </li>
        </ul>
    </header>

    <div class="app-body">
        <!-- // Load Nav  -->
        @include('usercpanel.layouts.nav')
        <!-- // End Load Nav -->
        <!-- Main content -->
        <main class="main">
            <!-- Breadcrumb -->
            @include('usercpanel.layouts.breadcrumbs')
             <!-- Alert msg -->
            @include('usercpanel.layouts.alert-message')
            <!-- // End Load Breadcrumbs -->
            <!-- // Load Content -->
            @yield('content')
            <!-- // End Load Content -->

        </main>
        {{-- Yield Modals --}}
        @yield('modals')
    </div>

    <footer class="app-footer">
        {{ getConfigSite('copyright') }}
    </footer>
    
    

    <!-- Bootstrap and necessary plugins -->
    <script src="{{admin_public_url()}}/js/libs/jquery.min.js"></script>
    <script src="{{admin_public_url()}}/js/libs/tether.min.js"></script>
    <script src="{{admin_public_url()}}/js/libs/bootstrap.min.js"></script>
    <script src="{{admin_public_url()}}/js/libs/pace.min.js"></script>

    <!-- Main function -->
    <script src="{{admin_public_url()}}/js/main.js"></script>

    <!-- Plugins and scripts required by all views -->
    <script src="{{admin_public_url()}}/js/libs/Chart.min.js"></script>


    <!-- GenesisUI main scripts -->

    <script src="{{admin_public_url()}}/js/app.js"></script>

     <!-- // DataTable -->
    <script src="{{admin_public_url()}}/js/libs/jquery.dataTables.min.js"></script>
    <script src="{{admin_public_url()}}/js/libs/dataTables.bootstrap4.min.js"></script>


    <!-- Custom scripts required by this view -->
    <script src="{{admin_public_url()}}/js/views/tables.js"></script>
    
    <!-- Select 2 (Slect multiple) -->
    <script src="{{admin_public_url()}}/js/select2/select2.min.js"></script>
    
    <!-- // End DataTable -->

    <script type="text/javascript" src="{{admin_public_url()}}/js/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    
    {{-- Input Tags --}}
    <script src="{{admin_public_url()}}/js/input_tags/bootstrap-tagsinput.min.js"></script> 

    {{-- Using script on Page --}}
    @yield('script')
    @stack('footer')
    <script type="text/javascript">
    // ************************ BEGIN:: TINYMCE ****************************
        var editor_config = {
        path_absolute : "/",
        selector: "textarea.myEditor",
        plugins: [
          "advlist autolink lists link image charmap print preview hr anchor pagebreak",
          "searchreplace wordcount visualblocks visualchars code fullscreen",
          "insertdatetime media nonbreaking save table contextmenu directionality",
          "emoticons template paste textcolor colorpicker textpattern textcolor colorpicker"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media fullpage | fullscreen | code forecolor backcolor",
        content_css : "/admin/admin/css/custom.css",
        relative_urls: false,
        convert_urls : false,
        verify_html: false,
        file_browser_callback : function(field_name, url, type, win) {
          var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
          var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

          var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
          if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
          } else {
            cmsURL = cmsURL + "&type=Files";
          }

          tinyMCE.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
          });
        }
      };

      tinymce.init(editor_config);
    </script>
    {{-- // ****************** BEGIN:: FUNCTION FILES ***************** --}}
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script type="text/javascript">
        // $('#lfm').filemanager('image');  
        $('[class*="lfm"]').each(function() {
            $(this).filemanager('image');
        });
        // *************** BEGIN:: CHOOSE TAGS && CATEGORIES ***************
        $(document).ready(function(){
            $('.categories').select2({
                 placeholder: "Select a category",
            });
            $('.tags').select2({
                 placeholder: "Select a tag",
            });
        });
    </script>
    <script type="text/javascript">
       document.onkeydown = preventEnterKey;
    </script>
</body>


<!-- Mirrored from genesisui.com/demo/prime/bootstrap4-static/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 23 Mar 2017 08:53:30 GMT -->
</html>