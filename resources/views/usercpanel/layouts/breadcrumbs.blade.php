<ol class="breadcrumb">
    @if(Request::is('usercpanel'))
    <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">{{ trans('back-app.dashboard') }}</a></li>
    <li class="breadcrumb-item active">
        {{ trans('back-app.dashboard') }}
    </li>
    @else
    <li class="breadcrumb-item"><a href="{{route('user.dashboard')}}">{{ trans('back-app.dashboard') }}</a></li>
    {{-- Breadcrumbs Level 2 --}}
    <li class="breadcrumb-item">
        @if(Request::is('usercpanel/profile*'))
        <a href="{{route('user.profile.index')}}">{{ trans('back-app.profile') }}</a>
        @endif
        {{-- Products --}}
        @if(Request::is('usercpanel/products*'))
        <a href="{{route('user.products.index')}}">{{ trans('back-app.products') }}</a>
        @endif
        {{-- Contacts --}}
        @if(Request::is('usercpanel/contacts*'))
            <a href="{{route('user.contacts.index')}}">{{ trans('back-app.contact') }}</a>
        @endif
    </li>
    {{-- Level 3 --}}
    <li class="breadcrumb-item active">
        {{-- Profile --}}
        {{(Request::is('usercpanel/profile/*/edit') ? trans('back-app.list') : '')}}
        {{-- Products --}}
        {{(Request::is('usercpanel/products') ? trans('back-app.list') : '')}}
        {{(Request::is('usercpanel/products/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('usercpanel/products/create') ? trans('back-app.add') : '')}}
        {{-- Contacts --}}
        {{(Request::is('usercpanel/contacts') ? trans('back-app.list') : '')}}
    </li>
    @endif
</ol>