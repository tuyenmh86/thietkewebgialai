@extends('usercpanel.layouts.main')
@section('title',trans('back-app.contact'))
@section('header')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <style>
        .readmore{
            overflow-y: hidden;
        }
    </style>
@stop
@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                <a href="{{route('user.dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
                {{-- ************** Option Action ***************************** --}}
                <div class="form-group row">
                    <div class="col-md-2 ">
                        <select id="action_table" name="action_table" class=" form-control pull-left" size="1">
                            <option value="0">-- {{ trans('back-app.action') }} --</option>
                            <option value="1">{{ trans('back-app.delete') }}</option>
                        </select>

                    </div>
                    <button type="button" id="submit_action" class="btn btn-sm btn-info pull-left"><i class="fa fa-location-arrow" aria-hidden="true"></i> {{ trans('back-app.submit') }}</button>&nbsp;
                </div>
                {{-- ************** End Option Action ***************************** --}}
                <table class="table  table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="checkAll" name="checkAll"/></th>
                            <th>{{ trans('back-app.name') }}</th>
                            <th>{{ trans('back-app.email') }}</th>
                            <th>{{ trans('back-app.phone') }}</th>
                            <th>{{ trans('back-app.message') }}</th>
                            <th>{{ trans('back-app.products') }}</th>
                            <th>{{ trans('back-app.status') }}</th>
                            <th>{{ trans('back-app.action') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($contacts))
                            @foreach($contacts as $contact)
                                <tr>
                                    <td><input type="checkbox" id="checkSingle" name="checkSingle[]" value="{{$contact->id}}" /></td>
                                    <td>{{$contact->name}}</td>
                                    <td>{{$contact->email}}</td>
                                    <td>{{$contact->phone}}</td>
                                    <td><div class="readmore">{{$contact->message }}</div></td>
                                    <td>{{ $contact->product ? $contact->product->language('title') : '' }}</td>
                                    <td id="status-{{$contact->id}}">
                                        @if(!$contact->is_published())
                                            <input type="checkbox" data-toggle="toggle" data-on="Success" data-off="Pending" data-id="{{$contact->id}}" onchange="updateStatus(this)">
                                        @else
                                            <span class="badge badge-success"><i class="fa fa-check-circle" aria-hidden="true"></i>Phê duyệt</span>
                                        @endif
                                    </td>
                                    <td>
                                        <form action="{{route('user.contacts.destroy',$contact->id)}}" method="Post" class="pull-left">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <input type="hidden" name="_method" value="DELETE">
                                            <a class="btn btn-danger" href="javascript:void(0);" onclick="$(this).closest('form').submit()">
                                                <i class="fa fa-trash-o "></i>
                                            </a>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
{{-- Using Script --}}
@section('script')
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://fastcdn.org/Readmore.js/2.1.0/readmore.min.js"></script>
    <script type="text/javascript">
        function updateStatus(e) {
            var id = $(e).data('id'),
                target = '#status-'+id;
            axios.get('contacts/updatestatus', {
                params: {
                    id: id
                }
            })
                .then(function (response) {
                    toastr.success(response.data);
                    $(target).html('<span class="badge badge-success">success</span>');
                })
                .catch(function (error) {
                    console.log(error.response.data.error);
                    toastr.error(error.response.data.error);
                });
        };
        $('document').ready(function(){
            $('#checkAll').change(function(){
                if($(this).prop('checked')){
                    $('#table_active tbody tr td input[type="checkbox"]').each(function(){
                        $(this).prop('checked', true);
                    });
                }else{
                    $('#table_active tbody tr td input[type="checkbox"]').each(function(){
                        $(this).prop('checked', false);
                    });
                }
            });
            $('#submit_action').on('click', function(){
                if( $('#action_table').val() == 1){
                    var id = [];
                    $('input[name="checkSingle[]"]:checked').each(function(){
                        id.push($(this).val());
                    });

                    axios.get('contacts/deleteSelect', {
                        params: {
                            id: id
                        }
                    })
                        .then(function (response) {
                            toastr.success(response.data);
                            location.reload();
                        })
                        .catch(function (error) {
                            console.log(error.response.data.error);
                            toastr.error(error.response.data.error);
                        });
                }
            });
            $('.readmore').readmore({
                speed: 75,
                collapsedHeight: 40
            });
        })
    </script>
@endsection