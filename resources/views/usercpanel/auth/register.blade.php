@extends('usercpanel.auth.layouts.main')
@section('title','Register')
@section('content')
<section id="wrapper" class="login-register">
  <div class="login-box" >
    <div class="white-box">
      <form class="form-horizontal form-material" method="POST" id="loginform" action="{{route('user.register')}}">
        {{ csrf_field() }}
        <h3 class="box-title m-b-20">Sign In</h3>
        <!-- Message error -->
        @if(Session::has('error'))
        <span class="help-block" style="color: #a94442">{{ Session('error')}}
        </span>
        @endif
        {{-- Alert Message --}}
        <div class="flash-message">
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
              @if(Session::has('alert-' . $msg))

              <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
              @endif
            @endforeach
        </div>
        <!-- end .flash-message -->
        <div class="form-group ">
          <div class="col-xs-6 {{ ($errors->has('full_name') ? 'has-error' : '')}}">
            <input class="form-control" value="{{old('full_name')}}" id="full_name" name="full_name" type="text" placeholder="Name">
            <span class="help-block">
                {{$errors->first('full_name')}}
            </span>
          </div>
          <div class="col-xs-6 {{ ($errors->has('email') ? 'has-error' : '')}}">
            <input class="form-control" value="{{old('email')}}" id="email" name="email" type="text" placeholder="Email">
            <span class="help-block">
                {{$errors->first('email')}}
            </span>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-6 {{ ($errors->has('phone') ? 'has-error' : '')}}">
            <input class="form-control" id="phone"  value="{{old('phone')}}" name="phone" type="number" placeholder="Phone">
            <span class="help-block">
                {{$errors->first('phone')}}
            </span>
          </div>
          <div class="col-xs-6 {{ ($errors->has('address') ? 'has-error' : '')}}">
            <input class="form-control" id="address" value="{{old('address')}}" name="address" type="text" placeholder="Address">
            <span class="help-block">
                {{$errors->first('address')}}
            </span>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-6 {{ ($errors->has('password') ? 'has-error' : '')}}">
            <input class="form-control" id="password" name="password" type="password" placeholder="Password">
            <span class="help-block">
                {{$errors->first('password')}}
            </span>
          </div>
          <div class="col-xs-6 {{ ($errors->has('re_password') ? 'has-error' : '')}}">
            <input class="form-control" id="re_password" name="re_password" type="password" placeholder="Confirm Password">
            <span class="help-block">
                {{$errors->first('re_password')}}
            </span>
          </div>
        </div>
        {{-- <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary p-t-0">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"> I agree to all <a href="#">Terms</a></label>
            </div>
          </div>
        </div> --}}
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Sign Up</button>
          </div>
        </div>
        <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p>Already have an account? <a href="{{route('user.login')}}" class="text-primary m-l-5"><b>Sign In</b></a></p>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
@endsection
@section('script')
<script type="text/javascript">
// ********************************* PREVEN EVEN KEYENTER ***************************
document.onkeydown = preventEnterKey;
</script>
@endsection