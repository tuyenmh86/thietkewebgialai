<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mail thông báo</title>
    <style>
            table { 
                width: 100%; 
                border-collapse: collapse; 
              }
              /* Zebra striping */
              tr:nth-of-type(odd) { 
                background: #eee; 
              }
              th { 
                background: #333; 
                color: white; 
                font-weight: bold; 
              }
              td, th { 
                padding: 6px; 
                border: 1px solid #ccc; 
                text-align: left; 
              }
    </style>
</head>
<body>

	<p>Dear!!! <strong> </strong></p>
    
    <p>Hệ thống đã nhận được đơn hàng :</p>
    <h4>Thông tin khách hàng:</h4>
    <table style="border:1px solid #4e4e4e;" class="table table-bordered table-responsive">
        <thead>
            <tr style="border:1px solid #4e4e4e;">
                <th >
                    Họ Tên
                </th>
                <th>
                    Công ty
                </th>
                <th>
                    Địa chỉ
                </th>
                <th>
                    Email
                </th>
                <th>
                    Điện thoại
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    {{$data['hoten']}}
                </td>
                <td>
                    {{$data['company_name']}}
                </td>
                <td>
                    {{$data['address']}}
                </td>
                <td>
                    {{$data['email']}}
                </td>
                <td>
                    {{$data['phone']}}
                </td>
            </tr>
        </tbody>
    </table>
    
    <h2>Sản phẩm đã đặt hàng:</h2>

    <table>
            <thead>
                    <tr style="border:1px solid #4e4e4e;">
                        <th>
                            Tên Sản phẩm
                        </th>
                        <th>
                            Số lượng
                        </th>
                        <th>
                            Giá sản phẩm
                        </th>
                        <th>
                            Thành tiền
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data['order_detail'] as $item)
                    <tr>
                        <td>
                            {{$item->name}}    
                        </td>   
                        <td>
                            {{$item->qty}}    
                        </td> 
                        <td>
                            {{$item->price}}    
                        </td> 
                        <td>
                            {{$item->qty*$item->price}}    
                        </td> 
                    </tr>
                    @endforeach
                </tbody>
    </table>
    <h5>Tổng giá trị đơn hàng: {{$data['total']}}</h5>

    <hr>

</body>
</html>