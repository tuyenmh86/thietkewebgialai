<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mail thông báo</title>
    <style>
            table { 
                width: 100%; 
                border-collapse: collapse; 
              }
              /* Zebra striping */
              tr:nth-of-type(odd) { 
                background: #eee; 
              }
              th { 
                background: #333; 
                color: white; 
                font-weight: bold; 
              }
              td, th { 
                padding: 6px; 
                border: 1px solid #ccc; 
                text-align: left; 
              }
    </style>
</head>
<body>

	<p>Dear!!! <strong> </strong></p>
    
    <p>Hệ thống đã nhận được đơn hàng :</p>
    <h4>Thông tin khách hàng:</h4>
    <table style="border:1px solid #4e4e4e;" class="table table-bordered table-responsive">
        <thead>
            <tr style="border:1px solid #4e4e4e;">
            @if($name)
                
                        <th>
                            Tên khách hàng
                        </th>
            @endif
            @if($email)
                    <th>
                        Email khách hàng
                    </th>
            @endif
            @if($custommer_message)
                    <th>
                        Thông điệp
                    </th>
            @endif
            @if($phone)
                    <th>
                        Điện thoại
                    </th>
            @endif
            @if($product_id)
                <th>
                   Tên sản phẩm
                </th>
            @endif
            </tr>
        </thead>
        <tbody>
            <tr style="border:1px solid #4e4e4e;">
            @if($name)
                    <td>
                        {{$name}}
                    </td>
            @endif
            @if($email)
                    <td>
                        {{$email}}
                    </td>
            @endif
            @if($custommer_message)
                    <td>
                        {!!$custommer_message!!}
                    </td>
            @endif
            @if($phone)
                    <td>
                       {{$phone}}
                    </td>
            @endif
            @if($product_id)
                    <td>
                       {{$product_id}}
                    </td>
            @endif
            </tr>
        </tbody>
    </table>
    
    <h5> Vui lòng gọi lại cho khách hàng!!!</h5>
    <hr>

</body>
</html>