@include('admincp.menus.popup')
<?php
$currentUrl = url()->current();

?>
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="{{asset('vendor/harimayco-menu/style.css')}}" rel="stylesheet">
<div id="hwpwrap">
<div class="custom-wp-admin wp-admin wp-core-ui js   menu-max-depth-0 nav-menus-php auto-fold admin-bar">
<div id="wpwrap">
<div id="wpcontent">
<div id="wpbody">
<div id="wpbody-content">

<div class="wrap">
	
	<div class="manage-menus">
		<form method="get" action="{{ $currentUrl }}">
			<label for="menu" class="selected-menu">Chọn trình đơn mà bạn muốn chỉnh sửa:</label>

			{!! Menu::select('menu', $menulist) !!}

			<span class="submit-btn">
				<input type="submit" class="button-secondary" value="Chọn">
			</span>
			<span class="add-new-menu-action"> or <a href="{{ $currentUrl }}?action=edit&menu=0">Tạo trình đơn</a>. </span>
		</form>
	</div>
	<div id="nav-menus-frame">

		@if(request()->has('menu')  && !empty(request()->input("menu")))
		<div id="menu-settings-column" class="metabox-holder">

			<div class="clear"></div>

			<form id="nav-menu-meta" action="{{ route('admincp.haddcustommenu') }}" class="nav-menu-meta" method="post" enctype="multipart/form-data">
				{!! csrf_field() !!}
				<input type="hidden" name="menu" id="idmenu" value="@if(isset($indmenu)){{$indmenu->id}}@endif" />
				<div id="side-sortables" class="accordion-container">
					<ul class="outer-border">
						<li class="control-section accordion-section  open add-page" id="add-page">
							<h3 class="accordion-section-title hndle" tabindex="0"> Tùy chọn đường dẫn <span class="screen-reader-text">Nhấn trở lại hoặc nhập để mở rộng</span></h3>
							<div class="accordion-section-content ">
								<div class="inside">
									<div class="customlinkdiv" id="customlinkdiv">
										<p id="menu-item-url-wrap">
											<label class="howto" for="custom-menu-item-url"> <button type="button" class="btn btn-primary button-primary left" data-toggle="modal" data-target="#exampleModal" data-whatever=""><i class="fa fa-external-link" aria-hidden="true"></i></button>&nbsp;
												<input id="custom-menu-item-url" name="link" type="text" class="code menu-item-textbox custom-menu-item-url" value="http://">
											</label>
										</p>
										<div class="animated fadeIn">
											<div class="card">
												<div class="card-header">
													<i class="fa fa-info-circle" aria-hidden="true"></i><strong>{{ trans('back-app.name') }}</strong>
												</div>

												@if(count(config('cms.locale')) > 1)
													<ul class="nav nav-tabs" role="tablist">
														@foreach(config('cms.locale') as $key => $item)
															<li class="nav-item">
																<a class="nav-link {{ $key == 0 ? 'active' : '' }}" data-toggle="tab" href="#language_{{$item['code']}}" role="tab" aria-controls="home"><img src="{{$item['flag']}}"><span style="text-transform: uppercase;"> {{ $item['code'] }} </a>
															</li>
														@endforeach
													</ul>
												@endif
												<div class="tab-content">
													@foreach(config('cms.locale') as $key => $item)
														<div class="row tab-pane {{ $key == 0 ? 'active' : '' }}" id="language_{{$item['code']}}" role="tabpanel">
															<div class="col-md-12">
																<input id="custom-menu-item-name" name="language[{{$item['code']}}][label]" type="text" class="regular-text menu-item-textbox input-with-default-title custom-menu-item-name" title="Label menu">
															</div>
													</div>
													@endforeach
												</div>
											</div>
										</div>

										<p class="button-controls">
											<button type="submit" class="btn button-secondary">Thêm mới</button>
										</p>
									</div>
								</div>
							</div>
						</li>

					</ul>
				</div>
			</form>

		</div>
		@endif
		<div id="menu-management-liquid">
			<div id="menu-management">
				<form id="update-nav-menu" action="" method="post" enctype="multipart/form-data">
					<div class="menu-edit ">
						<div id="nav-menu-header">
							<div class="major-publishing-actions">
								<label class="menu-name-label howto open-label" for="menu-name"> <span>Tên</span>
									<input name="menu-name" id="menu-name" type="text" class="menu-name regular-text menu-item-textbox" title="Enter menu name" value="@if(isset($indmenu)){{$indmenu->name}}@endif">
									<input type="hidden" name="menu" id="idmenu" value="@if(isset($indmenu)){{$indmenu->id}}@endif" />
								</label>

								@if(request()->has('action'))
								<div class="publishing-action">
									<a onclick="createnewmenu()" name="save_menu" id="save_menu_header" class="button button-primary menu-save">Tạo trình đơn</a>
								</div>
								@elseif(request()->has("menu"))
								<div class="publishing-action">
									<a onclick="getmenus()" name="save_menu" id="save_menu_header" class="button button-primary menu-save">Lưu trình đơn</a>
									<span class="spinner" id="spincustomu2"></span>
								</div>

								@else
								<div class="publishing-action">
									<a onclick="createnewmenu()" name="save_menu" id="save_menu_header" class="button button-primary menu-save">Tạo trình đơn</a>
								</div>
								@endif
							</div>
						</div>
						<div id="post-body">
							<div id="post-body-content">

								@if(request()->has("menu"))
								<h3>Cấu trúc trình đơn</h3>
								<div class="drag-instructions post-body-plain" style="">
									<p>
										Đặt mỗi mặt hàng theo thứ tự bạn thích. Nhấp vào mũi tên ở bên phải mục để hiển thị thêm các tùy chọn cấu hình.
									</p>
								</div>

								@else
								<h3>Trình đơn</h3>
								<div class="drag-instructions post-body-plain" style="">
									<p>
										Vui lòng nhập tên và chọn nút "Tạo trình đơn"
									</p>
								</div>
								@endif

								<ul class="menu ui-sortable" id="menu-to-edit">
									@if(isset($menus))
									@foreach($menus as $key_w => $m)
									<li id="menu-item-{{$m->id}}" class="menu-item menu-item-depth-{{$m->depth}} menu-item-page menu-item-edit-inactive pending" style="display: list-item;">
										<dl class="menu-item-bar">
											<dt class="menu-item-handle">
												<span class="item-title"> <span class="menu-item-title"> <span id="menutitletemp_{{$m->id}}">{{$m->label}}</span> <span style="color: transparent;">|{{$m->id}}|</span> </span> <span class="is-submenu" style="@if($m->depth==0)display: none;@endif">Subelement</span> </span>
												<span class="item-controls"> <span class="item-type">Đường dẫn</span> <span class="item-order hide-if-js"> <a href="{{ $currentUrl }}?action=move-up-menu-item&menu-item={{$m->id}}&_wpnonce=8b3eb7ac44" class="item-move-up"><abbr title="Move Up">↑</abbr></a> | <a href="{{ $currentUrl }}?action=move-down-menu-item&menu-item={{$m->id}}&_wpnonce=8b3eb7ac44" class="item-move-down"><abbr title="Move Down">↓</abbr></a> </span> <a class="item-edit" id="edit-{{$m->id}}" title=" " href="{{ $currentUrl }}?edit-menu-item={{$m->id}}#menu-item-settings-{{$m->id}}"> </a> </span>
											</dt>
										</dl>

										<div class="menu-item-settings" id="menu-item-settings-{{$m->id}}">
											<input type="hidden" class="edit-menu-item-id" name="menuid_{{$m->id}}" value="{{$m->id}}" />

											<p class="description description-thin">
												<label for="edit-menu-item-title-{{$m->id}}"> Nhãn
													<br>
													<input type="text" id="idlabelmenu_{{$m->id}}" class="widefat edit-menu-item-title" name="idlabelmenu_{{$m->id}}" value="{{$m->label}}">
												</label>
											</p>

											<p class="field-css-classes description description-thin">
												<label for="edit-menu-item-classes-{{$m->id}}"> Class CSS (optional)
													<br>
													<input type="text" id="clases_menu_{{$m->id}}" class="widefat code edit-menu-item-classes" name="clases_menu_{{$m->id}}" value="{{$m->class}}">
												</label>
											</p>

											<p class="field-css-url description description-wide">
												<label for="edit-menu-item-url-{{$m->id}}"> <button onclick="return change_link_item('{{ $m->id }}');" type="button" class="btn btn-primary button-primary left myModalEdit" data-toggle="modal" data-whatever=""><i class="fa fa-link" aria-hidden="true"></i></button>
													<br>
													<input type="text" id="url_menu_{{$m->id}}" class="widefat code edit-menu-item-url custom-menu-item-url-edit-{{$m->id}}" id="url_menu_{{$m->id}}" value="{{$m->link}}">
												</label>
											</p>

											<p class="field-move hide-if-no-js description description-wide">
												<label> <span>Di chuyển</span> <a href="{{ $currentUrl }}" class="menus-move-up" style="display: none;">Di chuyển lên</a> <a href="{{ $currentUrl }}" class="menus-move-down" title="Mover uno abajo" style="display: inline;">Di chuyển xuống</a> <a href="{{ $currentUrl }}" class="menus-move-left" style="display: none;"></a> <a href="{{ $currentUrl }}" class="menus-move-right" style="display: none;"></a> <a href="{{ $currentUrl }}" class="menus-move-top" style="display: none;">Lên đầu</a> </label>
											</p>

											<div class="menu-item-actions description-wide submitbox">

												<a class="item-delete submitdelete deletion" id="delete-{{$m->id}}" href="{{ $currentUrl }}?action=delete-menu-item&menu-item={{$m->id}}&_wpnonce=2844002501">Xóa</a>
												<span class="meta-sep hide-if-no-js"> | </span>
												<a class="item-cancel submitcancel hide-if-no-js button-secondary" id="cancel-{{$m->id}}" href="{{ $currentUrl }}?edit-menu-item={{$m->id}}&cancel=1424297719#menu-item-settings-{{$m->id}}">Thoát</a>
												<span class="meta-sep hide-if-no-js"> | </span>
												<a onclick="updateitem({{$m->id}})" class="button button-primary updatemenu" id="update-{{$m->id}}" href="javascript:void(0)">Cập nhập</a>

											</div>

										</div>
										<ul class="menu-item-transport"></ul>
									</li>
									@endforeach
									@endif
								</ul>
								<div class="menu-settings">

								</div>
							</div>
						</div>
						<div id="nav-menu-footer">
							<div class="major-publishing-actions">

								@if(request()->has('action'))
								<div class="publishing-action">
									<a onclick="createnewmenu()" name="save_menu" id="save_menu_header" class="button button-primary menu-save">Thêm trình đơn</a>
								</div>
								@elseif(request()->has("menu"))
								<span class="delete-action"> <a class="submitdelete deletion menu-delete" onclick="deletemenu()" href="javascript:void(9)">Xóa trình đơn</a> </span>
								<div class="publishing-action">

									<a onclick="getmenus()" name="save_menu" id="save_menu_header" class="button button-primary menu-save">Lưu trình đơn</a>
									<span class="spinner" id="spincustomu2"></span>
								</div>

								@else
								<div class="publishing-action">
									<a onclick="createnewmenu()" name="save_menu" id="save_menu_header" class="button button-primary menu-save">Thêm trình đơn</a>
								</div>
								@endif
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="clear"></div>
</div>

<div class="clear"></div>
</div>
<div class="clear"></div>
</div>

<div class="clear"></div>
</div>
</div>
</div>

@push('footer')
<script>
var menus = {
"oneThemeLocationNoMenus" : "",
"moveUp" : "Move up",
"moveDown" : "Mover down",
"moveToTop" : "Move top",
"moveUnder" : "Move under of %s",
"moveOutFrom" : "Out from under  %s",
"under" : "Under %s",
"outFrom" : "Out from %s",
"menuFocus" : "%1$s. Element menu %2$d of %3$d.",
"subMenuFocus" : "%1$s. Menu of subelement %2$d of %3$s."
};
var arraydata = [];     
var addcustommenur= '{{ route("admincp.haddcustommenu") }}';
var updateitemr= '{{ route("admincp.hupdateitem")}}';
var generatemenucontrolr= '{{ route("admincp.hgeneratemenucontrol") }}';
var deleteitemmenur= '{{ route("admincp.hdeleteitemmenu") }}';
var deletemenugr= '{{ route("admincp.hdeletemenug") }}';
var createnewmenur= '{{ route("admincp.hcreatenewmenu") }}';
var csrftoken="{{ csrf_token() }}";
var menuwr = "{{ url()->current() }}";

$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': csrftoken
}
});
</script>
<script type="text/javascript" src="{{asset('vendor/harimayco-menu/scripts.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/harimayco-menu/scripts2.js')}}"></script>
<script type="text/javascript" src="{{asset('vendor/harimayco-menu/menu.js')}}"></script>
@endpush