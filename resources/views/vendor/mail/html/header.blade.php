<tr>
    <td class="header"  style="padding: 5px 0;
                                    background-color: #ffffff;
                                    border-top: 5px solid #77b842;
                                    width: 100%;
                                    text-align: center;">
        <a href="{{ $url }}">
            {{ $slot }}
        </a>
    </td>
</tr>
