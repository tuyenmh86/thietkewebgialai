@extends('site.layouts.main')
@section('content')
    <!--Page Title-->
    <section class="page-title" style="background-image:url(site/images/background/bg-page-title-1.jpg);">
    	<div class="auto-container">
        	<div class="clearfix">
            	<!--Title -->
            	<div class="title-column">
                	<h1>Contact</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column">
                    <ul class="bread-crumb clearfix">
                        <li><a href="index.html">Home</a></li>
                        <li class="active">Contact Us</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <!--Contact Section-->
    <section class="contact-section">
        <div class="auto-container">
            <div class="row clearfix">

                <!--Contact Column-->
                <div class="form-column col-md-8 col-sm-12 col-xs-12">

                    <!--Contact Form Form-->
                    <div class="default-form contact-form">
                    	<h2>SEND US MESSAGE</h2>
                        <form method="post" action="" id="contact-form">
                        	<div class="row clearfix">
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="username" value="" placeholder="Name">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" name="email" value="" placeholder="Email">
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                     <input type="text" name="subject" value="" placeholder="Subject">
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <textarea name="message" placeholder="Your Message"></textarea>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="theme-btn btn-style-two">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>

                <!--Contact Column-->
                <div class="contact-column col-md-4 col-sm-12 col-xs-12">

                    <!--Contact Info-->
                    <div class="contact-info">
                    	<h2>ADDRESS</h2>
                    	<ul>
                        	<li class="address"><div class="icon"><span class="flaticon-map"></span></div> 10, Mc Donald Ave, Sunset Park, Brooklyn, Newyork</li>
                            <li><div class="icon"><span class="flaticon-technology"></span></div> +87 456 1230</li>
                            <li><div class="icon"><span class="flaticon-note"></span></div> Email: info@tallinnfinancial.com</li>

                        </ul>
                    </div>

                </div>

            </div>
        </div>
    </section>


    <!--Map Section-->
    <section class="map-section">
    	<!--Map Box-->
        <div class="map-box">
            <!--Map Canvas-->
            <div class="map-canvas"
                data-zoom="8"
                data-lat="-37.817085"
                data-lng="144.955631"
                data-type="roadmap"
                data-hue="#ffc400"
                data-title="Envato"
                data-content="Melbourne VIC 3000, Australia<br><a href='mailto:info@youremail.com'>info@youremail.com</a>">
            </div>
        </div>
    </section>
@stop
@section('script')
    <!--Google Map APi Key-->
    <script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>
    <script src="site/js/map-script.js"></script>
@endsection
