@extends('site.layouts.main')
@section('content')
    <!--Page Title-->
    <section class="page-title" style="background-image:url(site/images/background/bg-page-title-1.jpg);">
    	<div class="auto-container">
        	<div class="clearfix">
            	<!--Title -->
            	<div class="title-column">
                	<h1>BLOG</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column">
                    <ul class="bread-crumb clearfix">
                        <li><a href="index.html">Home</a></li>
                        <li class="active">Blog</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <!--News Section-->
    <section class="news-section">
    	<div class="auto-container">
            <div class="row clearfix">

                <!--News Style One-->
                <div class="news-style-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<figure class="image"><a href="blog-details.html"><img src="site/images/resource/blog-image-1.jpg" alt=""></a></figure>
                        </div>
                        <div class="lower-content">
                        	<div class="post-meta">
                            	<ul class="clearfix">
                                	<li><a href="#"><span class="fa fa-calendar"></span> Sep 14, 2016</a></li>
                                    <li><a href="#"><span class="fa fa-comment-o"></span> 4 Comments</a></li>
                                </ul>
                            </div>
                            <h4><a href="blog-details.html">Find the right property for you</a></h4>
                            <div class="text-desc">Great turbulent clouds, kindling renet the energy hidden in matter prime number, rich in heavy atoms venture courage of our rig veda. </div>
                            <div class="more-link"><a href="blog-details.html"><span class="fa fa-long-arrow-right"></span> <span class="txt">Read More</span></a></div>
                        </div>
                    </div>
                </div>

                <!--News Style One-->
                <div class="news-style-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<figure class="image"><a href="blog-details.html"><img src="site/images/resource/blog-image-2.jpg" alt=""></a></figure>
                        </div>
                        <div class="lower-content">
                        	<div class="post-meta">
                            	<ul class="clearfix">
                                	<li><a href="#"><span class="fa fa-calendar"></span> Sep 15, 2016</a></li>
                                    <li><a href="#"><span class="fa fa-comment-o"></span> 3 Comments</a></li>
                                </ul>
                            </div>
                            <h4><a href="blog-details.html">Luxury home design ideas</a></h4>
                            <div class="text-desc">Great turbulent clouds, kindling renet the energy hidden in matter prime number, rich in heavy atoms venture courage of our rig veda. </div>
                            <div class="more-link"><a href="blog-details.html"><span class="fa fa-long-arrow-right"></span> <span class="txt">Read More</span></a></div>
                        </div>
                    </div>
                </div>

                <!--News Style One-->
                <div class="news-style-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<figure class="image"><a href="blog-details.html"><img src="site/images/resource/blog-image-3.jpg" alt=""></a></figure>
                        </div>
                        <div class="lower-content">
                        	<div class="post-meta">
                            	<ul class="clearfix">
                                	<li><a href="#"><span class="fa fa-calendar"></span> Sep 19, 2016</a></li>
                                    <li><a href="#"><span class="fa fa-comment-o"></span> 6 Comments</a></li>
                                </ul>
                            </div>
                            <h4><a href="blog-details.html">A new residential project launched</a></h4>
                            <div class="text-desc">Great turbulent clouds, kindling renet the energy hidden in matter prime number, rich in heavy atoms venture courage of our rig veda. </div>
                            <div class="more-link"><a href="blog-details.html"><span class="fa fa-long-arrow-right"></span> <span class="txt">Read More</span></a></div>
                        </div>
                    </div>
                </div>

                <!--News Style One-->
                <div class="news-style-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<figure class="image"><a href="blog-details.html"><img src="site/images/resource/blog-image-4.jpg" alt=""></a></figure>
                        </div>
                        <div class="lower-content">
                        	<div class="post-meta">
                            	<ul class="clearfix">
                                	<li><a href="#"><span class="fa fa-calendar"></span> Sep 14, 2016</a></li>
                                    <li><a href="#"><span class="fa fa-comment-o"></span> 4 Comments</a></li>
                                </ul>
                            </div>
                            <h4><a href="blog-details.html">Micro homes on the rise</a></h4>
                            <div class="text-desc">Great turbulent clouds, kindling renet the energy hidden in matter prime number, rich in heavy atoms venture courage of our rig veda. </div>
                            <div class="more-link"><a href="blog-details.html"><span class="fa fa-long-arrow-right"></span> <span class="txt">Read More</span></a></div>
                        </div>
                    </div>
                </div>

                <!--News Style One-->
                <div class="news-style-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<figure class="image"><a href="blog-details.html"><img src="site/images/resource/blog-image-5.jpg" alt=""></a></figure>
                        </div>
                        <div class="lower-content">
                        	<div class="post-meta">
                            	<ul class="clearfix">
                                	<li><a href="#"><span class="fa fa-calendar"></span> Sep 15, 2016</a></li>
                                    <li><a href="#"><span class="fa fa-comment-o"></span> 3 Comments</a></li>
                                </ul>
                            </div>
                            <h4><a href="blog-details.html">Tips to sell your property</a></h4>
                            <div class="text-desc">Great turbulent clouds, kindling renet the energy hidden in matter prime number, rich in heavy atoms venture courage of our rig veda. </div>
                            <div class="more-link"><a href="blog-details.html"><span class="fa fa-long-arrow-right"></span> <span class="txt">Read More</span></a></div>
                        </div>
                    </div>
                </div>

                <!--News Style One-->
                <div class="news-style-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<figure class="image"><a href="blog-details.html"><img src="site/images/resource/blog-image-6.jpg" alt=""></a></figure>
                        </div>
                        <div class="lower-content">
                        	<div class="post-meta">
                            	<ul class="clearfix">
                                	<li><a href="#"><span class="fa fa-calendar"></span> Sep 19, 2016</a></li>
                                    <li><a href="#"><span class="fa fa-comment-o"></span> 6 Comments</a></li>
                                </ul>
                            </div>
                            <h4><a href="blog-details.html">Commercial property guides</a></h4>
                            <div class="text-desc">Great turbulent clouds, kindling renet the energy hidden in matter prime number, rich in heavy atoms venture courage of our rig veda. </div>
                            <div class="more-link"><a href="blog-details.html"><span class="fa fa-long-arrow-right"></span> <span class="txt">Read More</span></a></div>
                        </div>
                    </div>
                </div>

                <!--News Style One-->
                <div class="news-style-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<figure class="image"><a href="blog-details.html"><img src="site/images/resource/blog-image-7.jpg" alt=""></a></figure>
                        </div>
                        <div class="lower-content">
                        	<div class="post-meta">
                            	<ul class="clearfix">
                                	<li><a href="#"><span class="fa fa-calendar"></span> Sep 14, 2016</a></li>
                                    <li><a href="#"><span class="fa fa-comment-o"></span> 4 Comments</a></li>
                                </ul>
                            </div>
                            <h4><a href="blog-details.html">Manhattan new farm house</a></h4>
                            <div class="text-desc">Great turbulent clouds, kindling renet the energy hidden in matter prime number, rich in heavy atoms venture courage of our rig veda. </div>
                            <div class="more-link"><a href="blog-details.html"><span class="fa fa-long-arrow-right"></span> <span class="txt">Read More</span></a></div>
                        </div>
                    </div>
                </div>

                <!--News Style One-->
                <div class="news-style-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<figure class="image"><a href="blog-details.html"><img src="site/images/resource/blog-image-8.jpg" alt=""></a></figure>
                        </div>
                        <div class="lower-content">
                        	<div class="post-meta">
                            	<ul class="clearfix">
                                	<li><a href="#"><span class="fa fa-calendar"></span> Sep 15, 2016</a></li>
                                    <li><a href="#"><span class="fa fa-comment-o"></span> 3 Comments</a></li>
                                </ul>
                            </div>
                            <h4><a href="blog-details.html">Decorate your homes</a></h4>
                            <div class="text-desc">Great turbulent clouds, kindling renet the energy hidden in matter prime number, rich in heavy atoms venture courage of our rig veda. </div>
                            <div class="more-link"><a href="blog-details.html"><span class="fa fa-long-arrow-right"></span> <span class="txt">Read More</span></a></div>
                        </div>
                    </div>
                </div>

                <!--News Style One-->
                <div class="news-style-one col-md-4 col-sm-6 col-xs-12">
                	<div class="inner-box">
                    	<div class="image-box">
                        	<figure class="image"><a href="blog-details.html"><img src="site/images/resource/blog-image-9.jpg" alt=""></a></figure>
                        </div>
                        <div class="lower-content">
                        	<div class="post-meta">
                            	<ul class="clearfix">
                                	<li><a href="#"><span class="fa fa-calendar"></span> Sep 19, 2016</a></li>
                                    <li><a href="#"><span class="fa fa-comment-o"></span> 6 Comments</a></li>
                                </ul>
                            </div>
                            <h4><a href="blog-details.html">Residential project at miami</a></h4>
                            <div class="text-desc">Great turbulent clouds, kindling renet the energy hidden in matter prime number, rich in heavy atoms venture courage of our rig veda. </div>
                            <div class="more-link"><a href="blog-details.html"><span class="fa fa-long-arrow-right"></span> <span class="txt">Read More</span></a></div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Styled Pagination -->
            <div class="styled-pagination">
                <ul>
                    <li><a href="#" class="active">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#" class="next">Next</a></li>
                </ul>
            </div>

        </div>
    </section>
@stop
