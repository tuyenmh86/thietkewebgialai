@extends('site.layouts.main')
@section('content')
@include('site.layouts.heading')
  @if(Cart::count() > 0)
   <div class="woocommerce-area padding-bottom">
    {{--  <div class="container">  --}}
      <div class="row">
        <div class="col-md-12 col-sm-12"> 
          <!-- Section Title -->
          <div class="cart-heading">
            <h2>Giỏ hàng</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <form action="#" method="post">
            <div class="table-responsive"> 
              <!-- Start Cart Products List -->
              <table class="shop-cart-table table-hover table-bordered">
                <thead>
                  <tr>
                    <th class="product" colspan="3">Sản phẩm</th>
                    <th class="product-price">Giá</th>
                    <th class="product-quantity">Số lượng</th>
                    <th class="product-total">Tổng cộng</th>
                  </tr>
                </thead>
                <tbody>
                  
                  @foreach($carts as $item)
                    <tr>
                      <td class="product-img">
                        <a href="javascript::void(0)"> <img src="{{ $item->options->image }}" alt=""> </a></td>
                      <td class="product-name"><a href="javascript::void(0)"> {{ $item->name }} </a></td>
                      <td class="product-remove"><a class="remove-item" data-id="{{ $item->rowId }}" href="javascript::void(0)" data-toggle="tooltip" data-placement="auto" title="Remove"> <i class="icon_close " aria-hidden="true"></i> </a></td>
                      <td class="product-price"> {{ price_format($item->price) }} </td>
                      <td class="product-quantity-form"><div class="cart-plus-minus">
                          <input data-id="{{ $item->rowId }}" class="cart-plus-minus-box" type="text" name="qtybutton" min="1" value="{{ $item->qty }}">
                        </div></td>
                      <td class="product-total"> {{ price_format($item->price * $item->qty) }} </td>
                      </tr>
                  @endforeach
                  
                </tbody>
              </table>
              <!-- /End Cart Products List --> 
              <!-- Start Cart Action Buttons -->
              <div class="cart-bottom-btn">
                <div class="row">
                  <div class="col-md-6 col-sm-4">
                    <div class="cart-btn cart-con-shop"> <a href="/" class="submit-btn"> Tiếp tục mua hàng </a> </div>
                  </div>
                  <div class="col-md-6 col-sm-8">
                    <div class="cart-btn cart-clear-update"> <a href="javascript::void(0)" class="clear-cart submit-btn"> Xóa giỏ hàng </a> <a href="" class="submit-btn update-cart"> Cập nhập giỏ hàng</a> </div>
                  </div>
                </div>
              </div>
              <!-- /End Cart Action Buttons --> 
            </div>
          </form>
        </div>
      </div>
      
      <!-- Start Cart Total Calculation Area -->
      <div class="row">
        <div class="cart-total-area section-padding clearfix"> 
          <div class="col-md-6 col-sm-12">
            <div class="cart-total-right-calculation">
              <div class="calcuation border-spacing">
                <h3 class="total">Tổng cộng: <span class="ammount">{{ Cart::subTotal() }}</span></h3>
              </div>
              <div class="checkout-btn clearfix"> <a href="{{ route('product.checkout') }}" class="submit-btn"> Thanh toán </a> </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /End Cart Total Calculation Area --> 
      
    {{--  </div>  --}}
  </div>
  @else
    <center>
        <h3>Giỏ hàng trống.</h3>
            <div class="cart-btn cart-con-shop"> <a href="/" class="submit-btn"> Tiếp tục mua hàng </a> 
            </div>
    </center><br>
  @endif
@stop


@push('footer')
<script type="text/javascript">
    $(document).ready(function(){

        $('.remove-item').click(function(){
            var rowId = $(this).attr('data-id');
            axios.post('{{route('product.cart.removeItem')}}', {
                params: {
                    rowId: rowId
                }
            })
            .then(function (response) {
                window.location.reload();
            })
            .catch(function (error) {
                 toastr.error('Xóa sản phẩm thất bại.Vui lòng thử lại.');
            });

        });

        $('.clear-cart').click(function(){
            var rowId = $(this).attr('data-id');
            axios.post('{{route('product.cart.destroy')}}')
            .then(function (response) {
                location.reload();
            })
            .catch(function (error) {
                 toastr.error('Xóa giỏ hàng thất bại.Vui lòng thử lại.');
            });
        });

        $('.qtybutton').click(function(){
            var qty = $('.cart-plus-minus-box').val();
            var rowId = $('.cart-plus-minus-box').attr('data-id');

            axios.post('{{route('product.cart.update.qty')}}', {
                qty: qty,
                rowId: rowId
            })
            .then(function (response) {
                toastr.success('Cập nhập số lượng thành công.');
                window.location.reload();
            })
            .catch(function (error) {
                 toastr.error('Cập nhập số lượng thất bại.Vui lòng thử lại.');
            });
        });
    });
</script>
@endpush