@extends('site.layouts.main')
@section('content')
@include('site.layouts.heading')
<div class="checkout-page">
    <div class="checkout-area padding-bottom">
    {{--  <div class="container">  --}}
      <div class="row"> 
        <div class="col-md-12 col-sm-12">
          <div class="main-accordian-area">
            <div class="chekout-steps">
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default single-accordion">
                  <div class="panel-heading" role="tab" id="headingSix">
                    <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix"> 1.Hóa đơn </a> </h4>
                  </div>
                  <div id="collapseSix" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingSix">
                    <div class="panel-body">
                      <div class="row">
                        <div class="col-md-12 col-sm-12">
                          <div class="order-review table-responsive">
                            <table class="table-bordered order-table">
                              <thead>
                                <tr>
                                  <td class="product-name"> Tên sản phẩm </td>
                                  <td class="product-price"> Giá </td>
                                  <td class="product-quantity"> Số lượng </td>
                                  <td class="product-subtotal"> Thành tiền </td>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($carts as $item)
                                  <tr>
                                    <td class="product-name"> {{ $item->name }} </td>
                                    <td class="product-price"> {{ price_format($item->price) }} </td>
                                    <td class="product-quantity"> {{ $item->qty }} </td>
                                    <td class="product-subtotal"> {{ price_format($item->price * $item->qty) }} </td>
                                  </tr>
                                @endforeach
                                <tr class="grand-total-row">
                                  <td colspan="3">
                                    {{--  <p class="total-text"> subtotal </p>
                                    <p class="total-text"> Shipping & Handling (Flat Rate - Fixed) </p>  --}}
                                    <p class="total-text grand-total"> Tổng tiền </p></td>
                                  <td>{{--<p class="total-text highlighted-text"> $1024 </p>
                                    <p class="total-text highlighted-text"> $125 </p> --}}
                                    <p class="total-text grand-total highlighted-text"> {{ Cart::subtotal() }} </p></td>
                                </tr>
                                <tr>
                                  <td colspan="4"><div class="order-review-bottom">
                                      <p class="edit-item"> Bạn muốn mua thêm sản phẩm khác nữa ? <a href="/">Tiếp tục mua hàng </a> </p>
                                      {{--  <button class="place-order-btn submit-btn"> Place Order </button>  --}}
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel panel-default single-accordion">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo"> 2.Thông tin của bạn </a> </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                      <div class="row">
                        <div class="billing-form">
                          <form action="{{ route('finished.checkout') }}" method="POST">
                            {!! csrf_field() !!}
                            <div class="col-md-6 col-sm-12">
                              <div class="form-group">
                                <label>Tên <sup>*</sup></label>
                                <input type="text" name="first_name" placeholder="ví dụ: Tuyên" required>
                              </div>
                              <div class="form-group">
                                <label>Tên công ty</label>
                                <input type="text" name="company_name" placeholder="Ví dự: Công ty TNHH Quốc Thái">
                              </div>
                              <div class="form-group">
                                <label>Địa chỉ<sup>*</sup></label>
                                <input type="text" name="address" placeholder="Ví dụ: 72 Lạc Long Quân - P. Thắng Lợi - TP. Pleiku - Gia Lai" required>
                              </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                              <div class="form-group">
                                <label>Họ<sup>*</sup></label>
                                <input type="text" name="last_name" placeholder="Ví dụ: Mai Hà" required>
                              </div>
                              <div class="form-group">
                                <label>E-Mail<sup>*</sup></label>
                                <input type="text" name="email" placeholder="ví dụ: vidu@gmail.com" required>
                              </div>
                              <div class="form-group">
                                <label>Số điện thoại<sup>*</sup></label>
                                <input type="text" name="phone" placeholder="ví dụ: 0978737568" required>
                              </div>
                              <div class="form-group">
                                <div class="continue-btn clearfix">
                                  <button type="submit" class="submit-btn">Hoàn thành</button>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /End Checkout Steps Accordian --> 
      </div>
    {{--  </div>  --}}
  </div>
</div>

@stop