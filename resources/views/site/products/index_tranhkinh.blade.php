@extends('site.layouts.main')
@section('title',$title)
@section('description',$desc)
@section('content')
@include('site.layouts.heading')
<section class="product-section section-padding">



   @php
   $sub_categories_list = get_list_product_categories(true,$category->id);
   @endphp


   @if(count($sub_categories_list)>0)
    
    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
        <div class="main-products-area">
            <div class="col-xs-12 danh-muc-title">{{$category->language('title')}}</div>
            <div class="product-grid">
                @foreach($sub_categories_list as $key=>$value)
                <div class="col-xs-6 col-sm-4 col-md-3 cate-item">
                <div class="cate-img">
                <a href="{{$value->link()}}" title="{{$value->language('title')}}">
                    <img src="{{$value->featured_image}}" alt="{{$value->language('title')}}">
                    </a>
                </div>
                <div class="cate-title">
                    <a href="{{$value->link()}}" title="{{$value->language('title')}}">{{ $value->language('title') }}</a>
                </div>
                </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>

   @else
   <div class="main-products-area col-md-12">
        <div class="col-xs-12 danh-muc-title">{{$category->language('title')}}</div>
   @foreach($products as $product)
   
    <div class="products-area col-xs-6 col-sm-3 col-md-3 col-lg-3">
        <div class="wrp_item_small product-col wrapper-item">
            <div class="product-box">
                <div class="product-thumbnail">
                <div class="sale-flash new">

                    @if($product->percent_price())
                    -{{ $product->percent_price() }}
                    @else
                        New
                    @endif
                </div>
                <a class="image_link display_flex" href="{{$product->link()}}" title="{{$title. " " .$product->language('title') }}">
                {{-- <img alt="{{$title. " " .$product->language('title') }}" src="{{$product->featured_image}}" data-lazyload="{{$product->featured_image}}"> --}}
                <img alt="{{$title. " " .$product->language('title') }}" class="lazy img-responsive screenshot" title="{{$title." " .$product->language('title') }}" data-original="{{ $product->featured_image }}"> 
                <noscript>
                    <img class="lazy img-responsive screenshot" title="{{ $product->featured_image }}" src="{{ $product->featured_image }}"> 
            </noscript>
                </a>
                {{-- <div class="summary_grid hidden-xs hidden-sm hidden-md" onclick="window.location.href='{{$product->link()}}';">
                    <div class="rte description">
                        
                        <p>Mặt bàn: 1600 x&nbsp;815 x 30mmH<br>
                            Sofa bộ Italia :&nbsp;2850x1800x900 mm</p>
                                        
                    </div>
                </div> --}}
                {{-- <div class="product-action-grid clearfix">
                    <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="{{$product->id}}" enctype="multipart/form-data">
                        <div>
                            
                            <input class="hidden" type="hidden" name="variantId" value="14992946">
                            <button class="btn-cart button_wh_40 left-to" title="Tùy chọn" type="button" onclick="window.location.href='{{$product->link()}}'">
                                Chi tiết
                            </button> --}}
                            
                            {{--  <a title="Xem nhanh" href="{{$product->link()}}" data-handle="sofa-da-ma-ntx1824" class="button_wh_40 btn_view right-to quick-view">
                                Xem nhanh
                            </a>  --}}
{{--         
                        </div>
                     </form> 
                </div> --}}

                </div>
                <div class="product-info effect a-left">
                <div class="info_hhh">
                    <h3 class="product-name product-name-hover"><a href="{{$product->link()}}" title="Sofa bộ Italia - Canova">{{ $product->language('title') }}</a></h3>
                    <div class="price-box clearfix">
                        @if($product->price > 0)		
                        <span class="price product-price">{{ price_format($product->promotion_price) }}</span>

                        <span class="price product-price-old">
                            {{ price_format($product->price) }}	
                        </span>
                            @if($product->percent_price())
                            <span class="sale-off"> 
                                    -{{ $product->percent_price() }}
                            </span>
                            @endif
                           
                        @else
                            Liên hệ
                        @endif
                    </div>
                    {{--  <div class="product-action"> <a class="add-to-cart" data-id="{{ $product->id }}" href="javascript::void(0)">Thêm vào giỏ hàng</a></div>  --}}
                </div>
                </div>
            </div>
        </div>
    </div>

   @endforeach
   </div>

   @endif
   <center>{{ $products->links() }}</center>
</section>
@stop

