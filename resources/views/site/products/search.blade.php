@extends('site.layouts.main')
@section('content')
    <!--Page Title-->
    @include('site.layouts.heading')
    <!--Sidebar Page-->
    <div class="clearfix"></div>
    <div class="product-grid">
        <div class="product-grid">
            @if(count($products) > 0)
                
            <!-- Start Single Product -->
            @foreach($products as $product)

            <div class="single-product col-md-3 col-lg-3 col-sm-4 col-xs-6">
                    <div class="single-product-border">
                            <div class="single-product-top">
                                    <a class="link-to-detail" href="{{ $product->link() }}"> 
                                            <img class="lazy img-responsive screenshot" title="{{str_replace("/thumbs/","/",$product->featured_image)}}" data-original="{{ $product->featured_image }}"> 
                                            <noscript>
                                                    <img class="lazy img-responsive screenshot" title="{{ $product->featured_image }}" src="{{ $product->featured_image }}"> 
                                            </noscript>
                                    </a>
                            </div>
                            <div class="single-product-bottom">
                                    <div class="product-title">
                                                    <a href="{{ $product->link() }}" class="link-btn-to-detail btn btn-link" style="">{{ $product->language('title') }}</a>
                                    </div>
                                    <div class="product-title">
                                                    <a type="button" class="btn btn-link modal_link" data-toggle="modal" data-target="#modal-{{$product->id}}" style="">Đặt hàng </a>
                                    </div>
                            </div>
                    </div>

                    <div id="modal-{{$product->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                            <h4 id="myModalLabel">Bạn đã chọn mua sản phẩm: {{ $product->language('title') }}</h4>
                                            <h5 id="myModalLabel">Vui lòng cho chúng tôi biết thông tin của bạn</h5>
                                    </div>
                                    <div class="modal-body">
                                            <form class="form-horizontal col-sm-12" action="{{ route('contact.mail')}}" id="form_contact" method="post">
                                            {!! csrf_field() !!}
                                            <div class="form-group">
                                            <input id="product_id" type="hidden" name="product_id"  value="{{ $product->language('title') }}"/>
                                            <input class="form-control required" name="name" placeholder="Tên của bạn:" data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." type="text">
                                            </div>
                                            <div class="form-group">
                                            <textarea class="form-control" name="txtMessage" placeholder="Lời nhắn cho chúng tôi.." data-placement="top" data-trigger="manual"></textarea>
                                            </div>
                                            <div class="form-group">
                                            <input class="form-control email" name="email" placeholder="Nhập email của bạn " data-placement="top" data-trigger="manual" data-content="Must be a valid e-mail address (user@gmail.com)" type="text">
                                            </div>
                                            <div class="form-group">
                                            <input class="form-control phone" name="phone" placeholder="Số điện thoại của bạn" data-placement="top" data-trigger="manual" data-content="Must be a valid phone number (999-999-9999)" type="text">
                                            </div>
                                            <div class="form-group">
                                            <button type="submit" class="btn btn-success">Gửi</button>
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">Hủy</button>
                                            <p class="help-block pull-left text-danger hide" id="form-error">  The form is not valid. </p>
                                            </div>
                                            </form>
                                    </div>
                                    <div class="modal-footer"></div>
                                    </div>
                            </div>
                    </div>

                    </div>
            @endforeach
            <!-- End Single Product --> 
            @else
                <div class="container">{{ trans('front-app.no_results') }}</div>
            @endif
            <div class="col-md-12 text-center">
                <div class="pagination-box text-center section padT30">
                    {{ $products->appends(request()->only('q'))->links() }}
                </div>
            </div>
        </div>
    </div>
@stop
