@extends('site.layouts.main')
@section('title',$product->language('title') ? $category->language('title') ." ". $product->language('title') : $product->language('title'))
@section('description',$product->language('seo_description') ? $product->language('description') : $product->language('description'))
@section('keywords',$product->language('seo_keywords'))
@section('content')
    <!--Page Title-->
    @include('site.layouts.heading')

    <div class="single-product-deatils-1">
          <div class="single-product-details-area">
            {{--  <div class="container">  --}}
              <div class="single-product-info"> 
                <!-- Start Product Details Image -->
                <div class="col-md-7 col-sm-12 col-xs-12 lrpadding5 ">
                  <div class="single-product-large-image">
                    <div class="tab-content">
                        @if($product->image_list)
                            @foreach($product->image_list as $key => $image)
                                <div role="tabpanel" class="tab-pane {{ $loop->first ? 'active' : '' }} fade in" id="single-img-{{$key}}"> 
                                    {{--  <img src="{{ $image }}" alt="{{$product->categories->title}}">   --}}
                                    <img src="{{$image }}" alt="{{ $category->language('title') }} {{$product->title}}" title="{{ $category->language('title') }} {{$product->title}}"> 
                                </div>
                            @endforeach
                        @else
                            <div role="tabpanel" class="tab-pane active fade in" id="single-img-1"> 
                                <img src="{{ $product->featured_image }}" alt="{{$product->featured_image}}"> 
                            </div>
                        @endif
                    </div>
                  </div>
                      @if($product->image_list)
                          <div class="single-product-small-image col-md-12 col-sm-12 col-xs-12 col-lg-12">
                            <ul class="sm-img-nav" role="tablist">
                                @foreach($product->image_list as $key => $image)
                                    <li role="presentation" class="col-md-3 col-sm-3 col-xs-12 col-lg-3 {{ $loop->first ? 'active' : '' }}"> 
                                        <a href="#single-img-{{$key}}" aria-controls="single-img-{{$key}}" role="tab" data-toggle="tab"> 
                                            <img src="{{ $image }}" alt=""> 
                                        </a> 
                                    </li>
                                @endforeach
                            </ul>
                          </div>
                      @endif
                </div>
                <!-- /End Product Details Image --> 
                <!-- Start Product Details Content -->
                <div class="col-md-5 col-sm-12 col-xs-12 lrpadding5">
                  <div class="product-details"> 
                    <!-- Product Name -->
                    <h2 class="product-name"> <a href="{{ $product->link() }}">{{ $product->language('title') }}</a> </h2>
                    <div class="product-price-area">
                        <p class="product-price">
                            @if($product->price > 0)
                                 @if($product->promotion_price > 0 && $product->promotion_price < $product->price )
                                    {{ price_format($product->promotion_price) }}
                                    <span class="old-price">{{ $product->price }}</span>
                                @else
                                    {{ price_format($product->price) }}
                                @endif
                            @else
                                <a href="{{ url('lien-he') }}">Liên hệ</a> 
                            @endif
                        </p>
                    </div>
                    <!-- Products Short Description -->
                    <p class="product-details-content">{{ $product->language('description') }}</p>
                    <!-- Product Stock Availability -->
                    <p class="availability">Mã sản phẩm: <span class="instock">#{{ $product->sku }}</span></p>
                    @if($product->price > 0)
                        <!-- Products Fillter Options -->
                        
                        <form class="contact-form" action="{{ route('contact.mail')}}" id="form_contact" method="post">
                        {!! csrf_field() !!}
                        <div class="fillter-option">
                          <div class="cart-quantity">
                            <p>Số lượng</p>
                            <div class="cart-plus-minus">
                              <input class="cart-plus-minus-box quantity" type="text" name="qtybutton" value="1" min="1">
                            <div class="dec qtybutton">-</div><div class="inc qtybutton">+</div></div>
                          </div>
                        </div>
                        <!-- Product Action Buttons -->
                        </form>  
                        <div class="product-action"> 
                            <a class="add-to-cart" data-id="{{$product->id}}" href="javascript::void(0)">Thêm vào giỏ hàng</a>
                        </div>
                        <!-- Product Share Options -->
                    @else
                        <div class="product-action"> 
                            <a href="{{ url('lien-he') }}">Liên hệ</a> 
                        </div>
                    @endif


                    <div class="share-post clearfix">
                      <h3>Chia sẻ</h3>
                      <div class="social-media">
                        <ul>
                          <li> <a href="#"> <i class="social_twitter"></i> </a> </li>
                          <li> <a href="#"> <i class="social_facebook"></i> </a> </li>
                          <li> <a href="#"> <i class="social_linkedin"></i> </a> </li>
                          <li> <a href="#"> <i class="social_googleplus "></i> </a> </li>
                          <li> <a href="#"> <i class="social_pinterest"></i> </a> </li>
                        </ul>
                      </div>
                    </div>
                    <form class="contact-form" action="{{ route('contact.mail')}}" id="form_contact" method="post">
                            {!! csrf_field() !!}
                            <div class="fillter-option">
                                <div class="form-group pull-left ">
                                    <div class="box">
                                    <label for="phone" style="color:red;font-weight:600">Để lại số điện thoại chúng tôi sẽ gọi lại ngay!</label>
                                     <input id="product_id" type="hidden" name="product_id"  value="{{ $product->language('title') }}"/>
                                    <input id="phone" class="col-xs-9 col-sm-9 col-md-9 pull-left" type="text" name="phone" required />
                                    <input id="submit_message"  class="btn btn-send col-xs-3 col-sm-3 col-md-3" type="submit" value="Gửi"/>
                                    </div>
                                </div>
                                
                            </div>
                            </form>
                    
                  </div>
                </div>
                <!-- /End Product Details Content --> 
              </div>
             <!-- Start Products Descriptions Tabs -->
            <div class="row">
                <div class="product-descriptions-tabs clearfix padding-bottom">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <ul class="descriptions-tabs-nav" role="tablist">
                      <li role="presentation" class="active"> <a href="#description" aria-controls="description" role="tab" data-toggle="tab"> Chi tiết </a> </li>
                      <li role="presentation"> 
                          <a href="#ungdung" aria-controls="ungdung" role="tab" data-toggle="tab"> Ứng dụng sản phẩm </a> 
                        </li>
                        <li role="presentation"> 
                                <a href="#huong-dan-thi-cong" aria-controls="huong-dan-thi-cong" role="tab" data-toggle="tab"> Hướng dẫn thi công </a> 
                              </li>
                      <li role="presentation"> <a href="#comment" aria-controls="comment" role="tab" data-toggle="tab"> Bình luận </a> </li>
                    </ul>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="tab-content descriptions-content">
                      <div role="tabpanel" class="tab-pane active fade in" id="description">
                       
                            {!! $product->language('content') !!}
                        
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="ungdung">
                            {!! $product->language('ung-dung') !!}
                      </div>
                      <div role="tabpanel" class="tab-pane fade" id="huong-dan-thi-cong">
                            {!! $product->language('huong-dan-thi-cong') !!}
                      </div>
                      
                      <div role="tabpanel" class="tab-pane fade" id="comment">
                            <div class="fb-comments" data-href="{{ $product->link() }}" data-width="100%" data-numposts="5"></div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /End Products Descriptions Tabs --> 
              </div>
              
          </div>
          <div class="clearfix"></div>
          <div class="related">
                @include('site.layouts.related_product',['product_id' => $product->categories[0]->id])
            </div> 
    
@stop

@push('footer')

<script type="text/javascript">
    $(document).ready(function(){
        $('.add-to-cart').click(function(){
            var data =  $(this).attr('data-id');
            var qty =   $('.quantity').val();

           $.ajax({
              url: '{{route('product.addToCart')}}',
              type: 'POST',
              dataType: 'json',
              data: {'_token': '{{ csrf_token() }}','data': data,'qty': qty},
              success: function(data){
                  toastr.success(data.status);
                  $('.cart_count').html(data.cart_count);
              }
           });
        });
    });
</script>

<script type="text/javascript">
 
    $(document).ready(function(){
        $('.sm-img-nav img,.single-product-large-image img').each(function () {
            let title = $(this).prop('title');
            $(this).wrap($('<a/>', {

                href: $(this).attr('src'),
                
                class: "fancybox",

            }));

            $('.fancybox').attr('data-fancybox','images');
            $('.fancybox').attr('data-caption',title);
        });

        $('[data-fancybox="images"]').fancybox({
            infobar : true,
            caption : function( instance, item ) {
                let caption = $(this).data('caption') || '';
                return ( caption.length ? caption + '<br />' : '' ) + 'Image <span data-fancybox-index></span> of <span data-fancybox-count></span>';
            },
            thumbs : {
                autoStart : true
            }
            
        })

    });

</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.add-to-cart').click(function(){
            var data =  $(this).attr('data-id');
            var qty =   $('.quantity').val();

           $.ajax({
              url: '{{route('product.addToCart')}}',
              type: 'POST',
              dataType: 'json',
              data: {'_token': '{{ csrf_token() }}','data': data,'qty': qty},
              success: function(data){
                  toastr.success(data.status);
                  $('.cart_count').html(data.cart_count);
              }
           });
        });
    });
</script>
<script type="text/javascript">
    // Zoom One
     jQuery(document).ready(function(){
            $(".zoom").elevateZoom({
                tint:true,
                tintColour:'#000',
                tintOpacity:0.5,
                zoomWindowWidth:375,
                zoomWindowHeight:330,
                responsive: true,
                easing : true
            });

            //Zoom Two
            $("#zoom-2").elevateZoom({
                responsive: true,
                tint:true,
                tintColour:'#000000',
                tintOpacity:0.4,
                easing: true,
                zoomWindowWidth:450,
                zoomWindowHeight:274,
                borderSize: 1,
                borderColour: '#252525',
                gallery:'images-menu',
                cursor: 'pointer',
                galleryActiveClass: "active"
            });

            $("#zoom-2").bind("click", function(e) {
                var ez = $('#zoom-2').data('elevateZoom');
                $.fancybox(ez.getGalleryList());
                return false; 
            });

            //Zoom Three
            $("#zoom-3").elevateZoom({
                responsive: true,
                zoomType: 'inner',
                easing: true,
                zoomWindowWidth:450,
                zoomWindowHeight:274,
                borderSize: 1,
                borderColour: '#252525',
                gallery:'images-menu',
                cursor: 'pointer',
                galleryActiveClass: "active"
            }); 
        });
</script>
@endpush
