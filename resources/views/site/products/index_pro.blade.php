@extends('site.layouts.main')
@section('title',$title)
@section('description',$desc)
@section('content')
@include('site.layouts.heading')
<section class="product-section section-padding">
   @php
   $sub_categories_list = get_list_product_categories(true,$category->id);
   @endphp
   {{-- @if(count($sub_categories_list)>0)
   <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12 hidden-xs">
      <aside class="aside-item sidebar-category collection-category">
         <div class="aside-title">
            <h2 class="title-head margin-top-0"><span> {{ trans('front-app.product-category') }}</span></h2>
         </div>
         <div class="aside-content">
            <nav class="nav-category navbar-toggleable-md">
               <ul class="nav navbar-pills">
                  @foreach($sub_categories_list as $key=>$value)
                  <li class="nav-item lv1">
                     <a href="{{$value->link()}}" class="nav-link">{{$value->language('title')}}</a>
                  </li>
                  @endforeach
               </ul>
            </nav>
         </div>
      </aside>
   </div>
   @endif --}}
   
      <div class="row">
          <div class="container">
                <h1 class="danh-muc-title">{{$category->language('title')}}</h1>
                <p> {!!$category->language('description')!!}</p>
          </div>
      </div>

      @if(count($sub_categories_list)>0)
      <div class="main-products-area">
      
        @foreach($sub_categories_list as $key=>$sub_categories_list_item)
      <div class="row">
         <div class="col-md-12 col-sm-12">
            <!-- Section Title  -->
            <div class="cate-title">
               <h2 class="title_link">
               <a class="title_link" href="{{$sub_categories_list_item->link()}}">{{$sub_categories_list_item->language('title')}}</a>
               </h2>
            </div>
         </div>
      </div>
        <div class="products-area">
         @foreach(get_list_products(true,$sub_categories_list_item->id)  as $product)
         <div class="product-box">
            <div class="product-thumbnail">
               <div class="sale-flash new">
                  @if($product->percent_price())
                  -{{ $product->percent_price() }}
                  @else
                  New
                  @endif
               </div>
               <a class="image_link display_flex" href="{{$product->link()}}" title="Sofa bộ Italia - Canova">
               <img alt="{{$title. " " .$product->language('title') }}" src="{{$product->featured_image}}" data-lazyload="{{$product->featured_image}}">
               </a>
            </div>
            <div class="product-info effect a-left">
               <div class="info_hhh">
                  <h3 class="product-name product-name-hover"><a href="{{$product->link()}}" title="Sofa bộ Italia - Canova">{{ $product->language('title') }}</a></h3>
                  <div class="price-box clearfix">
                     @if($product->price > 0)		
                     <span class="price product-price">{{ price_format($product->promotion_price) }}</span>
                     <span class="price product-price-old">
                     {{ price_format($product->price) }}	
                     </span>
                     @if($product->percent_price())
                     <span class="sale-off"> 
                     -{{ $product->percent_price() }}
                     </span>
                     @endif
                     @else
                     Liên hệ
                     @endif
                  </div>
               </div>
            </div>
         </div>
         @endforeach
      </div>
      @endforeach

      </div>

      @else
      <div class="main-products-area">
      <div class="products-area-1">
          <div class="row display-flex">
        @foreach(get_list_products(true,$category->id)  as $product)
         
         <div class="col-md-3 col-lg-3 col-xs-6 border" style="padding:5px;">
            <div class="product-box">
                <div class="product-thumbnail">
                <div class="sale-flash new">
                    @if($product->percent_price())
                    -{{ $product->percent_price() }}
                    @else
                    New
                    @endif
                </div>
                <a class="image_link display_flex" href="{{$product->link()}}" title="Sofa bộ Italia - Canova">
                <img alt="{{$title. " " .$product->language('title') }}" src="{{$product->featured_image}}" data-lazyload="{{$product->featured_image}}">
                </a>
                </div>
                <div class="product-info effect a-left">
                <div class="info_hhh">
                    <h3 class="product-name product-name-hover"><a href="{{$product->link()}}" title="Sofa bộ Italia - Canova">{{ $product->language('title') }}</a></h3>
                    <div class="price-box clearfix">
                        @if($product->price > 0)		
                        <span class="price product-price">{{ price_format($product->promotion_price) }}</span>
                        <span class="price product-price-old">
                        {{ price_format($product->price) }}	
                        </span>
                        @if($product->percent_price())
                        <span class="sale-off"> 
                        -{{ $product->percent_price() }}
                        </span>
                        @endif
                        @else
                        {{-- Liên hệ --}}
                        @endif
                    </div>
                </div>
                </div>
            </div>
         </div>
         @endforeach
          </div>
      </div>

      </div>
      @endif

</section>
@stop