@extends('site.layouts.main')
@section('title',$page->language('seo_title') ? $page->language('seo_title') : $page->language('title'))
@section('description',$page->language('seo_description') ? $page->language('seo_description') : $page->language('description'))
@section('keywords',$page->language('seo_keywords'))

@section('content')
	@include('site.layouts.heading')
    <!--Page <Title--></Title-->
	{{--  <div class="container">  --}}
		<div class="clearfix"></div>
    	<div class="content"> {!! $page->language('content') !!}</div>

        <div class="share-post clearfix">
          <h3>Chia sẽ</h3>
          <div class="social-media">
            <ul>
              <li> <a href="#"> <i class="social_twitter"></i> </a> </li>
              <li> <a href="#"> <i class="social_facebook"></i> </a> </li>
              <li> <a href="#"> <i class="social_linkedin"></i> </a> </li>
              <li> <a href="#"> <i class="social_googleplus "></i> </a> </li>
              <li> <a href="#"> <i class="social_pinterest"></i> </a> </li>
            </ul>
          </div>
        </div>
		
		<div class="clearfix"></div>
	    	<div class="fb-comments" data-href="{{ $page->link() }}" data-width="100%" data-numposts="5"></div>
	{{--  </div>     --}}
@stop
