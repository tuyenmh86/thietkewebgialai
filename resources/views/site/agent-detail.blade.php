@extends('site.layouts.main')
@section('content')
    <!--Page Title-->
    <section class="page-title" style="background-image:url(site/images/background/bg-page-title-1.jpg);">
    	<div class="auto-container">
        	<div class="clearfix">
            	<!--Title -->
            	<div class="title-column">
                	<h1>AGENT DETAILS</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column">
                    <ul class="bread-crumb clearfix">
                        <li><a href="index.html">Home</a></li>
                        <li class="active">Agent Details</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>


    <!--Agent Details-->
    <div class="agent-details">
    	<div class="auto-container">

            <!--Basic Details-->
            <section class="basic-details">
            	<div class="title"><h2>CONTACT AGENT</h2></div>
            	<div class="row clearfix">
                	<!--Details Column-->
                    <div class="details-column col-md-7 col-sm-12 col-xs-12">
                    	<div class="row clearfix">
                        	<!--Image Column-->
                            <div class="image-column col-md-5 col-sm-4 col-xs-12">
                            	<figure class="image"><img src="site/images/resource/team-image-7.jpg" alt=""></figure>
                            </div>
                            <!--Content Column-->
                            <div class="content-column col-md-7 col-sm-8 col-xs-12">
                            	<div class="inner">
                                	<div class="upper-info">
                                    	<h3>Eva Rodriges</h3>
                                        <div class="designation">Real Estate Agent</div>
                                    </div>
                                    <div class="description">Cosmic ocean science Tunguska event the only home we ever known Orion's sword, concept of the number one billions.</div>
                                    <ul class="contact-info">
                                        <li><div class="icon"><span class="fa fa-envelope"></span></div> eva@companyname.com</li>
                                        <li><div class="icon"><span class="fa fa-phone"></span></div> 147 025 3689</li>
                                        <li><div class="icon"><span class="fa fa-globe"></span></div> www.companyname.com</li>
                                    </ul>
                                    <ul class="social-links">
                                        <li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                                        <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                        <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                                        <li><a href="#"><span class="fa fa-linkedin"></span></a></li>
                                        <li><a href="#"><span class="fa fa-instagram"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Form Column-->
                    <div class="form-column col-md-5 col-sm-12 col-xs-12">
                    	<div class="inner">
                    		<div class="default-form agent-contact-form">
                                <form method="post" action="http://t.commonsupport.com/marseille/contact.html">
                                    <div class="clearfix">
                                        <div class="form-group">
                                        	<input type="text" name="field-name" value="" placeholder="Name" required>
                                        </div>
                                        <div class="form-group">
                                        	<input type="email" name="field-name" value="" placeholder="Email" required>
                                        </div>
                                        <div class="form-group">
                                        	<textarea name="field-name" placeholder="Message" required></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="theme-btn btn-style-two">SEND MESSAGE</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--Agent Properties-->
            <section class="agent-properties">
            	<div class="title"><h2>EVA’S PROPERTIES</h2></div>
            	<div class="row clearfix">
                	<!--Default Property Box-->
                    <div class="default-property-box col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="property-details.html"><img src="site/images/resource/featured-image-9.jpg" alt=""></a></figure>
                                <div class="upper-info clearfix">
                                    <div class="property-label">Sale</div>
                                    <div class="add-fav"><a href="#"><span class="fa fa-heart-o"></span></a></div>
                                </div>
                                <div class="property-price">$ 3,240,000</div>
                            </div>
                            <div class="lower-content">
                                <div class="property-title">
                                    <h3><a href="property-details.html">Enfield Homes, VC Street</a></h3>
                                    <div class="location">Miami</div>
                                </div>
                                <div class="text-desc">Former high end beach home converted to Class “A” office space. 2519 sq ft complete with three and 1/2 baths. Large ...</div>
                                <div class="property-specs">
                                    <ul class="clearfix">
                                        <li><span class="icon flaticon-bed-1"></span> 6</li>
                                        <li><span class="icon flaticon-vintage-bathtub"></span> 4</li>
                                        <li><span class="icon flaticon-car-inside-a-garage"></span> 3</li>
                                        <li><span class="icon flaticon-blog-template"></span> 1240 sq ft</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Default Property Box-->
                    <div class="default-property-box col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="property-details.html"><img src="site/images/resource/featured-image-10.jpg" alt=""></a></figure>
                                <div class="upper-info clearfix">
                                    <div class="property-label">Rent</div>
                                    <div class="add-fav"><a href="#"><span class="fa fa-heart-o"></span></a></div>
                                </div>
                                <div class="property-price">$ 450 / month</div>
                            </div>
                            <div class="lower-content">
                                <div class="property-title">
                                    <h3><a href="property-details.html">Citywharf Property, Lemans Street</a></h3>
                                    <div class="location">California</div>
                                </div>
                                <div class="text-desc">Former high end beach home converted to Class “A” office space. 2519 sq ft complete with three and 1/2 baths. Large ...</div>
                                <div class="property-specs">
                                    <ul class="clearfix">
                                        <li><span class="icon flaticon-bed-1"></span> 5</li>
                                        <li><span class="icon flaticon-vintage-bathtub"></span> 3</li>
                                        <li><span class="icon flaticon-car-inside-a-garage"></span> 2</li>
                                        <li><span class="icon flaticon-blog-template"></span> 1550 sq ft</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Default Property Box-->
                    <div class="default-property-box col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image-box">
                                <figure class="image"><a href="property-details.html"><img src="site/images/resource/featured-image-11.jpg" alt=""></a></figure>
                                <div class="upper-info clearfix">
                                    <div class="property-label">Sale</div>
                                    <div class="add-fav"><a href="#"><span class="fa fa-heart-o"></span></a></div>
                                </div>
                                <div class="property-price">$ 1,450,700</div>
                            </div>
                            <div class="lower-content">
                                <div class="property-title">
                                    <h3><a href="property-details.html">Foxtons Apartment, E1 Street</a></h3>
                                    <div class="location">San Francisco</div>
                                </div>
                                <div class="text-desc">Former high end beach home converted to Class “A” office space. 2519 sq ft complete with three and 1/2 baths. Large ...</div>
                                <div class="property-specs">
                                    <ul class="clearfix">
                                        <li><span class="icon flaticon-bed-1"></span> 4</li>
                                        <li><span class="icon flaticon-vintage-bathtub"></span> 3</li>
                                        <li><span class="icon flaticon-car-inside-a-garage"></span> 2</li>
                                        <li><span class="icon flaticon-blog-template"></span> 1130 sq ft</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
@stop
