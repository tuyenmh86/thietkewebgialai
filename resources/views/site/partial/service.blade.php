

 <section class="lib-section-2 clearfix">	
	<section class="fw section_privacy">
		<div class="row">
			<div class="privacy">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="item icon_sm">
						<div class="privacy_icon privacy_bg_icon_1">
                            <span class="service-icon"> <img src="/site/assets/img/transport.svg" class="img-responsive" width="40px" height="32px"/> </span>
						</div>
						<div class="privacy_info">
							<h4 class="privacy_title fw">
								Giao hàng miễn phí
							</h4>
							<div class="privacy_desc fw">
								Đơn hàng trên 500k
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="item icon_sm">
						<div class="privacy_icon privacy_bg_icon_1">
                            <img src="/site/assets/img/policy.svg" class="img-responsive" width="40px" height="32px"/>
						</div>
						<div class="privacy_info">
							<h4 class="privacy_title fw">
								Bảo đảm chất lượng
							</h4>
							<div class="privacy_desc fw">
                                Sản phẩm bảo đảm chất lượng
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="item ">
						<div class="privacy_icon privacy_bg_icon_1">
                            <img src="/site/assets/img/support.svg" class="img-responsive" width="40px" height="32px"/>
						</div>
						<div class="privacy_info">
							<h4 class="privacy_title fw">
								Tư vấn trực tuyến
							</h4>
							<div class="privacy_desc fw">
								Hỗ trợ 24/7
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="item ">
						<div class="privacy_icon privacy_bg_icon_1">
                            <span class="service-icon"> <img src="/site/assets/img/Z.svg" class="img-responsive" width="40px" height="32px"/></span>
						</div>
						<div class="privacy_info">
							<h4 class="privacy_title fw">
								Thanh toán
							</h4>
							<div class="privacy_desc fw">
								Bảo mật thanh toán
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
</section>
</section>