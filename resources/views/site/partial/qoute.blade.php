<section class="lib-section-12">	
    
    <section class="fw section_testimonials">
    
        <div class="viewed-list">
        
            <div class="item">
                <div class="testimonial_image">
                    <img class="img-responsive" src="//bizweb.dktcdn.net/thumb/small/100/311/132/themes/661007/assets/testimonial_image_2.jpg?1526658693954" data-lazyload="//bizweb.dktcdn.net/thumb/small/100/311/132/themes/661007/assets/testimonial_image_2.jpg?1526658693954" alt="craig-walsh-portland-oregon">
                </div>
                <div class="testimonial_comment">
                    <span>
                        Mẫu mã, chất lượng thật tuyệt với 
                    </span>
                </div>
                <div class="testimonial_desc">
                    <span>Anh Nguyễn Văn Trường - Yên Thế - Pleiku - Gia Lai</span>
                </div>
            </div>
            
            <div class="item">
                <div class="testimonial_image">
                    <img class="img-responsive" src="//bizweb.dktcdn.net/thumb/small/100/311/132/themes/661007/assets/testimonial_image_2.jpg?1526658693954" data-lazyload="//bizweb.dktcdn.net/thumb/small/100/311/132/themes/661007/assets/testimonial_image_2.jpg?1526658693954" alt="craig-walsh-portland-oregon">
                </div>
                <div class="testimonial_comment">
                    <span>Mẫu mã đẹp, phong cách hiện đại, tôi rất ưng ý</span>
                </div>
                <div class="testimonial_desc">
                    <span>Chị Nguyễn Thu Hà - Hùng Vương - Pleiku</span>
                </div>
            </div>
            
            <div class="item">
                <div class="testimonial_image">
                    <img class="img-responsive" src="//bizweb.dktcdn.net/thumb/small/100/311/132/themes/661007/assets/testimonial_image_2.jpg?1526658693954" data-lazyload="//bizweb.dktcdn.net/thumb/small/100/311/132/themes/661007/assets/testimonial_image_2.jpg?1526658693954" alt="craig-walsh-portland-oregon">
                </div>
                <div class="testimonial_comment">
                    <span>Tôi thật sự yên tâm khi mua hàng tại Hoàn Mỹ Gia Lai !!</span>
                </div>
                <div class="testimonial_desc">
                    <span>Mr Hoàng - Giám đốc thietkewebgialai.net</span>
                </div>
            </div>

        </div>
    </section>
</section>