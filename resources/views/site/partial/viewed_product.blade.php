
@php    
    $viewed = session('viewed_posts', []);
    @endphp
    @if($viewed>0)
    <h4 class="sideLtitle spTitle">{{ trans('front-app.viewed_products') }}</h4>
        <div class="viewed-list">
            @foreach ($viewed as $id=>$time)
                @php
                        $item = get_viewed_products($id);
                @endphp
                            <div class="product-box">
                                        <div class="product-thumbnail">
                                           <div class="sale-flash new">Mới</div>
                                           <a class="image_link display_flex" href="{{ $item->link() }}" title="{{ $item->language('title') }}">
                                           <img src="{{$item->featured_image}}" alt="{{ $item->language('title') }}">
                                           </a>
                                           {{--  <div class="summary_grid hidden-xs hidden-sm hidden-md" onclick="window.location.href='/sofa-da-ma-ntx1824';">
                                              <div class="rte description">
                                                 <p>
                                                     Mặt bàn: 1600 x&nbsp;815 x 30mmH
                                                     <br>
                                                    Sofa bộ Italia :&nbsp;2850x1800x900 mm
                                                 </p>
                                              </div>
                                           </div>  --}}
                                           {{-- <div class="product-action-grid clearfix">
                                              <form action="/cart/add" method="post" class="variants form-nut-grid" data-id="product-actions-9387021" enctype="multipart/form-data">
                                                 <div>
                                                    <input class="hidden" type="hidden" name="variantId" value="14992946">
                                                    <button class="btn-cart button_wh_40 left-to" title="Tùy chọn" type="button" onclick="window.location.href='/sofa-da-ma-ntx1824'">
                                                    Tùy chọn
                                                    </button>
                                                    <a title="Xem nhanh" href="/sofa-da-ma-ntx1824" data-handle="sofa-da-ma-ntx1824" class="button_wh_40 btn_view right-to quick-view">
                                                    Xem nhanh
                                                    </a>
                                                 </div>
                                              </form>
                                           </div> --}}
                                        </div>
                                        <div class="product-info effect a-left">
                                           <div class="info_hhh">
                                              <h3 class="product-name product-name-hover"><a href="{{ $item->link()}}" title="{{ $item->language('title') }}">{{ $item->language('title') }}</a></h3>
                                              <div class="price-box clearfix">		
                                                  @if($item->price)
                              
                                                 <span class="price product-price">{{ price_format($item->price) }} ₫</span>
                                                 <span class="price product-price-old">
                                                 {{ price_format($item->promotion_price) }}₫			
                                                 </span>
                                                 @else
                                                  <span class="price product-price">Liên hệ</span>
                                                 @endif
                                                 @if($item->percent_price())
                                                 <span class="sale-off"> 
                                                          -{{ $item->percent_price() }}%
                                                 </span>
                                                 @endif
                                              </div>
                                              {{-- <div class="reviews-product-grid">
                                                 <div class="bizweb-product-reviews-badge" data-id="9387021">
                                                    <div class="bizweb-product-reviews-star" data-score="0" data-number="5" title="Not rated yet!" style="color: rgb(255, 190, 0);"><i data-alt="1" class="star-off-png" title="Not rated yet!"></i>&nbsp;<i data-alt="2" class="star-off-png" title="Not rated yet!"></i>&nbsp;<i data-alt="3" class="star-off-png" title="Not rated yet!"></i>&nbsp;<i data-alt="4" class="star-off-png" title="Not rated yet!"></i>&nbsp;<i data-alt="5" class="star-off-png" title="Not rated yet!"></i><input name="score" type="hidden" readonly=""></div>
                                                    <div>
                                                       <p>0</p>
                                                    </div>
                                                    <div><img src="https://productreviews.bizwebapps.vn//assets/images/user.png" width="18" height="17"></div>
                                                 </div>
                                              </div> --}}
                                           </div>
                                        </div>
                                     </div>
                              
                              <div class="product-box-modal">
                                  
                              </div>
            @endforeach
        </div>
@endif
