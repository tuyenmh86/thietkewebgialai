<div id="section-verticalmenu" class="active-desk block block-verticalmenu float-vertical float-vertical-left">
		<div class="bg-vertical"></div>
		<h4 class="block-title float-vertical-button">
		   {{-- <span class="verticalMenu-toggle"></span> --}}
		   <span class="verticalMenu-text">Danh mục sản phẩm</span>	
		</h4>
		<div class="block_content">
		   <div id="verticalmenu" class="verticalmenu" role="navigation">
			  <ul class="nav navbar-nav nav-verticalmenu">
				 @php 
				 $category = get_list_product_categories(true,null,null,null,'featured_asc');
				 @endphp
				 @foreach($category as $category_item)
				 <li class="parent vermenu-option-2 dropdown">
					<a class="link-lv1" href="{{$category_item->link()}}">
					{{-- <i class="fa fa-male"></i> --}}
					<span class="menu-icon">
					<span class="menu-title">{{$category_item->language('title')}}</span>
					</span>
					@php
					$sub_cate = get_list_product_categories(true,$category_item->id,null,null,'sort_asc');
					@endphp
					@if(count($sub_cate))
					<b class="dropdown-toggle fa fa-angle-right button-verticalmenu" data-toggle="dropdown" aria-expanded="false"></b>
					</a>
					<div class="dropdown-menu level1 sub-ver sub-vermegamenu-1" style="width:230px;">
					   <div class="dropdown-menu-inner">
						  <div class="row">
							 <div class="mega-col col-sm-12">
								<div class="menu-items">
								   <div class="row">
									  <div class="col-sm-12 padding-small menu-clearfix">
										 <div class="mega-col-inner ">
											{{-- 
											<div class="menu-title"><a href="/phu-kien-nam">Phụ kiện nam</a></div>
											--}}
											<div class="widget-inner">
											   <ul class="nav-links">
												  @foreach ($sub_cate as $item)
												  <li><a href="{{$item->link()}}" title="{{$item->language('title')}}">{{$item->language('title')}}</a></li>
												  @endforeach
											   </ul>
											</div>
										 </div>
									  </div>
								   </div>
								</div>
							 </div>
						  </div>
					   </div>
					</div>
					@else
					</a>
					@endif
				 </li>
				 @endforeach
			  </ul>
		   </div>
		</div>
	 </div>