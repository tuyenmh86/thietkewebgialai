@if(count(slide('galary')))

<section id="project_section" class="section general-row">
		<section id="room-dimention">
				<div class="section-title text-center">
					<span>Công trình sử dụng sản phẩm</span>
					<h2><span>Hoàn Mỹ Gia Lai</span></h2>
					
						
				</div>
				<ul class="list grid galary">
                    @foreach(slide('galary') as $key => $item)
                        <li class="galaryslide col-md-12 text-center animate wow fadeInDown" style="visibility: visible; animation-name: fadeInDown; padding:5px;">
								<img class="img-responsive lazy img-responsive galaryImg" title="{{ $item->caption}}" src="{{ $item->image }}">
								<div class="utilities_padding">{{ $item->caption}}</div>
                        </li>
                    @endforeach
					
				</ul>
		</section>




	<div class="clearfix"></div>

</section>
{{-- 
@push('footer')

    <script type="text/javascript">

        $(document).ready(function(){
            $('#project_section img').each(function () {
				let title = $(this).prop('title');
				$(this).wrap($('<a/>', {
	
					href: $(this).attr('src'),
					
					class: "fancybox",
	
				}));
	
				$('.fancybox').attr('data-fancybox','images');
				$('.fancybox').attr('data-caption',title);
			});
	
			$('[data-fancybox="images"]').fancybox({
				infobar : true,
				caption : function( instance, item ) {
					let caption = $(this).data('caption') || '';
					return ( caption.length ? caption + '<br />' : '' ) + 'Image <span data-fancybox-index></span> of <span data-fancybox-count></span>';
				},
				thumbs : {
					autoStart : true
				}
				
			})

        });

    </script>

@endpush --}}

@endif