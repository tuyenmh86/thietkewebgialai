<div class="sidebar-box">

        <div class="col-xs-12">

            <h4 class="marB20">Bài viết nổi bật</h4>

            @foreach(get_list_posts(true,null,5,'featured') as $item)

                <div class="feature-post marB10">

                    <div class="col-md-4 col-sm-4 col-xs-4 padL0">

                        <figure>

                            <a href="{{ $item->link() }}"><img src="{{ $item->featured_image }}" alt=""></a>

                        </figure>

                    </div>

                    <div class="col-md-8 col-sm-8 col-xs-8 padL0 padR0 text-left">

                        <p>{{ $item->language('title') }}</p>

                        <p><a href="{{ $item->link() }}" class="theme-color"> {{ trans('front-app.details') }}</a></p>

                    </div>

                </div>

            @endforeach

        </div>

    </div>