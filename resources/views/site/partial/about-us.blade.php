<section id="about-section" class="parallax-section">
			<div class="about">
				<div class="row">
					<div class="col-lg-6 col-md-6 text-center-sm text-center-xs">
						<!-- .img-holder -->
						<div class="img-holder">
                            {{-- <iframe width="100%" height="315" src="https://www.youtube.com/embed/1ZS8M7UvAIk" frameborder="0" allow=" encrypted-media" allowfullscreen></iframe> --}}
							<img src="/photos/tin-tuc/toa_nha_duc_long.jpg" alt="image">
						</div><!-- /.img-holder -->
					</div>
					<!-- .about-text -->
					<div class="col-lg-6 col-md-6 about-text">
						<!-- .section-title -->
						<div class="about-title text-center">
							<span>Đức Long Gia Lai</span>
                            <h2><span>Xưởng sản xuất số 2 </span></h2>		
                            <div style="text-align:justify;padding:10px;">
							Sản xuất và chế biến gỗ: Chế biến đồ gỗ nội - ngoại thất cao cấp là một trong những ngành truyền thống, có năng lực, uy tín và thương hiệu của Đức Long Gia Lai. Sản phẩm đồ gỗ Đức Long Gia Lai chất lượng cao, phong phú về chủng loại và mẫu mã, giá thành hợp lý được khách hàng trong nước và nước ngoài ưa chuộng.


						</div>		
						</div><!-- /.section-title -->
						
						<!-- .room-amount -->
						
						<!-- /.room-amount -->
						<a href="/trang-noi-dung/gioi-thieu-31337" class="viewmore sec1">Xem thêm</a>
					</div><!-- /.about-text -->
				</div>
			</div>
</section>
