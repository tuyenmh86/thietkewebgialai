<section class="countto clearfix">	
          <div class="col-md-4 col-sm-6 col-xs-4 col-lg-4">
             <div class="wow fadeIn animated" style="visibility: visible;">
                   <i class="wow zoomIn animated" style="visibility: visible; animation-name: zoomIn; animation-delay: 0s;" data-wow-delay="0"></i>
                    <img class="count" src="/site/assets/img/location.png" alt="Mẫu thiết kế"/>
                   <h1 class="text-center"><span class="timer" data-from="0" data-to="100" data-speed="2000" data-refresh-interval="50"></span> <span>Mẫu giao diện</span></h1>
                  
             </div>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-4 col-lg-4">
            <div class="wow fadeIn animated" style="visibility: visible;">
                  <i class="wow zoomIn animated" style="visibility: visible; animation-name: zoomIn; animation-delay: 0s;" data-wow-delay="0"></i>
                  <img class="count" src="/site/assets/img/order_management.png" alt="Dự án hoàn thành"/>
                  <h1 class="text-center"><span class="timer" data-from="0" data-to="20" data-speed="2000" data-refresh-interval="50"></span><span> Dự án</span></h1>
            </div>
         </div>

         <div class="col-md-4 col-sm-6 col-xs-4 col-lg-4">
            <div class="wow fadeIn animated" style="visibility: visible;">
                  <i class="wow zoomIn animated" style="visibility: visible; animation-name: zoomIn; animation-delay: 0s;" data-wow-delay="0"></i>
                  <img class="count" src="/site/assets/img/custommer.png" alt="Khách hàng"/>
                  <h1 class="text-center"><span class="timer" data-from="0" data-to="20" data-speed="2000" data-refresh-interval="50"></span><span> Khách hàng</span></h1>
            </div>
         </div>
</section>

 @push('footer')
<script src="/site/assets/js/jquery.countTo.js"></script> 
 <script type="text/javascript">
    $('.timer').countTo();
 </script>
 @endpush