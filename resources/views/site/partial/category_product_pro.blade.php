<section class="awe-section-3">	
	<section id="danh-muc">
	{{--  <div class="container">  --}}
		<div class="row">
			    <div class="col-xs-12 danh-muc-title" style="color:white;">Danh mục sản phẩm Hoàn Mỹ Gia Lai</div>
                @php 
                    $category = get_list_product_categories(true,null,null,null,'featured_asc');
                @endphp
                 <div class="cate-item">
                @foreach($category as $category_item)
               
                    <div class="item" style="padding:5px;">

                        <figure>
                            <a href="{{$category_item->link()}}" target="_self"><img src="{{$category_item->featured_image}}" alt="{{$category_item->language('title')}} Gia lai"></a>
                            <figcaption>
                            <h3 style="padding:10px 0 10px 0;"><a href="{{$category_item->link()}}" target="_self">{{$category_item->language('title')}}</a></h3>
                            <div class="desc">{!!$category_item->description!!}</div>
                            </figcaption>
                        </figure>
                        @php
                        $sub_categories_list = get_list_product_categories(true,$category_item->id);
                        @endphp
                        @if(count($sub_categories_list)>0)
                        <ul>
                            @foreach($sub_categories_list as $key=>$value)
                            <li><a href="{{$value->link()}}" title="{{ $value->language('title') }}" target="_self"><i class="fa fa-angle-right"></i> <span>{{ $value->language('title') }}</span></a></li>
                            @endforeach
                        </ul>
                        @endif
                        
                    </div>
                

                @endforeach
            </div>
	</div>
</section>
</section>