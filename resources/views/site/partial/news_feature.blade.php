<section class="product_section section-padding">
    @foreach($featured_categories_post as $key => $item)
        
        @php
        $category = getViaPostCateId($item->id);
        @endphp
        <div class="latest-news-area padding-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <!-- Section Title -->
                        <div class="section-title">
                            <h2>{{ $category->language('title') }}</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="latest-news-list">
                        <!-- Start Single Blog -->
                        @foreach(get_list_posts(true,$item->id) as $item2)
                            <div class="col-lg-12">
                                <div class="single-latest-news">
                                    <div class="single-latest-news-top">
                                        <a href="{{ $item2->link() }}"> <img src="{{ $item2->featured_image }}" alt=""> </a>
                                        <div class="latest-news-time"> <span class="date">06</span> <span class="month">May,</span> <span class="year">2016</span> </div>
                                    </div>
                                    <div class="single-latest-news-bottom">
                                        <h2 class="latest-news-title"><a href="{{ $item2->link() }}">{{ $item2->language('title') }}</a></h2>
                                        <p class="latest-news-details">{{ $item2->language('description') }}</p>
                                        <a href="{{ $item2->link() }}" class="latest-news-read-more"> Xem thêm <i class="fa fa-angle-double-right"></i> </a> 
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <!-- /End Single Blog -->
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</section>