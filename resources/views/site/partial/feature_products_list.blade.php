

<section class="list-product-container clearfix">
        
        @foreach($featured_category as $key => $item)

                @php
                $category = getViaProductCateId($item->id);
              
                @endphp


                @if($category)
                <div class="wapper-list">
                <!-- Start Products Row -->
                        <div class="title-block">
                                <div class="wrap-content">
                                        <h2 class="title-group"title="Bán {{ $category->language('title') }} tại Hà Nội và Tp HCM">
                                                {{ $category->language('title') }}
                                        </h2>
                                </div>
                        </div>
                        <div class="wapper_item">
                                @foreach(get_list_products(true,$category->id,12,'pos_asc') as $product)

                                <div class="single-product col-md-3 col-lg-3 col-sm-4 col-xs-6">
                                        <div class="single-product-border">
                                                <div class="single-product-top">
                                                        <a class="link-to-detail" href="{{ $product->link() }}"> 
                                                                <img alt="{{ $category->language('title')." " .$product->language('title') }}" class="lazy img-responsive screenshot" title="{{ $category->language('title')." " .$product->language('title') }}" data-original="{{ $product->featured_image }}"> 
                                                                <noscript>
                                                                        <img class="lazy img-responsive screenshot" title="{{ $product->featured_image }}" src="{{ $product->featured_image }}" alt="{{$category->language('title') }}"> 
                                                                </noscript>
                                                        </a>
                                                </div>
                                                <div class="single-product-bottom">
                                                        <div class="product-title">
                                                                        <a href="{{ $product->link() }}" class="link-btn-to-detail btn btn-link" style=""><h3 class="h3s14"> {{$product->language('title') }}</h3></a>
                                                        </div>
                                                        <div class="product-title">
                                                                        <a type="button" class="btn btn-link modal_link" data-toggle="modal" data-target="#modal-{{$item->id}}" style="">Đặt hàng </a>
                                                        </div>
                                                </div>
                                        </div>
                                        <div id="modal-{{$item->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                        <div class="modal-content">
                                                        <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                                                                <h4 id="myModalLabel">Bạn đã chọn mua sản phẩm: {{ $product->language('title') }}</h4>
                                                                <h5 id="myModalLabel">Vui lòng cho chúng tôi biết thông tin của bạn</h5>
                                                        </div>
                                                        <div class="modal-body">
                                                                <form class="form-horizontal col-sm-12" action="{{ route('contact.mail')}}" id="form_contact" method="post">
                                                                {!! csrf_field() !!}
                                                                <div class="form-group">
                                                                <input id="product_id" type="hidden" name="product_id"  value="{{ $product->language('title') }}"/>
                                                                <input class="form-control required" name="name" placeholder="Tên của bạn:" data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." type="text">
                                                                </div>
                                                                <div class="form-group">
                                                                <textarea class="form-control" name="txtMessage" placeholder="Lời nhắn cho chúng tôi.." data-placement="top" data-trigger="manual"></textarea>
                                                                </div>
                                                                <div class="form-group">
                                                                <input class="form-control email" name="email" placeholder="Nhập email của bạn " data-placement="top" data-trigger="manual" data-content="Must be a valid e-mail address (user@gmail.com)" type="text">
                                                                </div>
                                                                <div class="form-group">
                                                                <input class="form-control phone" name="phone" placeholder="Số điện thoại của bạn" data-placement="top" data-trigger="manual" data-content="Must be a valid phone number (999-999-9999)" type="text">
                                                                </div>
                                                                <div class="form-group">
                                                                <button type="submit" class="btn btn-success">Gửi</button>
                                                                <button class="btn" data-dismiss="modal" aria-hidden="true">Hủy</button>
                                                                <p class="help-block pull-left text-danger hide" id="form-error">  The form is not valid. </p>
                                                                </div>
                                                                </form>
                                                        </div>
                                                        <div class="modal-footer"></div>
                                                        </div>
                                                </div>
                                        </div>
                                </div>

                                @endforeach
                        </div>
                        

                        <div class="clearfix text-center">
                                
                                <a class="btn-view-more" href="{{route('products.category', ['alias' => $category->alias])}}">Xem tất cả</a>
                                
                        </div>

                <!-- end Products Row -->
                </div>
                @endif

        @endforeach      
</section>