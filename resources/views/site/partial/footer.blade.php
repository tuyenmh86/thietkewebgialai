<footer class="footer">		
        <div class="site-footer">		
            <div class="container">			
                <div class="footer-inner padding-top-25 padding-bottom-10">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="footer-widget widget1">
                                <h3><span>Giới thiệu</span></h3>
                                <p>SKT Decor &amp; Interior sẽ giúp bạn hiện thực căn nhà mơ ước từ trong ý tưởng trở thành chốn đi về thân thương sau những lo toan cuộc sống.</p>
                                <ul class="list-menu">
                                    <li><a href="/lien-he"><i class="fas fa-map-marker-alt"></i>191 Nguyễn Thị Nhỏ, Phường 9, Tân Bình, Hồ Chí Minh</a></li>
                                    <li><a href="tel:0932325681"><i class="fa fa-phone"></i> 0932325681</a></li>
                                    <li><a href="mailto:dichvupr@gmail.com"><i class="fa fa-envelope"></i>dichvupr@gmail.com</a></li>
                                </ul>
                                <ul class="inline-list social-icons">
      
        <li>
          <a class="icon-fallback-text" href="#">
            <i class="fab fa-twitter"></i>
            <span class="fallback-text" hidden="hidden">Twitter</span>
          </a>
        </li>
      
      
        <li>
          <a class="icon-fallback-text" href="#">
            <i class="fab fa-facebook-f"></i>
            <span class="fallback-text" hidden="hidden">Facebook</span>
          </a>
        </li>
      
      
        <li>
          <a class="icon-fallback-text" href="#">
            <i class="fab fa-pinterest-p"></i>
            <span class="fallback-text" hidden="hidden">Pinterest</span>
          </a>
        </li>
      
      
        <li>
          <a class="icon-fallback-text" href="#" rel="publisher">
            <i class="fab fa-google"></i>
            <span class="fallback-text" hidden="hidden">Google</span>
          </a>
        </li>
      
      
        <li>
          <a class="icon-fallback-text" href="#">
            <i class="fab fa-instagram"></i>
            <span class="fallback-text" hidden="hidden">Instagram</span>
          </a>
        </li>
      
      
      
      
      
    </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <div class="footer-widget">
                                <h3><span>Hướng dẫn</span></h3>
                                
                                <article>
                                    <a href="/khoac-ao-moi-cho-phong-khach-don-he" class="article-thumb" title="Khoác áo mới cho phòng khách đón hè">
                                        
                                        <picture>
                                            <source srcset="//bizweb.dktcdn.net/thumb/small/100/314/759/articles/1.jpg?v=1527521156167" media="(max-width: 480px)">										
                                            <img src="//bizweb.dktcdn.net/thumb/small/100/314/759/articles/1.jpg?v=1527521156167" alt="Khoác áo mới cho phòng khách đón hè">
                                        </picture>							
                                        
                                    </a>
                                    <div class="article-info">
                                        <h3 class="blog-item-name"><a title="Khoác áo mới cho phòng khách đón hè" href="/khoac-ao-moi-cho-phong-khach-don-he">Khoác áo mới cho phòng khách đón hè</a></h3>
                                        <div class="post-info">
                                            <span><i class="far fa-clock"></i>28/05/2018</span>
                                            <span><i class="far fa-comment"></i>0</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </article>
                                
                                <article>
                                    <a href="/xu-huong-thiet-ke-noi-that-trong-nam-2018" class="article-thumb" title="Xu hướng thiết kế nội thất trong năm 2018">
                                        
                                        <picture>
                                            <source srcset="//bizweb.dktcdn.net/thumb/small/100/314/759/articles/18b01001-tckt-vn-03.jpg?v=1527521148050" media="(max-width: 480px)">										
                                            <img src="//bizweb.dktcdn.net/thumb/small/100/314/759/articles/18b01001-tckt-vn-03.jpg?v=1527521148050" alt="Xu hướng thiết kế nội thất trong năm 2018">
                                        </picture>							
                                        
                                    </a>
                                    <div class="article-info">
                                        <h3 class="blog-item-name"><a title="Xu hướng thiết kế nội thất trong năm 2018" href="/xu-huong-thiet-ke-noi-that-trong-nam-2018">Xu hướng thiết kế nội thất trong năm 2018</a></h3>
                                        <div class="post-info">
                                            <span><i class="far fa-clock"></i>28/05/2018</span>
                                            <span><i class="far fa-comment"></i>1</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </article>
                                
                                <article>
                                    <a href="/anh-sang-trong-thiet-ke-noi-that" class="article-thumb" title="Ánh sáng trong thiết kế nội thất">
                                        
                                        <picture>
                                            <source srcset="//bizweb.dktcdn.net/thumb/small/100/314/759/articles/02a.jpg?v=1527521216730" media="(max-width: 480px)">										
                                            <img src="//bizweb.dktcdn.net/thumb/small/100/314/759/articles/02a.jpg?v=1527521216730" alt="Ánh sáng trong thiết kế nội thất">
                                        </picture>							
                                        
                                    </a>
                                    <div class="article-info">
                                        <h3 class="blog-item-name"><a title="Ánh sáng trong thiết kế nội thất" href="/anh-sang-trong-thiet-ke-noi-that">Ánh sáng trong thiết kế nội thất</a></h3>
                                        <div class="post-info">
                                            <span><i class="far fa-clock"></i>28/05/2018</span>
                                            <span><i class="far fa-comment"></i>0</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </article>
                                
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <div class="footer-widget">
                                <h3><span>Ý kiến khách hàng</span></h3>
                                <ul class="owl-carousel testi owl-loaded owl-drag" data-margin="5" data-md-items="1" data-sm-items="1" data-xs-items="1">
                                    
                                <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: 0s; width: 730px;"><div class="owl-item active" style="width: 360px; margin-right: 5px;"><li>
                                        <div class="avt"><img src="//bizweb.dktcdn.net/100/314/759/themes/663587/assets/author1_avt.jpg?1531448932415" alt="THAI NGUYEN">
                                            <p><strong>THAI NGUYEN</strong>
                                                Q7, HCM
                                            </p>
                                        </div>
                                        <div class="comment"><i class="fa fa-quote-left"></i>SKT Decor &amp; Interior đã giúp thiết kế lên ngôi nhà mà vợ chồng tôi hằng mơ ước, tôi rất hài lòng với không gian, tông màu, cách bài trí nội thất các bạn đã tư vấn và thực hiện. Xin cảm ơn!</div>
    
                                    </li></div><div class="owl-item" style="width: 360px; margin-right: 5px;"><li>
                                        <div class="avt"><img src="//bizweb.dktcdn.net/100/314/759/themes/663587/assets/author2_avt.jpg?1531448932415" alt="MY NGOC">
                                            <p><strong>MY NGOC</strong>
                                                Bình Thạnh, HCM
                                            </p>
                                        </div>
                                        <div class="comment"><i class="fa fa-quote-left"></i>Thiết kế đẹp, thi công chuyên nghiệp – đó là cảm nhận khi cty SKT giúp Decor lại căn hộ của tôi. Tuy diện căn hộ nhỏ nhưng nhìn mọi thứ rất hài hòa, dễ thương. Chúc cty ngày càng thành công hơn nữa!</div>
    
                                    </li></div></div></div><div class="owl-nav"><div class="owl-prev disabled">prev</div><div class="owl-next">next</div></div><div class="owl-dots disabled"></div></ul>
                            </div>
                        </div>
    
                    </div>
                </div>
            </div>
        </div>	
        <div class="copyright clearfix">
            <div class="container">
                <div class="inner clearfix">
                    <div class="row">
                        <div class="col-sm-12 a-center copyright-lice">
                            <span>© Bản quyền thuộc về <b>Timomedia.com</b> </span>
                            <span class="fix-line-footer hidden-xs">|</span> 
                            <span>Cung cấp bởi 
                                <a href="https://www.sapo.vn/?utm_campaign=cpn%3Asite_khach_hang-plm%3Afooter&amp;utm_source=site_khach_hang&amp;utm_medium=referral&amp;utm_content=fm%3Atext_link-km%3A-sz%3A&amp;utm_term=&amp;campaign=site_khach_hang" title="Sapo" rel="nofollow" target="_blank">Sapo</a>
                            </span>
                            
                        </div>
                    </div>
                </div>
                
                <div class="back-to-top show"><i class="fa fa-long-arrow-alt-up"></i></div>
                
            </div>
        </div>
    
    </footer>