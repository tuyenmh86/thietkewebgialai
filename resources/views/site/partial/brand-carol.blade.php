<section class="lib-section-14">
   <section class="section_partners fw">
       <div class="brand-logos-carousel">
           @if(slide('partner'))
             @foreach(slide('partner') as $key => $item)
                    <div class="item">
                        <a href="javascript:;">
                        <img class="img-responsive lazy img-responsive galaryImg" title="{{ $item->caption}}" src="{{ $item->image }}">
                        </a>
                    </div>
            @endforeach
            @endif
        </div>
   </section>
</section>