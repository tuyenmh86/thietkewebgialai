<h4 class="sideLtitle spTitle">Đăng ký làm đại lý</h4>
            
<form class="form-horizontal col-sm-12" action="{{ route('contact.mail')}}" id="form_contact" method="post" style="border: 1px solid #ebebebeb;padding:20px;">
    
    {!! csrf_field() !!}
    <div class="form-group">
    
        <input class="form-control required" name="name" placeholder="Tên của bạn:" data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." type="text">
    </div>

    <div class="form-group">
    
        <textarea class="form-control" name="txtMessage" placeholder="Địa chỉ của bạn.." data-placement="top" data-trigger="manual"></textarea>
    
    </div>

    <div class="form-group">
    
        <input class="form-control phone" name="phone" placeholder="Số điện thoại của bạn" data-placement="top" data-trigger="manual" data-content="Must be a valid phone number (999-999-9999)" type="text">
    
    </div>
    
    <div class="form-group">
    
        <button type="submit" class="btn btn-success">Gửi</button>
    
        <button class="btn" data-dismiss="modal" aria-hidden="true">Hủy</button>
    
        <p class="help-block pull-left text-danger hide" id="form-error">  The form is not valid. </p>

    </div>

</form>