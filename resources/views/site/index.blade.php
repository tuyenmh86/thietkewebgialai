@extends('site.layouts.webgialai')
@section('title',getConfigSite('site_title'))
@section('description')
@foreach($featured_category as $description)
{{$description->title . '|'}}
@endforeach 
@stop
@section('keywords')

@foreach($featured_category as $description)
{{$description->title . '|'}}
@endforeach 
@stop
@section('content')
@include('site.layouts.fullwidth_slidercaption')
@include('site.layouts.about')
@include('site.layouts.feature')  
{{-- @include('site.layouts.showcase') --}}
@include('site.layouts.screenshot')
{{-- @include('site.layouts.price') --}}
{{-- @include('site.layouts.team') --}}
@include('site.layouts.download')
@include('site.layouts.blog')

@stop
