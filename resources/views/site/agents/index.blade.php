@extends('site.layouts.main')
@section('title',$title)
@section('description',getConfigSite('site_title').' | '.$title)
@section('keywords',$title)
@section('content')
<!--Page Title-->
@include('site.layouts.heading')
<!--Team Section-->
<section class="team-section">
    <div class="auto-container">

        <div class="row clearfix">
            <!--Team Member-->
            @foreach($agents as $item)
                <div class="team-member col-md-3 col-sm-6 col-xs-12">
                    <div class="inner-box">
                        <figure class="image-box"><img src="{{ $item->profile_image }}" alt="{{ $item->link() }}"></figure>
                        <div class="overlay-box">
                            {!! $item->language('introduce') !!}
                        </div>
                        <div class="lower-content">
                            <div class="inner">
                                <h4><a href="{{ $item->link() }}">{{ $item->full_name }}</a></h4>
                                <div class="designation">{{ trans('front-app.real_astate_agent') }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>

    </div>
</section>
@stop
