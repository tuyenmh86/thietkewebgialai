@extends('site.layouts.main')
@section('title',$title)
@section('description',getConfigSite('site_title').' | '.$title)
@section('keywords',$title)
@section('content')
    <!--Page Title-->
    @include('site.layouts.heading')

    <!--Agent Details-->
    <div class="agent-details">
        <div class="auto-container">

            <!--Basic Details-->
            <section class="basic-details">
                <div class="title"><h2>{{ trans('front-app.agent_contact') }}</h2></div>
                <div class="row clearfix">
                    <!--Details Column-->
                    <div class="details-column col-md-7 col-sm-12 col-xs-12">
                        <div class="row clearfix">
                            <!--Image Column-->
                            <div class="image-column col-md-5 col-sm-4 col-xs-12">
                                <figure class="image"><img src="{{ $agent->profile_image }}" alt=""></figure>
                            </div>
                            <!--Content Column-->
                            <div class="content-column col-md-7 col-sm-8 col-xs-12">
                                <div class="inner">
                                    <div class="upper-info">
                                        <h3>{{ $agent->full_name }}</h3>
                                        {{-- <div class="designation">{{ trans('front-app.real_astate_agent') }}</div> --}}
                                    </div>
                                    <div class="description">
                                        {!! $agent->language('introduce') !!}
                                    </div>
                                    <ul class="contact-info">
                                        <li><div class="icon"><span class="fa fa-envelope"></span></div> {{ $agent->email }}</li>
                                        <li><div class="icon"><span class="fa fa-phone"></span></div> {{ $agent->phone }}</li>
                                        <li><div class="icon"><span class="fa fa-globe"></span></div> {{ $agent->address }}</li>
                                    </ul>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--Form Column-->
                    <div class="form-column col-md-5 col-sm-12 col-xs-12">
                        <div class="inner">
                            <div class="default-form agent-contact-form">
                                <form method="post" action="{{ route('contact.send')}}">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="product_id" value="">
                                    <input type="hidden" name="manager_id" value="{{ $agent->id }}">
                                    <div class="clearfix">
                                        <div class="form-group">
                                            <input type="text" name="name" value="" placeholder="{{ trans('front-app.name') }}" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" value="" placeholder="{{ trans('front-app.email') }}" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="phone" value="" placeholder="{{ trans('front-app.phone') }}" required>
                                        </div>
                                        <div class="form-group">
                                            <textarea name="message" placeholder="{{ trans('front-app.message') }}" required></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="theme-btn btn-style-two">{{ trans('front-app.send_message') }}</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--Agent Properties-->
            <section class="agent-properties">
                <div class="title"><h2>{{ trans('front-app.projects') }}</h2></div>
                <div class="row clearfix">
                    <!--Default Property Box-->
                    @foreach($products as $item)
                        <div class="default-property-box col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image-box">
                                    <figure class="image"><a href="{{ $item->link() }}"><img src="{{ $item->featured_image }}" alt=""></a></figure>
                                    <div class="upper-info clearfix">
                                        <div class="property-label">Sale</div>
                                        <div class="add-fav"><a href="{{ $item->link() }}"><span class="fa fa-heart-o"></span></a></div>
                                    </div>
                                    <div class="property-price">
                                        @if($item->attribute)
                                            @if($item->attribute->price > 0)
                                                {!! price_format($item->attribute->price) !!}
                                            @else
                                                {{ trans('front-app.contact') }}
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="lower-content">
                                    <div class="property-title">
                                        <h3><a href="{{ $item->link() }}">{{ str_limit($item->language('title'),50) }}</a></h3>
                                        <div class="location">{{ $item->attribute ? $item->attribute->address : '' }}</div>
                                    </div>
                                    <div class="text-desc">{{ str_limit($item->language('description'),100) }}</div>
                                    <div class="property-specs">
                                        <ul class="clearfix">
                                            <li><span class="icon flaticon-bed-1"></span> {{ $item->attribute ? $item->attribute->bedroom : '' }}</li>
                                            <li><span class="icon flaticon-vintage-bathtub"></span> {{ $item->attribute ?  $item->attribute->bathroom : '' }}</li>
                                            {{-- <li><span class="icon flaticon-buildings"></span> {{ $item->attribute ? $item->attribute->floor : '' }}</li> --}}
                                            <li><span class="icon flaticon-blog-template"></span> {{ $item->attribute ? $item->attribute->acreage : '' }}  sq ft</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="clearfix"></div>
                    <center>{{ $products->links() }}</center>
                </div>
            </section>

        </div>
    </div>
@stop
