@extends('site.layouts.main')
@section('title',$title)
@section('description',$desc)


@section('content')



<div class="main-blog-area">
        <!-- Start Blogs Section  -->
        <div class="col-md-12 col-sm-12">
          <!-- Single Blog Row -->
          <div class="blog-lists padding-bottom">
            <div class="row"> 
                  <!-- Start Single Blog -->
                  @foreach($posts as $post)
                      <div class="col-md-12 col-sm-12">
                            <div class="single-latest-news">
                                <div class="single-latest-news-top"> 
                                    <a href="{{ $post->link() }}"> <img src="{{ $post->featured_image }}" alt=""> </a> 
                                    <div class="latest-news-time"> <span class="date">{{ $post->created_at->format('d') }}</span> <span class="month">{{ $post->created_at->format('m') }},</span> <span class="year">{{ $post->created_at->format('Y') }}</span> </div>
                                </div>
                                <div class="single-latest-news-bottom"> 
                                    <h2 class="latest-news-title"><a href="{{ $post->link() }}">{{ str_limit($post->language('title'),45) }}</a></h2>
                                    <p class="latest-news-details">{{ str_limit($post->language('description'),60) }}</p>
                                    <a href="{{ $post->link() }}" class="latest-news-read-more"> Xem thêm<i class="fa fa-angle-double-right"></i> </a> 
                                </div>
                            </div>
                      </div>
                  @endforeach
                  <!-- /End Single Blog --> 
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <center>{{ $posts->links() }}</center>
            </div>
          </div>
        </div>
        <!-- /End Blogs Section --> 
</div>
@stop
