@extends('site.layouts.main')
@section('title',$post->language('seo_title') ? $post->language('seo_title') : $post->language('title'))
@section('description',$post->language('seo_description') ? $post->language('seo_description') : $post->language('description'))
@section('keywords',$post->language('seo_keywords'))
@section('content')
@include('site.layouts.heading')
    <!--Sidebar Page-->
    <!--//================Blog start==============//-->
        <section id="section-detail" class="padT30 padB30">
             <div class="container"> 
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <div class="blog full border-bottom padB20 marB50 ">
                            <h3 class="marB20">{{ $post->language('title') }}</h3>
                            <p> 
                                <a href="{{ $post->link() }}"><span class="marR10"><i class="fa fa-calendar marR5" aria-hidden="true"></i>{{ format_datetime($post->create_at) }}</span></a>
                                <a href="{{ $post->link() }}"><i class="fa fa-comment" aria-hidden="true"></i> <span class="text-left fb-comments-count" data-href="{{ $post->link() }}">0</span></a>
                            </p>
                            <blockquote> <i>{{ $post->language('description') }}</i>
                            </blockquote>
                            {!! $post->language('content') !!}
                        </div>
                        <div class="theme-form marB30">
                            <div class="row">
                                <div class="comments-box">
                                    <div class="comment">
                                        <div class="fb-comments" data-href="{{ $post->link() }}" data-width="100%" data-numposts="5"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        @include('site.partial.new_post_list')
                        <div class="hidden-xs">
                        @include('site.partial.tags_cloud')
                        </div>
                    </div>
                </div>
             </div> 
        </section>
        <!--//================Blog end==============//-->
        <div class="clear"></div>
@stop

@push('footer')

    <script type="text/javascript">

        $(document).ready(function(){
            $('#section-detail .blog img').each(function () {
				let title = $(this).prop('title');
				$(this).wrap($('<a/>', {
	
					href: $(this).attr('src'),
					
					class: "fancybox",
	
				}));
	
				$('.fancybox').attr('data-fancybox','images');
				$('.fancybox').attr('data-caption',title);
			});
	
			$('[data-fancybox="images"]').fancybox({
				infobar : true,
				caption : function( instance, item ) {
					let caption = $(this).data('caption') || '';
					return ( caption.length ? caption + '<br />' : '' ) + 'Image <span data-fancybox-index></span> of <span data-fancybox-count></span>';
				},
				thumbs : {
					autoStart : true
				}
				
			})

        });

    </script>

@endpush