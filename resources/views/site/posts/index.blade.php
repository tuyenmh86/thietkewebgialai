@extends('site.layouts.main')
@section('title',$title)
@section('description',$desc)


@section('content')
@include('site.layouts.heading')
<div class="news-wapper">
    @php
    @endphp
    @foreach($posts as $post)
    <div class="news-item col-md-12 col-sm-12 col-xs-12 col-lg-12">
        <div class="news-header col-md-12 col-sm-12 col-xs-12 col-lg-12">
            <h2 class="news-title">
                <a href="{{ $post->link() }}">
                {{ str_limit($post->language('title'),120) }}
                </a>
            </h2>
            <time datetime="2018-05-16 19:02+0700"><span class="date">{{ $post->created_at->format('d') }}</span>/<span class="month">{{ $post->created_at->format('m') }}</span>/<span class="year">{{ $post->created_at->format('Y') }}</span></time>
        </div>
        <div class="news-img pull-left col-md-3 col-sm-3 col-xs-12 col-lg-3">
            <a href="{{ $post->link() }}">
                <img class="img-responsive" src="{{ $post->featured_image }}">
            </a>
        </div>
        <div class="news-description pull-right col-md-9 col-sm-9 col-xs-12 col-lg-9">
            <p class="news-description">
                
                {{ str_limit($post->language('description'),500) }}
                
            </p>
                <a href="{{ $post->link() }}">Xem thêm</a>
        </div>
    </div>
    @endforeach
    <div class="clearfix"></div>
    <div class="row">
        <center>{{ $posts->links() }}</center>
    </div>
</div>
@stop
