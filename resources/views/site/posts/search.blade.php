@extends('site.layouts.main')
@section('title',$title)
@section('content')
    <!--Page Title-->
    @include('site.layouts.heading')
    <!--News Section-->
    <!--//================Bredcrumb end==============//-->
        <div class="clear"></div>
        <!--//================Blog start==============//-->
        <!--//================Blog start==============//-->
        <section class="padT30 padB30 posts-list">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8 col-xs-12  blog-side-bar">
                        <div class="row marB20 ">
                            @if(count($posts) > 0)
                                @foreach($posts as $item)
                                    <div class="col-md-4 col-sm-6 col-xs-12 box-shadow-hover">
                                        <div class="collection-box hover theme-hover marB30">
                                            <figure class="">
                                                <img src="{{ $item->featured_image }}" alt=""/>
                                            </figure>
                                            <div class="product-text-sec-box">
                                                <div class="product-text-sec-icons1">
                                                    <ul>
                                                        <li><a href="{{ $item->link() }}"><i class="fa fa-heart" aria-hidden="true"></i>{{ rand(10,100) }}</a></li>
                                                            <li><a href="{{ $item->link() }}"><i class="fa fa-comment" aria-hidden="true"></i> <span class="text-left fb-comments-count" data-href="{{ $item->link() }}">0</span></a></li>
                                                    </ul>
                                                </div>
                                                <div class="product-text-sec-btn1">
                                                    <a href="{{ $item->link() }}" class="itg-button light">Chi tiết</a>
                                                </div>
                                            </div>
                                            <h4 class="product-lilte-headings"><a href="{{ $item->link() }}">{{ str_limit($item->language('title'),55) }}</a></h4>
                                            <p>{{ str_limit($item->language('description'),50) }}
                                            </p>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                {{ trans('front-app.no_results') }}
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="pagination-box marB30">
                                    <center>{{ $posts->appends(request()->only('q'))->links() }}</center>
                                </div> 
                            </div>
                        </div>
                    </div>
                    @include('site.layouts.sidebar_post')
                </div>
            </div>
        </section>
        <!--//================Blog end==============//-->
        <div class="clear"></div>
@stop
