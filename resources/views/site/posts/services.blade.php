@extends('site.layouts.main')
@section('title',$title)
@section('description',$desc)
@section('content')
    <!--Page Title-->
    @include('site.layouts.heading')
        <!--//================Bredcrumb end==============//-->
        <div class="clear"></div>
       <!--//================Services starts==============//-->
        <section class="padT30 padB30">
            <!--- Theme heading start-->
            <div class="theme-heading marB50 positionR">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-sm-8 col-xs-12  col-md-offset-3 col-sm-offset-2 col-xs-offset-0 heading-box text-center">
                            <h1>Our service</h1>
                            <div class="heading-lines"><span class="saf-boxs"></span></div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Integer lorem  quam, adipiscing
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!--- Theme heading end-->
            <div class="container">
                @foreach($posts as $key => $post)
                    @if($key % 2 == 0)
                        <div class="row">
                            <div class="service-detail marB20">
                                <div class="col-md-6 col-sm-6 col-xs-12 pull-right">
                                    <figure class="marB30">
                                        <a href="{{ $post->link() }}"><img src="{{ $post->featured_image }}" alt=""/></a>
                                    </figure>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <h3 class="marB10"><a href="{{ $post->link() }}">{{ $post->language('title') }}</a></h3>
                                    <p>{{ $post->language('description') }}
                                    </p>
                                    <a href="{{ $post->link() }}" class="itg-button light marB30">{{ trans('front-app.details') }}</a>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="service-detail marB20">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <figure class="marB30">
                                        <a href="{{ $post->link() }}"><img src="{{ $post->featured_image }}" alt=""/></a>
                                    </figure>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 pull-left">
                                    <h3 class="marB10"><a href="{{ $post->link() }}">{{ $post->language('title') }}</a></h3>
                                    <p>{{ $post->language('description') }}
                                    </p>
                                    <a href="{{ $post->link() }}" class="itg-button light marB30">{{ trans('front-app.details') }}</a>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
                <div class="clear"></div> 
                {{ $posts->links() }}
            </div>
        </section>
        <!--//================Services end==============//-->
        <div class="clear"></div>
@stop
