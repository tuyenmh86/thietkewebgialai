<!--Page Title-->
<section class="page-title" style="background-image:url(images/background/bg-page-title-1.jpg);">
    <div class="auto-container">
        <div class="clearfix">
            <!--Title -->
            <div class="title-column">
                <h1>{{$title}}</h1>
            </div>
            <!--Bread Crumb -->
            <div class="breadcrumb-column">
                <ul class="bread-crumb clearfix">
                    <li><a href="/">{{}}</a></li>
                    <li class="active">Contact Us</li>
                </ul>
            </div>
        </div>
    </div>
</section>