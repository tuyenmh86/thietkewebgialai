<div class="contact-area">
    {{--  <div class="container">   --}}
      <!-- Start Contact Locaton Google Map -->
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="contact-map">
                [map]   
          </div>
        </div>
      </div>
      <!-- /End Contact Locaton Google Map --> 
      <!-- Start Contact Information -->
      <div class="row section-padding"> 
        <!-- Contact Info -->
        <div class="col-md-12 col-sm-12">
          <div class="right-side-contact-info">
            <div class="contact-info-title border-spacing">
              <h2>Thông tin liên hệ</h2>
            </div>
            <div class="contact-info-details">
              <ul>
                [contact-information]   
              </ul>
            </div>
          </div>
        </div>
        <!-- Contact Form -->
        <div class="col-md-12 col-sm-12">
          <div class="left-side-contact-form">
            <div class="contact-info-title border-spacing">
              <h2>Gửi liên hệ cho chúng tôi</h2>
            </div>
            <div class="">
              <form class="contact-form" action="{{ route('contact.send')}}" id="form_contact" method="post">
                {!! csrf_field() !!}
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <div class="box">
                        <input id="name" class="form-control" type="text" name="name" required />
                        <label>Họ tên *</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <div class="box">
                        <input id="email" class="form-control" type="email" name="email" required />
                        <label>E-Mail *</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <div class="box">
                        <input id="mysubject"  class="form-control" type="text" name="subject" required />
                        <label>Tiêu đề *</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="box">
                    <textarea id="mymessage" class="form-control" rows="10" name="message" required></textarea>
                    <label>Nội dung *</label>
                  </div>
                </div>
                <div class="text-left">
                  <input id="submit_message"  class="btn btn-primary" type="submit" value="Send message"/>
                  <span class="loading"><i class="fa-pulse"></i></span>
                  <div class="clearfix"></div>
                  <div id="reply_message"></div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- /End Contact Information --> 
    {{--  </div>  --}}
  </div>
