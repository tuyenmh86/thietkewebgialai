<div class="footer-top-newsletter">
    <p>Đăng ký nhận bản tin của chúng tôi!</p>
    <form action="{{ route('contact.send')}}" method="post">
    	{!! csrf_field() !!}
    	<input type="hidden" name="message" value="E-Mail subcrible" /> 
        <input type="email" placeholder="Nhập E-Mail của bạn ..." name="email" required>
        <button type="submit" class="submit-btn">Theo dõi</button>
    </form>
</div>