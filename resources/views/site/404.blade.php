@extends('site.layouts.main')
@section('title','404')
@section('content')
@include('site.layouts.heading')
<!--//================Error start==============//-->

<section class="padT100 padB100 dishes">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="error-page text-center">
                    <div class="row">
                        <div class="clear"></div>
                        <div class="col-md-12 col-sm-12">
                            <h1>Không tìm thấy trang</h1>
                        </div>
                        <div class="clear"></div>
                        <div class="col-md-12 col-sm-12">
                            <h3 class="marB30">Trang bạn đang tìm kiếm, không tìm thấy</h3>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <a href="/" class="itg-button light">Trang chủ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--//================Error end==============//-->
<div class="clear"></div>
@endsection