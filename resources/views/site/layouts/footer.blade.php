<!-- footer section start -->
<footer class="footer" id="contact">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="contact-form">
					<h4>Gửi yêu cầu cho chúng tôi</h4>
					<p class="form-message"></p>
					<form id="contact-form" action="#" method="POST">
						<input type="text" name="name" placeholder="Enter Your Name">
						<input type="email" name="email" placeholder="Enter Your Email">
						<input type="text" name="subject" placeholder="Your Subject">
						<textarea placeholder="Messege" name="message"></textarea>
						<button type="submit" name="submit">Gửi</button>
					</form>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="contact-address">
					<h4>Địa chỉ</h4>
					<p>72 Lạc Long Quân - Tp. Pleiku - Gia Lai </p>
					<ul>
						<li>
							<div class="contact-address-icon">
								<i class="icofont icofont-headphone-alt"></i>
							</div>
							<div class="contact-address-info">
								<a href="callto:0905286123">0905286123</a>
								<a href="callto:0978737568">0978737568</a>
							</div>
						</li>
						<li>
							<div class="contact-address-icon">
								<i class="icofont icofont-envelope"></i>
							</div>
							<div class="contact-address-info">
								<a href="mailto:#">tuyenmh86@gmail.com</a>
							</div>
						</li>
						<li>
							<div class="contact-address-icon">
								<i class="icofont icofont-web"></i>
							</div>
							<div class="contact-address-info">
								<a href="www.jsoftbd.com">www.thietkewebgialai.net</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="subscribe-form">
					<form action="#">
						<input type="text" placeholder="Your email address here">
						<button type="submit">Subcribe</button>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="copyright-area">
					<ul>
						<li><a href="#"><i class="icofont icofont-social-facebook"></i></a></li>
						<li><a href="#"><i class="icofont icofont-social-twitter"></i></a></li>
						<li><a href="#"><i class="icofont icofont-brand-linkedin"></i></a></li>
						<li><a href="#"><i class="icofont icofont-social-pinterest"></i></a></li>
						<li><a href="#"><i class="icofont icofont-social-google-plus"></i></a></li>
					</ul>
					<p>&copy; <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://thietkewebgialai.net" target="_blank">thietkewebgialai.net</a>
</p>
				</div>
			</div>
		</div>
	</div>
</footer><!-- footer section end -->