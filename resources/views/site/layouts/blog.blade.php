<!-- blog section start -->
		<section class="blog-area ptb-90" id="blog">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
					    <div class="sec-title">
							<h2>Tin tức khuyến mãi<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
							<p>HT Solution luôn đưa ra các chương trình khuyến mãi hàng tháng </p>
						</div>
					</div>
				</div>
				<div class="row">
					@foreach(get_list_posts(true,null,5,'pos') as $post)
					<div class="col-lg-4 col-md-6">

					    <div class="single-post">
							<div class="post-thumbnail">
							<a href="{{$post->link()}}" title="{{ $post->language('title') }}"><img src="{{ $post->featured_image }}" alt="{{ $post->language('title') }}"></a>
							</div>
							<div class="post-details">
								<div class="post-author">
									<a href="{{$post->link()}}"><i class="icofont icofont-user"></i>Admin</a>
									<a href="{{$post->link()}}"><i class="icofont icofont-speech-comments"></i>Comments</a>
									<a href="{{$post->link()}}"><i class="icofont icofont-calendar"></i>{{ $post->updated_at }}</a>
								</div>
								<h4 class="post-title"><a href="blog.html">{{ $post->language('title') }}</a></h4>
							<p>{{ $post->language('description')}}</p>
							</div>
						</div>
					</div>
					@endforeach
					{{-- <div class="col-lg-4 col-md-6">
					    <div class="single-post">
							<div class="post-thumbnail">
								<a href="blog.html"><img src="site/assets/img/blog/blog2.jpg" alt="blog"></a>
							</div>
							<div class="post-details">
								<div class="post-author">
									<a href="blog.html"><i class="icofont icofont-user"></i>John</a>
									<a href="blog.html"><i class="icofont icofont-speech-comments"></i>Comments</a>
									<a href="blog.html"><i class="icofont icofont-calendar"></i>21 Feb 2018</a>
								</div>
								<h4 class="post-title"><a href="blog.html">Lorem ipsum dolor sit</a></h4>
								<p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 d-md-none d-lg-block">
					    <div class="single-post">
							<div class="post-thumbnail">
								<a href="blog.html"><img src="site/assets/img/blog/blog3.jpg" alt="blog"></a>
							</div>
							<div class="post-details">
								<div class="post-author">
									<a href="blog.html"><i class="icofont icofont-user"></i>John</a>
									<a href="blog.html"><i class="icofont icofont-speech-comments"></i>Comments</a>
									<a href="blog.html"><i class="icofont icofont-calendar"></i>21 Feb 2018</a>
								</div>
								<h4 class="post-title"><a href="blog.html">Lorem ipsum dolor sit</a></h4>
								<p>Lorem ipsum dolor sit amet, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad</p>
							</div>
						</div>
					</div> --}}
				</div>
			</div>
		</section>
		<!-- blog section end -->