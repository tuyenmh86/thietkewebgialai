<!-- download section start -->
<section class="download-area ptb-90">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="sec-title">
                    <h2>thiet ke web gia lai<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
                    <p>Hiển thị tốt trên </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul>
                    <li>
                        <a href="#" class="download-btn flexbox-center">
                            <div class="download-btn-icon">
                                <i class="icofont icofont-brand-android-robot"></i>
                            </div>
                            <div class="download-btn-text">
                                <p>Available on</p>
                                <h4>Điện thoại Android</h4>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="download-btn flexbox-center">
                            <div class="download-btn-icon">
                                <i class="icofont icofont-brand-apple"></i>
                            </div>
                            <div class="download-btn-text">
                                <p>Available on</p>
                                <h4>Điện thoại Iphone</h4>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="download-btn flexbox-center">
                            <div class="download-btn-icon">
                                <i class="icofont icofont-brand-windows"></i>
                            </div>
                            <div class="download-btn-text">
                                <p>Available on</p>
                                <h4>Điện thoại Windows Phone</h4>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section><!-- download section end -->