<!-- Start Main Slider Area -->
@if(slide('slide-home'))

    <section id="main-slider">
        {{--  <div class="container">  --}}
        <div class="tp-banner-container">
            <div class="fullscreenbanner tp-banner banenr-streated">
                <ul>
                    <!-- SLIDE  -->
                    @foreach(slide('slide-home') as $key => $item)
                        <li data-transition="fade" data-slotamount="6" data-delay="5000">
                            <!-- MAIN IMAGE -->
                            <img src="{{ $item->image }}" alt="slidebg1" 
                            data-bgposition="center top">

                            {{--  [slider-caption]   --}}
                        </li>
                    @endforeach
                    <!-- SLIDE  -->
                </ul>
            </div>
        </div>
    {{--  </div>  --}}
    </section>
@endif