<header id="main-header" class="main-header">

    {{--  <div class="mega-menu-relative visible-xs-block visible-sm-block">

            @include('site.layouts.mobile_left_sidebar')  

    </div>  --}}

    <!--Start Header Top -->
<div class="header-top">

        <div class="container">

            <div class="row">

                <div class="col-md-6 col-sm-8">

                    <!-- Header Top Left Text -->

                    <div class="header-top-left padding15">

                        [top-header-info]   

                    </div>

                </div>

                <div class="col-md-6 col-sm-4">

                    <!-- Header Top Right Menu -->

                    <div class="header-top-right text-right">

                        <ul>

                            <li class="mobile-search"><a href="#"><i class="icon_search redIcon"></i></a>

                                <div class="search-box-area">

                                    <form action="{{ route('searchs') }}" method="POST">

                                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>

                                        <input name="q" value="" type="search" placeholder="Nhập tên sản phẩm ...">

                                    </form>

                                </div>

                            </li>

                             <li><a href="#"><i class="icon_globe-2"></i></a>

                                <div class="setting-form">

                                    <div class="language border-spacing">

                                        <ul>

                                            <li class="active"><a href="#">En</a></li>

                                            <li><a href="#">Bn</a></li>

                                            <li><a href="#">Sa</a></li>

                                        </ul>

                                    </div>

                                    <div class="currency">

                                        <ul>

                                            <li class="active"><a href="#">&#x24;</a></li>

                                            <li><a href="#">&#x20ac;</a></li>

                                            <li><a href="#">&#x20b9;</a></li>

                                        </ul>

                                    </div>

                                </div>

                            </li> 
                            

                             <li><a href="#"><i class="icon_profile redIcon"></i></a>

                                <ul class="header-top-right-dropdown">

                                    <li><a href="{{route('user.login')}}">Đăng nhập / Đăng ký</a></li>

                                    <li><a href="{{route('user.profile.index')}}">Tài khoản của tôi</a></li>

                                    <li><a href="{{ route('product.cart') }}">Giỏ Hàng <i class="icon_cart_alt redIcon"></i><span class="cart_count"> {{ Cart::count() }}</span></a></li>

                                    <li><a href="{{ route('product.checkout') }}">Thanh toán</a></li>

                                </ul>

                            </li> 

                            <li>
                                <a href="{{ route('product.cart') }}">
                                    <i class="icon_cart_alt redIcon"></i>
                                    <span class="cart_count redIcon"> 
                                        {{ Cart::count() }}
                                    </span>
                                </a>

                            </li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </div>  

    <!--/End Header Top -->



    <div class="banner-top">

            {{-- [home-banner-01] --}}

    </div>

                {{-- <!-- Mobile Menu Area -->

                <div class="mobile-menu-area">

                    <nav id="mobile-menu">

                        <ul>

                            @foreach(app_menu('main-menu') as $item)

                                @if($item->parent == 0)

                                    @if($item->children->count())

                                        <li class="mainmenu-dropdown"> <a href="index-2.html">Home</a>

                                            <ul>

                                                @foreach($item->children as $item2)

                                                    <li> <a href="{{ $item2->renderLink($item2->link) }}">{{ $item2->language('label') }}</a> </li>

                                                @endforeach

                                            </ul>

                                        </li>

                                    @else

                                        <li> <a href="{{ $item->renderLink($item->link) }}">{{ $item->language('label') }}</a> </li>

                                    @endif

                                @endif

                            @endforeach

                        </ul>

                    </nav>

                </div> --}}



       

    <div class="mega-menu-relative visible-md-block visible-lg-block">

            @include('site.layouts.mobile_left_sidebar')  
            {{--  @include('site.layouts.mega_menu')    --}}
            

    </div>

    <!-- /End Header Bottom -->

</header>

<!-- /End Mian Header