<!-- feature section start -->
<section class="feature-area ptb-90" id="feature">
    <div class="container">
        <div class="row flexbox-center">
            <div class="col-lg-4">
                <div class="single-feature-box text-lg-right text-center">
                    <ul>
                        <li>
                            <div class="feature-box-info">
                                <h4>Không giới hạn tính năng</h4>
                                <p>Khách hàng được trải nghiệm tất cả tính năng của website</p>
                            </div>
                            <div class="feature-box-icon">
                                <i class="icofont icofont-brush"></i>
                            </div>
                        </li>
                        <li>
                            <div class="feature-box-info">
                                <h4>Responsive Design</h4>
                                <p>Thiết kế chuẩn hiển thị tốt trên mọi trình duyệt</p>
                            </div>
                            <div class="feature-box-icon">
                                <i class="icofont icofont-computer"></i>
                            </div>
                        </li>
                        <li>
                            <div class="feature-box-info">
                                <h4>Được support suốt quá trình sử dụng</h4>
                                <p>Khách hàng của HT Solution được support trong suốt quá trình sử dụng</p>
                            </div>
                            <div class="feature-box-icon">
                                <i class="icofont icofont-law-document"></i>
                            </div>
                        </li>
                        <li>
                            <div class="feature-box-info">
                                <h4>FREE Chat Messenger</h4>
                                <p>Tăng khả năng tương tác với khách hàng</p>
                            </div>
                            <div class="feature-box-icon">
                                <i class="icofont icofont-heart-beat"></i>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="single-feature-box text-center">
                    <img src="site/assets/img/feature.png" alt="feature">
                </div>
            </div>
            <div class="col-lg-4">
                <div class="single-feature-box text-lg-left text-center">
                    <ul>
                        <li>
                            <div class="feature-box-icon">
                                <i class="icofont icofont-eye"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4>Hiệu ứng đẹp</h4>
                                <p>Được trang bị cho phép website có tăng khả năng tương tác bắt mắt</p>
                            </div>
                        </li>
                        <li>
                            <div class="feature-box-icon">
                                <i class="icofont icofont-sun-alt"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4>Hệ thống quản trị nội dung</h4>
                                <p>Hệ thống CMS đơn giản, dễ sử dụng</p>
                            </div>
                        </li>
                        <li>
                            <div class="feature-box-icon">
                                <i class="icofont icofont-code-alt"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4>Clean Codes</h4>
                                <p>Code được HT Solution trải qua nhiều công đoạn kiểm tra cho đảm bảo code sạch </p>
                            </div>
                        </li>
                        <li>
                            <div class="feature-box-icon">
                                <i class="icofont icofont-headphone-alt"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4>24/7 Supports</h4>
                                <p>Hỗ trợ khách hàng tất cả các ngày trong tuần  </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section><!-- feature section end -->