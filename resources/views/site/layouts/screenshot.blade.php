<!-- screenshots section start -->
<section class="screenshots-area ptb-90" id="screenshot">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="sec-title">
                    <h2>Mẫu Web gia lai thiết kế<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
                    <p></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="screenshot-wrap">
                    <div class="single-screenshot">
                        <img src="site/assets/img/screenshot/screenshot1.jpg" alt="screenshot" />
                    </div>
                    <div class="single-screenshot">
                        <img src="site/assets/img/screenshot/screenshot2.jpg" alt="screenshot" />
                    </div>
                    <div class="single-screenshot">
                        <img src="site/assets/img/screenshot/screenshot3.jpg" alt="screenshot" />
                    </div>
                    <div class="single-screenshot">
                        <img src="site/assets/img/screenshot/screenshot4.jpg" alt="screenshot" />
                    </div>
                    <div class="single-screenshot">
                        <img src="site/assets/img/screenshot/screenshot5.jpg" alt="screenshot" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- screenshots section end -->