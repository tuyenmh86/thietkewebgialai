<div class="container">
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">MegaMenu</a>
          </div>
      
      
          <div class="collapse navbar-collapse js-navbar-collapse">
            <ul class="nav navbar-nav">
              <li class="dropdown mega-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Collection <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>
      
                <ul class="dropdown-menu mega-dropdown-menu row">
                    @php

                    $category = get_list_product_categories(true,26,null,null,'sort_asc');
    
                    @endphp
    
                    @foreach($category as $key => $item)

                        <li class="col-sm-3">
                            <a class="" href="{{$item->link()}}">

                            <i class="fa fa-angle-right marR10" aria-hidden="true"></i>

                            {{ $item->language('title') }}

                            </a>

                        </li>

                    @endforeach
              </li>
            </ul>
      
          </div>
          <!-- /.nav-collapse -->
        </nav>
      </div>