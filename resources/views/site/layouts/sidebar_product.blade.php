<div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <aside class="sidebar">

        <!-- Recent Posts -->
        <div class="sidebar-widget prop-search-widget">
            <!--Search Form-->
            <div class="search-form-panel">
                <div class="widget-header">{{ trans('front-app.search_properties') }}</div>
                <div class="form-box">
                    <div class="default-form property-search-form">
                        <form method="GET" action="{{ trans('route.searchs').'/'.trans('route.products') }}">
                            <div class="row clearfix">
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <div class="field-label">{{ trans('front-app.keyword') }}</div>
                                    <input type="text" name="keyword" value="" placeholder="{{ trans('front-app.enter_keyword') }}" >
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <div class="field-label">{{ trans('front-app.project_type') }}</div>
                                    <select class="custom-select-box" name="type">
                                        <option value="">{{ trans('front-app.project_type') }}</option>
                                        @foreach(get_list_product_categories(true) as $item)
                                            @if(in_array($item->id,array(14,15)))
                                                <option value="{{ $item->id }}">{{ $item->language('title') }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <div class="field-label">{{ trans('front-app.status') }}</div>
                                    <select class="custom-select-box"  name="status">
                                        <option value="">{{ trans('front-app.status') }}</option>
                                        <option value="featured">{{ trans('front-app.featured') }}</option>
                                        <option value="popular">{{ trans('front-app.popular') }}</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <div class="field-label">{{ trans('front-app.location') }}</div>
                                    <input type="text" name="location" value="" placeholder="{{ trans('front-app.location') }}" >
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="field-label">{{ trans('front-app.bedroom') }}</div>
                                    <select class="custom-select-box" name="bedroom">
                                        <option value="">{{ trans('front-app.all') }}</option>
                                        <option value="1-3">1-3</option>
                                        <option value="3-5">3-5</option>
                                        <option value="5">5+</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="field-label">{{ trans('front-app.bathroom') }}</div>
                                    <select class="custom-select-box" name="bathroom">
                                        <option value="">{{ trans('front-app.all') }}</option>
                                        <option value="1-3">1-3</option>
                                        <option value="3-5">3-5</option>
                                        <option value="5">5+</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <div class="range-slider-one">
                                        <div class="slider-header">
                                            <div class="clearfix">
                                                <div class="title">Price ($):</div>
                                                <div class="input"><input type="text" class="property-amount" name="price" readonly></div>
                                            </div>
                                        </div>

                                        <div class="price-range-slider"></div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <div class="range-slider-one">
                                        <div class="slider-header">
                                            <div class="clearfix">
                                                <div class="title">Area (ft<sup>2</sup>):</div>
                                                <div class="input"><input type="text" class="area-size" name="area" readonly></div>
                                            </div>
                                        </div>

                                        <div class="area-range-slider"></div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="theme-btn btn-style-two">{{ trans('front-app.search_properties') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Recent Posts -->
        @php
            $products = isset($product) ? get_list_products(true,$product->categories->first()->id) : get_list_products(true,null,5,'featured');
        @endphp
        @if(count($products) > 0)
            <div class="sidebar-widget popular-posts">
                <div class="sidebar-title"><h3>{{ trans('front-app.related_product') }}</h3></div>
                @foreach($products as $item)
                    <article class="post">
                        <figure class="post-thumb"><a href="{{ $item->link() }}"><img src="{{ $item->featured_image }}" alt=""></a></figure>
                        <h4><a href="{{ $item->link() }}">{{ str_limit($item->language('title'),55) }}</a></h4>
                        <div class="location"><span class="fa fa-map-marker"></span> {{ $item->attribute ? $item->attribute->address : '' }}</div>
                        <div class="price"><strong>
                            @if($item->attribute)
                                @if($item->attribute->price > 0)
                                    {!! price_format($item->attribute->price) !!}
                                @else
                                    {{ trans('front-app.contact') }}
                                @endif
                            @endif
                        </strong></div>
                    </article>
                @endforeach
            </div>
        @endif

    </aside>
</div>