<header class="header">
	<div class="container">
		<div class="row flexbox-center">
			<div class="col-lg-2 col-md-3 col-6">
				<div class="logo">
					<a href="#home"><img src="site/assets/img/logo.png" alt="logo" /></a>
				</div>
			</div>
			<div class="col-lg-10 col-md-9 col-6">
				<div class="responsive-menu"></div>
				<div class="mainmenu">
					<ul id="primary-menu">
						<li><a class="nav-link active" href="#home">Home</a></li>
						<li><a class="nav-link" href="#feature">Tính năng</a></li>
						<li><a class="nav-link" href="#screenshot">Mẫu web</a></li>
						{{-- <li><a class="nav-link" href="#pricing">Pricing</a></li> --}}
						{{-- <li><a class="nav-link" href="#team">Team</a></li> --}}
						<li><a class="nav-link" href="#blog">Blog</a></li>
						<li><a class="nav-link" href="#contact">Contact Us</a></li>
						{{-- <li><a class="appao-btn" href="#">Download</a></li> --}}
					</ul>
				</div>
			</div>
		</div>
	</div>
</header><!-- header section end -->