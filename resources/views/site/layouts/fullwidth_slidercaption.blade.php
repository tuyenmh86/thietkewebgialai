<!-- hero area start -->
<section class="hero-area" id="home">
    <div class="hero-area-slider">
        <div class="hero-area-single-slide">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="hero-area-content">
                            <h1>Nâng tầm kinh doanh của bạn</h1>
                            <p>Website là nơi cho phép bạn đưa những thông tin chính thức về doanh nghiệp, sản phẩm của bạn. Một Website tốt sẽ giúp uy tín của công ty, sản phẩm của bạn đi xa hơn </p>
                            <a href="#" class="appao-btn">Liên hệ 0905286123</a>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="hand-mockup text-lg-left text-center">
                            <img src="site/assets/img/hand-mockup.png" alt="Hand Mockup" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hero-area-single-slide">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="hero-area-content">
                            <h1>Kinh doanh thời 4.0</h1>
                            <p>Bạn có thể kinh doanh ở bất kỳ đâu trên thế giới. Bạn không có cửa hàng ở đường phố lớn, bạn có thể trong ngõ hẻm, bạn chỉ cần kinh doanh qua mạng và website giúp khách hàng tìm tới bạn </p>
                        <a href="#" class="appao-btn">Liên hệ 0905286123</a>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="hand-mockup text-lg-left text-center">
                            <img src="site/assets/img/hand-mockup.png" alt="Hand Mockup" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hero-area-single-slide">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="hero-area-content">
                            <h1>Bí quyết kinh doanh thành công </h1>
                            <p>Cách mạng công nghệ 4.0 giai đoạn mà công nghệ thông tin, tự động hóa, được áp dụng ở tất cả mọi mặt của đời sống, con người cuốn theo nhịp sống công nghệ và khách hàng của bạn chỉ sử dụng điện thoại, laptop để tìm kiếm sản phẩm của bạn</p>
                            <a href="#" class="appao-btn">Liên hệ 0905286123</a>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="hand-mockup text-lg-left text-center">
                            <img src="site/assets/img/hand-mockup.png" alt="Hand Mockup" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- hero area end -->