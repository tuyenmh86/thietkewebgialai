<header class="header other-page">

        <nav>

        <div class="container">

            

            <div class="hidden-lg hidden-md menu-offcanvas">

                <div class="head-menu clearfix">

                    <div class="menuclose"><i class="fa fa-close"></i></div>

                    <ul class="list-inline">
                            <li class="li-search">
                                <div class="header_search search_form">
                                <form class="input-group search-bar search_form" action="{{ route('searchs') }}" method="POST" role="search">		
                                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                        <input name="q" value="" type="search" class="input-group-field st-default-search-input search-text auto-search" placeholder="Nhập tên sản phẩm ..." autocomplete="off">
                                    <span class="input-group-btn">
                                        <button class="btn icon-fallback-text">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </form>
                            </div>						
        
                            </li>
                        </ul>

                </div>

                <ul id="nav-mobile" class="nav hidden-md hidden-lg">

                 

                    <li class="nav-item "><a class="nav-link" href="/">Trang chủ</a></li>		

                    

                    {{-- <li class="nav-item">

                        <a href="#" class="nav-link">Sản phẩm <i class="fa faa fa-angle-right"></i></a>

                       

                        <ul class="dropdown-menu">

                                @php

                                $category = get_list_product_categories(true,null,null,null,'sort_asc');

                                @endphp

                                @foreach($category as $item)

                                <li class="dropdown-submenu nav-item-lv2">

                                        <a class="" href="{{$item->link()}}">

                                        <i class="fa fa-angle-right marR10" aria-hidden="true"></i>

                                        {{ $item->language('title') }}

                                        </a>

                                </li>

                                @endforeach

                        </ul>

                    </li> --}}

                        {{--  <ul class="dropdown-menu">

                            <li class="dropdown-submenu nav-item-lv2">

                                <a class="nav-link" href="#">Rau củ <i class="fa faa fa-angle-right"></i></a>

                                <ul class="dropdown-menu">

                                    <li class="nav-item-lv3">

                                        <a class="nav-link" href="#">Rau tươi</a>

                                    </li>						

                                    <li class="nav-item-lv3">

                                        <a class="nav-link" href="#">Rau vườn</a>

                                    </li>						

                                    <li class="nav-item-lv3">

                                        <a class="nav-link" href="#">Rau sạch</a>

                                    </li>						

                                    <li class="nav-item-lv3">

                                        <a class="nav-link" href="#">Củ nhập khẩu</a>

                                    </li>						

                                </ul>                      

                            </li>

                        </ul>  --}}

                   

                    

                    

                    @foreach(app_menu('dailong_menu') as $item)



                        @if($item->parent == 0)



                            @if($item->children->count())



                                <li class="nav-item">

                                    <a href="#" class="nav-link">  

                                        {{ $item->language('label') }}

                                        <i class="fa faa fa-angle-right"></i>

                                    </a>

                                    <ul class="dropdown-menu">

                                        @foreach($item->children as $item2)



                                        <li class="nav-item-lv2">

                                            <a class="nav-link" href="{{ $item2->renderLink($item2->link) }}">

                                                {{ $item2->language('label')}}

                                                <i class="fa faa fa-angle-right"></i>

                                                </a> 



                                            @if($item2->children->count())



                                                <ul class="dropdown-menu">

                                                        @foreach($item2->children as $item3)

                                                            <li class="nav-item-lv3">  <a href="{{ $item3->renderLink($item3->link) }}">{{ $item3->language('label') }}</a> </li>

                                                        @endforeach



                                                </ul>



                                            @endif

                                        

                                        </li>



                                        @endforeach



                                    </ul>



                                </li>

                            @else

                                <li class="nav-item">

                                                                

                                    <a class="nav-link" href="{{ $item->renderLink($item->link) }}">{{ $item->language('label') }}</a> 

                                

                                </li>

                            

                            @endif



                        @endif



                    @endforeach

                </ul>

            </div>

        </div>

    </nav>

    </header>



    @push('footer')

        

    @endpush