<nav class="nav-baop navbar navbar-static-top">

    <div class="container-fluid">



      <div class="nav_bg_dl navbar-header">

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">

            

          <span class="sr-only">Toggle navigation</span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>

          <span class="icon-bar"></span>

        </button>

      </div>

      <div id="navbar" class="navbar-collapse collapse">


        <div class="col-md-9 col-lg-9">
        <ul class="nav navbar-nav">

          <li><a href="/">Trang chủ</a> </li>

          {{-- <li class="dropdown mega-dropdown">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown" />
                Sản phẩm  

                <span class="caret"></span>  
                </a>
                <ul class="dropdown-menu mega-dropdown-menu row">

                    @php

                    $category = get_list_product_categories(true,null,null,null,'sort_asc');
    
                    @endphp
    
                    @foreach($category as $key => $item)

                        <li class="col-sm-3">
                            <a class="" href="{{$item->link()}}">

                            <i class="fa fa-angle-right marR10" aria-hidden="true"></i>

                            {{ $item->language('title') }}

                            </a>

                        </li>

                    @endforeach

            </ul>

          </li> --}}

          @foreach(app_menu('dailong_menu') as $item)



              @if($item->parent == 0)



                  @if($item->children->count())



                      <li class="dropdown">

                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">  

                                                              

                              {{ $item->language('label') }}

                              <span class="caret"></span>

                          </a>

                          <ul class="dropdown-menu">

                              @foreach($item->children as $item2)



                              <li>

                                  <a href="{{ $item2->renderLink($item2->link) }}">{{ $item2->language('label')}}</a> 



                                  @if($item2->children->count())



                                      <ul class="dropdown-menu">

                                              @foreach($item2->children as $item3)

                                                  <li>  <a href="{{ $item3->renderLink($item3->link) }}">{{ $item3->language('label') }}</a> </li>

                                              @endforeach



                                      </ul>



                                  @endif

                              

                              </li>



                              @endforeach



                          </ul>



                      </li>

                  @else

                      <li> 

                                                      

                          <a href="{{ $item->renderLink($item->link) }}">{{ $item->language('label') }}</a> 

                      

                      </li>

                  

                  @endif



              @endif



          @endforeach

      </ul>
      </div>
      {{-- <div class="col-md-3 col-lg-3">
        <div class="menu-search pull-right">
                <div class="header_search search_form">
                    <form action="{{ route('searchs') }}" method="POST"  class="input-group search-bar search_form" role="search">	
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <input name="q" value="" type="search" class="input-group-field st-default-search-input search-text auto-search" placeholder="Nhập tên sản phẩm ..." autocomplete="off">
                        <span class="input-group-btn">
                            <button class="btn icon-fallback-text">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </form>
                    <div id="search_suggestion">
                        <div id="search_top">
                            <div id="product_results"></div>
                            <div id="article_results"></div>
                        </div>
                        <div id="search_bottom">
                            <a class="show_more" href="#">Hiển thị tất cả kết quả cho "<span></span>"</a>
                        </div>
                    </div>
                </div>

        </div>
    </div> --}}

      </div><!--/.nav-collapse -->

    </div><!--/.container-fluid -->

  </nav>