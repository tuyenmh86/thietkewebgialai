<!DOCTYPE html>

<html class="no-js" lang="en">

<head>

    <meta charset="utf-8">

    <!-- Responsive -->

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="google-site-verification" content="{{getConfigSite('google_site_verification')}}" />

    @hasSection('title')

        <title>@yield('title')|{{ getConfigSite('site_title') }}</title>

    @else

        <title>{{ getConfigSite('site_title') }}</title>

    @endif



    @hasSection('description')

        <meta name="description" content="@yield('description')">

    @else

        <meta name="description" content="{{ getConfigSite(session()->get('locale').'_seo_description') }}">

    @endif



    @hasSection('keywords')

        <meta name="keywords" content="@yield('keywords')">

    @else

        <meta name="keywords" content="{{ getConfigSite(session()->get('locale').'_seo_keywords') }}">

    @endif





    <link rel="shortcut icon" href="{{ getConfigSite('site_logo') }}" type="image/x-icon">

    <link rel="icon" href="{{ getConfigSite('site_logo') }}" type="image/x-icon">

    

    <base href="{{ asset('') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="site/assets/css/bootstrap.min.css" media="all" />
    <!-- Slick nav CSS -->
    <link rel="stylesheet" type="text/css" href="site/assets/css/slicknav.min.css" media="all" />
    <!-- Iconfont CSS -->
    <link rel="stylesheet" type="text/css" href="site/assets/css/icofont.css" media="all" />
    <!-- Slick CSS -->
    <link rel="stylesheet" type="text/css" href="site/assets/css/slick.css">

    <link rel="stylesheet" href="site/assets/css/font-awesome.min.css">
    <!-- Owl carousel CSS -->
    <link rel="stylesheet" type="text/css" href="site/assets/css/owl.carousel.css">
    <!-- Popup CSS -->
    <link rel="stylesheet" type="text/css" href="site/assets/css/magnific-popup.css">
    <!-- Switcher CSS -->
    <link rel="stylesheet" type="text/css" href="site/assets/css/switcher-style.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" type="text/css" href="site/assets/css/animate.min.css">
    <!-- Main style CSS -->
    <link rel="stylesheet" type="text/css" href="site/assets/css/style.css" media="all" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" type="text/css" href="site/assets/css/responsive.css" media="all" />
    <!-- Favicon Icon -->
    <link rel="icon" type="image/png" href="site/assets/img/favcion.png" />
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('style')

</head>

<body data-spy="scroll" data-target=".header" data-offset="50">
    <!-- Page loader -->
    <div id="preloader"></div>
    <!-- header section start -->
    <!-- Header -->

        @include('site.layouts.header_pro')
        <!-- End header -->

       

        @yield('content')

        <!-- google map area start -->
		<div class="google-map"></div>
        <!-- google map area end -->
        
        <a href="#" class="scrollToTop">
			<i class="icofont icofont-arrow-up"></i>
		</a>
		{{-- <div class="switcher-area" id="switch-style">
			<div class="display-table">
				<div class="display-tablecell">
					<a class="switch-button" id="toggle-switcher"><i class="icofont icofont-wheel"></i></a>
					<div class="switched-options">
						<div class="config-title">Home variation:</div>
						<ul>
							<li><a href="index.html">Home - Fixed Text</a></li>
							<li class="active"><a href="index-slider.html">Home - Slider Effect</a></li>
							<li><a href="index-video.html">Home - Video Background</a></li>
						</ul>
						<div class="config-title">Other Pages</div>
						<ul>
							<li><a href="blog.html">Blog</a></li>
							<li><a href="blog-detail.html">Blog Details</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div> --}}
        @include('site.layouts.footer')
       
   
   

<!--Scroll to top-->
<!-- jquery main JS -->
<script src="site/assets/js/jquery.min.js"></script>
<!-- Bootstrap JS -->
<script src="site/assets/js/bootstrap.min.js"></script>
<!-- Slick nav JS -->
<script src="site/assets/js/jquery.slicknav.min.js"></script>
<!-- Slick JS -->
<script src="site/assets/js/slick.min.js"></script>
<!-- owl carousel JS -->
<script src="site/assets/js/owl.carousel.min.js"></script>
<!-- Popup JS -->
<script src="site/assets/js/jquery.magnific-popup.min.js"></script>
<!-- Counter JS -->
<script src="site/assets/js/jquery.counterup.min.js"></script>
<!-- Counterup waypoints JS -->
<script src="site/assets/js/waypoints.min.js"></script>
<!-- YTPlayer JS -->
<script src="site/assets/js/jquery.mb.YTPlayer.min.js"></script>
<!-- jQuery Easing JS -->
<script src="site/assets/js/jquery.easing.1.3.js"></script>
<!-- Gmap JS -->
{{-- <script src="site/assets/js/gmap3.min.js"></script> --}}
<!-- Google map api -->
{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnKyOpsNq-vWYtrwayN3BkF3b4k3O9A_A"></script> --}}
<!-- Custom map JS -->
<script src="site/assets/js/custom-map.js"></script>
<!-- WOW JS -->
<script src="site/assets/js/wow-1.3.0.min.js"></script>
<!-- Switcher JS -->
<script src="site/assets/js/switcher.js"></script>
<!-- main JS -->
<script src="site/assets/js/main.js"></script>


{{-- 
@if(session()->has('message'))

    <script type="text/javascript">

        toastr.success("{{ session()->get('message') }}");

    </script>

@endif



@if(session()->has('order_success'))

    <script type="text/javascript">

        toastr.success("{{ session()->get('order_success') }}");

    </script>

@endif --}}


{{-- 
<script type="text/javascript">

    $(document).ready(function(){
        
        $("img.lazy").lazyload({

            threshold : 500

        });

        $('.single-product-top').click(function(){

            var link = $(this).children('.link').attr('href');

            window.location.href = link;

        });

        $("#tagcloud a").tagcloud({

            size: {start: 8, end: 26, unit: "px"},

            color: {start: '#3498DB', end: '#46CFB0'}

        });

        

    });



</script> --}}



@stack('footer')

</body>

</html>

