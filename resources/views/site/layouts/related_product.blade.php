<!-- Related Products Title -->

<div class="related-product-wrapper">

        <div class="title-related">

        <h2 class="tittle_section">SẢN PHẨM LIÊN QUAN</h2>

        </div>

      <div class="product-list related-products"> 

            @php
                $products = get_list_products(true,$product_id);

            @endphp

            @foreach($products as $product)

            <div class="single-product">

                    <div class="single-product-border">

                    <div class="single-product-top">

                        <a class="link" href="{{ $product->link() }}"> 

                        {{--  @foreach($product->image_list as $key => $image)  --}}

                        <img class="lazy img-responsive screenshot" title="{{ $category->language('title')." ". $product->language('title') }}" src="{{ $product->featured_image }}" alt=""> 

                        {{--  @endforeach  --}}

                        </a>

                    </div>

                    <div class="single-product-bottom">

                        <h2 class="product-title">

                            <a href="{{ $product->link() }}" class="btn btn-link">{{ $product->language('title') }}</a>

                        </h2>

                    </div>

                    </div>

                    {{-- <div id="modal-{{$item->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                    <div class="modal-dialog">

                        <div class="modal-content">

                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>

                                <h4 id="myModalLabel">Bạn đã chọn mua sản phẩm: {{ $product->language('title') }}</h4>

                                <h5 id="myModalLabel">Vui lòng cho chúng tôi biết thông tin của bạn</h5>

                            </div>

                            <div class="modal-body">

                                <form class="form-horizontal col-sm-12" action="{{ route('contact.mail')}}" id="form_contact" method="post">

                                {!! csrf_field() !!}

                                <div class="form-group">

                                    <input id="product_id" type="hidden" name="product_id"  value="{{ $product->language('title') }}"/>

                                    <input class="form-control required" name="name" placeholder="Tên của bạn:" data-placement="top" data-trigger="manual" data-content="Must be at least 3 characters long, and must only contain letters." type="text">

                                </div>

                                <div class="form-group">

                                    <textarea class="form-control" name="txtMessage" placeholder="Lời nhắn cho chúng tôi.." data-placement="top" data-trigger="manual"></textarea>

                                </div>

                                <div class="form-group">

                                    <input class="form-control email" name="email" placeholder="Nhập email của bạn " data-placement="top" data-trigger="manual" data-content="Must be a valid e-mail address (user@gmail.com)" type="text">

                                </div>

                                <div class="form-group">

                                    <input class="form-control phone" name="phone" placeholder="Số điện thoại của bạn" data-placement="top" data-trigger="manual" data-content="Must be a valid phone number (999-999-9999)" type="text">

                                </div>

                                <div class="form-group">

                                    <button type="submit" class="btn btn-success">Gửi</button>

                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Hủy</button>

                                    <p class="help-block pull-left text-danger hide" id="form-error">  The form is not valid. </p>

                                </div>

                                </form>

                            </div>

                            <div class="modal-footer">

                            </div>

                        </div>

                    </div>

                    </div> --}}

            </div>

            @endforeach

        
</div>

