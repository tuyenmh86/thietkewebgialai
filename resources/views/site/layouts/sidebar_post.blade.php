<div class="col-md-3 col-sm-4 col-xs-12 pull-left">
    <div class="row">
        <div class="sidebar">
            <div class="col-xs-12">
                <div class="sidebar-box marB30">
                    <div class="col-xs-12">
                        <div class="search_bar">
                            <form action="{{ route('get.searchs') }}" method="GET">
                                <input type="text" name="q" placeholder="Tìm kiếm ..">
                                <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="sidebar-box">
                    <div class="col-xs-12">
                        <h4 class="marB20">{{ trans('front-app.categories') }}</h4>
                        <ul class="meta-link">
                            @foreach(app_menu('main-menu') as $item)
                                @if($item->parent == 0)
                                    <li><a href="{{ $item->renderLink($item->link) }}"><i class="fa fa-angle-right marR10" aria-hidden="true"></i>{{ $item->language('label') }}</a></li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="sidebar-box">
                    <div class="col-xs-12">
                        <h4 class="marB20">Bài viết nổi bật</h4>
                        @foreach(get_list_posts(true,null,5,'featured') as $item)
                            <div class="feature-post marB10">
                                <div class="col-md-4 col-sm-4 col-xs-4 padL0">
                                    <figure>
                                        <a href="{{ $item->link() }}"><img src="{{ $item->featured_image }}" alt=""></a>
                                    </figure>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-8 padL0 padR0 text-left">
                                    <p>{{ $item->language('title') }}</p>
                                    <p><a href="{{ $item->link() }}" class="theme-color"> {{ trans('front-app.details') }}</a></p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="sidebar-box">
                    <div class="col-xs-12">
                        <h4 class="marB20">Bài viết mới nhất</h4>
                        @foreach(get_list_posts(true,null,5,'pos') as $item)
                            <div class="feature-post marB10">
                                <div class="col-md-4 col-sm-4 col-xs-4 padL0">
                                    <figure>
                                        <a href="{{ $item->link() }}"><img src="{{ $item->featured_image }}" alt=""></a>
                                    </figure>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-8 padL0 padR0 text-left">
                                    <p>{{ $item->language('title') }}</p>
                                    <p><a href="{{ $item->link() }}" class="theme-color"> {{ trans('front-app.details') }}</a></p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="sidebar-box">
                    <div class="col-xs-12">
                        <h4 class="marB20">Kết nối với chúng tôi</h4>
                        [join-us-sidebar]
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>