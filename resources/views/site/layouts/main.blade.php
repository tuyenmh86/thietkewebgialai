<!DOCTYPE html>

<html class="no-js" lang="en">

<head>

    <meta charset="utf-8">

    <!-- Responsive -->

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="google-site-verification" content="{{getConfigSite('google_site_verification')}}" />

    @hasSection('title')

        <title>@yield('title')|{{ getConfigSite('site_title') }}</title>

    @else

        <title>{{ getConfigSite('site_title') }}</title>

    @endif



    @hasSection('description')

        <meta name="description" content="@yield('description')">

    @else

        <meta name="description" content="{{ getConfigSite(session()->get('locale').'_seo_description') }}">

    @endif



    @hasSection('keywords')

        <meta name="keywords" content="@yield('keywords')">

    @else

        <meta name="keywords" content="{{ getConfigSite(session()->get('locale').'_seo_keywords') }}">

    @endif





    <link rel="shortcut icon" href="{{ getConfigSite('site_logo') }}" type="image/x-icon">

    <link rel="icon" href="{{ getConfigSite('site_logo') }}" type="image/x-icon">

    

    <base href="{{ asset('') }}">



    <!-- Stylesheets -->

    {{--  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Open+Sans:400,600,700" rel="stylesheet">   --}}

    

    <!-- all css here -->

    <!-- Slider -->



    <link rel="stylesheet" href="/site/assets/css/font-awesome.min.css">



    <link rel="stylesheet" type="text/css" href="/site/assets/css/settings.css">

    <!-- bootstrap v3.3.7 css -->

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- fancy box -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />

    <!-- animate css -->

    <link rel="stylesheet" href="/site/assets/css/animate.css">

    <!-- jquery-ui.min css -->

    <link rel="stylesheet" href="/site/assets/css/jquery-ui.min.css">

    <!-- meanmenu css -->

    <link rel="stylesheet" href="/site/assets/css/meanmenu.css">

    <!-- owl.carousel css -->

    <link rel="stylesheet" href="/site/assets/css/owl.carousel.css">

    <!-- font-legant css -->

    <link rel="stylesheet" href="/site/assets/css/legant.css">



    <!-- style css -->

    <link rel="stylesheet" href="/site/assets/css/style.css">

    <link rel="stylesheet" href="/site/assets/css/hotline.css">

    <link rel="stylesheet" href="/site/assets/js/tooltipster-master/dist/css/tooltipster.bundle.css">

    <link rel="stylesheet" href="/site/assets/js/tooltipster-master/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-shadow.min.css">

    <link rel="stylesheet" href="/site/assets/css/mobile_left.css"> 

    <link rel="stylesheet" href="/site/assets/css/responsive2.scss.css"> 
    <link rel="stylesheet" href="/site/assets/css/verticalmnu.css"> 
    

    <link rel="stylesheet" href="/site/assets/css/iwish.css"> 

    <link rel="stylesheet" href="/site/assets/css/bpr.min.css"> 



    <link rel="stylesheet" href="/site/assets/css/style2.scss.css"> 

    <link rel="stylesheet" href="/site/assets/css/mega_menu.css"> 
    
    <link rel="stylesheet" href="/site/assets/css/style-theme.scss.css"> 

    <link href="/site/assets/css/custom.css" rel="stylesheet">

    <link href="/site/assets/css/fullwidth-slider.css" rel="stylesheet">

    <link href="/site/assets/css/header.css" rel="stylesheet">
    <!-- responsive css -->

    <link rel="stylesheet" href="/site/assets/css/responsive.css">

    <!-- modernizr css -->

    

    <link rel="stylesheet" href="/site/assets/css/toastr.min.css">



    {{--  <script src="/site/assets/js/modernizr-2.8.3.min.js"></script>  --}}


    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>



    <script type="text/javascript">

        var token =  document.querySelector('meta[name="csrf-token"]').getAttribute('content');;
        window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token;

    </script>

    @yield('style')

</head>

<body>
       <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0&appId=169231053723159&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!-- Home One Start -->
    <div class="zalo-chat-widget" data-oaid="816529515038713585" data-welcome-message="Rất vui khi được hỗ trợ bạn!" data-autopopup="0" data-width="350" data-height="420"></div>

    <script src="https://sp.zalo.me/plugins/sdk.js"></script>
    <div class="home-one">

        <!-- Header -->

        @include('site.layouts.header_pro')

       

        <!-- End header -->

        

        <div class="container-fluit">

            

               
                @include('site.layouts.mobile_menu')

                

                {{-- <div class="lsidebar col-md-3 col-sm-3 col-xs-12 pull-left hidden-xs">

                        @include('site.layouts.sidebar_category')
                        

                </div>

                <div class="col-md-9 col-sm-9 col-xs-12 pull-right content_right"> --}}
                 <div> 
                    <div class="">
                            @include('site.layouts.fullwidth_slidercaption')
                    </div>
                    <div class="menu-bar hidden-md hidden-lg">
                        <i class="fa fa-align-right" style="font-size: 28px; color:darkorange;"></i>
                    </div>

                    
                </div>

                

        </div>

       
        


    </div>
    <div class="footer clearfix">

        @include('site.layouts.footer')
        [hotline]
</div>
    
   

<!--Scroll to top-->

<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>


<!-- bootstrap js --> 

<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>


<!-- owl.carousel js --> 

<script src="/site/assets/js/owl.carousel.min.js"></script> 

<script src="/site/assets/js/jquery.lazyload.min.js"></script> 

<!-- meanmenu js --> 

<script src="/site/assets/js/jquery.meanmenu.js"></script> 

<!-- jquery-ui js --> 

<script src="/site/assets/js/jquery-ui.min.js"></script> 

<!-- wow js --> 

<script src="/site/assets/js/WOW-master/dist/wow.min.js"></script> 

{{--  <script src="/site/assets/js/wow.min.js"></script>   --}}

<!-- Scroll To Top js --> 

<script src="/site/assets/js/jquery.scrollUp.js"></script> 

<!-- main js --> 

<!-- Slider  js --> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />

<script src="/site/assets/js/jquery.themepunch.revolution.min.js"></script>

<script src="/site/assets/js/jquery.themepunch.tools.min.js"></script>

<script src="/site/assets/js/jssor.slider.min.js"></script> 

<script src="/site/assets/js/tooltipster-master/dist/js/tooltipster.bundle.min.js"></script> 



<script src="/site/assets/js/main.js"></script> 

<script src="/site/assets/js/jquery.awesomeCloud-0.2.js"></script> 

<script src="/site/assets/js/jquery.tagcloud.js"></script> 

<!-- plugins js --> 

<script src="/site/assets/js/plugins.js"></script> 



<script src="/site/assets/js/toastr.min.js"></script>





@if(session()->has('message'))

    <script type="text/javascript">

        toastr.success("{{ session()->get('message') }}");

    </script>

@endif



@if(session()->has('order_success'))

    <script type="text/javascript">

        toastr.success("{{ session()->get('order_success') }}");

    </script>

@endif



<script type="text/javascript">

    $(document).ready(function(){
        
        $("img.lazy").lazyload({

            threshold : 500

        });

        $('.single-product-top').click(function(){

            var link = $(this).children('.link').attr('href');

            window.location.href = link;

        });

        $("#tagcloud a").tagcloud({

            size: {start: 8, end: 26, unit: "px"},

            color: {start: '#3498DB', end: '#46CFB0'}

        });

        

    });



</script>

<script type="text/javascript">

    this.screenshotPreview = function(){	

        /* CONFIG */

            

            xOffset = 0;

            yOffset = 0;

            

            // these 2 variable determine popup's distance from the cursor

            // you might want to adjust to get the right result

            

        /* END CONFIG */

        $(".screenshot").hover(function(e){

            $("body").append("<p id='screenshot'><img src='"+this.src +"' alt='url preview' />"+ "</p>");								 

            $("#screenshot")

                .css("top",(e.pageY - xOffset) + "px")

                .css("left",(e.pageX + yOffset) + "px")

                .fadeIn("fast");						

        },

        function(){

            $("#screenshot").remove();

        });	

        $(".screenshot").mouseout(function(e){

            $("#screenshot")

                .css("top",(e.pageY - xOffset) + "px")

                .css("left",(e.pageX + yOffset) + "px");

        });	

    };

    

    // Load this script once the document is ready

        $(document).ready(function() {

            screenshotPreview();

        });

 </script>



@stack('footer')

</body>

</html>

