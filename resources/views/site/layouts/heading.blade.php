
        <div class="bredcrumb-section"> 
                <div class="page-head">
                    {{--  <div class="page-header-heading">
                        <h3 class="theme-color">{{ isset($title) ? $title : '' }}</h3>
                    </div>    --}}
                    <div class="breadcrumb-box"> 
                        <ul class="breadcrumb colorW marB0">
                            <li>
                               <a href="/">{{ trans('front-app.home') }}</a>
                            </li>
                            @if(isset($category))
                                <li><a href="{{ $category->link() }}">{{ $category->language('title') }}</a><i class="fa fa-chevron-right"></i>

                                </li>
                            @endif
                            <li class="active">{{ isset($title) ? $title : '' }}</li>
                        </ul>
                    </div>  
                </div>
        </div>

<div class="banner-child-page" style="display: none;">
    [banner-child-page]
</div>

@push('footer')
    <script type="text/javascript">
        $(document).ready(function(){
            
            var imgLink = $('.banner-child-page').find('img').attr('src');
            $('.bredcrumb-section').css('background-image','url('+imgLink+')');
        });
    </script>
@endpush