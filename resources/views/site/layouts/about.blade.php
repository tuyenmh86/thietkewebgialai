<!-- about section start -->
<section class="about-area ptb-90">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="sec-title">
                    <h2>About HT Solution<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
                    <p>Chúng tôi cung cấp dịch vụ website giá rẻ chất lượng cao trên toàn quốc</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="single-about-box">
                    <i class="icofont icofont-ruler-pencil"></i>
                    <h4>Responsive Design</h4>
                    <p>Hiển thị tốt trên nhiều thiết bị laptop, desktop, mobile, tablet</p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="single-about-box active">
                    <i class="icofont icofont-computer"></i>
                    <h4>Xử lý nhanh</h4>
                    <p>Tất cả các website do HT Solution đều được tối ưu về tất cả các mặt code, hình ảnh, cache giúp cho quá trình xử lý nhanh nhất </p>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="single-about-box">
                    <i class="icofont icofont-headphone-alt"></i>
                    <h4>Đa nền tảng</h4>
                    <p>Cung cấp dịch vụ trên nhiều nền tảng khác nhau</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- about section end -->