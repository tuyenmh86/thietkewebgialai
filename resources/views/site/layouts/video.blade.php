<!-- video section start -->
<section class="video-area ptb-90">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="video-popup">
                    <a href="https://www.youtube.com/watch?v=RZXnugbhw_4" class="popup-youtube"><i class="icofont icofont-ui-play"></i></a>
                    <h1>Watch Video Demo</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- video section end -->