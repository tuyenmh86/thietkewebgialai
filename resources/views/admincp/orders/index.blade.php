@extends('admincp.layouts.main')
@section('title','Order')
@section('content')
<div class="container-fluid">
    {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">
    
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                <a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
                {{-- ************** END:: ACTION OPTION ******************* --}}
                <table class="table table-striped table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            <th><input type="checkbox" onchange="return check_lists();" id="checkAll" name="checkAll"/></th>
                            <th>Id</th>
                            <th>{{ trans('back-app.name') }}</th>
                            <th>E-Mail</th>
                            <th>Tổng cộng</th>
                            <th>{{ trans('back-app.created') }}</th>
                            @if(CAN('orders','update') || CAN('orders','destroy'))
                            <th>{{ trans('back-app.action') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($data))
                        @foreach($data as $item)
                        <tr>
                            <td><input type="checkbox" id="checkSingle" name="checkSingle" value="{{$item->id}}" /></td>
                            <td>{{ $item->id }}</td>
                            <td><span class="badge badge-success">
                                {{ $item->customer_info['first_name'].' '.$item->customer_info['last_name'] }} </span></td>
                            <td><span class="" style="font-weight: 600;color: #CC0000">
                            {{ $item->customer_info['email'] }}
                            </span></td>
                            <td>
                                {{ $item->total }}
                            </td>
                            <td>{{Carbon\Carbon::parse($item->created_at)->format('d-m-Y')}}</td>
                             @if(CAN('orders','update') || CAN('orders','destroy'))
                            <td>
                                {{-- Edit --}}
                                @if(CAN('orders','update'))
                                    <a class="btn btn-info pull-left" href="{{route('admincp.product.orders.edit',$item->id)}}">
                                    <i class="fa fa-edit "></i></a>
                                @endif
                                    {{-- Delete --}}
                                    @if(CAN('orders','destroy'))
                                    <form action="{{route('admincp.product.orders.destroy',$item->id)}}" method="post" class="pull-left">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                        <input type="hidden" name="_method" value="DELETE"> 
                                        <a class="btn btn-danger" href="#" onclick="$(this).closest('form').submit()">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </form>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection