@extends('admincp.layouts.main')
@section('title','Order')
@section('content')
<div class="container-fluid">
    {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">
    
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                <a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <table class="table table-striped table-bordered datatable" id="table_active">
                  <thead>
                    <tr>
                      <td class="product-name"> <strong>TÊN SẢN PHẨM</strong> </td>
                      <td class="product-price"> <strong>GIÁ</strong> </td>
                      <td class="product-quantity"> <strong>SỐ LƯỢNG</strong> </td>
                      <td class="product-subtotal"> <strong>THÀNH TIỀN</td>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data->order_detail as $item)
                      <tr>
                        <td class="product-name"> {{ $item['name'] }} </td>
                        <td class="product-price"> {{ price_format($item['price']) }} </td>
                        <td class="product-quantity"> {{ $item['qty'] }} </td>
                        <td class="product-subtotal"> {{ price_format($item['price'] * $item['qty']) }} </td>
                      </tr>
                    @endforeach
                    <tr class="grand-total-row">
                      <td colspan="3">
                        <p class="total-text grand-total"> <strong>Tổng tiền</strong> </p></td>
                      <td>
                        <p class="total-text grand-total highlighted-text"> <h4>{{ $data->total }}</h4> </p></td>
                    </tr>
                  </tbody>
            </table>
        </div><br>
        <div class="card">
            <table class="table table-striped table-bordered datatable" id="table_active">
                <strong style="margin: 10px auto">THÔNG TIN KHÁCH HÀNG</strong> 
                <tbody>
                      <tr>
                        <td> <strong>Tên:</strong> {{ $data->customer_info['first_name'] }} </td>
                        <td> <strong>Họ:</strong> {{ $data->customer_info['last_name'] }} </td>
                        <td> <strong>Tên công ty:</strong> {{ $data->customer_info['company_name'] }} </td>
                      </tr>
                      <tr>
                        <td> <strong>E-Mail:</strong> {{ $data->customer_info['email'] }} </td>
                        <td> <strong>Địa chỉ:</strong> {{ $data->customer_info['address'] }} </td>
                        <td> <strong>Số điện thoại:</strong> {{ $data->customer_info['phone'] }} </td>
                      </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection