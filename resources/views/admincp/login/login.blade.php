@extends('admincp.login.layouts.main')
@section('content')
<div class="login-box">
    <div class="white-box">
      <form class="form-horizontal form-material" id="loginform" method="POST" role="form"  action="{{(Session::get('timesLog') == 3) ? '' : route('admincp.login')}}">
        {{ csrf_field() }}
        <h3 class="box-title m-b-20">{{ trans('back-app.login') }}</h3>
        <!-- Message error -->
        @if(Session::has('error'))
        <span class="help-block" style="color: #a94442">
            {{ Session('error')}}
        </span>
        @endif
        <div class="form-group ">
          <div class="col-xs-12 {{ ($errors->has('username') ? 'has-error' : '')}}">
            <input class="form-control" id="username" name="username" type="text" value="{{ old('username') }}" required="" autofocus placeholder="administrator">
            <span class="help-block">
                 {{$errors->first('username')}}
            </span>
          </div>

        </div>
        <div class="form-group">
          <div class="col-xs-12 {{($errors->has('password') ? 'has-error' : '')}}">
            <input class="form-control" type="password" id="password" name="password"  placeholder="Password" required="" value="{{old('password')}}" >
            <span class="help-block">
                  {{$errors->first('password')}}
            </span>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary pull-left p-t-0">
              <input id="checkbox-signup" name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
              <label for="checkbox-signup"> Ghi nhớ đăng nhập </label>
            </div>
           {{--  <a href="{{ route('password.request') }}" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock" aria-hidden="true"></i>
 Forgot pwd?</a> --}} </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" {{(Session::get('timesLog') == 3) ? 'disabled=""' : ''}}>{{ trans('back-app.login') }}</button>
          </div>
        </div>
       <!--  <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
            <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip"  title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip"  title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
          </div>
        </div> -->
        <!-- <div class="form-group m-b-0">
          <div class="col-sm-12 text-center">
            <p>Don't have an account? <a href="register.html" class="text-primary m-l-5"><b>Sign Up</b></a></p>
          </div>
        </div> -->
      </form>
      <!-- <form class="form-horizontal" id="recoverform" action="http://eliteadmin.themedesigner.in/demos/eliteadmin-wpmenu/index.html">
        <div class="form-group ">
          <div class="col-xs-12">
            <h3>Recover Password</h3>
            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Email">
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
          </div>
        </div>
      </form> -->
    </div>
  </div>
@endsection