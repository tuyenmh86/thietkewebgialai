<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="">
<title>Bog Service | Đăng nhập</title>
<!-- Bootstrap Core CSS -->
<link href="{{login_public_url()}}/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome CSS -->
<link href="{{login_public_url()}}/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- animation CSS -->
<link href="{{login_public_url()}}/css/style.css" rel="stylesheet">
{{-- Custom JS Site --}}
{{-- <script src="{{admin_public_url()}}/js/custom.js"></script>  --}}

</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <!-- // Load Content -->
    @yield('content')
  <!-- // End Load Content -->
</section>
{{-- Use Javascript --}}
@yield('script')
<!-- jQuery -->
<script src="{{login_public_url()}}/js/jquery.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="{{login_public_url()}}/js/custom.min.js"></script>
<!--Style Switcher -->
<script type="text/javascript">
	// ****************************** PREVEN EVEN KEYENTER ***************************
	document.onkeydown = preventEnterKey;
</script>
</body>
</html>
