@extends('admincp.layouts.main')
@section('title',trans('back-app.categories'))
@section('content')
<div class="container-fluid">
    {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">
    
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                {{-- Add new --}}
                @if(CAN('post.categorys','create'))
                <a href="{{route('admincp.post.categorys.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
                @endif
                {{-- Cancel --}}
                <a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
                {{-- ************** BEGIN:: ACTION OPTION ***************** --}}
                <div class="form-group row">
                    @if(CAN('post.categorys','update') || CAN('post.categorys','destroy'))
                    <div class="col-md-2 ">
                        <select id="action_table" name="action_table" class=" form-control pull-left" size="1">
                            <option value="">-- {{ trans('back-app.action') }} --</option>
                            @if(CAN('post.categorys','destroy'))
                            <option value="{{ route('admincp.post.categorys.destroys')}}">{{ trans('back-app.delete') }}</option>
                            @endif
                            @if(CAN('post.categorys','update'))
                            <option value="{{ route('admincp.post.categorys.disable')}}">{{ trans('back-app.disable') }}</option>
                            <option value="{{ route('admincp.post.categorys.public')}}">{{ trans('back-app.public') }}</option>
                            @endif
                        </select>
                    </div>
                    @endif
                    <button type="button"  onclick="return update_records();" id="submit_action" class="btn btn-sm btn-info pull-left"><i class="fa fa-location-arrow" aria-hidden="true"></i> {{ trans('back-app.submit') }}</button>&nbsp;  
                </div>
                {{-- ************** END:: ACTION OPTION ******************* --}}
                <table class="table table-striped table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            <th><input type="checkbox" onchange="return check_lists();" id="checkAll" name="checkAll"/></th>
                            <th>Id</th>
                            <th>{{ trans('back-app.title') }}</th>
                            <th>{{ trans('back-app.alias') }}</th>
                            <th>{{ trans('back-app.cate_parent') }}</th>
                            <th>{{ trans('back-app.created') }}</th>
                            <th>{{ trans('back-app.pos') }}</th>
                            <th>{{ trans('back-app.status') }}</th>
                            @if(CAN('post.categorys','update') || CAN('post.categorys','destroy'))
                            <th>{{ trans('back-app.action') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($categorys))
                        @foreach($categorys as $category)
                        <tr>
                            <td><input type="checkbox" id="checkSingle" name="checkSingle" value="{{$category->id}}" /></td>
                            <td>{{$category->id}}</td>
                            <td><span class="badge badge-success">{{$category->title}} </span></td>
                            <td>{{$category->alias}}</td>
                            <td>
                                @if(isset($category->parent))
                                <span class="badge badge-info"><a style="color: #FFF" href="{{ route('admincp.post.categorys.edit',$category->parent->id) }}">{{ $category->parent->title }} </a></span>
                                @else
                                    <span class="badge badge-danger">{{ trans('back-app.cate_parent') }}</span>
                                @endif
                            </td>
                            <td>{{Carbon\Carbon::parse($category->created_at)->format('d-m-Y')}}</td>
                            <td><input width="20px" type="number" onkeyup="return update_posision('{!! route('admincp.post.categorys.pos.update') !!}',{!! $category->id; !!},this.value);" name="pos" class="form-control updatePos" id="{{$category->id}}" value="{{ $category->pos ? $category->pos : 0 }}"></td>
                            <td>
                                @if($category->is_published())
                                <span class="badge badge-success">{{ trans('back-app.public') }}</span>
                                @else
                                <span class="badge badge-warning">{{ trans('back-app.disable') }}</span>
                                @endif
                            </td>
                             @if(CAN('post.categorys','update') || CAN('post.categorys','destroy'))
                            <td>
                                {{-- Edit --}}
                                @if(CAN('post.categorys','update'))
                                    <a class="btn btn-info pull-left" href="{{route('admincp.post.categorys.edit',$category->id)}}">
                                    <i class="fa fa-edit "></i></a>
                                @endif
                                    {{-- Delete --}}
                                    @if(CAN('post.categorys','destroy'))
                                    <form action="{{route('admincp.post.categorys.destroy',$category->id)}}" method="post" class="pull-left">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                        <input type="hidden" name="_method" value="DELETE"> 
                                        <a class="btn btn-danger" href="#" onclick="$(this).closest('form').submit()">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </form>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection