@extends('admincp.layouts.main')
@section('title',trans('back-app.categories'))
@section('content')
<div class="container">
	<form action="{{route('admincp.post.categorys.store')}}" method="post" class="form-horizontal " enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		{{-- Option --}}
		<div class="col-md-12 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						{{-- Add new --}}
						<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{ trans('back-app.save') }}</button>
						{{-- Reset --}}
						<button type="reset" class="btn btn-sm btn-info"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.reset') }}</button>
						{{-- Cancel --}}
						<a href="{{route('admincp.post.categorys.index')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 1 --}}
		<div class="col-md-8 pull-left">
			@include('components.data_form_other')

			@include('components.customfield',['table' => 'categories_post'])
		</div>
		{{-- Block 2 --}}
		<div class="col-md-4 pull-right">
			{{-- Block 2 | Block 1 --}}
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-globe" aria-hidden="true"></i><strong>{{ trans('back-app.option') }}</strong>
					</div>
					<div class="card-block">
						@if(CAN('post.categorys','create',true))
						<div class="form-group row">
							<div class="col-md-12">
								<select id="published" name="published" class="form-control" size="1">
									<option value="0">{{ trans('back-app.disable') }}</option>
									<option value="1">{{ trans('back-app.public') }}</option>
								</select>
							</div>
						</div>
						@endif
						<div class="form-group row">
							<div class="col-md-12">
								<input type="number" id="pos" name="pos" class="form-control {{ ($errors->has('pos') ? 'has-error' : '')}}" value="{{ \App\Models\Post\Category::max('pos')+1 }}" placeholder="Position">
								<span class="help-block mess-err">{{$errors->first('pos')}}</span>
							</div>
						</div>
						{{-- ****** BEGIN :: FEATURED ******** --}}
						@if(CAN('post.categorys','create',true))
						<div class="form-group row">
							<div class="col-md-12"><br>
								<div class="form-group row">
									<label class="col-md-3 form-control-label" for="cn_title"><strong>{{ trans('back-app.featured') }}</strong></label>
									<div class="col-md-9">
										<label class="switch switch-icon switch-pill switch-danger-outline-alt">
											<input type="checkbox" class="checkboxClass switch-input" name="featured" value="1">
											<span class="switch-label" data-on="" data-off=""></span>
											<span class="switch-handle"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
						@endif
						{{-- ****** END :: FEATURED ******** --}}
					</div>
				</div>
			</div>
		</div>
		{{-- Block 2 --}}
		<div class="col-md-4 pull-right">
			{{-- Block 2 | Block 3 --}}
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<strong><i class="fa fa-gg" aria-hidden="true"></i> {{ trans('back-app.categories') }}</strong>
					</div>
					<div class="card-block">
						<div class="form-group row">
							<div class="col-md-12">
								<select id="parent_id" name="parent_id" class="form-control" size="1">
									<option value="0">-- {{ trans('back-app.cate_parent') }} --</option>
									@if(isset($categorys))
									{{recursiveCategorys($categorys)}}
									@endif
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
