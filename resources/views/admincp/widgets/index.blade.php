@extends('admincp.layouts.main')
@section('title','Widgets')
@section('content')
<div class="container-fluid">
    {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">
    
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                {{-- Add new --}}
                @if(CAN('widgets','create'))
                <a href="{{route('admincp.widgets.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
                @endif
                {{-- Cancel --}}
                <a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
                {{-- ************** BEGIN:: ACTION OPTION ***************** --}}
                <div class="form-group row">
                    @if(CAN('widgets','update') || CAN('widgets','destroy'))
                    <div class="col-md-2 ">
                        <select id="action_table" name="action_table" class=" form-control pull-left" size="1">
                            <option value="">-- {{ trans('back-app.action') }} --</option>
                            @if(CAN('widgets','destroy'))
                            <option value="{{ route('admincp.widgets.destroys')}}">{{ trans('back-app.delete') }}</option>
                            @endif
                            @if(CAN('widgets','update'))
                            <option value="{{ route('admincp.widgets.disable')}}">{{ trans('back-app.disable') }}</option>
                            <option value="{{ route('admincp.widgets.public')}}">{{ trans('back-app.public') }}</option>
                            @endif
                        </select>
                    </div>
                    @endif
                    <button type="button"  onclick="return update_records();" id="submit_action" class="btn btn-sm btn-info pull-left"><i class="fa fa-location-arrow" aria-hidden="true"></i> {{ trans('back-app.action') }}</button>&nbsp;  
                </div>
                {{-- ************** END:: ACTION OPTION ******************* --}}
                <table class="table table-striped table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            <th><input type="checkbox" onchange="return check_lists();" id="checkAll" name="checkAll"/></th>
                            <th>Id</th>
                            <th>{{ trans('back-app.title') }}</th>
                            <th>Shortcode</th>
                            <th>{{ trans('back-app.created') }}</th>
                            <th>{{ trans('back-app.status') }}</th>
                            @if(CAN('widgets','update') || CAN('widgets','destroy'))
                            <th>{{ trans('back-app.action') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($widgets))
                        @foreach($widgets as $widget)
                        <tr>
                            <td><input type="checkbox" id="checkSingle" name="checkSingle" value="{{$widget->id}}" /></td>
                            <td>{{ $widget->id }}</td>
                            <td><span class="badge badge-success">{{$widget->title}} </span></td>
                            <td><span class="" style="font-weight: 600;color: #CC0000">
                            {{ $widget->alias }}
                            </span></td>
                            <td>{{Carbon\Carbon::parse($widget->created_at)->format('d-m-Y')}}</td>
                            <td>
                                @if($widget->is_published())
                                <span class="badge badge-success">{{ trans('back-app.public') }}</span>
                                @else
                                <span class="badge badge-warning">{{ trans('back-app.disable') }}</span>
                                @endif
                            </td>
                             @if(CAN('widgets','update') || CAN('widgets','destroy'))
                            <td>
                                {{-- Edit --}}
                                @if(CAN('widgets','update'))
                                    <a class="btn btn-info pull-left" href="{{route('admincp.widgets.edit',$widget->id)}}">
                                    <i class="fa fa-edit "></i></a>
                                @endif
                                    {{-- Delete --}}
                                    @if(CAN('widgets','destroy'))
                                    <form action="{{route('admincp.widgets.destroy',$widget->id)}}" method="post" class="pull-left">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                        <input type="hidden" name="_method" value="DELETE"> 
                                        <a class="btn btn-danger" href="#" onclick="$(this).closest('form').submit()">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </form>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection