@extends('admincp.layouts.main')

@section('title','Widgets')

@section('content')

<div class="container">

	<form action="{{route('admincp.widgets.update',$widget->id)}}" method="post" class="form-horizontal " enctype="multipart/form-data">

		<input type="hidden" name="_token" value="{{csrf_token()}}">

		<input type="hidden" name="_method" value="PUT">

		{{-- Option --}}

		<div class="col-md-12 pull-left">

			<div class="animated fadeIn">

				<div class="card">

					<div class="card-header">

						{{-- Add new --}}

						<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{ trans('back-app.save') }}</button>

						{{-- Reset --}}

						<button type="reset" class="btn btn-sm btn-info"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.reset') }}</button>

						{{-- Cancel --}}

						<a href="{{route('admincp.widgets.index')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>

					</div>

				</div>

			</div>

		</div>

		{{-- Block 1 --}}

		<div class="col-md-8 pull-left">

			<div class="animated fadeIn">

				<div class="card">

					<div class="card-header">

						<i class="fa fa-info-circle" aria-hidden="true"></i><strong>{{ trans('back-app.information') }}</strong>

					</div>

					<div class="col-md-12">

						<div class="card-block">

							<div class="form-group row">

								<label class="col-md-3 form-control-label" for="vi_title">{{ trans('back-app.title') }}(<span class="required_cl">*</span>)</label>

								<div class="col-md-9">

									<input type="text" id="vi_title" name="title" class="form-control {{ ($errors->has('vi_title') ? 'has-error' : '')}}" value="{{ $widget->title }}" placeholder="Bất động sản Cần Bán" onkeyup="">

									<span class="help-block mess-err">{{$errors->first('vi_title')}} </span>

								</div>

							</div>

							{{-- <div class="form-group row">

								<label class="col-md-3 form-control-label" for="vi_alias">Shortcode</label>

								<div class="col-md-9">

									<input type="text" id="alias" name="alias" class="form-control {{ ($errors->has('vi_alias') ? 'has-error' : '')}}" value="{{ $widget->alias }}" placeholder="bat-dong-san-can-ban" readonly="">

									<span class="help-block mess-err">{{$errors->first('vi_alias')}} </span>

								</div>

							</div> --}}

						</div>

					</div>

				</div>

				<div class="card">

					<div class="col-md-12">

						<div class="card-block">


							@if(count(config('cms.locale')) > 1)

								<ul class="nav nav-tabs" role="tablist">

									@foreach(config('cms.locale') as $key => $item)

										<li class="nav-item">
											<a class="nav-link {{ $key+5 == 5 ? 'active' : '' }}" data-toggle="tab" href="#language_form{{$key}}" role="tab" aria-controls="home">
												<img src="{{ $item['flag'] }}"><span style="text-transform: uppercase;"> {{ $item['code'] }} </a>
												</li>

										</li>

									@endforeach

								</ul>

							@endif

							{{-- ============== BEGIN::VI ================= --}}

							<div class="tab-content">

								@foreach(config('cms.locale') as $key => $item)

									<div class="row tab-pane {{ $key+5 == 5 ? 'active' : '' }}" id="language_form{{$key}}" role="tabpanel">

										<div class="col-md-12">

											<div class="form-group row">
												<label class="col-md-3 form-control-label" for="language[{{$item['code']}}][content]">{{ trans('back-app.content') }}(<span class="required_cl">*</span>)</label>
												<div class="col-md-9">
													<textarea type="text" id="language[{{$item['code']}}][content]" rows="40" name="language[{{$item['code']}}][content]" class="myEditor form-control {{ ($errors->has('content') ? 'has-error' : '')}}" value="" placeholder="">{{old("language[".$item['code']."][content]",@$widget->language[$item['code']]['content'])}}</textarea>
													<span class="help-block mess-err">{{$errors->first('content')}} </span>
												</div>
											</div>

										</div>

									</div>

								@endforeach

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		{{-- Block 2 --}}

		<div class="col-md-4 pull-right">

			{{-- Block 2 | Block 1 --}}

			<div class="animated fadeIn">

				<div class="card">

					<div class="card-header">

						<i class="fa fa-globe" aria-hidden="true"></i><strong>{{ trans('back-app.option') }}</strong>

					</div>

					<div class="card-block">

						@if(CAN('widgets','update',true))

						<div class="form-group row">

							<div class="col-md-12">

								<select id="published" name="published" class="form-control" size="1">

								@if($widget->is_published())

									<option value="1">{{ trans('back-app.public') }}</option>

									<option value="0">{{ trans('back-app.disable') }}</option>

								@else

									<option value="0">{{ trans('back-app.disable') }}</option>

									<option value="1">{{ trans('back-app.public') }}</option>

								@endif

								</select>

							</div>

						</div>

						@endif

					</div>

				</div>

			</div>

		</div>

	</form>

</div>

@endsection

