@if(!isset($imgArr1))
	<div class="col-md-6 card-block">
		<div>
			<div class="row">
			 	<div class="input-group">
				   <span class="input-group-btn">
				     <a id="" data-input="thumbnail{{$pos_image1}}" data-preview="holder{{$pos_image1}}" class="lfm btn btn-primary">
				       <i class="fa fa-picture-o"></i> {{ trans('back-app.choose') }}
				     </a>
				   </span>
				   <input id="thumbnail{{$pos_image1}}" class="form-control" type="text" name="{{ $input_name1 }}[]" value="">
				   <a href="javascript:void(0);" class="add_button1" title="Add field"><i style="color: green;font-size: 18px" class="fa fa-plus-circle" aria-hidden="true"></i></a>
			 	</div>
				<img id="holder{{$pos_image1}}" style="margin-top:15px;max-height:100px;"> 
			</div>
		</div>	
	</div>
@else
	@foreach(is_array($imgArr1) ? $imgArr1 : json_decode($imgArr1) as $key => $item)
		<div class="col-md-6 card-block">
			<div>
				<div class="row">
				 	<div class="input-group">
					   <span class="input-group-btn">
					     <a id="" data-input="thumbnail{{$pos_image1}}" data-preview="holder{{$pos_image1}}" class="lfm btn btn-primary">
					       <i class="fa fa-picture-o"></i> {{ trans('back-app.choose') }}
					     </a>
					   </span>
					   <input id="thumbnail{{$pos_image1}}" class="form-control" type="text" name="{{ $input_name1 }}[]" value="{{ $item }}">
					   @if($key == 0)
					   		<a href="javascript:void(0);" class="add_button1" title="Add field"><i style="color: green;font-size: 18px" class="fa fa-plus-circle" aria-hidden="true"></i></a>
					   @else
					   		<a href="javascript:void(0);" class="remove_button" title="Remove field"><i style="color: red;font-size: 18px" class="fa fa-minus-circle" aria-hidden="true"></i></a>
					   @endif
				 	</div>
					<img id="holder{{$pos_image1}}" src="{{ $item }}" style="margin-top:15px;max-height:100px;"> 
				</div>
			</div>	
		</div>
		@php $pos_image1++; @endphp
	@endforeach
@endif

@push('footer')
<script type="text/javascript">
	$(document).ready(function(){
	    var maxField = 10;
	    var addButton1 = $('.add_button1'); 
	    var wrapper1 = $('.field_wrapper1'); 
	    var k = {{ $pos_image1 }} + 1;
	    var x = 1; 
	    $(addButton1).click(function(){ 
	        if(x < maxField){ 
	            x++; 
	            $(this).parent().parent().parent().parent().parent().append(input_append1(k)); 
	            k++;
	            $('[class*="lfm"]').each(function() {
				    $(this).filemanager('image');
				});
	        }
	    });
	    $(wrapper1).on('click', '.remove_button', function(e){ 
	        e.preventDefault();
	        $(this).parent().parent().parent().parent().remove(); 
	        x--; 
	    });
	});

	function input_append1 (auto_increment) {
		var fieldHTML = '<div class="col-md-6 card-block">';
			fieldHTML += '<div>';
			fieldHTML += '<div class="row">';
			fieldHTML += '<div class="input-group">';
			fieldHTML += '<span class="input-group-btn">';
			fieldHTML += '<a id="" data-input="thumbnail'+auto_increment+'" data-preview="holder'+auto_increment+'" class="lfm btn btn-primary">';
			fieldHTML += '<i class="fa fa-picture-o"></i> {{ trans('back-app.choose')}}</a>';
			fieldHTML += '</span>';
			fieldHTML += '<input id="thumbnail'+auto_increment+'" class="form-control" type="text" name="{{ $input_name1 }}[]">';
			fieldHTML += '<a href="javascript:void(0);" class="remove_button" title="Remove field"><i style="color: red;font-size: 18px" class="fa fa-minus-circle" aria-hidden="true"></i></a>';
			fieldHTML += '</div>';
			fieldHTML += '<img id="holder'+auto_increment+'" style="margin-top:15px;max-height:100px;">';
			fieldHTML += '</div>';
			fieldHTML += '</div>';	
			fieldHTML += '</div>';
	    return fieldHTML;
	}
</script>
@endpush