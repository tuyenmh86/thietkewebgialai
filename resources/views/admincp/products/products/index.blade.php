@extends('admincp.layouts.main')
@section('title',trans('back-app.products'))
@section('content')
<div class="container-fluid">
    {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">

    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                {{-- Add new --}}
                @if(CAN('products','create'))
                <a href="{{route('admincp.products.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
                @endif
                {{-- Add new Category --}}
                @if(CAN('product.categorys','create'))
                <a href="{{route('admincp.product.categorys.create')}}"><button type="button" class="btn btn-sm btn-info"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add_cate') }}</button></a>
                @endif
                {{-- List Categorys --}}
                @if(CAN('product.categorys','index'))
                <a href="{{route('admincp.product.categorys.index')}}"><button type="button" class="btn btn-sm btn-warning"><i class="fa fa-list-ul" aria-hidden="true"></i> {{ trans('back-app.categories') }}</button></a>
                @endif
                {{-- Cancel --}}
                <a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
                {{-- ************** Option Action ***************************** --}}
                <div class="form-group row">
                    @if(CAN('products','update') || CAN('products','destroy'))
                   
                        <div class="col-md-2 ">
                            <form action="{{route('admincp.products.category')}}" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                            <select id="categories" class="categories form-control" name="categories">
                                @foreach($categorys as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                        <button type="submit" id="submit_action_category" class="btn btn-sm btn-info pull-left"><i class="fa fa-location-arrow" aria-hidden="true"></i> Lọc</button>&nbsp;          
                    </form>
                </div>
                {{-- ************** End Option Action ***************************** --}}
                
                {{-- ************** Option Action ***************************** --}}
                <div class="form-group row">
                    @if(CAN('products','update') || CAN('products','destroy'))
                    <div class="col-md-2 ">
                        <select id="action_table" name="action_table" class=" form-control pull-left" size="1">
                            <option value="">-- {{ trans('back-app.action') }} --</option>
                            @if(CAN('products','destroy'))
                            <option value="{{ route('admincp.products.destroys')}}">{{ trans('back-app.delete') }}</option>
                            @endif
                            @if(CAN('products','update'))
                            <option value="{{ route('admincp.products.disable')}}">{{ trans('back-app.disable') }}</option>
                            <option value="{{ route('admincp.products.public')}}">{{ trans('back-app.public') }}</option>
                            @endif
                        </select>
                    </div>
                    @endif
                    <button type="button" onclick="return update_records();" id="submit_action" class="btn btn-sm btn-info pull-left"><i class="fa fa-location-arrow" aria-hidden="true"></i> {{ trans('back-app.submit') }}</button>&nbsp;          
                </div>
                {{-- ************** End Option Action ***************************** --}}
                <table class="table table-striped table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            <th><input type="checkbox" onchange="return check_lists();" id="checkAll" name="checkAll"/></th>
                            <th>Id</th>
                            <th>{{ trans('back-app.title') }}</th>
                            <th>{{ trans('back-app.featured_image')}}</th>
                            <th>{{ trans('back-app.keyword')}}</th>
                            <th>{{ trans('back-app.admin_group') }}</th>
                            {{-- <th>Category</th> --}}
                            <th>{{ trans('back-app.created') }}</th>
                            <th>{{ trans('back-app.pos') }}</th>
                            <th>{{ trans('back-app.status') }}</th>
                            @if(CAN('products','update') || CAN('products','destroy'))
                            <th>{{ trans('back-app.action') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($products))
                        @foreach($products as $product)
                        <tr>
                            <td><input type="checkbox" id="checkSingle" name="checkSingle" value="{{$product->id}}" /></td>
                            <td>{{$product->id}}</td>
                            <td><span class="badge badge-success">{{$product->title}} </span></td>
                            <td>
                                <img src="{{site_public_url().'/../'.$product->featured_image}}" class="img-thumbnail img-responsive" style="max-width:120px;"/>
                            </td>
                            <td><span class="badge badge-success">{{$product->keyword}} </span></td>
                            <td>
                                @if($product->admin_id)
                                    <a href="{{route('admincp.admins.edit',$product->admin_id)}}" ><strong>[admin]</strong>{{ isset($product->admin->username) ? $product->admin->username : 'Null'}} </a>
                                @else
                                    <a href="{{route('admincp.users.edit',$product->user_id)}}" ><strong>[user]</strong>{{ isset($product->user->email) ? $product->user->email : 'Null'}} </a>
                                @endif
                            </td>
                           {{--  <td><span class="badge badge-info">{{(isset($product->category->vi_title)) ? $product->category->vi_title : 'Null'}} </span></td>  --}}
                            <td>{{Carbon\Carbon::parse($product->created_at)->format('d-m-Y')}}</td>
                            <td><input width="20px" type="number" name="pos" class="form-control updatePos"  onkeyup="return update_posision('{!! route('admincp.products.pos.update') !!}',{!! $product->id; !!},this.value);" id="{{$product->id}}" value="{{$product->pos}}"></td>
                            <td>
                                @if($product->is_published())
                                <span class="badge badge-success">{{ trans('back-app.public') }}</span>
                                @else
                                <span class="badge badge-warning">{{ trans('back-app.disable') }}</span>
                                @endif
                            </td>
                            @if(CAN('products','update') || CAN('products','destroy'))
                            <td>
                                {{-- Edit --}}
                                @if(CAN('products','update'))
                                <a class="btn btn-info pull-left" href="{{route('admincp.products.edit',$product->id)}}">
                                    <i class="fa fa-edit "></i></a>
                                    @endif
                                    {{-- Delete --}}
                                    @if(CAN('products','destroy'))
                                    <form action="{{route('admincp.products.destroy',$product->id)}}" method="post" class="pull-left">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                        <input type="hidden" name="_method" value="DELETE"> 
                                        <a class="btn btn-danger" href="#" onclick="$(this).closest('form').submit()">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </form>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
