<div class="tab-pane" id="home2" role="tabpanel">
	<div class="col-md-12">
		<div class="card-block">
			<div class="form-group row">
				<label class="col-md-2 form-control-label" for="price"><strong>#Mã sản phẩm:</strong></label>
				<div class="col-md-10">
					<input type="text" id="sku" name="sku" class="form-control" value="{{ isset($product->sku) ? $product->sku : \Carbon\Carbon::now()->format('Ymd').\App\Models\Product\Product::max('id') }}" placeholder=""  >
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2 form-control-label" for="price">Giá:</label>
				<div class="col-md-10">
					<input type="number" id="price" name="price" class="form-control" value="{{ isset($product->price) ? $product->price : old('price') }}" placeholder="2.000.000₫"  >
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2 form-control-label" for="price">Giá khuyến mãi:</label>
				<div class="col-md-10">
					<input type="number" id="promotion_price" name="promotion_price" class="form-control" value="{{ isset($product->promotion_price) ? $product->promotion_price : old('promotion_price') }}" placeholder="1.000.000₫"  >
				</div>
			</div>
		</div>
	</div>

	@if(config('cms.productAdvanced'))
		<div class="product-attributes">
			@include('components.attributes')
		</div>
	@endif
</div>