@extends('admincp.layouts.main')
@section('title',trans('back-app.products'))
@section('content')

<div class="container">
    <br />
    <a href="{{ URL::to('/admincp/product/downloadFile') }}">
        <button class="btn btn-success">Download Excel xlsx</button>
    </a>
    <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ route('admincp.products.import') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="file" name="import_file" />
        <button class="btn btn-primary">Import File</button>
    </form>
</div>
@endsection

