@extends('admincp.layouts.main')
@section('title',trans('back-app.attribute'))
@section('content')
<div class="container">
	<form action="{{route('admincp.product.product-attributes.update',$item->id)}}" method="post" class="form-horizontal " enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="PUT">
		{{-- Option --}}
		<div class="col-md-12 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						{{-- Add new --}}
						<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{ trans('back-app.save') }}</button>
						{{-- Reset --}}
						<button type="reset" class="btn btn-sm btn-info"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.reset') }}</button>
						{{-- Cancel --}}
						<a href="{{route('admincp.product.product-attributes.index')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
					</div>
				</div>
			</div>
		</div>

		{{-- Block 1 --}}
		<div class="col-md-8 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-info-circle" aria-hidden="true"></i><strong>Field</strong>
					</div>
					<div class="card-block">
						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="field">Tên</label>
							<div class="col-md-9">
								<input type="text" id="field" name="field" class="form-control" value="{{ $item->field }}" placeholder="">
								<span class="help-block mess-err"> </span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="label">Label</label>
							<div class="col-md-9">
								<input type="text" id="label" name="label" class="form-control" value="{{ $item->label }}" placeholder="">
								<span class="help-block mess-err"> </span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="label">Loại</label>
							<div class="col-md-9">
								<select id="type" name="type" class="form-control" size="1">
									<option value="Integer" {{ $item->type == 'Integer' ? 'selected=""' : '' }}>Integer</option>
									<option value="Varchar" {{ $item->type == 'Varchar' ? 'selected=""' : '' }}>Varchar</option>
									<option value="Text" {{ $item->type == 'Text' ? 'selected=""' : '' }}>Text</option>
									<option value="Decimal" {{ $item->type == 'Decimal' ? 'selected=""' : '' }}>Decimal</option>
								</select>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="label">Ràng buộc</label>
							<div class="col-md-9">
								<select class="categories form-control" name="is_required[]" multiple="multiple">
									@if(is_array($item->is_required))
										<option value="1" {{ in_array(1, $item->is_required) ? 'selected=""' : ''}}>Không được trống</option>
										<option value="2" {{ in_array(2, $item->is_required) ? 'selected=""' : ''}}>Phải là số</option>
									@else
										<option value="1">Không được trống</option>
										<option value="2">Phải là số</option>
									@endif
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 2 --}}
		<div class="col-md-4 pull-right">
			{{-- Block 2 | Block 1 --}}
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-globe" aria-hidden="true"></i><strong>{{ trans('back-app.option') }}</strong>
					</div>
					<div class="card-block">
						@if(CAN('product.product-attributes','create',true))
						<div class="form-group row">
							<div class="col-md-12">
								<select id="published" name="published" class="form-control" size="1">
									@if($item->is_published())

										<option value="1">{{ trans('back-app.public') }}</option>

										<option value="0">{{ trans('back-app.disable') }}</option>

									@else

										<option value="0">{{ trans('back-app.disable') }}</option>

										<option value="1">{{ trans('back-app.public') }}</option>

									@endif
								</select>
							</div>
						</div>
						@endif

						<div class="form-group row">
							<div class="col-md-12">
								<input type="number" id="position" name="position" class="form-control {{ ($errors->has('position') ? 'has-error' : '')}}" value="{{ $item->position }}" placeholder="Vị trí">
								<span class="help-block mess-err">{{$errors->first('position')}}</span>
							</div>
						</div>
						
						{{-- INIQUE --}}
						<div class="form-group row">
							<div class="col-md-12"><br>
								<div class="form-group row">
									<label class="col-md-3 form-control-label" for="is_unique"><strong>Unique</strong></label>
									<div class="col-md-9">
										<label class="switch switch-icon switch-pill switch-danger-outline-alt">
											<input type="checkbox" class="checkboxClass switch-input" name="is_unique" value="1" {{ $item->is_unique ? 'checked=""' : '' }}>
											<span class="switch-label" data-on="" data-off=""></span>
											<span class="switch-handle"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
						
						@if(config('cms.productHasManyAttribute'))
							<div class="animated fadeIn">
								<div class="card">
									<div class="card-header">
										<strong><i class="fa fa-gg" aria-hidden="true"></i> Loại sản phẩm</strong>
									</div>
									<div class="card-block">
										<div class="form-group row">
											<div class="col-md-12">
												<select class="catalog form-control" name="catalog">
													@foreach($categories as $category)
														<option value="{{ $category->id }}" {{ $item->catalog == $category->id ? 'selected=""' : '' }}>
															{{ $category->title }}
														</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
