@extends('admincp.layouts.main')
@section('title','Product Type')
@section('content')
<div class="container-fluid">
    {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">
    
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                {{-- Add new --}}
                @if(CAN('product.product-types','create'))
                <a href="{{route('admincp.product.product-types.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
                @endif
                {{-- Cancel --}}
                <a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
                <table class="table table-striped table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            {{-- <th><input type="checkbox" onchange="return check_lists();" id="checkAll" name="checkAll"/></th> --}}
                            <th>Id</th>
                            <th>{{ trans('back-app.name') }}</th>
                            <th>{{ trans('back-app.description') }}</th>
                            <th>{{ trans('back-app.status') }}</th>
                            <th>{{ trans('back-app.created') }}</th>
                            @if(CAN('product.product-types','update') || CAN('product.product-types','destroy'))
                            <th>{{ trans('back-app.action') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($data))
                        @foreach($data as $item)
                        <tr>
                            {{-- <td><input type="checkbox" id="checkSingle" name="checkSingle" value="{{$item->id}}" /></td> --}}
                            <td>{{$item->id}}</td>
                            <td><span class="badge badge-success">{{$item->name}} </span></td>
                            <td>{{ str_limit($item->description,30)}}</td>
                            <td>{{Carbon\Carbon::parse($item->created_at)->format('d-m-Y')}}</td>
                            <td>
                                @if($item->is_published())
                                <span class="badge badge-success">{{ trans('back-app.public') }}</span>
                                @else
                                <span class="badge badge-warning">{{ trans('back-app.disable') }}</span>
                                @endif
                            </td>
                             @if(CAN('product.product-types','update') || CAN('product.product-types','destroy'))
                            <td>
                                {{-- Edit --}}
                                @if(CAN('product.product-types','update'))
                                    <a class="btn btn-info pull-left" href="{{route('admincp.product.product-types.edit',$item->id)}}">
                                    <i class="fa fa-edit "></i></a>
                                @endif
                                    {{-- Delete --}}
                                    @if(CAN('product.product-types','destroy'))
                                    <form action="{{route('admincp.product.product-types.destroy',$item->id)}}" method="post" class="pull-left">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                        <input type="hidden" name="_method" value="DELETE"> 
                                        <a class="btn btn-danger" href="#" onclick="$(this).closest('form').submit()">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </form>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection