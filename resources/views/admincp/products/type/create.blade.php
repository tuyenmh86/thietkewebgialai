@extends('admincp.layouts.main')
@section('title','Product Type')
@section('content')
<div class="container">
	<form action="{{route('admincp.product.product-types.store')}}" method="post" class="form-horizontal " enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		{{-- Option --}}
		<div class="col-md-12 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						{{-- Add new --}}
						<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{ trans('back-app.save') }}</button>
						{{-- Reset --}}
						<button type="reset" class="btn btn-sm btn-info"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.reset') }}</button>
						{{-- Cancel --}}
						<a href="{{route('admincp.product.product-types.index')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
					</div>
				</div>
			</div>
		</div>

		{{-- Block 1 --}}
		<div class="col-md-8 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-info-circle" aria-hidden="true"></i><strong>{{ trans('back-app.information') }}</strong>
					</div>
					<div class="card-block">

						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="name">Tên</label>
							<div class="col-md-9">
								<input type="text" id="name" name="name" class="form-control" value="" placeholder="">
								<span class="help-block mess-err"> </span>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="description">{{ trans('back-app.description') }}</label>
							<div class="col-md-9">
								<textarea type="text" id="description" rows="4" name="description" class="form-control " value="" placeholder=""></textarea>
								<span class="help-block mess-err"> </span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 2 --}}
		<div class="col-md-4 pull-right">
			{{-- Block 2 | Block 1 --}}
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-globe" aria-hidden="true"></i><strong>{{ trans('back-app.option') }}</strong>
					</div>
					<div class="card-block">
						@if(CAN('product.product-types','create',true))
						<div class="form-group row">
							<div class="col-md-12">
								<select id="published" name="published" class="form-control" size="1">
									<option value="0">{{ trans('back-app.disable') }}</option>
									<option value="1">{{ trans('back-app.public') }}</option>
								</select>
							</div>
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
