@extends('admincp.layouts.main')
@section('title',trans('back-app.tags'))
@section('content')
<div class="container-fluid">
    {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">

    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                {{-- Add new --}}
                @if(CAN('product.tags','create'))
                <a href="{{route('admincp.product.tags.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
                @endif
                {{-- Cancel --}}
                <a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
                {{-- ************** BEGIN:: ACTION OPTION ***************** --}}
                <div class="form-group row">
                    @if(CAN('product.tags','update') || CAN('product.tags','destroy'))
                    <div class="col-md-2 ">
                        <select id="action_table" name="action_table" class=" form-control pull-left" size="1">
                            <option value="">-- {{ trans('back-app.action') }} --</option>
                            @if(CAN('product.tags','destroy'))
                            <option value="{{ route('admincp.product.tags.destroys')}}">{{ trans('back-app.delete') }}</option>
                            @endif
                            @if(CAN('product.tags','update'))
                            <option value="{{ route('admincp.product.tags.disable')}}">{{ trans('back-app.disable') }}</option>
                            <option value="{{ route('admincp.product.tags.public')}}">{{ trans('back-app.public') }}</option>
                            @endif
                        </select>
                    </div>
                    @endif
                    <button type="button"  onclick="return update_records()" id="submit_action" class="btn btn-sm btn-info pull-left"><i class="fa fa-location-arrow" aria-hidden="true"></i> {{ trans('back-app.submit') }}</button>&nbsp;  
                </div>
                {{-- ************** END:: ACTION OPTION ******************* --}}
                <table class="table table-striped table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="checkAll" onchange="return check_lists();" name="checkAll"/></th>
                            <th>Id</th>
                            <th>{{ trans('back-app.title') }}</th>
                            <th>{{ trans('back-app.created') }}</th>
                            <th>{{ trans('back-app.pos') }}</th>
                            <th>{{ trans('back-app.status') }}</th>
                            @if(CAN('product.tags','update') || CAN('product.tags','destroy'))
                            <th>{{ trans('back-app.action') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($tags))
                        @foreach($tags as $tag)
                        <tr>
                            <td><input type="checkbox" id="checkSingle" name="checkSingle" value="{{$tag->id}}" /></td>
                            <td>{{$tag->id}}</td>
                            <td><span class="badge badge-success">{{$tag->title}} </span></td>
                            <td>{{Carbon\Carbon::parse($tag->created_at)->format('d-m-Y')}}</td>
                            <td><input width="20px" type="number" onkeyup="return update_posision('{!! route('admincp.product.tags.pos.update') !!}',{!! $tag->id; !!},this.value);" name="pos" class="form-control updatePos" value="{{$tag->pos ? $tag->pos : 0 }}"></td>
                            <td>
                                @if($tag->is_published())
                                <span class="badge badge-success">{{ trans('back-app.public') }}</span>
                                @else
                                <span class="badge badge-warning">{{ trans('back-app.disable') }}</span>
                                @endif
                            </td>
                             @if(CAN('product.tags','update') || CAN('product.tags','destroy'))
                            <td>
                                {{-- Edit --}}
                                @if(CAN('product.tags','update'))
                                    <a class="btn btn-info pull-left" href="{{route('admincp.product.tags.edit',$tag->id)}}">
                                    <i class="fa fa-edit "></i></a>
                                @endif
                                    {{-- Delete --}}
                                    @if(CAN('product.tags','destroy'))
                                    <form action="{{route('admincp.product.tags.destroy',$tag->id)}}" method="post" class="pull-left">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                        <input type="hidden" name="_method" value="DELETE"> 
                                        <a class="btn btn-danger" href="#" onclick="$(this).closest('form').submit()">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </form>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection