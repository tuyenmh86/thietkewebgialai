@extends('admincp.layouts.main')
@section('title',trans('back-app.tags'))
@section('content')
<div class="container">
	<form action="{{route('admincp.product.tags.update',$tag->id)}}" method="post" class="form-horizontal " enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="PUT">
		{{-- Option --}}
		<div class="col-md-12 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						{{-- Add new --}}
						@if(CAN('product.tags','create'))
						<a href="{{route('admincp.product.tags.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
						@endif
						{{-- Save --}}
						<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{ trans('back-app.save') }}</button>
						{{-- Reset --}}
						<button type="reset" class="btn btn-sm btn-info"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.reset') }}</button>
						{{-- Cancel --}}
						<a href="{{route('admincp.product.tags.index')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 1 --}}
		<div class="col-md-8 pull-left">
			@include('components.data_form_other',['old_value' => $tag])
		</div>
		{{-- Block 2 --}}
		<div class="col-md-4 pull-right">
			{{-- Block 2 | Block 1 --}}
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-globe" aria-hidden="true"></i><strong>{{ trans('back-app.option') }}</strong>
					</div>
					<div class="card-block">
						@if(CAN('product.tags','update',true))
						<div class="form-group row">
							<div class="col-md-12">
								<select id="published" name="published" class="form-control" size="1">
									@if($tag->is_published())
									<option value="1">{{ trans('back-app.public') }}</option>
									<option value="0">{{ trans('back-app.disable') }}</option>
									@else
									<option value="0">{{ trans('back-app.disable') }}</option>
									<option value="1">{{ trans('back-app.public') }}</option>	
									@endif
								</select>
							</div>
						</div>
						@endif
						<div class="form-group row">
							<div class="col-md-12">
								<input type="number" id="pos" name="pos" class="form-control {{ ($errors->has('pos') ? 'has-error' : '')}}" value="{{$tag->pos}}" placeholder="{{ trans('back-app.pos') }}">
								<span class="help-block mess-err">{{$errors->first('pos')}}</span>
							</div>
						</div>
						{{-- ****** BEGIN :: FEATURED ******** --}}
						@if(CAN('product.tags','update',true))
						<div class="form-group row">
							<div class="col-md-12"><br>
								<div class="form-group row">
									<label class="col-md-3 form-control-label" for="cn_title"><strong>{{ trans('back-app.featured') }}</strong></label>
									<div class="col-md-9">
										<label class="switch switch-icon switch-pill switch-danger-outline-alt">
											<input type="checkbox" class="checkboxClass switch-input" name="featured" value="1"
											{{ $tag->is_featured() ? 'checked=""' : '' }}
											>
											<span class="switch-label" data-on="" data-off=""></span>
											<span class="switch-handle"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
						@endif
						{{-- ****** END :: FEATURED ******** --}}
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
