@extends('admincp.layouts.main')
@section('title',trans('back-app.categories'))
@section('content')
<div class="container">
	<form action="{{route('admincp.product.categorys.update',$category->id)}}" method="post" class="form-horizontal " enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="PUT">
		{{-- Option --}}
		<div class="col-md-12 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						{{-- Add new --}}
						@if(CAN('product.categorys','create'))
						<a href="{{route('admincp.product.categorys.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
						@endif
						<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{ trans('back-app.save') }}</button>
						{{-- Reset --}}
						<button type="reset" class="btn btn-sm btn-info"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.reset') }}</button>
						{{-- Cancel --}}
						<a href="{{route('admincp.product.categorys.index')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 1 --}}
		<div class="col-md-8 pull-left">
			@include('components.data_form_other',['old_value' => $category])

			@include('components.customfield',['table' => 'categories_product','old_value' => $category])
		</div>
		{{-- Block 2 --}}
		<div class="col-md-4 pull-right">
			{{-- Block 2 | Block 1 --}}
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-globe" aria-hidden="true"></i><strong>{{ trans('back-app.option') }}</strong>
					</div>
					<div class="card-block">
						@if(CAN('product.categorys','update',true))
							<div class="form-group row">
								<div class="col-md-12">
									<select id="published" name="published" class="form-control" size="1">
										@if($category->is_published())
											<option value="1">{{ trans('back-app.public') }}</option>
											<option value="0">{{ trans('back-app.disable') }}</option>
										@else
											<option value="0">{{ trans('back-app.disable') }}</option>
											<option value="1">{{ trans('back-app.public') }}</option>	
										@endif
									</select>
								</div>
							</div>
						@endif
						<div class="form-group row">
							<div class="col-md-12">
								<input type="number" id="pos" name="pos" class="form-control {{ ($errors->has('pos') ? 'has-error' : '')}}" value="{{ $category->pos ? $category->pos : 0 }}" placeholder="{{ trans('back-app.pos') }}">
								<span class="help-block mess-err">{{$errors->first('pos')}}</span>
							</div>
						</div>
						
						{{-- ****** BEGIN :: FEATURED ******** --}}
						@if(CAN('product.tags','update',true))
							<div class="form-group row">
								<div class="col-md-12"><br>
									<div class="form-group row">
										<label class="col-md-3 form-control-label" for="cn_title"><strong>{{ trans('back-app.featured') }}</strong></label>
										<div class="col-md-9">
											<label class="switch switch-icon switch-pill switch-danger-outline-alt">
												<input type="checkbox" class="checkboxClass switch-input" name="featured" value="1"
												{{ $category->is_featured() ? 'checked=""' : '' }}
												>
												<span class="switch-label" data-on="" data-off=""></span>
												<span class="switch-handle"></span>
											</label>
										</div>
									</div>
								</div>
							</div>
						@endif
						{{-- ****** END :: FEATURED ******** --}}

						
					</div>
				</div>
			</div>
		</div>
		{{-- Block 2 --}}
		<div class="col-md-4 pull-right">
			{{-- Block 2 | Block 3 --}}
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<strong><i class="fa fa-gg" aria-hidden="true"></i> {{ trans('back-app.categories') }}</strong>
					</div>
					<div class="card-block">
						<div class="form-group row">
							<div class="col-md-12">
								<select id="parent_id" name="parent_id" class="form-control" size="1">
									@if($category->parent_id != 0)
										{{-- Recursive By Cate Selected --}}
										<option value="{{$category->parent_id}}">{{$category->title}}</option>
										{{ recursiveCategorys($categorys) }}
										<option value="0">-- {{ trans('back-app.cate_parent') }} --</option>
									@else
										<option value="0">-- {{ trans('back-app.cate_parent') }} --</option>
										{{ recursiveCategorys($categorys) }}
									@endif	
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="card">

						<div class="card-header">

							<i class="fa fa-picture-o" aria-hidden="true"></i><strong>{{ trans('back-app.featured_image') }}</strong>

						</div>

						<div class="card-block">

							<div class="form-group row">

								<div class="col-md-12 input-group">

									<div class="input-group">

										<span class="input-group-btn">

											<a id="" data-input="thumbnail" data-preview="holder" class="lfm btn btn-primary">

												<i class="fa fa-picture-o"></i> {{ trans('back-app.choose') }}

											</a>

										</span>

										<input id="thumbnail" class="form-control" type="text" name="featured_image" value="{{ $category->featured_image }}">

									</div>

								</div>

								<div class="col-md-12 input-group">

									<img id="holder" src="{{ $category->featured_image}}" style="margin-top:15px;max-height:100px;">

								</div>

								<span class="col-md-12 help-block mess-err">{{$errors->first('featured_image')}}</span>

							</div>

						</div>

				</div>

			</div>


		</div>
	</form>
</div>
@endsection
