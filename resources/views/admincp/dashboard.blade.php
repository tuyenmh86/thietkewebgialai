@extends('admincp.layouts.main')
@section('title',trans('back-app.dashboard'))
@section('content')
<div class="container">
<div class="animated fadeIn">
<div class="card card-block">
<div class="row">
    <!--/.col-->
    <div class="col-md-{{ CAN('products','index') ? '6' : '12' }}">
        <div class="row">
            @if(CAN('products','index'))
                <div class="col-sm-6">
                    <div class="callout callout-warning">
                        <small class="text-muted">Sản phẩm mới</small>
                        <br>
                        <strong class="h4">{{(isset($product_c) ? $product_c : '0')}}</strong>
                    </div>
                </div>
            @endif
            <!--/.col-->
            <div class="col-sm-6">
                <div class="callout callout-success">
                    <small class="text-muted">Tin tới mới</small>
                    <br>
                    <strong class="h4">{{(isset($post_c) ? $post_c : '0')}}</strong>
                </div>
            </div>
            <!--/.col-->
        </div>
        <!--/.row-->
        <hr class="mt-0">
        {{-- Statistics Top Order View --}}
        <div class="card">
            <div class="card-header">
                <i class="icon-check"></i>Lượt xem tin tức
            </div>
            <div class="card-block p-0">
                <div class="tab-content"> 
                    <div class="tab-pane active" id="tickets" aria-expanded="true">
                        <table class="table table-hover mb-0">
                            <thead>
                                <tr>
                                    <th>{{ trans('back-app.status') }}</th>
                                    <th>{{ trans('back-app.created') }}</th>
                                    <th>{{ trans('back-app.title') }}</th>
                                    <th>{{ trans('back-app.view') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($post_top_v))
                                    @foreach($post_top_v as $value )
                                    <tr>
                                        <td>
                                            @if($value->is_published())
                                            <span class="badge badge-success">{{ trans('back-app.public') }}</span>
                                            @else
                                            <span class="badge badge-danger">{{ trans('back-app.disable') }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            {{ date_format($value->created_at, 'l jS F Y') }}
                                        </td>
                                        <td><a href="{{route('admincp.posts.edit',$value->id)}}"> {{ str_limit($value->title,10)}}</a></td>
                                        <td><b>[#{{$value->view}}]</b>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--/.col-->
    <div class="col-md-6">
    	<div class="row">
            @if(CAN('products','index'))
        		<div class="col-sm-6">
        			<div class="callout callout-info">
        				<small class="text-muted">Danh mục sản phẩm mới</small>
        				<br>
        				<strong class="h4">{{(isset($product_cate_c) ? $product_cate_c : '0')}}</strong>
        			</div>
        		</div>
            @endif
    		<!--/.col-->
    		<div class="col-sm-6">
    			<div class="callout callout-danger">
    				<small class="text-muted">Người dùng mới</small>
    				<br>
    				<strong class="h4">{{(isset($user_c) ? $user_c : '0')}}</strong>
    			</div>
    		</div>
    		<!--/.col-->
    	</div>
    	<!--/.row-->
    	<hr class="mt-0">
    	{{-- Statistics Top Product View --}}
        @if(CAN('products','index'))
        	<div class="card">
                <div class="card-header">
                    <i class="icon-check"></i>Lượt xem sản phẩm
                </div>
                <div class="card-block p-0">
                    <div class="tab-content"> 
                        <div class="tab-pane active" id="tickets" aria-expanded="true">
                            <table class="table table-hover mb-0">
                                <thead>
                                    <tr>
                                        <th>{{ trans('back-app.status') }}</th>
                                        <th>{{ trans('back-app.created') }}</th>
                                        <th>{{ trans('back-app.title') }}</th>
                                        <th>{{ trans('back-app.view') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@if(isset($produc_top_v))
                                	@foreach($produc_top_v as $value )
                                    <tr>
                                        <td>
                                        	@if($value->is_published())
                                            <span class="badge badge-success">{{ trans('back-app.public') }}</span>
                                            @else
                                            <span class="badge badge-danger">{{ trans('back-app.disable') }}</span>
                                            @endif
                                        </td>
                                        <td>
                                        	{{date_format($value->created_at, 'l jS F Y')}}
                                        </td>
                                        <td><a href="{{route('admincp.products.edit',$value->id)}}"> {{str_limit($value->vi_title,10)}}</a></td>
                                        <td><b>[#{{$value->view}}]</b>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
<!--/.row-->
</div>
</div>
</div>
@endsection