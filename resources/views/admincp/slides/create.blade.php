@extends('admincp.layouts.main')
@section('title',trans('back-app.slides'))
@section('content')
<div class="container">
	<form action="{{route('admincp.slides.store')}}" method="post" class="form-horizontal " enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		{{-- Option --}}
		<div class="col-md-12 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						{{-- Add new --}}
						<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{ trans('back-app.save') }}</button>
						{{-- Reset --}}
						<button type="reset" class="btn btn-sm btn-info"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.reset') }}</button>
						{{-- Cancel --}}
						<a href="{{route('admincp.slides.index')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 1 --}}
		<div class="col-md-8 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-info-circle" aria-hidden="true"></i><strong>{{ trans('back-app.information') }}</strong>
					</div>
					{{-- ============== BEGIN::VI ================= --}}
					<div class="field_wrapper">
						<div class="tab-content">
							<div class="col-md-12 card-block">
								<div>

									<div class="row">
									 	<div class="input-group">
										   <span class="input-group-btn">
										     <a id="" data-input="thumbnail1" data-preview="holder1" class="lfm btn btn-primary">
										       <i class="fa fa-picture-o"></i> {{ trans('back-app.choose') }}
										     </a>
										   </span>
										   <input id="thumbnail1" class="form-control" type="text" name="image[]" value="">
										   <a href="javascript:void(0);" class="add_button" title="Add field"><i style="color: green;font-size: 18px" class="fa fa-plus-circle" aria-hidden="true"></i></a>
									 	</div>
										<img id="holder1" style="margin-top:15px;max-height:100px;"> 
									</div>

									<div class="row">

										<div class="input-group">

											<input id="caption" class="form-control" type="text" name="caption[]" value="" placeholder="{{ trans('back-app.caption') }}">

											</div>

									</div>

									<div class="row">

										<div class="input-group">
											
											<input id="content" class="form-control" type="text" name="content[]" value="" placeholder="{{ trans('back-app.imgContent') }}">

										</div>

									</div>


									<div class="row">

										<div class="input-group">

											<input id="link" class="form-control" type="text" name="link[]" value="" placeholder="{{ trans('back-app.link') }}">

										</div>

									</div>


								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 2 --}}
		<div class="col-md-4 pull-right">
			{{-- Block 2 | Block 1 --}}
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-globe" aria-hidden="true"></i><strong>{{ trans('back-app.option') }}</strong>
					</div>
					<div class="card-block">
						<div class="form-group row">
							<div class="col-md-12">
								<input type="text" id="vi_title" name="title" class="form-control {{ ($errors->has('title') ? 'has-error' : '')}}" value="{{old('title')}}" placeholder="Slide" onkeyup="changeToSlug2(this.value,'');">
								<span class="help-block mess-err">{{$errors->first('title')}}</span>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<input type="text" id="_alias" name="alias" class="form-control {{ ($errors->has('alias') ? 'has-error' : '')}}" value="{{old('alias')}}" readonly="" placeholder="slide">
								<span class="help-block mess-err">{{$errors->first('alias')}}</span>
							</div>
						</div>
						@if(CAN('slides','create',true))
						<div class="form-group row">
							<div class="col-md-12">
								<select id="published" name="published" class="form-control" size="1">
									<option value="0">{{ trans('back-app.disable') }}</option>
									<option value="1">{{ trans('back-app.public') }}</option>
								</select>
							</div>
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection



@push('footer')
<script type="text/javascript">
	$(document).ready(function(){
	    var maxField = 10;
	    var addButton = $('.add_button'); 
	    var wrapper = $('.field_wrapper'); 
	    var k = 2;
	    var x = 1; 
	    $(addButton).click(function(){ 
	        if(x < maxField){ 
	            x++; 
	            $(this).parent().parent().parent().parent().parent().append(input_append(k)); 
	            k++;
	            $('[class*="lfm"]').each(function() {
				    $(this).filemanager('image');
				});
	        }
	    });
	    $(wrapper).on('click', '.remove_button', function(e){ 
	        e.preventDefault();
	        $(this).parent().parent().parent().parent().remove(); 
	        x--; 
	    });
	});

	function input_append (auto_increment) {
		var fieldHTML = '<div class="col-md-12 card-block">';
			fieldHTML += '<div>';


			fieldHTML += '<div class="row">';
			fieldHTML += '<div class="input-group">';
			fieldHTML += '<span class="input-group-btn">';
			fieldHTML += '<a id="" data-input="thumbnail'+auto_increment+'" data-preview="holder'+auto_increment+'" class="lfm btn btn-primary">';
			fieldHTML += '<i class="fa fa-picture-o"></i>{{ trans('back-app.choose')}}</a>';
			fieldHTML += '</span>';
			fieldHTML += '<input id="thumbnail'+auto_increment+'" class="form-control" type="text" name="image[]">';
			fieldHTML += '<a href="javascript:void(0);" class="remove_button" title="Remove field"><i style="color: red;font-size: 18px" class="fa fa-minus-circle" aria-hidden="true"></i></a>';
			fieldHTML += '</div>';
			fieldHTML += '<img id="holder'+auto_increment+'" style="margin-top:15px;max-height:100px;">';
			fieldHTML += '</div>';


			fieldHTML += '<div class="row">';

			fieldHTML +='<div class="input-group">';

			fieldHTML +='<input id="caption'+auto_increment+'" class="form-control" type="text" name="caption[]" placeholder="{{ trans('back-app.caption') }}">';
			
			fieldHTML +='</div>';

			fieldHTML +='</div>';



			fieldHTML += '<div class="row">';

			fieldHTML +='<div class="input-group">';

			fieldHTML +='<input id="content'+auto_increment+'" class="form-control" type="text" name="content[]" placeholder="{{ trans('back-app.imgContent') }}">';
			
			fieldHTML +='</div>';

			fieldHTML +='</div>';


			fieldHTML += '<div class="row">';

			fieldHTML +='<div class="input-group">';

			fieldHTML +='<input id="link'+auto_increment+'" class="form-control" type="text" name="link[]" placeholder="{{ trans('back-app.link') }}">';
			
			fieldHTML +='</div>';

			fieldHTML +='</div>';	

			fieldHTML += '</div>';	
			fieldHTML += '</div>';
	    return fieldHTML;
	}
</script>
@endpush