@extends('admincp.layouts.main')
@section('title',trans('back-app.slides'))
@section('content')
<div class="container-fluid">
    {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">
    
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                {{-- Add new --}}
                @if(CAN('slides','create'))
                <a href="{{route('admincp.slides.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
                @endif
                {{-- Cancel --}}
                <a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
                {{-- ************** BEGIN:: ACTION OPTION ***************** --}}
                <div class="form-group row">
                    @if(CAN('slides','update') || CAN('slides','destroy'))
                    <div class="col-md-2 ">
                        <select id="action_table" name="action_table" class=" form-control pull-left" size="1">
                            <option value="">-- {{ trans('back-app.action') }} --</option>
                            @if(CAN('slides','destroy'))
                            <option value="{{ route('admincp.slides.destroys')}}">{{ trans('back-app.delete') }}</option>
                            @endif
                            @if(CAN('slides','update'))
                            <option value="{{ route('admincp.slides.disable')}}">{{ trans('back-app.disable') }}</option>
                            <option value="{{ route('admincp.slides.public')}}">{{ trans('back-app.public') }}</option>
                            @endif
                        </select>
                    </div>
                    @endif
                    <button type="button"  onclick="return update_records();" id="submit_action" class="btn btn-sm btn-info pull-left"><i class="fa fa-location-arrow" aria-hidden="true"></i> {{ trans('back-app.submit') }}</button>&nbsp;  
                </div>
                {{-- ************** END:: ACTION OPTION ******************* --}}
                <table class="table table-striped table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            <th><input type="checkbox" onchange="return check_lists();" id="checkAll" name="checkAll"/></th>
                            <th>Id</th>
                            <th>{{ trans('back-app.title') }}</th>
                            <th>Shortcode</th>
                            <th>{{ trans('back-app.created') }}</th>
                            <th>{{ trans('back-app.status') }}</th>
                            @if(CAN('slides','update') || CAN('slides','destroy'))
                            <th>{{ trans('back-app.action') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($slides))
                        @foreach($slides as $slide)
                        <tr>
                            <td><input type="checkbox" id="checkSingle" name="checkSingle" value="{{$slide->id}}" /></td>
                            <td>{{$slide->id}}</td>
                            <td><span class="badge badge-success">{{$slide->title}} </span></td>
                            <td>
                                <span style="color: #d23f3f;font-weight: 500">
                                    @php
                                        echo "slide('";
                                        echo $slide->alias;
                                        echo "')";
                                    @endphp
                                </span>
                            </td>
                            <td>{{Carbon\Carbon::parse($slide->created_at)->format('d-m-Y')}}</td>
                            <td>
                                @if($slide->is_published())
                                <span class="badge badge-success">{{ trans('back-app.public') }}</span>
                                @else
                                <span class="badge badge-warning">{{ trans('back-app.disable') }}</span>
                                @endif
                            </td>
                             @if(CAN('slides','update') || CAN('slides','destroy'))
                            <td>
                                {{-- Edit --}}
                                @if(CAN('slides','update'))
                                    <a class="btn btn-info pull-left" href="{{route('admincp.slides.edit',$slide->id)}}">
                                    <i class="fa fa-edit "></i></a>
                                @endif
                                    {{-- Delete --}}
                                    @if(CAN('slides','destroy'))
                                    <form action="{{route('admincp.slides.destroy',$slide->id)}}" method="post" class="pull-left">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                        <input type="hidden" name="_method" value="DELETE"> 
                                        <a class="btn btn-danger" href="#" onclick="$(this).closest('form').submit()">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </form>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection