@extends('admincp.layouts.main')
@section('title',trans('back-app.user_group'))
@section('content')
<div class="container-fluid">
    {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">
    {{-- ************** Option Redirect ***************************** --}}
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                {{-- Add new --}}
                @if(CAN('users','create'))
                <a href="{{route('admincp.users.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
                @endif
                {{-- Cancel --}}
                <a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    {{-- ************** End Option Redirect ***************************** --}}
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
                {{-- ************** Option Action ***************************** --}}
                <div class="form-group row">
                    @if(CAN('users','destroy') || CAN('users','update'))
                    <div class="col-md-2 ">
                        <select id="action_table" name="action_table" class=" form-control pull-left" size="1">
                            <option value="">-- {{ trans('back-app.action') }} --</option>
                            @if(CAN('users','destroy'))
                            <option value="{{ route('admincp.users.destroys')}}">{{ trans('back-app.delete') }}</option>
                            @endif
                            @if(CAN('users','update'))
                            <option value="{{ route('admincp.users.disable')}}">{{ trans('back-app.disable') }}</option>
                            <option value="{{ route('admincp.users.public')}}">{{ trans('back-app.public') }}</option>
                            @endif
                        </select>
                    </div>
                    <button type="button" onclick="return update_records();" id="submit_action" class="btn btn-sm btn-info pull-left"><i class="fa fa-location-arrow" aria-hidden="true"></i> {{ trans('back-app.submit') }}</button>&nbsp;
                    @endif          
                </div>
                {{-- ************** End Option Action ***************************** --}}
                <table class="table table-striped table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            <th><input type="checkbox" onchange="return check_lists();" id="checkAll" name="checkAll"/></th>
                            <th>STT</th>
                            <th>{{ trans('back-app.avatar') }}</th>
                            <th>{{ trans('back-app.name') }}</th>
                            <th>{{ trans('back-app.email') }}</th>
                            <th>{{ trans('back-app.phone') }}</th>
                            <th>{{ trans('back-app.created') }}</th>
                            <th>{{ trans('back-app.status') }}</th>
                            @if(CAN('users','update') || CAN('users','destroy'))
                            <th>Action</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @if(isset($users))
                        @foreach($users as $user)
                        <tr>
                            <td><input type="checkbox" id="checkSingle" name="checkSingle" value="{{$user->id}}" /></td>
                            <td>{{$i}}</td>
                            <td><img width="50px" src="{{$user->profile_image }}"></td>
                            <td><span class="badge badge-info">{{$user->full_name}}</span></td>
                            <td><span class=""><span class="badge badge-primary">{{$user->email}}</span></td>
                            <td>
                               <span class="badge badge-info">{{$user->phone}}</span>
                            </td>
                            <td>{{Carbon\Carbon::parse($user->created_at)->format('d-m-Y')}}</td>
                            <td>
                                @if($user->is_published())
                                <span class="badge badge-success">{{ trans('back-app.public') }}</span>
                                @else
                                <span class="badge badge-warning">{{ trans('back-app.disable') }}</span>
                                @endif
                            </td>
                             @if(CAN('users','update') || CAN('users','destroy'))
                                <td>
                                    @if(CAN('users','update'))
                                    <a class="btn btn-info pull-left" href="{{route('admincp.users.edit',$user->id)}}">
                                        <i class="fa fa-edit "></i></a>
                                    @endif
                                    @if(CAN('users','destroy'))
                                    {{-- Delete --}}
                                    <form action="{{route('admincp.users.destroy',$user->id)}}" method="post" class="pull-left">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                        <input type="hidden" name="_method" value="DELETE"> 
                                        <a class="btn btn-danger" href="#" onclick="$(this).closest('form').submit()">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </form>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
