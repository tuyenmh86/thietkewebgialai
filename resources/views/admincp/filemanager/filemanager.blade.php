@extends('admincp.layouts.main')
@section('title',trans('back-app.media'))
@section('content')
<div class="container">
	<div class="animated fadeIn">
		<div class="container">
			<iframe src="/laravel-filemanager?type=image" style="width: 100%; height: 800px; overflow: hidden; border: none;"></iframe>
		</div>
	</div>
</div>
@endsection