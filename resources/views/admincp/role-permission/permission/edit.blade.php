@extends('admincp.layouts.main')
@section('title',trans('back-app.permissions'))
@section('content')
<div class="container">
	<form action="{{route('admincp.permission.update',$permission->id)}}" method="post" class="form-horizontal " enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="PUT">
		{{-- Option --}}
		<div class="col-md-12 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						{{-- Add new --}}
						<a href="{{route('admincp.permission.store')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.save') }}</button></a>
						{{-- Save --}}
						<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{ trans('back-app.save') }}</button>
						{{-- Reset --}}
						<button type="reset" class="btn btn-sm btn-info"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.reset') }}</button>
						{{-- Cancel --}}
						<a href="{{route('admincp.permission.index')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 1 --}}
		<div class="col-md-8 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-info-circle" aria-hidden="true"></i><strong>{{ trans('back-app.information') }}</strong>
					</div>
					<div class="card-block">
						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="name">{{ trans('back-app.name') }}(<span class="required_cl">*</span>)</label>
							<div class="col-md-9">
								<input type="text" id="name" name="name" class="form-control {{ ($errors->has('name') ? 'has-error' : '')}}" value="{{$permission->name}}" placeholder="Create Role">
								<span class="help-block mess-err">{{$errors->first('name')}}</span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="display_name">{{ trans('back-app.display_name') }}(<span class="required_cl">*</span>)</label>
							<div class="col-md-9">
								<input type="text" id="display_name" name="display_name" class="form-control {{ ($errors->has('display_name') ? 'has-error' : '')}}" value="{{$permission->display_name}}" placeholder="Create.role">
								<span class="help-block mess-err">{{$errors->first('display_name')}}</span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="module_name">Chức năng</label>
							<div class="col-md-9">
								<input type="text" id="module_name" name="module_name" class="form-control {{ ($errors->has('module_name') ? 'has-error' : '')}}" value="{{$permission->module_name}}" placeholder="Roles">
								<span class="help-block mess-err">{{$errors->first('module_name')}}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 2 --}}
		<div class="col-md-4 pull-right">
			{{-- Block 2 | Block 1 --}}
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-globe" aria-hidden="true"></i><strong>{{ trans('back-app.option') }}</strong>
					</div>
					<div class="card-block">
							<div class="form-group row">
								<div class="col-md-12">
									@if($permission->is_published())
									<select id="published" name="published" class="form-control" size="1">
										<option value="1">{{ trans('back-app.public') }}</option>
										<option value="0">{{ trans('back-app.disable') }}</option>
									</select>
									@else
									<select id="published" name="published" class="form-control" size="1">
										<option value="0">{{ trans('back-app.disable') }}</option>
										<option value="1">{{ trans('back-app.public') }}</option>
									</select>
									@endif
								</div>
							</div>
						<div class="form-group row">
							<div class="col-md-12">
								<input type="number" id="pos" name="pos" class="form-control {{ ($errors->has('pos') ? 'has-error' : '')}}" value="{{$permission->pos	}}" placeholder="Position">
								<span class="help-block mess-err">{{$errors->first('pos')}}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
