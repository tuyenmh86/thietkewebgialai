@extends('admincp.layouts.main')
@section('title',trans('back-app.permissions'))
@section('content')
<div class="container-fluid">
     {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">
    
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                {{-- Add new  --}}
                <a href="{{route('admincp.permission.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
                <a href="{{route('admincp.role.index')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
                {{-- ************** Option Action ***************************** --}}
                <div class="form-group row">
                    <div class="col-md-2 ">
                        <select id="action_table" name="action_table" class=" form-control pull-left" size="1">
                            <option value="">-- {{ trans('back-app.action') }} --</option>
                            <option value="{{ route('admincp.permission.destroys')}}">{{ trans('back-app.delete') }}</option>
                            <option value="{{ route('admincp.permission.disable')}}">{{ trans('back-app.disable') }}</option>
                            <option value="{{ route('admincp.permission.public')}}">{{ trans('back-app.public') }}</option>
                        </select>
                    </div>
                    <button type="button" onclick="return update_records();" id="submit_action" class="btn btn-sm btn-info pull-left"><i class="fa fa-location-arrow" aria-hidden="true"></i> {{ trans('back-app.submit') }}</button>&nbsp;
                </div>
                {{-- ************** End Option Action ***************************** --}}
                <table class="table table-striped table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="checkAll" onchange="return check_lists();" name="checkAll"/></th>
                            <th>Id</th>
                            <th>{{ trans('back-app.name') }}</th>
                            <th>Chức năng</th>
                            <th>{{ trans('back-app.display_name') }}</th>
                            <th>{{ trans('back-app.created') }}</th>
                            <th>{{ trans('back-app.pos') }}</th>
                            <th>{{ trans('back-app.status') }}</th>
                            <th>{{ trans('back-app.action') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($permissions))
                        @foreach($permissions as $permission)
                        <tr>
                            <td><input type="checkbox" id="checkSingle" name="checkSingle" value="{{$permission->id}}" /></td>
                            <td>{{$permission->id}}</td>
                            <td><span class="badge badge-success">{{$permission->name}} </span></td>
                            <td><span class="badge badge-info">{{$permission->module_name}} </span></td>
                            <td><input type="text" name="display_name" class="form-control updateDisplayName" id="{{$permission->id}}" value="{{$permission->display_name}} " style="color: #444444;font-weight: 500"></td>
                            {{-- <td>{{($permission->module_name) ? $permission->module_name : '...' }}</td> --}}
                            <td>{{Carbon\Carbon::parse($permission->created_at)->format('d-m-Y')}}</td>
                            <td><input type="number" name="pos" class="form-control updatePos" onkeyup="return update_posision('{!! route('admincp.permission.pos.update') !!}',{!! $permission->id; !!},this.value);" id="{{$permission->id}}" value="{{$permission->pos}}"></td>
                            <td>
                                @if($permission->is_published())
                                    <span class="badge badge-success">{{ trans('back-app.public') }}</span>
                                @else
                                    <span class="badge badge-danger">{{ trans('back-app.disable') }}</span>
                                @endif
                            </td>
                            <td> 
                                  {{-- Edit --}}
                                <a class="btn btn-info pull-left" href="{{route('admincp.permission.edit',$permission->id)}}">
                                    <i class="fa fa-edit "></i></a>
                                    <form action="{{route('admincp.permission.destroy',$permission->id)}}" method="post" class="pull-left">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                        <input type="hidden" name="_method" value="DELETE"> 
                                        <a class="btn btn-danger" href="#" onclick="$(this).closest('form').submit()">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection
    {{-- Using Script --}}
    @section('script')
    <script type="text/javascript">
        // ************************** Update DisplayName ********************
        $('.updateDisplayName').change(function(){
            if($.trim($(this).val().length)  > 50){
                alert('Không được nhập quá 50 ký tự!');
            }
            else if(hasWhiteSpace($.trim($(this).val()))){
                alert('Chuỗi không được chứa khoảng trắng!');
            }
            else {
                var displayName = $.trim($(this).val());
                var id = $(this).attr("id");
                $.ajax({
                url: "{{route('admincp.permission.displayname.update')}}",
                type: "post",
                data: {"_token": "{{ csrf_token() }}", "displayName":displayName,"id":id} ,
                success: function (success) {
                    if(success == 'success'){
                        location.reload(); 
                    } 
                    else {
                        alert('Cập nhập thất bại.Vui lòng thử lại.');
                    }
                },
                error: function() {
                    alert('Cập nhập thất bại.Vui lòng thử lại.');
                }
            });
            }
        });
</script>
@endsection