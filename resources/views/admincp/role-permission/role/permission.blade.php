@extends('admincp.layouts.main')
@section('title',trans('back-app.role_permission'))
@section('content')
<div class="container">
<form action="{{route('admincp.permission-role.post')}}" method="post" class="form-horizontal " enctype="multipart/form-data">
<input type="hidden" name="_token" value="{{csrf_token()}}">
<input type="hidden" name="role_permission" id="role_permission" value="">
<div class="animated fadeIn">
<div class="card">
<div class="card-header">
{{-- Save --}}
<button type="submit" class="btn btn-sm btn-info" id="submit_frm_per"><i class="fa fa-floppy-o" aria-hidden="true" ></i> {{ trans('back-app.save') }}</button>
{{-- Reset --}}
<button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.reset') }}</button>

{{-- Cancel --}}
<a href="{{route('admincp.role.index')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
</div>
</div>
</div>
<div class="animated fadeIn">
<div class="card">
<div class="card-block">
<div class="card">
<div class="card-header">
<i class="fa fa-align-justify"></i> {{ trans('back-app.role_permission') }}
</div>
<div class="card-block">
<div class="col-md-12 mb-2">
	<div class="row">
		<div class="col-md-3">
			<ul class="nav nav-tabs" role="tablist"  style="display: block;background: #444D58;color: black">
				{{-- Handing Active --}}
				<?php $i= 1; ?>
				@if(isset($modules))
				{{-- Module Feature For Posion --}}
				@foreach($modules as $pos)
				<li class="nav-item">
					<a class="nav-link {{($i == 1) ? 'active' : ''}}" data-toggle="tab" href="#home{{$pos}}" role="tab" aria-controls="home">
						{{ strtoupper(getListsByPosision($pos,1)->module_name) }}
					</a>
				</li>
				<?php $i++; ?>
				@endforeach
				@endif
			</ul>
		</div>
		<div class="col-md-9" style="top: -15px">
			<div class="tab-content">
				{{-- Handing Active --}}
				<?php $j = 1; ?>
				@if(isset($modules))
				@foreach($modules as $pos)			
				<div class="row tab-pane{{($j == 1) ? ' active' : ''}}" id="home{{$pos}}" role="tabpanel">
				<?php $j++; ?>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Action Module</th>
								@if(isset($roles))
								@foreach($roles as $role)
								<th><i class="fa fa-check" aria-hidden="true"></i>{{$role->name}}</th>
								@endforeach
								@endif
							</tr>
						</thead>
						<tbody>
						{{-- // Eeach Data By Pos --}}
							@foreach(getListsByPosision($pos) as $permission)
							<tr>
								<td>{{$permission->name}}</td>
								@if(isset($roles))
									@foreach($roles as $role)
										<td>
											<label class="switch switch-icon switch-pill switch-danger-outline-alt">
												<input type="checkbox" class="checkboxClass switch-input" name="checkbox_role" value="{{$permission->id}}_{{$role->id}}"
												<?php
												$role_permission = DB::table('permission_role')->where('permission_id',$permission->id)->where('role_id',$role->id)->first();
												if($role_permission) echo 'checked=""';
												?>
												>
												<span class="switch-label" data-on="" data-off=""></span>
												<span class="switch-handle"></span>
											</label>
										</td>
									@endforeach
								@endif
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				@endforeach
				@endif
			</div>
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</form>
</div>
@endsection
@section('script')
<script type="text/javascript">
$('#submit_frm_per').on('click',function(){
var permission = new Array();
var n = jQuery(".checkboxClass:checked").length;
if (n > 0){
jQuery(".checkboxClass:checked").each(function(){
permission.push($(this).val());
});
}
$('#role_permission').attr('value',permission);
});     
</script>
@endsection