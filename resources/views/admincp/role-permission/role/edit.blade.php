@extends('admincp.layouts.main')
@section('title',trans('back-app.roles'))
@section('content')
<div class="container">
	<form action="{{route('admincp.role.update',$role->id)}}" method="post" class="form-horizontal " enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="PUT">
		{{-- Option --}}
		<div class="col-md-12 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<a href="{{route('admincp.role.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
						<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{ trans('back-app.save') }}</button>
						{{-- Reset --}}
						<button type="reset" class="btn btn-sm btn-info"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.disable') }}</button>
						<a href="{{route('admincp.role.index')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 1 --}}
		<div class="col-md-8 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-info-circle" aria-hidden="true"></i><strong>{{ trans('back-app.information') }}</strong>
					</div>
					<div class="card-block">
						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="name">{{ trans('back-app.name') }}(<span class="required_cl">*</span>)</label>
							<div class="col-md-9">
								<input type="text" id="name" name="name" class="form-control {{ ($errors->has('name') ? 'has-error' : '')}}" value="{{$role->name}}" placeholder="Administrator">
								<span class="help-block mess-err">{{$errors->first('name')}}</span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="display_name">{{ trans('back-app.display_name') }}(<span class="required_cl">*</span>)</label>
							<div class="col-md-9">
								<input type="text" id="display_name" name="display_name" class="form-control {{ ($errors->has('display_name') ? 'has-error' : '')}}" value="{{$role->display_name}}" placeholder="Administrator">
								<span class="help-block mess-err">{{$errors->first('display_name')}}</span>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-3 form-control-label" for="description">{{ trans('back-app.desc') }}</label>
							<div class="col-md-9">
								<input type="text" id="description" name="description" class="form-control {{ ($errors->has('description') ? 'has-error' : '')}}" value="{{$role->description}}" placeholder="Administrator is ...">
								<span class="help-block mess-err">{{$errors->first('description')}}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 2 --}}
		<div class="col-md-4 pull-right">
			{{-- Block 2 | Block 1 --}}
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-globe" aria-hidden="true"></i><strong>{{ trans('back-app.option') }}</strong>
					</div>
					<div class="card-block">
						<div class="form-group row">
							<div class="col-md-12">
								@if($role->is_published())
									<select id="published" name="published" class="form-control" size="1">
										<option value="1">{{ trans('back-app.public') }}</option>
										<option value="0">{{ trans('back-app.disable') }}</option>
									</select>
								@else
									<select id="published" name="published" class="form-control" size="1">
										<option value="0">{{ trans('back-app.disable') }}</option>
										<option value="1">{{ trans('back-app.public') }}</option>
									</select>
								@endif
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<input type="number" id="pos" name="pos" class="form-control {{ ($errors->has('pos') ? 'has-error' : '')}}" value="{{$role->pos	}}" placeholder="{{ trans('back-app.pos') }}">
								<span class="help-block mess-err">{{$errors->first('pos')}}</span>
							</div>
						</div>
						{{-- ****** BEGIN :: FEATURED ******** --}}
						<div class="form-group row">
							<div class="col-md-12"><br>
								<div class="form-group row">
									<label class="col-md-5 form-control-label" for="cn_title"><strong>Quyền nâng cao</strong></label>
									<div class="col-md-7">
										<label class="switch switch-icon switch-pill switch-danger-outline-alt">
											<input type="checkbox" class="checkboxClass switch-input" name="advanced" value="1"
											{{ $role->hasAdvanced() ? 'checked=""' : '' }}
											>
											<span class="switch-label" data-on="" data-off=""></span>
											<span class="switch-handle"></span>
										</label>
									</div>
								</div>
							</div>
						</div>
						{{-- ****** END :: FEATURED ******** --}}
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
