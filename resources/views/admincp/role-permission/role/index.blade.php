@extends('admincp.layouts.main')
@section('title',trans('back-app.roles'))
@section('content')
<div class="container-fluid">
    {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                {{-- Add new --}}
                <a href="{{route('admincp.role.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
                <a href="{{route('admincp.permission-role')}}"><button type="button" class="btn btn-sm btn-warning"><i class="fa fa-cogs" aria-hidden="true"></i> {{ trans('back-app.role_permission') }}</button></a>
                <a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
            {{-- ************** Option Action ***************************** --}}
                <div class="form-group row">
                    <div class="col-md-2 ">
                        <select id="action_table" name="action_table" class=" form-control pull-left" size="1">
                            <option value="">-- {{ trans('back-app.action') }} --</option>
                            <option value="{{ route('admincp.role.destroys')}}">{{ trans('back-app.delete') }}</option>
                            <option value="{{ route('admincp.role.disable')}}">{{ trans('back-app.disable') }}</option>
                            <option value="{{ route('admincp.role.public')}}">{{ trans('back-app.public') }}</option>
                        </select>
                    </div>
                    <button type="button" onclick="update_records()" id="submit_action" class="btn btn-sm btn-info pull-left"><i class="fa fa-location-arrow" aria-hidden="true"></i> {{ trans('back-app.submit') }}</button>&nbsp;     
                </div>
                {{-- ************** End Option Action ***************************** --}}
                <table class="table table-striped table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="checkAll" name="checkAll" onchange="return  check_lists();" /></th>
                            <th>STT</th>
                            <th>{{ trans('back-app.name') }}</th>
                            <th>Quyền</th>
                            <th>{{ trans('back-app.created') }}</th>
                            <th>{{ trans('back-app.pos') }}</th>
                            <th>{{ trans('back-app.status') }}</th>
                            <th>{{ trans('back-app.action') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($roles))
                        @php
                        $i = 1;
                        @endphp
                        @foreach($roles as $role)
                        <tr>
                            <td><input type="checkbox" id="checkSingle" name="checkSingle" value="{{$role->id}}" /></td>
                            <td>{{$i}}</td>
                            <td><span class="badge badge-danger">{{$role->name}} </span></td>
                            <td>{{$role->display_name}}</td>
                            <td>{{Carbon\Carbon::parse($role->created_at)->format('d-m-Y')}}</td>
                            <td><input width="20px" type="number" onkeyup="return update_posision('{!! route('admincp.role.pos.update') !!}',{!! $role->id; !!},this.value);" name="pos" class="form-control updatePos" value="{{$role->pos}}"></td>
                            <td>
                                @if($role->is_published())
                                <span class="badge badge-success">{{ trans('back-app.public') }}</span>
                                @else
                                <span class="badge badge-warning">{{ trans('back-app.disable') }}</span>
                                @endif
                            </td>
                            <td>
                                {{-- Edit --}}
                                <a class="btn btn-info pull-left" href="{{route('admincp.role.edit',$role->id)}}">
                                    <i class="fa fa-edit "></i></a>
                                    <form action="{{route('admincp.role.destroy',$role->id)}}" method="post" class="pull-left">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                        <input type="hidden" name="_method" value="DELETE"> 
                                        <a class="btn btn-danger" href="#" onclick="$(this).closest('form').submit()">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </form>
                                </td>
                            </tr>
                            @php
                            $i++;
                            @endphp
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection