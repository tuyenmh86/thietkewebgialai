@extends('admincp.layouts.main')
@section('title',trans('back-app.menu'))
@section('content')
<div class="container">
	<div class="animated fadeIn">
		<div class="container">
			{!! Menu::render() !!}
		</div>
	</div>
</div>
@endsection