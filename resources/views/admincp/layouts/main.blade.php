<!--

 * GenesisUI - Bootstrap 4 Admin Template

 * @version v1.8.0

 * @link https://genesisui.com

 * Copyright (c) 2017 creativeLabs Łukasz Holeczek

 * @license Commercial

 -->

<!DOCTYPE html>

<html lang="en">





<!-- Mirrored from genesisui.com/demo/prime/bootstrap4-static/ by HTTrack Website Copier/3.x [XR&CO2014], Thu, 23 Mar 2017 08:53:09 GMT -->

<head>



    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="#">

    <meta name="author" content="{{ getConfigSite('site_copyright') }}">

    <meta name="keyword" content="#">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ getConfigSite('site_favicon')}}">



    <title>{{ getConfigSite('site_title') }} | @yield('title')</title>



    <!-- Icons -->

    <link href="{{admin_public_url()}}/css/font-awesome.min.css" rel="stylesheet">

    <link href="{{admin_public_url()}}/css/simple-line-icons.css" rel="stylesheet">



    <!-- Premium Icons -->

    <link href="{{admin_public_url()}}/css/glyphicons.css" rel="stylesheet">

    <link href="{{admin_public_url()}}/css/glyphicons-filetypes.css" rel="stylesheet">

    <link href="{{admin_public_url()}}/css/glyphicons-social.css" rel="stylesheet">

    

    {{-- Input Tags --}}

    <link href="{{admin_public_url()}}/css/input_tags/bootstrap-tagsinput.css" rel="stylesheet">

    <link href="{{admin_public_url()}}/css/input_tags/custom_label_tag.css" rel="stylesheet">



    <!-- Main styles for this application -->

    <link href="{{admin_public_url()}}/css/style.css" rel="stylesheet">
    
    <link href="{{admin_public_url()}}/css/custom.css" rel="stylesheet">

    

    {{-- Select Multilpe --}}

    {{-- <link href="{{admin_public_url()}}/css/select2/select2.css" rel="stylesheet">

     --}}

     <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

    

    {{-- Log-Viewer Packed --}}

    <link href="{{admin_public_url()}}/css/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">

    

    <!-- Fancybox -->

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.css">

    

     {{-- TINYMCE --}}

    {{-- <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=k41xq0nvnxj4z9j4ynlc3lft8d7rzl13pj9x1werh2rio8ho"></script> --}}

    <script src="{{admin_plugin_url()}}/tinymce/js/tinymce/tinymce.min.js"></script>

    

    @yield('header')

    @stack('header')





</head>



<!-- BODY options, add following classes to body to change options



// Header options

1. '.header-fixed'                  - Fixed Header



// Sidebar options

1. '.sidebar-fixed'                 - Fixed Sidebar

2. '.sidebar-hidden'                - Hidden Sidebar

3. '.sidebar-off-canvas'        - Off Canvas Sidebar

4. '.sidebar-compact'               - Compact Sidebar Navigation (Only icons)



// Aside options

1. '.aside-menu-fixed'          - Fixed Aside Menu

2. '.aside-menu-hidden'         - Hidden Aside Menu

3. '.aside-menu-off-canvas' - Off Canvas Aside Menu



// Footer options

1. 'footer-fixed'                       - Fixed footer



-->



<body class="app aside-menu-fixed aside-menu-hidden">

    <header class="app-header navbar">

        <button class="navbar-toggler mobile-sidebar-toggler hidden-lg-up" type="button">☰</button>

        <a class="navbar-brand" href="{{route('dashboard')}}"><center><img width="70" src="{{getConfigSite('site_logo')}}" alt=""></center></a>

        <ul class="nav navbar-nav hidden-md-down">

            <li class="nav-item">

                <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>

            </li>

            {{-- <li class="nav-item px-1">

                <a class="nav-link" href="{{admin_public_url()}}/#">Dashboard</a>

            </li> --}}

        </ul>

        <ul class="nav navbar-nav ml-auto">

            {{-- <li class="nav-item dropdown hidden-md-down">

                <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">

                    <i class="fa fa-language"></i>

                    <span class="badge badge-pill badge-danger">{{$lang = session('backend-locale') ?: 'en'}}</span>

                </a>

                <div class="dropdown-menu dropdown-menu-right">

                    <a href="#" class="dropdown-item" data-lang="vi" onclick="changelangBackend(this)">

                        <img src="admin/admin/img/flag-vietnam.png" alt="" style="margin-right:10px"> {{trans('back-app.viLang')}}

                    </a>

                    <a href="#" class="dropdown-item" data-lang="en" onclick="changelangBackend(this)">

                        <img src="admin/admin/img/flag-united-kingdom.png" alt="" style="margin-right:10px"> {{trans('back-app.engLang')}}

                    </a>

                </div>

            </li> --}}

            <li class="nav-item dropdown">

                <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">

                    <img width="50px" src="{{Auth::guard('admin')->user()->profile_image}}" class="img-avatar" alt="">

                    <span class="hidden-md-down">{{Auth::guard('admin')->user()->username}}</span>

                </a>

                <div class="dropdown-menu dropdown-menu-right">

                    <a class="dropdown-item" href="{{route('admincp.profile.edit',auth('admin')->user()->id)}}"><i class="fa fa-bell-o"></i> {{ trans('back-app.profile') }}<span class="badge badge-info">1</span></a>

                    <a class="dropdown-item" href="{{route('admincp.logout')}}"><i class="fa fa-lock"></i> {{ trans('back-app.logout') }}</a>

                </div>

            </li>

        </ul>

    </header>



    <div class="app-body">

        <!-- // Load Nav  -->

        @include('admincp.layouts.nav')

        <!-- // End Load Nav -->

        <!-- Main content -->

        <main class="main">

            <!-- Breadcrumb -->

            @include('admincp.layouts.breadcrumbs')

             <!-- Alert msg -->

            @include('admincp.layouts.alert-message')

            <!-- // End Load Breadcrumbs -->

            <!-- // Load Content -->

            @yield('content')

            <!-- // End Load Content -->



        </main>

        {{-- Yield Modals --}}

        @yield('modals')

    </div>



    <footer class="app-footer">

        {{getConfigSite('site_copyright')}}

    </footer>

    

    

    <!-- Main function -->

    <script src="{{admin_public_url()}}/js/main.js"></script>



    <!-- Bootstrap and necessary plugins -->

    <script src="{{admin_public_url()}}/js/libs/jquery.min.js"></script>

    {{-- <script src="{{admin_public_url()}}/js/libs/tether.min.js"></script> --}}

    <script src="{{admin_public_url()}}/js/libs/bootstrap.min.js"></script>

    {{-- <script src="{{admin_public_url()}}/js/libs/pace.min.js"></script> --}}



    

    <!-- Plugins and scripts required by all views -->

    <script src="{{admin_public_url()}}/js/libs/Chart.min.js"></script>



    <!-- Fancybox -->

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.js"></script>



    <!-- GenesisUI main scripts -->



    <script src="{{admin_public_url()}}/js/app.js"></script>



     <!-- // DataTable -->

    <script src="{{admin_public_url()}}/js/libs/jquery.dataTables.min.js"></script>

    <script src="{{admin_public_url()}}/js/libs/dataTables.bootstrap4.min.js"></script>





    <!-- Custom scripts required by this view -->

    <script src="{{admin_public_url()}}/js/views/tables.js"></script>

    

    <!-- Select 2 (Slect multiple) -->

    {{-- <script src="{{admin_public_url()}}/js/select2/select2.min.js"></script> --}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    

    <!-- // End DataTable -->



    <!-- Custom scripts required by this view -->

    <script src="{{admin_public_url()}}/js/custom.js"></script>

    

    {{-- Input Tags --}}

    <script src="{{admin_public_url()}}/js/input_tags/bootstrap-tagsinput.min.js"></script>



    {{-- include ajax language --}}

    <script src="{{admin_public_url()}}/js/language.js"></script> 

    {{-- Using script on Page --}}

    @yield('script')

    @stack('footer')

    <script type="text/javascript">

    // ************************ BEGIN:: TINYMCE ****************************

        var editor_config = {

        path_absolute : "/",

        selector: "textarea.myEditor",

        plugins: [

          "advlist autolink lists link image charmap print preview hr anchor pagebreak",

          "searchreplace wordcount visualblocks visualchars code fullscreen",

          "insertdatetime media nonbreaking save table contextmenu directionality",

          "emoticons template paste textcolor colorpicker textpattern textcolor colorpicker"

        ],

        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media fullpage | fullscreen | code forecolor backcolor",

        content_css : "/admin/admin/css/custom.css",

        relative_urls: false,

        convert_urls : false,

        verify_html: false,

        file_browser_callback : function(field_name, url, type, win) {

          var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;

          var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;



          var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;

          if (type == 'image') {

            cmsURL = cmsURL + "&type=Images";

          } else {

            cmsURL = cmsURL + "&type=Files";

          }



          tinyMCE.activeEditor.windowManager.open({

            file : cmsURL,

            title : 'Filemanager',

            width : x * 0.8,

            height : y * 0.8,

            resizable : "yes",

            close_previous : "no"

          });

        }

      };



      tinymce.init(editor_config);

    </script>

    {{-- // ****************** BEGIN:: FUNCTION FILES ***************** --}}

    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>

    <script type="text/javascript">

        // $('#lfm').filemanager('image');  

        $('[class*="lfm"]').each(function() {

            $(this).filemanager('image');

        });

        // *************** BEGIN:: CHOOSE TAGS && CATEGORIES ***************

        $(document).ready(function(){

            $('.categories').select2({

                 placeholder: "Select a category",

            });

            $('.tags').select2({

                 placeholder: "Select a tag",

            });

        });

    </script>

    <script type="text/javascript">

       document.onkeydown = preventEnterKey;

    </script>



</body>

<!-- Mirrored from genesisui.com/demo/prime/bootstrap4-static/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 23 Mar 2017 08:53:30 GMT -->

</html>