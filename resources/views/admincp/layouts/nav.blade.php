<div class="sidebar">

    <nav class="sidebar-nav">

        <ul class="nav">

            <li class="nav-item">

                <a class="nav-link" href="{{route('dashboard')}}"><i class="icon-speedometer"></i> {{ trans('back-app.dashboard') }} <span class="badge badge-info">{{ trans('back-app.new') }}</span></a>

            </li>



            <li class="divider"></li>

            {{--**********************  MODULE USERS ****************--}}

            @if(CAN('admins','index') || CAN('users','index') || CAN('roles','index'))

            <li class="nav-title">

                {{ trans('back-app.usermanager') }}

            </li>

            <li class="nav-item nav-dropdown">

                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-clipboard" aria-hidden="true"></i> {{ trans('back-app.usermanager') }}</a>

                <ul class="nav-dropdown-items">

                    {{-- Admins Manager --}}

                    @if(CAN('admins','index'))

                     <li class="nav-item">

                        <a class="nav-link" href="{{route('admincp.admins.index')}}"><i class="fa fa-users" aria-hidden="true"></i> {{ trans('back-app.admin_group') }}</a>

                    </li>

                    @endif

                    {{-- User Manager --}}

                    @if(CAN('users','index'))

                     <li class="nav-item">

                        <a class="nav-link" href="{{route('admincp.users.index')}}"><i class="fa fa-users" aria-hidden="true"></i> {{ trans('back-app.user_group') }}</a>

                    </li>

                    @endif

               

                    {{-- Role Permission --}}

                    @if(CAN('permissions','index'))

                     <li class="nav-item">

                        <a class="nav-link" href="{{route('admincp.permission.index')}}"><i class="fa fa-cogs" aria-hidden="true"></i> {{ trans('back-app.permissions') }}</a>

                    </li>

                    @endif


                    @if(CAN('roles','index'))

                     <li class="nav-item">

                        <a class="nav-link" href="{{route('admincp.role.index')}}"><i class="fa fa-cogs" aria-hidden="true"></i> {{ trans('back-app.role_permission') }}</a>

                    </li>

                    @endif

                </ul>

            </li>

            @endif

            

            {{--**********************  SLIDE ****************--}}

            @if(CAN('slides','index'))

                <li class="nav-title">

                    {{ trans('back-app.slides') }}

                </li>

                <li class="nav-item nav-dropdown">

                    <a class="nav-link" href="{{ route('admincp.slides.index') }}"><i class="fa fa-sliders" aria-hidden="true"></i> {{ trans('back-app.slides') }} </a>

                </li>

            @endif



            {{--**********************  MENU ****************--}}

            @if(CAN('menus','index'))

                <li class="nav-title">

                    {{ trans('back-app.menu') }}

                </li>

                <li class="nav-item nav-dropdown">

                    <a class="nav-link" href="{{ route('admincp.menu.index') }}"><i class="fa fa-bars" aria-hidden="true"></i> {{ trans('back-app.menu') }} </a>

                </li>

            @endif

            {{--**********************  FILE MANAGER****************--}}

            <li class="nav-title">

                {{ trans('back-app.media') }}

            </li>

            <li class="nav-item nav-dropdown">

                <a class="nav-link" href="{{ route('admincp.filemanager') }}"><i class="fa fa-camera-retro" aria-hidden="true"></i> {{ trans('back-app.media') }} </a>

            </li>



            {{--**********************  PAGE ****************--}}

            @if(CAN('pages','index'))

            <li class="nav-title">

                {{ trans('back-app.pages') }}

            </li>

            <li class="nav-item nav-dropdown">

                <a class="nav-link" href="{{ route('admincp.pages.index') }}"><i class="fa fa-file-text" aria-hidden="true"></i>{{ trans('back-app.pages') }} </a>

            </li>

            @endif
            
            {{-- CUSTOM FIELD --}}

            @if(CAN('customfields','index'))

            <li class="nav-item">

                <a class="nav-link" href="{{route('admincp.customfields.index')}}"><i class="fa fa-terminal" aria-hidden="true"></i> {{ trans('back-app.customfield') }}</a>

            </li>

            @endif


            {{-- ****************** MODULE POSTS && CATEGORYS ********--}}

            @if(CAN('post.categorys','index') ||CAN('posts','index') || CAN('post.tags','index'))

            <li class="nav-title">

                {{ trans('back-app.posts') }}

            </li>

            <li class="nav-item nav-dropdown">

                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-clipboard" aria-hidden="true"></i> {{ trans('back-app.posts') }}</a>

                <ul class="nav-dropdown-items">

                    {{-- Category --}}

                    @if(CAN('post.categorys','index'))

                        <li class="nav-item">

                            <a class="nav-link" href="{{route('admincp.post.categorys.index')}}"><i class="fa fa-list-ul" aria-hidden="true"></i> {{ trans('back-app.categories') }}</a>

                        </li>

                    @endif

                    {{-- posts --}}

                    @if(CAN('posts','index'))

                        <li class="nav-item">

                            <a class="nav-link" href="{{route('admincp.posts.index')}}"><i class="fa fa-clipboard" aria-hidden="true"></i> {{ trans('back-app.posts') }}</a>

                        </li>

                    @endif

                    {{-- Tags --}}

                    @if(CAN('post.tags','index'))

                        <li class="nav-item">

                            <a class="nav-link" href="{{route('admincp.post.tags.index')}}"><i class="fa fa-tags" aria-hidden="true"></i> {{ trans('back-app.tags') }}</a>

                        </li>

                    @endif

                </ul>

            </li>

            @endif

            {{-- ************ MODULE PRODUCTS && CATEGORYS && TAGS ********--}}

            @if(CAN('product.categorys','index') ||CAN('products','index') || CAN('product.tags','index'))

            <li class="nav-title">

                {{ trans('back-app.products') }}

            </li>

            <li class="nav-item nav-dropdown">

                <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-clipboard" aria-hidden="true"></i> {{ trans('back-app.products') }}</a>

                <ul class="nav-dropdown-items">

                    {{-- Category --}}

                    @if(CAN('product.categorys','index'))

                        <li class="nav-item">

                            <a class="nav-link" href="{{route('admincp.product.categorys.index')}}"><i class="fa fa-list-ul" aria-hidden="true"></i> {{ trans('back-app.categories') }}</a>

                        </li>

                    @endif

                    {{-- products --}}

                    @if(CAN('products','index'))

                        <li class="nav-item">

                            <a class="nav-link" href="{{route('admincp.products.index')}}"><i class="fa fa-clipboard" aria-hidden="true"></i> {{ trans('back-app.products') }}</a>

                        </li>

                    @endif

                    {{-- Tags --}}

                    @if(CAN('product.tags','index'))

                        <li class="nav-item">

                            <a class="nav-link" href="{{route('admincp.product.tags.index')}}"><i class="fa fa-tags" aria-hidden="true"></i> {{ trans('back-app.tags') }}</a>

                        </li>

                    @endif

                    {{-- Types --}}

                    @if(CAN('product.product-types','index'))

                        <li class="nav-item">

                            <a class="nav-link" href="{{route('admincp.product.product-types.index')}}"><i class="fa fa-tags" aria-hidden="true"></i> {{ trans('back-app.type') }}</a>

                        </li>

                    @endif

                    {{-- Attributes --}}

                    @if(CAN('product.product-attributes','index'))

                        <li class="nav-item">

                            <a class="nav-link" href="{{route('admincp.product.product-attributes.index')}}"><i class="fa fa-tags" aria-hidden="true"></i> {{ trans('back-app.attribute') }}</a>

                        </li>

                    @endif

                        <li class="nav-item">
                            <a class="nav-link" href="{{route('admincp.product.orders.index')}}"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i> Hóa đơn</a>
                        </li>
                        <li class="nav-item">

                                <a class="nav-link" href="{{route('admincp.products.importExport')}}"><i class="fa fa-tags" aria-hidden="true"></i>Import Export</a>
    
                            </li>
                </ul>
            </li>
            @endif

            {{-- ****************** MODULE CONTACT ********--}}

            @if(CAN('contacts','index') )

                <li class="nav-title">

                    {{ trans('back-app.contact')}}

                </li>

                <li class="nav-item">

                    <a class="nav-link" href="{{route('admincp.contacts.index')}}"><i class="fa fa-envelope" aria-hidden="true"></i>{{ trans('back-app.contact')}}

                        <span class="badge badge-info">{{ trans('back-app.new')}}</span>

                    </a>

                </li>

            @endif

            {{--**********************  WIDGETS ****************--}}

            @if(CAN('widgets','index'))

            <li class="nav-title">

                Widgets

            </li>

            <li class="nav-item nav-dropdown">

                <a class="nav-link" href="{{ route('admincp.widgets.index') }}"><i class="fa fa-code" aria-hidden="true"></i>Widgets </a>

            </li>

            @endif

            {{-- ****************** MODULE SETTING ********--}}

            {{-- @if(CAN('settings','index') ) --}}

                <li class="nav-title">

                    {{ trans('back-app.system')}}

                </li>

                <li class="nav-item">

                    <a class="nav-link" href="{{route('admincp.setting.index')}}"><i class="fa fa-cogs" aria-hidden="true"></i>{{ trans('back-app.system')}}

                    </a>

                </li>

            {{-- @endif --}}

        </ul>

    </nav>

</div>