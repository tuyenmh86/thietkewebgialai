<ol class="breadcrumb">
    @if(Request::is('admincp'))
    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{ trans('back-app.dashboard') }}</a></li>
    <li class="breadcrumb-item active">
        {{ trans('back-app.admincp') }}
    </li>
    @else
    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">{{ trans('back-app.dashboard') }}</a></li>
    {{-- Breadcrumbs Level 2 --}}
    <li class="breadcrumb-item">
        {{-- Profile --}}
        @if(Request::is('admincp/profile*'))
        <a href="{{route('admincp.profile.index')}}">{{ trans('back-app.profile') }}</a>
        @endif
        {{-- Admins --}}
        @if(Request::is('admincp/admins*'))
        <a href="{{route('admincp.admins.index')}}">{{ trans('back-app.admin_group') }}</a>
        @endif
        {{-- Users --}}
        @if(Request::is('admincp/users*'))
        <a href="{{route('admincp.users.index')}}">{{ trans('back-app.user_group') }}</a>
        @endif
        {{-- Role --}}
        @if(Request::is('admincp/role*'))
        <a href="{{route('admincp.role.index')}}">{{ trans('back-app.roles') }}</a>
        @endif
        {{-- Permission --}}
        @if(Request::is('admincp/permission*'))
        <a href="{{route('admincp.permission.index')}}">{{ trans('back-app.permissions') }}</a>
        @endif
        {{-- Settings --}}
        @if(Request::is('admincp/setting*'))
        <a href="{{route('admincp.setting.index')}}">{{ trans('back-app.setting') }}</a>
        @endif
        {{-- Post/Categorys --}}
        @if(Request::is('admincp/post/categorys*'))
        <a href="{{route('admincp.post.categorys.index')}}">{{ trans('back-app.categories') }}</a>
        @endif
        {{-- Tag/Categorys --}}
        @if(Request::is('admincp/post/tags*'))
        <a href="{{route('admincp.post.tags.index')}}">{{ trans('back-app.tags') }}</a>
        @endif
        {{-- Posts --}}
        @if(Request::is('admincp/posts*'))
        <a href="{{route('admincp.posts.index')}}">{{ trans('back-app.posts') }}</a>
        @endif
        {{-- Product/Categorys --}}
        @if(Request::is('admincp/product/categorys*'))
        <a href="{{route('admincp.product.categorys.index')}}">{{ trans('back-app.categories') }}</a>
        @endif
        {{-- Product/Tags --}}
        @if(Request::is('admincp/product/tags*'))
        <a href="{{route('admincp.product.tags.index')}}">{{ trans('back-app.tags') }}</a>
        @endif
       
        {{-- Products --}}
        @if(Request::is('admincp/products*'))
        <a href="{{route('admincp.products.index')}}">{{ trans('back-app.products') }}</a>
        @endif
        
        {{-- Contacts --}}
        @if(Request::is('admincp/contacts*'))
            <a href="{{route('admincp.contacts.index')}}">{{ trans('back-app.contact') }}</a>
        @endif
        {{-- Pages --}}
        @if(Request::is('admincp/pages*'))
            <a href="{{route('admincp.pages.index')}}">{{ trans('back-app.pages') }}</a>
        @endif
        {{-- Menu --}}
        @if(Request::is('admincp/menu*'))
            <a href="{{route('admincp.menu.index')}}">{{ trans('back-app.menu') }}</a>
        @endif
        {{-- Widgets --}}
        @if(Request::is('admincp/widgets*'))
            <a href="{{route('admincp.widgets.index')}}">Widgets</a>
        @endif
        {{-- Media --}}
        @if(Request::is('admincp/file/manager*'))
            <a href="{{route('admincp.filemanager')}}">Quản lý tập tin</a>
        @endif
        {{-- Slide --}}
        @if(Request::is('admincp/slides*'))
            <a href="{{route('admincp.slides.index')}}">Slide}</a>
        @endif

        {{-- CustomField --}}
        @if(Request::is('admincp/customfields*'))
            <a href="{{route('admincp.customfields.index')}}">CustomField</a>
        @endif

        {{-- Product Type --}}
        @if(Request::is('admincp/product/product-types*'))
            <a href="{{route('admincp.product.product-types.index')}}">Product Type</a>
        @endif

        {{-- Product Attribute --}}
        @if(Request::is('admincp/product/product-attributes*'))
            <a href="{{route('admincp.product.product-attributes.index')}}">Product Attribute</a>
        @endif

        {{-- Product Order --}}
        @if(Request::is('admincp/product/orders*'))
            <a href="{{route('admincp.product.orders.index')}}">Hóa đơn</a>
        @endif

        {{-- 404 Page --}}
        @if(Request::is('admincp/errors*'))
        <a href="{{route('errors.404')}}">Errors</a>
        @endif
    </li>
    {{-- Level 3 --}}
    <li class="breadcrumb-item active">
        {{-- Profile --}}
        {{(Request::is('admincp/profile') ? trans('back-app.list') : '')}}
        {{-- Profile --}}
        {{(Request::is('admincp/profile/*/edit') ? trans('back-app.edit') : '')}}
        {{-- Admins --}}
        {{(Request::is('admincp/admins/create') ? trans('back-app.add') : '')}}
        {{(Request::is('admincp/admins/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/admins') ? trans('back-app.list') : '')}}

        {{-- Users --}}
        {{(Request::is('admincp/users/create') ? trans('back-app.add') : '')}}
        {{(Request::is('admincp/users/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/users') ? trans('back-app.list') : '')}}

         {{-- Role --}}
        {{(Request::is('admincp/role') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/role/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/role/create') ? trans('back-app.add') : '')}}
        {{-- Permission --}}
        {{(Request::is('admincp/permission') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/permission/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/permission/create') ? trans('back-app.add') : '')}}
        {{-- Role-permission --}}
        {{(Request::is('admincp/permission-role') ? 'Permission-Role' : '')}}
        {{-- Setting --}}
        {{(Request::is('admincp/setting') ? trans('back-app.list') : '')}}
        {{-- Post/Categorys --}}
        {{(Request::is('admincp/post/categorys') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/post/categorys/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/post/categorys/create') ? trans('back-app.add') : '')}}
        {{-- Post/Categorys --}}
        {{(Request::is('admincp/post/tags') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/post/tags/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/post/tags/create') ? trans('back-app.add') : '')}}
        {{-- Posts --}}
        {{(Request::is('admincp/posts') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/posts/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/posts/create') ? trans('back-app.add') : '')}}
        {{-- Product/Categorys --}}
        {{(Request::is('admincp/product/categorys') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/product/categorys/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/product/categorys/create') ? trans('back-app.add') : '')}}
        {{-- Product/Tags --}}
        {{(Request::is('admincp/product/tags') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/product/tags/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/product/tags/create') ? trans('back-app.add') : '')}}
         {{-- Page --}}
        {{(Request::is('admincp/pages') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/pages/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/pages/create') ? trans('back-app.add') : '')}}
        {{-- Products --}}
        {{(Request::is('admincp/products') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/products/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/products/create') ? trans('back-app.add') : '')}}

        {{-- Product Type --}}
        {{(Request::is('admincp/product/product-types') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/product/product-types/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/product/product-types/create') ? trans('back-app.add') : '')}}

        {{-- Product Attributes --}}
        {{(Request::is('admincp/product/product-attributes') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/product/product-attributes/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/product/product-attributes/create') ? trans('back-app.add') : '')}}

        {{-- Widgets --}}
        {{(Request::is('admincp/widgets') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/widgets/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/widgets/create') ? trans('back-app.add') : '')}}

        {{-- Slides --}}
        {{(Request::is('admincp/slides') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/slides/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/slides/create') ? trans('back-app.add') : '')}}

        {{-- CustomField --}}
        {{(Request::is('admincp/customfields') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/customfields/create') ? trans('back-app.add') : '')}}

        {{-- Contacts --}}
        {{(Request::is('admincp/contacts') ? trans('back-app.list') : '')}}
        {{-- Menu --}}
        {{(Request::is('admincp/menu*') ? trans('back-app.menu') : '')}}
        {{-- 404 Page --}}
        {{(Request::is('admincp/errors/404') ? '404 ' : '')}}
        {{-- Media --}}
        {{(Request::is('admincp/file/manager*') ? 'Files ' : '')}}

        {{-- Orders --}}
        {{(Request::is('admincp/product/orders') ? trans('back-app.list') : '')}}
        {{(Request::is('admincp/product/orders/*/edit') ? trans('back-app.edit') : '')}}
        {{(Request::is('admincp/product/orders/create') ? trans('back-app.add') : '')}}
    </li>
    @endif
</ol>