@extends('admincp.layouts.main')
@section('title',trans('back-app.pages'))
@section('content')
<div class="container">
	<form action="{{route('admincp.pages.store')}}" method="post" class="form-horizontal " enctype="multipart/form-data" name="myform">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		{{-- Option --}}
		<div class="col-md-12 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						{{-- Save --}}
						<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{ trans('back-app.save') }}</button>
						{{-- Reset --}}
						<button type="reset" class="btn btn-sm btn-info"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.reset') }}</button>
						{{-- Cancel --}}
						<a href="{{route('admincp.pages.index')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
					</div>
				</div>
			</div>
		</div>
		{{-- ============ BEGIN:: BLOCK 1 ==============--}}
		<div class="col-md-12 pull-left">
			<div class="animated row fadeIn">
				<div class="col-md-12 mb-2">
					{{-- BEGIN:: CHOOSE TAB --}}
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#home1" role="tab" aria-controls="home"><i class="fa fa-question-circle" aria-hidden="true"></i> {{ trans('back-app.information') }}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#home3" role="tab" aria-controls="profile"><i class="fa fa-globe" aria-hidden="true"></i> {{ trans('back-app.web_master') }}</a>
						</li>
					</ul>
					{{-- END:: CHOOSE TAB --}}
					<div class="tab-content card">
						{{-- **************** INFOMATION BASIC *******  --}}
						<div class="row tab-pane active" id="home1" role="tabpanel">
							<div class="col-md-8 pull-left">
								@include('components.data_form')
								
								@include('components.customfield',['table' => 'pages'])
							</div>
							{{-- BENGIN:: BLOCK 2 --}}
							<div class="col-md-4 pull-right">
								{{-- Block 2 | Block 1 --}}
								<div class="animated fadeIn">
									<div class="card">
										<div class="card-header">
											<i class="fa fa-globe" aria-hidden="true"></i><strong>Option</strong>
										</div>
										<div class="card-block">
											@if(CAN('pages','create',true))
											<div class="form-group row">
												<div class="col-md-12">
													<select id="published" name="published" class="form-control" size="1">
														<option value="0">{{ trans('back-app.disable') }}</option>
														<option value="1">{{ trans('back-app.public') }}</option>
													</select>
												</div>
											</div>
											@endif
											<div class="form-group row">
												<div class="col-md-12">
													<input type="number" id="pos" name="pos" class="form-control {{ ($errors->has('pos') ? 'has-error' : '')}}" value="{{ \App\Models\Page\Page::max('pos')+1 }}" placeholder="Vị trí">
													<span class="help-block mess-err">{{$errors->first('pos')}}</span>
												</div>
											</div>
											{{-- ****** BEGIN :: FEATURED ******** --}}
											@if(CAN('pages','create',true))
											<div class="form-group row">
												<div class="col-md-12"><br>
													<div class="form-group row">
														<label class="col-md-3 form-control-label" for="cn_title"><strong>{{ trans('back-app.featured') }}</strong></label>
														<div class="col-md-9">
															<label class="switch switch-icon switch-pill switch-danger-outline-alt">
																<input type="checkbox" class="checkboxClass switch-input" name="featured" value="1">
																<span class="switch-label" data-on="" data-off=""></span>
																<span class="switch-handle"></span>
															</label>
														</div>
													</div>
												</div>
											</div>
											@endif
											{{-- ****** END :: FEATURED ******** --}}
										</div>
									</div>
								</div>

								{{-- Bolock 2 | Bock 3 --}}
								{{-- ==== BEGIN:: BLOCK 2 | BLOCK 4 ==== --}}
								<div class="animated fadeIn">
									<div class="card">
										<div class="card-header">
											<i class="fa fa-picture-o" aria-hidden="true"></i><strong>Featured Image</strong>
										</div>
										<div class="card-block">
											<div class="form-group row">
												<div class="col-md-12 input-group">
													<div class="input-group">
														<span class="input-group-btn">
															<a id="" data-input="thumbnail" data-preview="holder" class="lfm btn btn-primary">
																<i class="fa fa-picture-o"></i> {{ trans('back-app.choose') }}
															</a>
														</span>
														<input id="thumbnail" class="form-control" type="text" name="featured_image">
													</div>
												</div>
												<div class="col-md-12 input-group">
													<img id="holder" width="100%" style="margin-top:15px;"> 
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						{{-- ============== BEGIN::SEO ADVANCED ============ --}}
						@include('components.seo_advanced')
						{{-- ============== END::SEO ADVANCED ============ --}}
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection