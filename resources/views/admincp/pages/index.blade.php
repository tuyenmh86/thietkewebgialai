@extends('admincp.layouts.main')
@section('title',trans('back-app.pages'))
@section('content')
<div class="container-fluid">
    {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">

    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                {{-- Add new --}}
                @if(CAN('pages','create'))
                <a href="{{route('admincp.pages.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
                @endif
                {{-- Cancel --}}
                <a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
                {{-- ************** Option Action ***************************** --}}
                <div class="form-group row">
                    @if(CAN('pages','update') || CAN('pages','destroy'))
                    <div class="col-md-2 ">
                        <select id="action_table" name="action_table" class=" form-control pull-left" size="1">
                            <option value="">-- {{ trans('back-app.action') }} --</option>
                            @if(CAN('pages','destroy'))
                            <option value="{{ route('admincp.pages.destroys')}}">{{ trans('back-app.delete') }}</option>
                            @endif
                            @if(CAN('pages','update'))
                            <option value="{{ route('admincp.pages.disable')}}">{{ trans('back-app.disable') }}</option>
                            <option value="{{ route('admincp.pages.public')}}">{{ trans('back-app.public') }}</option>
                            @endif
                        </select>
                    </div>
                    @endif
                    <button type="button" onclick="return update_records();" id="submit_action" class="btn btn-sm btn-info pull-left"><i class="fa fa-location-arrow" aria-hidden="true"></i> {{ trans('back-app.submit') }}</button>&nbsp;          
                </div>
                {{-- ************** End Option Action ***************************** --}}
                <table class="table table-striped table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            <th><input type="checkbox" onchange="return check_lists();" id="checkAll" name="checkAll"/></th>
                            <th>Id</th>
                            <th>{{ trans('back-app.title') }}</th>
                            <th>{{ trans('back-app.admin_group') }}</th>
                            {{-- <th>Category</th> --}}
                            <th>{{ trans('back-app.created') }}</th>
                            <th>{{ trans('back-app.pos') }}</th>
                            <th>{{ trans('back-app.status') }}</th>
                            @if(CAN('pages','update') || CAN('pages','destroy'))
                            <th>{{ trans('back-app.action') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($pages))
                        @foreach($pages as $page)
                        <tr>
                            <td><input type="checkbox" id="checkSingle" name="checkSingle" value="{{$page->id}}" /></td>
                            <td>{{$page->id}}</td>
                            <td><span class="badge badge-success">{{substr($page->title,0,30)." ..."}} </span></td>
                            <td><a href="{{route('admincp.admins.edit',$page->admin_id)}}" >{{(isset($page->admin->username)) ? $page->admin->username : 'Null'}} </a></td>
                           {{--  <td><span class="badge badge-info">{{(isset($page->category->title)) ? $page->category->title : 'Null'}} </span></td> --}}
                            <td>{{Carbon\Carbon::parse($page->created_at)->format('d-m-Y')}}</td>
                            <td><input width="20px" type="number" name="pos" class="form-control updatePos"  onkeyup="return update_posision('{!! route('admincp.pages.pos.update') !!}',{!! $page->id; !!},this.value);" id="{{$page->id}}" value="{{$page->pos}}"></td>
                            <td>
                                @if($page->is_published())
                                <span class="badge badge-success">{{ trans('back-app.public') }}</span>
                                @else
                                <span class="badge badge-warning">{{ trans('back-app.disable') }}</span>
                                @endif
                            </td>
                            @if(CAN('pages','update') || CAN('pages','destroy'))
                            <td>
                                {{-- Edit --}}
                                @if(CAN('pages','update'))
                                <a class="btn btn-info pull-left" href="{{route('admincp.pages.edit',$page->id)}}">
                                    <i class="fa fa-edit "></i></a>
                                    @endif
                                    {{-- Delete --}}
                                    @if(CAN('pages','destroy'))
                                    <form action="{{route('admincp.pages.destroy',$page->id)}}" method="post" class="pull-left">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                        <input type="hidden" name="_method" value="DELETE"> 
                                        <a class="btn btn-danger" href="#" onclick="$(this).closest('form').submit()">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </form>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection
