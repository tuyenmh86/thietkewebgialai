@extends('admincp.layouts.main')
@section('title','CustomField Create')
@section('content')
<div class="container">
	<form action="{{route('admincp.customfields.store')}}" method="post" class="form-horizontal " enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		{{-- Option --}}
		<div class="col-md-12">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						{{-- Add new --}}
						<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{ trans('back-app.save') }}</button>
						{{-- Reset --}}
						<button type="reset" class="btn btn-sm btn-info"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.reset') }}</button>
						{{-- Cancel --}}
						<a href="{{route('admincp.customfields.index')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 1 --}}
		<div class="col-md-12">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<i class="fa fa-info-circle" aria-hidden="true"></i><strong>{{ trans('back-app.information') }}</strong>
					</div>
					<div class="col-md-12">
						<div class="card-block">
							<div class="form-group row">
								<label class="col-md-3 form-control-label" for="vi_title">{{ trans('back-app.title') }}(<span class="required_cl">*</span>)</label>
								<div class="col-md-9">
									<input type="text" id="vi_title" name="name" class="form-control {{ ($errors->has('vi_title') ? 'has-error' : '')}}" value="{{old('vi_title')}}" placeholder="" onkeyup="changeToSlug2(this.value,'');">
									<span class="help-block mess-err">{{$errors->first('vi_title')}} </span>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-md-3 form-control-label" for="vi_alias">Slug</label>
								<div class="col-md-9">
									<input type="text" id="_alias" name="field" class="form-control {{ ($errors->has('vi_alias') ? 'has-error' : '')}}" value="{{old('vi_alias')}}" placeholder="" readonly="">
									<span class="help-block mess-err">{{$errors->first('vi_alias')}} </span>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-md-3 form-control-label" for="vi_alias">{{ trans('back-app.categories') }}</label>
								<div class="col-md-9">
									<select class="form-control" name="table_name">
										<option value="categories_post">Danh mục bài viết</option>
										<option value="categories_product">Danh mục sản phẩm</option>
										<option value="pages">Trang nội dung</option>
										<option value="products">Sản phẩm</option>
										<option value="posts">Bài viết</option>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-md-3 form-control-label" for="vi_alias">{{ trans('back-app.type') }}</label>
								<div class="col-md-9">
									<select class="form-control" name="type">
										<option value="number">Number</option>
										<option value="text">Text</option>
										<option value="textarea">Textarea</option>
										<option value="editor">Editor</option>
										<option value="image">Image</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
