@extends('admincp.layouts.main')
@section('title',trans('back-app.customfield'))
@section('content')
<div class="container-fluid">
    {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">
    
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                {{-- Add new --}}
                @if(CAN('customfields','create'))
                <a href="{{route('admincp.customfields.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
                @endif
                {{-- Cancel --}}
                <a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
                <table class="table table-striped table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            <th><input type="checkbox" onchange="return check_lists();" id="checkAll" name="checkAll"/></th>
                            <th>Id</th>
                            <th>{{ trans('back-app.name') }}</th>
                            <th>{{ trans('back-app.field') }}</th>
                            <th>{{ trans('back-app.categories') }}</th>
                            <th>{{ trans('back-app.type') }}</th>
                            @if(CAN('customfields','update') || CAN('customfields','destroy'))
                            <th>{{ trans('back-app.action') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($data))
                        @foreach($data as $customfield)
                        <tr>
                            <td><input type="checkbox" id="checkSingle" name="checkSingle" value="{{$customfield->id}}" /></td>
                            <td>{{ $customfield->id }}</td>
                            <td><span class="badge badge-success">{{$customfield->name}} </span></td>
                            <td><span class="" style="font-weight: 600;color: #CC0000">
                            {{ $customfield->field }}
                            </span></td>
                            <td><span class="badge badge-primary">{{ $customfield->table_name }}</span></td>
                            <td><span class="badge badge-warning">{{ $customfield->type }}</span></td>
                             @if(CAN('customfields','destroy'))
                            <td>
                                    {{-- Delete --}}
                                    @if(CAN('customfields','destroy'))
                                    <form action="{{route('admincp.customfields.destroy',$customfield->id)}}" method="post" class="pull-left">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                        <input type="hidden" name="_method" value="DELETE"> 
                                        <a class="btn btn-danger" href="#" onclick="$(this).closest('form').submit()">
                                            <i class="fa fa-trash-o "></i>
                                        </a>
                                    </form>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection