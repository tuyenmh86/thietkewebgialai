@extends('admincp.layouts.main')
@section('title',trans('back-app.setting'))
@section('content')
<div class="container">
	<form action="{{route('admincp.setting.update',$setting->id)}}" method="post" class="form-horizontal " enctype="multipart/form-data" name="myform">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="_method" value="PUT">
		<!-- genarator password leng -->
		<input type="hidden" name="length" value="15">
		<div class="col-md-12 pull-left">
			<div class="animated fadeIn">
				<div class="card">
					<div class="card-header">
						<button type="submit" class="btn btn-sm btn-warning"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{ trans('back-app.save') }}</button>

						<button type="reset" class="btn btn-sm btn-info"><i class="fa fa-undo" aria-hidden="true"></i> {{ trans('back-app.reset') }}</button>

						<a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
					</div>
				</div>
			</div>
		</div>
		{{-- Block 1 --}}
		<div class="col-md-12 pull-left">
			<div class="animated row fadeIn">
				<div class="col-md-12 mb-2">
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="tab" href="#home1" role="tab" aria-controls="home"><i class="fa fa-question-circle" aria-hidden="true"></i> {{ trans('back-app.information') }}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#home3" role="tab" aria-controls="profile"><i class="fa fa-globe" aria-hidden="true"></i> {{ trans('back-app.web_master') }}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#home5" role="tab" aria-controls="messages"><i class="fa fa-code" aria-hidden="true"></i> Tùy biến Website</a>
						</li>
					</ul>

					<div class="tab-content card">
						{{-- ************************* INFOMATION BASIC *******  --}}
						<div class="row tab-pane active" id="home1" role="tabpanel">
							<div class="col-md-8 pull-left">
								<div class="animated fadeIn">
									<div class="card">
										<div class="card-header">
											<i class="fa fa-info-circle" aria-hidden="true"></i><strong>{{ trans('back-app.information') }}</strong>
										</div>
										<div class="card-block">
											<div class="form-group row">
												<label class="col-md-3 form-control-label" for="name">Tiêu đề website</label>
												<div class="col-md-9">
													<input type="text" id="site_title" name="site_title" class="form-control {{ ($errors->has('site_title') ? 'has-error' : '')}}" value="{{$setting->site_title}}" placeholder="BLOG SERVICE"  >
													<span class="help-block mess-err">{{$errors->first('site_title')}} </span>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-3 form-control-label" for="site_email">Email</label>
												<div class="col-md-9">
													<input type="email" id="site_email" name="site_email" class="form-control {{ ($errors->has('site_email') ? 'has-error' : '')}}" value="{{$setting->site_email}}" placeholder="CONTACT@BLOGSERVICE.COM"  >
													<span class="help-block mess-err">{{$errors->first('site_email')}}</span>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-3 form-control-label" for="site_phone">{{ trans('back-app.phone') }}</label>
												<div class="col-md-9">
													<input type="number" id="site_phone" name="site_phone" class="form-control {{ ($errors->has('site_phone') ? 'has-error' : '')}}" value="{{$setting->site_phone}}" placeholder="0938310064"  >
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-3 form-control-label" for="site_address">{{ trans('back-app.address') }}</label>
												<div class="col-md-9">
													<input type="text" id="site_address" name="site_address" class="form-control {{ ($errors->has('site_address') ? 'has-error' : '')}}" value="{{$setting->site_address}}" placeholder="02 NGUYEN DU ,7 WARDS ,3 DISTRICT , HCM.C">
													<span class="help-block mess-err">{{$errors->first('site_address')}}</span>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-3 form-control-label" for="site_copyright">Bản quyền</label>
												<div class="col-md-9">
													<input type="text" id="site_copyright" name="site_copyright" class="form-control {{ ($errors->has('site_copyright') ? 'has-error' : '')}}" value="{{$setting->site_copyright}}" placeholder="site_copyright &copy; 2017 - ALL RIGHT RESERVED">
													<span class="help-block mess-err">{{$errors->first('site_copyright')}}</span>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-3 form-control-label" for="site_description">{{ trans('back-app.desc') }}</label>
												<div class="col-md-9">
													<textarea type="text" id="site_description" name="site_description" class="form-control {{ ($errors->has('site_description') ? 'has-error' : '')}}" value="" placeholder="SITE BEST OF 2017">{{$setting->site_description}}</textarea>
													<span class="help-block mess-err">{{$errors->first('copyright')}}</span>
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>
							{{-- Block 2 --}}
							<div class="col-md-4 pull-right">
								{{-- Block 2 | Block 1 --}}
								<div class="animated fadeIn">
									<div class="card">
										<div class="card-header">
											<i class="fa fa-picture-o" aria-hidden="true"></i><strong>Favicon</strong>
										</div>
										<div class="card-block">
											<div class="form-group row">
												<div class="col-md-12 input-group">
													<div class="input-group">
														<span class="input-group-btn">
															<a id="" data-input="thumbnail" data-preview="holder" class="lfm btn btn-primary">
																<i class="fa fa-picture-o"></i> {{ trans('back-app.choose') }}
															</a>
														</span>
														<input id="thumbnail" class="form-control" type="text" value="{{ $setting->site_favicon }}" name="site_favicon">
													</div>
												</div>
												<div class="col-md-12 input-group">
													<img id="holder" width="200" height="200" src="{{ $setting->site_favicon }}" style="margin-top:15px;"> 
												</div>
											</div>
										</div>
									</div>
								</div>
								{{-- Bolock 2 | Bock 2 --}}
								<div class="animated fadeIn">
									<div class="card">
										<div class="card-header">
											<i class="fa fa-picture-o" aria-hidden="true"></i><strong>Logo</strong>
										</div>
										<div class="card-block">
											<div class="form-group row">
												<div class="col-md-12 input-group">
													<div class="input-group">
														<span class="input-group-btn">
															<a id="" data-input="thumbnail1" data-preview="holder1" class="lfm btn btn-primary">
																<i class="fa fa-picture-o"></i> {{ trans('back-app.choose') }}
															</a>
														</span>
														<input id="thumbnail1" class="form-control" type="text" value="{{ $setting->site_logo }}" name="site_logo">
													</div>
												</div>
												<div class="col-md-12 input-group">
													<img id="holder1" src="{{ $setting->site_logo }}"  width="200" height="200" style="margin-top:15px;"> 
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						{{-- ============= BEGIN::SEO ADDVANCED =========== --}}
						@include('components.seo_advanced',['old_value' => $setting])
						{{-- ============= END::SEO ADDVANCED =========== --}}

						<div class="tab-pane" id="home5" role="tabpanel">
							<div class="col-md-12 pull-left">
								<div class="animated fadeIn">
									<div class="card">
										<div class="card-header">
											<i class="fa fa-info-circle" aria-hidden="true"></i><strong>Infomation</strong>
										</div>
										<div class="card-block">
											<div class="form-group row">
												<label class="col-md-3 form-control-label" for="custom_css">Tùy biến Css</label>
												<div class="col-md-9">
													<textarea type="text" rows="15" id="custom_css" name="custom_css" class="form-control {{ ($errors->has('custom_css') ? 'has-error' : '')}}" value="{{old('custom_css')}}" placeholder="">
														{{$setting->custom_css}}
													</textarea>
													<span class="help-block mess-err">{{$errors->first('custom_css')}}</span>
												</div>
											</div>
											<div class="form-group row">
												<label class="col-md-3 form-control-label" for="custom_js">Tùy biến Javascript</label>
												<div class="col-md-9">
													<textarea type="text"  rows="15" id="custom_js" name="custom_js" class="form-control {{ ($errors->has('custom_js') ? 'has-error' : '')}}" value="{{old('custom_js')}}" placeholder="">
														{{$setting->custom_js}}
													</textarea>
													<span class="help-block mess-err">{{$errors->first('custom_js')}}</span>
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</form>
</div>
@endsection
