@extends('admincp.layouts.main')
@section('title',trans('back-app.profile'))
@section('content')
<div class="container-fluid">
	<div class="col-md-12 pull-left">
		<div class="animated fadeIn">
			<div class="card">
				<div class="card-header">
					{{-- Add new --}}
					<a href="{{route('admincp.profile.edit',$profile->id)}}"><button type="button" class="btn btn-sm btn-info"><i class="fa fa-edit "></i> {{ trans('back-app.edit') }}</button></a>
					{{-- Cancel --}}
					<a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>

				</div>
			</div>
		</div>
	</div>

	{{-- Profile --}}
	<div class="col-md-6 pull-left">
		<div class="animated fadeIn">
			<div class="card">
				<img width="100%" src="{{$profile->profile_image}}">
			</div>
		</div>
	</div>
	<div class="col-md-6 pull-left">
		<div class="animated fadeIn">
			<div class="card"><br>
				<div class="form-group row">
					<label class="col-md-2 badge badge-info">
						<strong>{{ trans('back-app.fullname') }}</strong>
					</label>
					<p class="col-md-10">{{$profile->full_name}}</p>
				</div>
				<div class="form-group row">
					<label class="col-md-2 badge badge-info">
						<strong>{{ trans('back-app.email') }}</strong>
					</label>
					<p class="col-md-10">{{$profile->email}}</p>
				</div>
				<div class="form-group row">
					<label class="col-md-2 badge badge-info">
						<strong>{{ trans('back-app.phone') }}</strong>
					</label>
					<p class="col-md-10">{{$profile->phone}}</p>
				</div>
				<div class="form-group row">
					<label class="col-md-2 badge badge-info">
						<strong>{{ trans('back-app.address') }}</strong>
					</label>
					<p class="col-md-10">{{$profile->address}}</p>
				</div>
				<div class="form-group row">
					<label class="col-md-2 badge badge-info">
						<strong>Role</strong>
					</label>
					<p class="col-md-10"><span class="badge badge-danger">{{($profile->active == 1) ? 'Admin' : ''}}</span></p>
				</div>
				<div class="form-group row">
					<label class="col-md-2 badge badge-info">
						<strong>{{ trans('back-app.created') }}</strong>
					</label>
					<p class="col-md-10">{{$profile->created_at}}</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection