@extends('admincp.layouts.main')
@section('title',trans('back-app.admin_group'))
@section('content')
<div class="container-fluid">
    {{-- *************** TOKEN ****************** --}}
    <input type="hidden" id="csrf_token" name="_token" value="{{csrf_token()}}">
    {{-- ************** Option Redirect ***************************** --}}
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-header">
                {{-- Add new --}}
                @if(CAN('admins','create'))
                <a href="{{route('admincp.admins.create')}}"><button type="button" class="btn btn-sm btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> {{ trans('back-app.add') }}</button></a>
                @endif
                {{-- Cancel --}}
                <a href="{{route('dashboard')}}"><button type="button" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('back-app.cancel') }}</button></a>
            </div>
        </div>
    </div>
    {{-- ************** End Option Redirect ***************************** --}}
    <div class="animated fadeIn">
        <div class="card">
            <div class="card-block">
                {{-- ************** Option Action ***************************** --}}
                <div class="form-group row">
                    @if(CAN('admins','destroy') || CAN('admins','update'))
                        <div class="col-md-2 ">
                            <select id="action_table" name="action_table" class=" form-control pull-left" size="1">
                                <option value="">-- {{ trans('back-app.action') }} --</option>
                                @if(CAN('admins','destroy'))
                                <option value="{{ route('admincp.admins.destroys')}}">{{ trans('back-app.delete') }}</option>
                                @endif
                                @if(CAN('admins','update'))
                                <option value="{{ route('admincp.admins.disable')}}">{{ trans('back-app.disable') }}</option>
                                <option value="{{ route('admincp.admins.public')}}">{{ trans('back-app.public') }}</option>
                                @endif
                            </select>
                        </div>  
                        <button type="button" onclick="update_records()" id="submit_action" class="btn btn-sm btn-info pull-left"><i class="fa fa-location-arrow" aria-hidden="true"></i> {{ trans('back-app.submit') }}</button>&nbsp;
                    @endif   
                </div>
                {{-- ************** End Option Action ***************************** --}}

                <table class="table table-striped table-bordered datatable" id="table_active">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="checkAll" onchange="return  check_lists();" name="checkAll"/></th>
                            <th>STT</th>
                            <th>{{ trans('back-app.avatar') }}</th>
                            <th>{{ trans('back-app.name') }}</th>
                            <th>{{ trans('back-app.email') }}</th>
                            <th>Role</th>
                            <th>{{ trans('back-app.created') }}</th>
                            <th>{{ trans('back-app.status') }}</th>
                            @if(CAN('admins','update') || CAN('admins','destroy'))
                            <th>{{ trans('back-app.action') }}</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @if(isset($admins))
                        @foreach($admins as $admin)
                        <tr>
                            <td><input type="checkbox" id="checkSingle" name="checkSingle" value="{{$admin->id}}" /></td>
                            <td>{{$i}}</td>
                            <td><img width="50px" src="{{$admin->profile_image }}"></td>
                            <td>{{$admin->username}}</td>
                            <td>{{$admin->email}}</td>
                            <td>
                                @if(isset($admin->role))
                                <span class="badge badge-danger">{{$admin->role->name}}</span>
                                @else
                                <span class="badge badge-warning">{{ trans('back-app.null') }}</span>
                                @endif
                            </td>
                            <td>{{Carbon\Carbon::parse($admin->created_at)->format('d-m-Y')}}</td>
                            <td>
                                @if($admin->is_published())
                                <span class="badge badge-success">{{ trans('back-app.public') }}</span>
                                @else
                                <span class="badge badge-warning">{{ trans('back-app.disable') }}</span>
                                @endif
                            </td>
                            @if(CAN('admins','update') || CAN('admins','destroy'))
                            <td>
                                    @if(CAN('admins','update'))
                                        <a class="btn btn-info pull-left" href="{{route('admincp.admins.edit',$admin->id)}}">
                                        <i class="fa fa-edit "></i></a>
                                    @endif
                                    @if(CAN('admins','destroy'))
                                        {{-- Delete --}}
                                        <form action="{{route('admincp.admins.destroy',$admin->id)}}" method="post" class="pull-left">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                            <input type="hidden" name="_method" value="DELETE"> 
                                            <a class="btn btn-danger" href="#" onclick="$(this).closest('form').submit()">
                                                <i class="fa fa-trash-o "></i>
                                            </a>
                                        </form>
                                    @endif
                                </td>
                                @endif
                            </tr>
                            <?php $i++; ?>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
