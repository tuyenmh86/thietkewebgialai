@extends('admincp.layouts.main')

@section('title','404')

@section('content')

<div class="container">

	<div class="container">

        <div class="row justify-content-center">

            <div class="col-md-6">

                <div class="clearfix">

                    <h1 class="float-left display-3 mr-2">404</h1>

                    <h4 class="pt-1">Rất tiếc !</h4>

                    <p class="text-muted">Không tìm thấy trang bạn đang tìm kiếm.</p>

                </div>

                <div class="input-prepend input-group">

                    <span class="input-group-addon"><i class="fa fa-search"></i>

                    </span>

                    <input id="prependedInput" class="form-control" size="16" type="text" placeholder="Bạn đang tìm kiếm gì?">

                    <span class="input-group-btn">

                        <button class="btn btn-info" type="button">Tìm kiếm</button>

                    </span>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection