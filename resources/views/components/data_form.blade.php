<div class="animated fadeIn">
	<div class="card">
		@if(count(config('cms.locale')) > 1)
		<ul class="nav nav-tabs" role="tablist">
			@foreach(config('cms.locale') as $key => $item)
			<li class="nav-item">
				<a class="nav-link {{ $key+5 == 5 ? 'active' : '' }}" data-toggle="tab" href="#language_form{{$key}}" role="tab" aria-controls="home">
					<img src="{{ $item['flag'] }}"><span style="text-transform: uppercase;"> {{ $item['code'] }} </a>
					</li>
					@endforeach
				</ul>
				@endif
				<div class="tab-content card">
					@foreach(config('cms.locale') as $key => $item)
					<div class="row tab-pane  {{ $key+5 == 5 ? 'active' : '' }}" id="language_form{{$key}}" role="tabpanel">
						<div class="col-md-12">
							<div class="card-block">
								<div class="form-group row">
									<label class="col-md-3 form-control-label" for="language[{{$item['code']}}][title]">{{ trans('back-app.title') }}(<span class="required_cl">*</span>)</label>
									<div class="col-md-9">
										<input type="text" id="language[{{$item['code']}}][title]" name="language[{{$item['code']}}][title]" class="form-control {{ ($errors->has("title") ? 'has-error' : '')}}" value="{{old("language[".$item['code']."][title]",@$old_value->language[$item['code']]['title'])}}" placeholder="" onkeyup="return changeToSlug(this.value,'{{ $item['code'] }}');">
										<span class="help-block mess-err">{{$errors->first("title")}} </span>
									</div>
								</div>


								<div class="form-group row" style="display: none">
									<label class="col-md-3 form-control-label" for="language[{{$item['code']}}][alias]">{{ trans('back-app.alias') }}(<span class="required_cl">*</span>)</label>
									<div class="col-md-9">
										<input type="text" id="{{ $item['code'] }}_alias" name="language[{{$item['code']}}][alias]" class="form-control {{ ($errors->has("alias") ? 'has-error' : '')}}" value="{{old("language[".$item['code']."][alias]",@$old_value->language[$item['code']]['alias'])}}" placeholder="">
										<span class="help-block mess-err">{{$errors->first("alias")}} </span>
									</div>
								</div>


								<div class="form-group row">
									<label class="col-md-3 form-control-label" for="language[{{$item['code']}}][description]">{{ trans('back-app.desc') }}(<span class="required_cl">*</span>)</label>
									<div class="col-md-9">
										<textarea type="text" id="language[{{$item['code']}}][description]" rows="4" name="language[{{$item['code']}}][description]" class="form-control {{ ($errors->has('description') ? 'has-error' : '')}}" value="" placeholder="">{{old("language[".$item['code']."][description]",@$old_value->language[$item['code']]['description'])}}</textarea>
										<span class="help-block mess-err">{{$errors->first("description")}} </span>
									</div>
								</div>


								<div class="form-group row">
									<label class="col-md-3 form-control-label" for="language[{{$item['code']}}][content]">{{ trans('back-app.content') }}(<span class="required_cl">*</span>)</label>
									<div class="col-md-9">
										<textarea type="text" id="language[{{$item['code']}}][content]" rows="40" name="language[{{$item['code']}}][content]" class="myEditor form-control {{ ($errors->has('content') ? 'has-error' : '')}}" value="" placeholder="">{{old("language[".$item['code']."][content]",@$old_value->language[$item['code']]['content'])}}</textarea>
										<span class="help-block mess-err">{{$errors->first('content')}} </span>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>