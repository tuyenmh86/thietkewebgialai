@if(count(customFileds($table)) > 0)
	<div class="animated fadeIn">
		<div class="card">
			<div class="card-header">
				<i class="fa fa-info-circle" aria-hidden="true"></i><strong>{{ trans('back-app.customfield') }}</strong>
			</div>

			@foreach(customFileds($table) as $k => $field)
				@if($field->type == 'number')
					<div class="col-md-12">
						<div class="card-block">
							<div class="form-group row">
								<label class="col-md-3 form-control-label" for="{{ $field->field }}">{{ $field->name }}</label>
								<div class="col-md-9">
									<input type="number" id="{{ $field->field }}" name="{{ $field->field }}" class="form-control " value="{{ isset($old_value) ? $old_value[$field->field] : '' }}" placeholder="">
								</div>
							</div>
						</div>
					</div>

				@elseif($field->type == 'image')
					<div class="col-md-12">
						<div class="card-block">
							<div class="card">
								<div class="card-header">
									<i class="fa fa-picture-o" aria-hidden="true"></i><strong>{{ $field->name }}</strong>
								</div>
								<div class="card-block">
									<div class="form-group row">
										<div class="col-md-12 input-group">
											<div class="input-group">
												<span class="input-group-btn">
													<a id="" data-input="thumbnail{{$k}}" data-preview="holder{{$k}}" class="lfm btn btn-primary">
														<i class="fa fa-picture-o"></i> {{ trans('back-app.choose') }}
													</a>
												</span>
												<input id="thumbnail{{$k}}" class="form-control" type="text" name="{{ $field->field }}">
											</div>
										</div>
										<div class="col-md-12 input-group">
											<img id="holder{{$k}}" width="100%" style="margin-top:15px;" src="{{ isset($old_value) ? $old_value[$field->field] : '' }}"> 
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@else


				<div class="col-md-12">
					<div class="card-block">
						@if(count(config('cms.locale')) > 1)
							<ul class="nav nav-tabs" role="tablist">
								@foreach(config('cms.locale') as $key => $item)
									<li class="nav-item">
									<a class="nav-link {{ $loop->first ? 'active' : '' }}" data-toggle="tab" href="#language_form{{$k+55}}{{$key}}" role="tab" aria-controls="home">
										<img src="{{ $item['flag'] }}"><span style="text-transform: uppercase;"> {{ $item['code'] }} </a>
									</li>
								@endforeach
							</ul>
						@endif
						<div class="tab-content card">
							@foreach(config('cms.locale') as $key => $item)
								<div class="row tab-pane {{ $loop->first ? 'active' : '' }}" id="language_form{{$k+55}}{{$key}}" role="tabpanel">
									<div class="col-md-12">
										<div class="card-block">
											<div class="form-group row">

												@if($field->type == 'editor')

													<label class="col-md-3 form-control-label" for="language[{{$item['code']}}][{{$field->field}}]">{{ $field->name }}</label>
													<div class="col-md-9">
														<textarea type="text" id="language[{{$item['code']}}][{{$field->field}}]" rows="10" name="language[{{$item['code']}}][{{$field->field}}]" class="myEditor form-control" value="" placeholder="">{{old("language[".$item['code']."][$field->field]",@$old_value->language[$item['code']][$field->field])}}</textarea>
													</div>

												@elseif($field->type == 'textarea')

													<label class="col-md-3 form-control-label" for="language[{{$item['code']}}][{{$field->field}}]">{{ $field->name }}</label>
													<div class="col-md-9">
														<textarea type="text" id="language[{{$item['code']}}][{{$field->field}}]" rows="4" name="language[{{$item['code']}}][{{$field->field}}]" class="form-control" value="" placeholder="">{{old("language[".$item['code']."][$field->field]",@$old_value->language[$item['code']][$field->field])}}</textarea>
													</div>

												@else

													<label class="col-md-3 form-control-label" for="language[{{$item['code']}}][{{$field->field}}]">{{ $field->name }}</label>
													<div class="col-md-9">
														<input type="text" id="language[{{$item['code']}}][{{$field->field}}]" name="language[{{$item['code']}}][{{$field->field}}]"  class="form-control" value="{{old("language[".$item['code']."][$field->field]",@$old_value->language[$item['code']][$field->field])}}" placeholder="">
													</div>

												@endif
											</div>
										</div>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			@endif
		@endforeach
	</div>
	</div>
@endif
