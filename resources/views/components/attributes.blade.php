

 @if(isset($attributes))

	@foreach($attributes as $k => $field)
		@if($field->type == 'integer')
			<div class="animated fadeIn">
				<div class="card">
					<div class="col-md-12">
						<div class="card-block">
							<div class="form-group row">
								<label class="col-md-2 form-control-label" for="attributes[{{$field->type}}][{{$field->id}}][{{ $field->field }}]">{{ $field->label }}</label>
								<div class="col-md-10">
									<input type="number" id="attributes[{{$field->type}}][{{$field->id}}][{{ $field->field }}]" name="attributes[{{$field->type}}][{{$field->id}}][{{ $field->field }}]" class="form-control " value="" placeholder="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		@elseif($field->type == 'text')
			<div class="animated fadeIn">
				<div class="card">
					<div class="col-md-12">
						<div class="card-block">
							@if(count(config('cms.locale')) > 1)
								<ul class="nav nav-tabs" role="tablist">
									@foreach(config('cms.locale') as $key => $item)
										<li class="nav-item">
										<a class="nav-link {{ $loop->first ? 'active' : '' }}" data-toggle="tab" href="#language_form{{$key.$k+55}}" role="tab" aria-controls="home">
											<img src="{{ $item['flag'] }}"><span style="text-transform: uppercase;"> {{ $item['code'] }} </a>
										</li>
									@endforeach
								</ul>
							@endif
							<div class="tab-content card">
								@foreach(config('cms.locale') as $key => $item)
									<div class="row tab-pane {{ $loop->first ? 'active' : '' }}" id="language_form{{$key.$k+55}}" role="tabpanel">
										<div class="col-md-12">
											<div class="card-block">
												<div class="form-group row">
													<label class="col-md-2 form-control-label" for="attributes[{{$field->type}}][{{$field->id}}][{{$field->field}}][{{$item['code']}}]">{{ $field->label }} </label>
													<div class="col-md-10">
														<textarea type="text" id="attributes[{{$field->type}}][{{$field->id}}][{{$field->field}}][{{$item['code']}}]" rows="10" name="attributes[{{$field->type}}][{{$field->id}}][{{$field->field}}][{{$item['code']}}]" class="myEditor form-control" value="" placeholder="" ></textarea>
													</div>
												</div>
											</div>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		@else
			<div class="animated fadeIn">
				<div class="card">
					<div class="col-md-12">
						<div class="card-block">
							@if(count(config('cms.locale')) > 1)
								<ul class="nav nav-tabs" role="tablist">
									@foreach(config('cms.locale') as $key => $item)
										<li class="nav-item">
										<a class="nav-link {{ $loop->first ? 'active' : '' }}" data-toggle="tab" href="#language_form{{$key.$k+55}}" role="tab" aria-controls="home">
											<img src="{{ $item['flag'] }}"><span style="text-transform: uppercase;"> {{ $item['code'] }} </a>
										</li>
									@endforeach
								</ul>
							@endif
							<div class="tab-content card">
								@foreach(config('cms.locale') as $key => $item)
									<div class="row tab-pane {{ $loop->first ? 'active' : '' }}" id="language_form{{$key.$k+55}}" role="tabpanel">
										<div class="col-md-12">
											<div class="card-block">
												<div class="form-group row">
													<label class="col-md-2 form-control-label" for="attributes[{{$field->type}}][{{$field->id}}][{{$field->field}}][{{$item['code']}}]">{{ $field->label }}</label>
													<div class="col-md-10">
														<input type="text" id="attributes[{{$field->type}}][{{$field->id}}][{{$field->field}}][{{$item['code']}}]" name="attributes[{{$field->type}}][{{$field->id}}][{{$field->field}}][{{$item['code']}}]"  class="form-control" value="" placeholder="" >
													</div>
												</div>
											</div>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		@endif
	@endforeach
@endif 

@if(isset($product))

	@foreach($product->productAttributeValue as $k => $valueType)
			
		@if($k === 'attribute_value_integer')
			@if($valueType->count())
				@foreach($valueType as $j => $item)
					<div class="animated fadeIn">
						<div class="card">
							<div class="col-md-12">
								<div class="card-block">
									@if(count(config('cms.locale')) > 1)
										<ul class="nav nav-tabs" role="tablist">
											@foreach(config('cms.locale') as $key => $lang)
												<li class="nav-item">
												<a class="nav-link {{ $loop->first ? 'active' : '' }}" data-toggle="tab" href="#language_form{{$j.$key+55}}" role="tab" aria-controls="home">
													<img src="{{ $lang['flag'] }}"><span style="text-transform: uppercase;"> {{ $lang['code'] }} </a>
												</li>
											@endforeach
										</ul>
									@endif
									<div class="tab-content card">
										@foreach(config('cms.locale') as $key => $lang)
											<div class="row tab-pane {{ $loop->first ? 'active' : '' }}" id="language_form{{$j.$key+55}}" role="tabpanel">
												<div class="col-md-12">
													<div class="card-block">
														<div class="form-group row">
															<label class="col-md-2 form-control-label" for="">{{ @$item->attribute->label }}</label>
															<div class="col-md-10">
																<input type="number" id="" name="attributes[{{@$item->attribute->type}}][{{@$item->attribute->id}}][{{@$item->attribute->field}}][{{$lang['code']}}]"  class="form-control" value="{{ @$item->value[$lang['code']]  }}" placeholder="" >
															</div>
														</div>
													</div>
												</div>
											</div>
										@endforeach
									</div>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			@endif
		@elseif($k === 'attribute_value_text')
			@if($valueType->count())
				@foreach($valueType as $j => $item)
					<div class="animated fadeIn">
						<div class="card">
							<div class="col-md-12">
								<div class="card-block">
									@if(count(config('cms.locale')) > 1)
										<ul class="nav nav-tabs" role="tablist">
											@foreach(config('cms.locale') as $key => $lang)
												<li class="nav-item">
												<a class="nav-link {{ $loop->first ? 'active' : '' }}" data-toggle="tab" href="#language_form{{$j.$key+65}}" role="tab" aria-controls="home">
													<img src="{{ $lang['flag'] }}"><span style="text-transform: uppercase;"> {{ $lang['code'] }} </a>
												</li>
											@endforeach
										</ul>
									@endif
									<div class="tab-content card">
										@foreach(config('cms.locale') as $key => $lang)
											<div class="row tab-pane {{ $loop->first ? 'active' : '' }}" id="language_form{{$j.$key+65}}" role="tabpanel">
												<div class="col-md-12">
													<div class="card-block">
														<div class="form-group row">
															<label class="col-md-2 form-control-label" for="">{{ @$item->attribute->label }}AAAAA</label>
															<div class="col-md-10">
																<textarea type="text" id="" rows="10" name="attributes[{{@$item->attribute->type}}][{{@$item->attribute->id}}][{{@$item->attribute->field}}][{{$lang['code']}}]" class="myEditor form-control" value="" placeholder="" >{{ @$item->value[$lang['code']]  }}
																</textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										@endforeach
									</div>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			@endif
		@else
			{{--  @if($valueType->count())
				@foreach($valueType as $j => $item)
					<div class="animated fadeIn">
						<div class="card">
							<div class="col-md-12">
								<div class="card-block">
									@if(count(config('cms.locale')) > 1)
										<ul class="nav nav-tabs" role="tablist">
											@foreach(config('cms.locale') as $key => $lang)
												<li class="nav-item">
												<a class="nav-link {{ $loop->first ? 'active' : '' }}" data-toggle="tab" href="#language_form{{$j.$key+75}}" role="tab" aria-controls="home">
													<img src="{{ $lang['flag'] }}"><span style="text-transform: uppercase;"> {{ $lang['code'] }} </a>
												</li>
											@endforeach
										</ul>
									@endif
									<div class="tab-content card">
										@foreach(config('cms.locale') as $key => $lang)
											<div class="row tab-pane {{ $loop->first ? 'active' : '' }}" id="language_form{{$j.$key+75}}" role="tabpanel">
												<div class="col-md-12">
													<div class="card-block">
														<div class="form-group row">
															<label class="col-md-2 form-control-label" for="">{{ @$item->attribute->label }}Loi</label>
															<div class="col-md-10">
																<input type="text" id="" name="attributes[{{@$item->attribute->type}}][{{@$item->attribute->id}}][{{@$item->attribute->field}}][{{$lang['code']}}]"  class="form-control" value="{{ @$item->value[$lang['code']]  }}" placeholder="" >
															</div>
														</div>
													</div>
												</div>
											</div>
										@endforeach
									</div>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			@endif  --}}
		@endif
	@endforeach
@endif

<script type="text/javascript">
	// ************************ BEGIN:: TINYMCE ****************************

        var editor_config = {

        path_absolute : "/",

        selector: "textarea.myEditor",

        plugins: [

          "advlist autolink lists link image charmap print preview hr anchor pagebreak",

          "searchreplace wordcount visualblocks visualchars code fullscreen",

          "insertdatetime media nonbreaking save table contextmenu directionality",

          "emoticons template paste textcolor colorpicker textpattern textcolor colorpicker"

        ],

        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media fullpage | fullscreen | code forecolor backcolor",

        content_css : "/admin/admin/css/custom.css",

        relative_urls: false,

        convert_urls : false,

        verify_html: false,

        file_browser_callback : function(field_name, url, type, win) {

          var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;

          var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;



          var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;

          if (type == 'image') {

            cmsURL = cmsURL + "&type=Images";

          } else {

            cmsURL = cmsURL + "&type=Files";

          }



          tinyMCE.activeEditor.windowManager.open({

            file : cmsURL,

            title : 'Filemanager',

            width : x * 0.8,

            height : y * 0.8,

            resizable : "yes",

            close_previous : "no"

          });

        }

      };



      tinymce.init(editor_config);
</script>