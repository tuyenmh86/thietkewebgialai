<div class="tab-pane" id="home3" role="tabpanel">
	<div class="col-md-12 pull-left" style="padding-left: 0">
		<div class="animated fadeIn">
			<div class="card">
				{{-- Lang En And Vi --}}
				@if(count(config('cms.locale')) > 1)
					<ul class="nav nav-tabs" role="tablist">
						@foreach(config('cms.locale') as $key => $item)
							<li class="nav-item">
								<a class="nav-link {{ $key+30 == 30 ? 'active' : '' }}" data-toggle="tab" href="#language_{{$item['code']}}" role="tab" aria-controls="home"><img src="{{ $item['flag'] }}"><span style="text-transform: uppercase;"> {{ $item['code'] }} </a>
							</li>
						@endforeach
					</ul>
				@endif
				<div class="tab-content">
					@foreach(config('cms.locale') as $key => $item)
						<div class="row tab-pane {{ $key+30 == 30 ? 'active' : '' }}" id="language_{{$item['code']}}" role="tabpanel">
							<div class="col-md-12">
								<div class="card-block">
									<div class="form-group row">
										<label class="col-md-2 form-control-label" for="language[{{ $item['code'] }}][seo_title]">{{ trans('back-app.title') }}</label>
										<div class="col-md-10">
											<input type="text" id="language[{{ $item['code'] }}][seo_title]" name="language[{{ $item['code'] }}][seo_title]" class="form-control" value="{{ old("language[".$item['code']."][seo_title]",@$old_value->language[$item['code']]['seo_title']) }}" placeholder=""  >
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-2 form-control-label" for="language[{{ $item['code'] }}][seo_description]">{{ trans('back-app.desc') }}</label>
										<div class="col-md-10">
											<textarea type="text" id="language[{{ $item['code'] }}][seo_description]" onkeyup="return seo_description(this)" name="language[{{ $item['code'] }}][seo_description]" class="form-control" value="" placeholder="">{{ old("language[".$item['code']."][seo_description]",@$old_value->language[$item['code']]['seo_description']) }}</textarea>
											<div style="color: red" id="charNum"></div>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-2 form-control-label" for="language[{{ $item['code'] }}][seo_keywords]">{{ trans('back-app.keyword') }}</label>
										<div class="col-md-10">
											<input type="text" id="language[{{ $item['code'] }}][seo_keywords]" name="language[{{ $item['code'] }}][seo_keywords]" class="form-control" value="{{ old("language[".$item['code']."][seo_keywords]",@$old_value->language[$item['code']]['seo_keywords']) }}" placeholder="">
										</div>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
			

			{{-- ======= BEGIN::SEO ADVANCE WITH GG SERVICE ====== --}}
			@if(request()->is('admincp/setting'))
			<div class="card">
				<div class="card-header">
					<i class="fa fa-info-circle" aria-hidden="true"></i><strong>{{ trans('back-app.web_master') }}</strong>
				</div>
				<div class="tab-content">
					{{-- VI --}}
					<div class="row tab-pane active" id="home10" role="tabpanel">
						<div class="col-md-12">
							<div class="card-block">
								<div class="form-group row">
									<label class="col-md-3 form-control-label" for="google_site_verification">Google site verification</label>
									<div class="col-md-9">
										<input type="text" id="google_site_verification" name="google_site_verification" class="form-control {{ ($errors->has('google_site_verification') ? 'has-error' : '')}}" value="{{$setting->google_site_verification}}" placeholder="uEtG4638yYT42PzvFua6iiR1v+iJ2lfnUpZ/cJPdzKM">
										<span class="help-block mess-err">{{$errors->first('google_site_verification')}}</span>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-md-3 form-control-label" for="google_analytics">Google  analystics</label>
									<div class="col-md-9">
										<textarea type="text" id="google_analytics" name="google_analytics" class="form-control {{ ($errors->has('google_analytics') ? 'has-error' : '')}}" value="" placeholder="hs2Js3WGGpf1pzyhUNehvVDSIrmO3XZpm7PggNZECxE">{{$setting->google_analytics}}
										</textarea>
										<span class="help-block mess-err">{{$errors->first('google_analytics')}}</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif
			{{-- ======= END::SEO ADVANCE WITH GG SERVICE ====== --}}
		</div>
	</div>
</div>